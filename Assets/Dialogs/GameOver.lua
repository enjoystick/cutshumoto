function OnItemClicked(self, item, buttons, x, y)
  if (item:GetID() == GUI:GetID("MAINMENU")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelFilename("Scenes/Scene_MainMenu")
  elseif (item:GetID() == GUI:GetID("REPEAT")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
	levelNum =G.enemyFactory:GetCurrentLevel()
	--levelNum=4
    G.enemyFactory:LoadLevelSwig(levelNum)
  end
end