
function OnCreate(self)
	self.pause=false
end

function OnSerialize(self, archive)
	self.pause=false
end

function OnItemClicked(self, item, buttons, x, y)
  if (item:GetID() == GUI:GetID("PAUSE")) then
	Timer:SetFrozen(true)
	if(self.pause==false) then
		Timer:SetFrozen(true)
		self.pause=true
	else
		Timer:SetFrozen(false)
		self.pause=false
	end
  end
end

function OnBeforeSceneUnloaded()
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
end