function OnItemClicked( self, item, buttons, x, y )
  if ( item:GetID() == GUI:GetID( "PAUSE" ) ) then
    --pause button clicked
    if Timer:IsFrozen() == false then
      --Pause game
      Timer:SetFrozen( true )
    else
      --Resume game
      Timer:SetFrozen( false )
    end
  end
end
