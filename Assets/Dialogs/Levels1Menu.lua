function OnItemClicked(self, item, buttons, x, y)
  if (item:GetID() == GUI:GetID("BACK")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelFilename("Scenes/Scene_Mundos")
  elseif (item:GetID() == GUI:GetID("LEVEL1")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelSwig(1)
  elseif (item:GetID() == GUI:GetID("LEVEL2")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelSwig(2)
	elseif (item:GetID() == GUI:GetID("BOSS")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelFilename("Scenes\SpiderBattle")
  end
end