function OnItemClicked(self, item, buttons, x, y)
  if (item:GetID() == GUI:GetID("BACK")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelFilename("Scenes/Scene_Mundos")
  elseif (item:GetID() == GUI:GetID("LEVEL3")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelSwig(3)
  elseif (item:GetID() == GUI:GetID("LEVEL4")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelSwig(4)
  end
end