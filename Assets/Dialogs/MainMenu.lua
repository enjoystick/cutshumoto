function OnItemClicked(self, item, buttons, x, y)
  if (item:GetID() == GUI:GetID("CREDITS")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelFilename("Scenes/Scene_Credits")
  elseif (item:GetID() == GUI:GetID("PLAY")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    G.enemyFactory:LoadLevelFilename("Scenes/Scene_Mundos")
  end
end