function OnItemClicked(self, item, buttons, x, y)
  if (item:GetID() == GUI:GetID("QUIT")) then
    self:SetVisible(false)
    GUI:SetCursorVisible(false)
    Application:LoadScene( "Scenes\\Level01.scene")
  elseif (item:GetID() == GUI:GetID("NEWGAME")) then
    Application:LoadScene( "Scenes\\Level01.scene")
  end
end