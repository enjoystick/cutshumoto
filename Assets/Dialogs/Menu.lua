function OnAfterSceneLoaded( self )
  --Enable debbuging
  Debug:Enable( true )
  
  --Show cursor
  GUI:LoadResourceFile( "Dialogs/MenuSystem.xml" ) --load resources  
  GUI:SetCursorVisible( true )
  
  w, h = Screen:GetViewportSize()
  self.mappedButtons = Input:CreateMap( "MappedButtons" )
  
  self.buttons = Game:CreateScreenMask( 0, 0, "Dialogs\\Textures\\resume.png" )
  self.buttons:SetBlending( Vision.BLEND_ALPHA )
  self.btnResumeRect = { w * .5 - (w * .35)/2, h * .5 - (h * .1)/2, w * .35, h * .1 }
  self.buttons:SetTargetSize( self.btnResumeRect[3], self.btnResumeRect[4] )
  self.buttons:SetPos( self.btnResumeRect[1], self.btnResumeRect[2] )

  self.mappedButtons:MapTrigger( "Mouse_Left_Button", "MOUSE", "CT_MOUSE_LEFT_BUTTON", {once = true} )
end

function OnThink( self )
  --pixel coords
  local x, y = Input:GetMousePosition()
  
  if self.mappedButtons:GetTrigger( "Mouse_Left_Button" ) > 0 then
    if ( x > self.btnResumeRect[1] and x < self.btnResumeRect[1] + self.btnResumeRect[3] ) and ( y > self.btnResumeRect[2] and y < self.btnResumeRect[2] + self.btnResumeRect[4] ) then --check mouse pos inside resume btn rect
      Application:LoadScene( "Scenes/Level01.vscene" )
    end
  end
end

function OnBeforeSceneUnloaded( self )
  local mode = Application:GetEditorMode()
  
  if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then
    GUI:SetCursorVisible( false )
  end
  
  Input:DestroyMap( self.mappedButtons )
  self.mappedButtons = nil
  
  Game:DeleteAllUnrefScreenMasks()
  
  Debug:ClearLines()
end
