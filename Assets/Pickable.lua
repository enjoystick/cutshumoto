-- bounding box setup for a pickable object
function OnAfterSceneLoaded(self)
  Debug:Enable(true)
  self:SetTraceAccuracy(Vision.TRACE_POLYGON)
end

function OnThink(self)
	--Debug:PrintAt(self:GetPosition(), "Pick me!", Vision.V_RGBA_CYAN)
end