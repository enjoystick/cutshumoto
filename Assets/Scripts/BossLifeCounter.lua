-- new script file

function OnThink(self)
  local score = G.enemyFactory:GetScore()
  local lifes = G.enemyFactory:GetPlayerLifes()
  if lifes > 0 then
  Debug:PrintAt(50, 50, "Points: "..score, Vision.V_RGBA_PURPLE, "Fonts/eras36")
  Debug:PrintAt(50, 90, "Lives: "..lifes, Vision.V_RGBA_GREEN, "Fonts/eras36")
  else
    Debug:PrintAt(400, 200, "G A M E   O V E R", Vision.V_RGBA_RED, "Fonts/eras36")
    --G.enemyFactory:LoadLevelFilename("Scenes/Scene_Mundos")
    G.enemyFactory:LoadLevelFilename("Scenes/Scene_GameOver")
  end
end