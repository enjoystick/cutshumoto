-- new script file

function OnObjectEnter(self,object)

  if object == nil then
    if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
      Debug:PrintLine( "HitandDelete OnObjectEnter object: es nil" )
    end
  elseif G.enemyFactory:GetKeyLua(object) == "Enemy" then

    --Debug:PrintLine(object:GetKey())
    G.enemyFactory:RemoveFromSlicedEntities(object)
    G.enemyFactory:DamagePlayer()
  elseif G.enemyFactory:GetKeyLua(object) == "Friend" then

    --Debug:PrintLine(object:GetKey())
    G.enemyFactory:RemoveFromSlicedEntities(object)
  elseif G.enemyFactory:GetKeyLua(object) == "SlicedChunk" then
	object:Remove()
  elseif G.enemyFactory:GetKeyLua(object) == "Finish" then
    G.levelFinish=true
  end
end