-- new script file

function OnAfterSceneLoaded()

  local mode = Application:GetEditorMode()
  --[[if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then
  
    --Camera set up-------------------------------
    local camPos = Game:GetEntity( "camPos" )
    local cam = Game:GetCamera()
    cam:SetUseEulerAngles( true )
    cam:SetPosition( camPos:GetPosition() )
    cam:SetOrientation( camPos:GetOrientation() )
    ----------------------------------------------
    
    --GUI init------------------------------------
    GUI:LoadResourceFile( "Dialogs/MenuSystem.xml" ) --load resources
    GUI:SetCursorVisible( true ) --show cursor
    ----------------------------------------------
  
  end]]
  PersistentData:Load("MaxScore")
  G.mapSpeed = 0
  G.currentLevel = 1
  G.levelFinish= false
  
  if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
    Debug:Enable( true )
  end
  G.enemyFactory = Game:CreateEntity( Vision.hkvVec3(0,0,0), "EnemyFactoryLUA" )
  
  G.enemyFactory:PreloadEnemy( 0, G.currentLevel ) -- kitsune
  G.enemyFactory:PreloadEnemy( 1, G.currentLevel ) -- armored  
  G.enemyFactory:PreloadEnemy( 3, G.currentLevel ) -- fatuo
  G.enemyFactory:PreloadEnemy( 4, G.currentLevel ) -- ninja
  G.enemyFactory:PreloadEnemy( 5, G.currentLevel ) -- civilian
  G.enemyFactory:PreloadEnemy( 6, G.currentLevel ) -- ghost
  G.enemyFactory:PreloadEnemy( 7, G.currentLevel )
  G.enemyFactory:PreloadEnemy( 9, G.currentLevel )
  G.enemyFactory:PreloadEnemy( 10, G.currentLevel )
  G.enemyFactory:PreloadEnemy( 41, G.currentLevel ) -- shuriken
  
  G.enemyFactory:ResetStats()
  
  G.CurrentLevelString = ""
  G.CurrentLevelString = G.CurrentLevelString .. G.currentLevel
  G.enemyFactory:SetCurrentLevel(G.currentLevel)
  --PersistentData:SetNumber("MaxScore",CurrentLevelString,630)
  --G.maxScoreGui=0
  --G.maxScoreGui=PersistentData:GetNumber("MaxScore","1",0)
end

function OnBeforeSceneUnloaded()
   --PersistentData:Load("MaxScore")
   -- maxScore=G.enemyFactory:GetNumberDataSwig("MaxScore",1,0)
   --if(  G.enemyFactory:GetScore()>maxScore) then
   -- G.enemyFactory:SetNumberDataSwig("MaxScore",1,G.enemyFactory:GetScore())
   --end
    --PersistentData:Save("MaxScore")
end



