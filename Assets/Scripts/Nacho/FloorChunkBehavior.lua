local endPointPos

local animationAdded=0


function OnAfterSceneLoaded(self)


  
  --self.CurrentAnim = 0
  --self.AllAnims = self.Animation:GetSequences()
  local endPoint = Game:GetEntity( "floorEndPoint" )
  endPointPos = endPoint:GetPosition()
end

function OnThink(self)
  if self.Animation==nil then
	self:AddAnimation("Animation")
  self.Animation:Play("Unfold",false)
  end
  
	local speed = 200.0
  --self.Animation:Play(self.AllAnims[1].Name)
  local step = Vision.hkvVec3()
  step.y = speed * Timer:GetTimeDiff()
  self:IncPosition( -step ) -- Translate towards y neg axe
  
  -- Check if must be destroyed
  if self:GetPosition().y <= endPointPos.y then
    -- Arrives end point -> Destroy
    G.onFloorChunkBecomeInvisible() -- Notify
    self:Remove()
  end
end

function OnCreate(self)
  
end
function OnExpose(self)
		
end


function OnSerialize(self, archive)

end
