local spamPointPos

function OnAfterSceneLoaded()
  --Camera set up-------------------------------
  local camPos = Game:GetEntity( "camPos" )
  local cam = Game:GetCamera()
  cam:SetUseEulerAngles(true)
  cam:SetPosition(camPos:GetPosition())
  cam:SetOrientation(camPos:GetOrientation())
  ----------------------------------------------
  
  local spamPoint = Game:GetEntity( "floorSpamPoint" )
  if spamPoint ~= nil then
    spamPointPos = spamPoint:GetPosition() --pos where chunks will show up
  end

  --GUI init------------------------------------
  GUI:LoadResourceFile( "Dialogs/MenuSystem.xml" ) --load resources
  
  G.levelDialog = GUI:ShowDialog( "Dialogs/LevelMenu.xml" ) --show level GUI
  
  GUI:SetCursorVisible( false )
  ----------------------------------------------
  
  G.onFloorChunkBecomeInvisible = OnFloorChunkBecomeInvisible
end

function OnBeforeSceneUnloaded()
  local mode = Application:GetEditorMode()
  
  if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then
    GUI:SetCursorVisible( false )
    
    GUI:CloseDialog( G.levelDialog )
  end
  
  -- Clean debug log
  Debug:ClearLines()
end

function OnFloorChunkBecomeInvisible()
  Game:InstantiatePrefab( spamPointPos, "Prefabs/Floor_Chunk2.vprefab", nil )
end
