function OnAfterSceneLoaded()
  --GUI init------------------------------------
  GUI:LoadResourceFile( "Dialogs/MenuSystem.xml" ) --load resources
  
  G.mainMenuDialog = GUI:ShowDialog( "Dialogs/MainMenu.xml" ) --show level GUI
  
  GUI:SetCursorVisible( true )
  ----------------------------------------------
end

function OnBeforeSceneUnloaded()
  local mode = Application:GetEditorMode()
  
  if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then
    GUI:SetCursorVisible( false )
    
    GUI:CloseDialog( G.mainMenuDialog )
  end
end

