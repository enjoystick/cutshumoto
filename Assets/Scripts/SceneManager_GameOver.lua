-- new script file
-- new script file
function OnAfterSceneLoaded(self)
      G.enemyFactory = Game:CreateEntity( Vision.hkvVec3(0,0,0), "EnemyFactoryLUA" )
      
      local width, height = Screen:GetViewportSize()

      -- create an input map with specific dimensions
      numTriggers = 1 --the max number of triggers we can use with this map
      numAlternatives = 2 --we are going to map 5 controls to the trigger "Jump"

      self.inputMap = Input:CreateMap("Player", numTriggers, numAlternatives)
      
      -- map a trigger to the touch area 
      self.inputMap:MapTrigger("Enter", {0,0, width,height},  "CT_TOUCH_ANY", {once = true})
      
      --map a trigger to mouse button
      self.inputMap:MapTrigger("Enter", "MOUSE", "CT_MOUSE_LEFT_BUTTON", {once = true})


        GUI:LoadResourceFile("Dialogs/MenuSystem.xml") --load resources

        if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
          G.myDialog = GUI:ShowDialog("Dialogs/GameOverLUA.xml") --show GUI
          GUI:SetCursorVisible(true) --make the mouse cursor visible
        end

        G.Path=0
        G.starMaxAct=0
        G.starMax=0
        G.LevelFinish=false
        local numLevel =G.enemyFactory:GetCurrentLevel()
        local numLevelString = ""
        numLevelString= numLevelString.. numLevel
        PersistentData:Load("levelFinish")
        PersistentData:Load("starMax")
        if(numLevel ~= 5 and numLevel ~= 10 and numLevel ~= 15) then
          local scoreMax =G.enemyFactory:GetScoreMax()
          local score= G.enemyFactory:GetScore()
          G.levelFinish=G.enemyFactory:GetLevelFinish()
          G.starMax =PersistentData:GetNumber("starMax",numLevelString,0)
          G.starMaxAct =G.enemyFactory:CalculateStars(score,scoreMax,G.levelFinish)
          --PersistentData:SetBoolean("levelFinish",numLevelString,G.levelFinish)
          --PersistentData:Save("levelFinish")
        else
        G.levelFinish=G.enemyFactory:GetLevelFinish()
        G.starMax =PersistentData:GetNumber("starMax",numLevelString,0)
        if(G.levelFinish==false) then
            G.starMaxAct=0
        else
            PersistentData:SetBoolean("levelFinish",numLevelString,true)
            PersistentData:Save("levelFinish")
            G.starMaxAct =G.enemyFactory:GetPlayerLifes()
            G.Path=1
        end
        if(G.starMaxAct>3) then
             G.Path=2
            G.starMaxAct=3
        end
        end
        if(G.starMaxAct>G.starMax) then
          G.Path=2
          G.starMax=G.starMaxAct
          PersistentData:SetNumber("starMax",numLevelString,starMax)
        end
      --G.starMax=0
      PersistentData:Save("starMax")
end

function OnThink(self)
  --if(self.inputMap:GetTrigger("Enter")~=0) then
   -- G.enemyFactory:LoadLevelFilename("Scenes/Scene_Splash")
  --end
  
    --Debug:PrintAt(50, 50, "Stars: "..G.starMax, Vision.V_RGBA_PURPLE, "Fonts/eras36")
    --Debug:PrintAt(50, 90, "Path: "..G.Path, Vision.V_RGBA_GREEN, "Fonts/eras36")
    --Debug:PrintAt(50, 130, "Lifes: "..G.enemyFactory:GetPlayerLifes(), Vision.V_RGBA_RED, "Fonts/eras36")
    --Debug:PrintAt(50, 150, "starMaxAct: "..G.starMaxAct, Vision.V_RGBA_BLUE, "Fonts/eras36")
    --Debug:PrintAt(50, 190, "numLevel: "..G.enemyFactory:GetCurrentLevel(), Vision.V_RGBA_BLUE, "Fonts/eras36")
end

function OnBeforeSceneUnloaded()

  local mode = Application:GetEditorMode()

  if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then

     GUI:SetCursorVisible(false)

     GUI:CloseDialog( G.myDialog )

  end

end