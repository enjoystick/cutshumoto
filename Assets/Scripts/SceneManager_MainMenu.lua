-- new script file
-- new script file
function OnAfterSceneLoaded(self)
      G.enemyFactory = Game:CreateEntity( Vision.hkvVec3(0,0,0), "EnemyFactoryLUA" )
      
      local width, height = Screen:GetViewportSize()

      -- create an input map with specific dimensions
      numTriggers = 1 --the max number of triggers we can use with this map
      numAlternatives = 2 --we are going to map 5 controls to the trigger "Jump"

      self.inputMap = Input:CreateMap("Player", numTriggers, numAlternatives)
      
      -- map a trigger to the touch area 
      self.inputMap:MapTrigger("Enter", {0,0, width,height},  "CT_TOUCH_ANY", {once = true})
      
      --map a trigger to mouse button
      self.inputMap:MapTrigger("Enter", "MOUSE", "CT_MOUSE_LEFT_BUTTON", {once = true})


        GUI:LoadResourceFile("Dialogs/MenuSystem.xml") --load resources

        if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
          G.myDialog = GUI:ShowDialog("Dialogs/MainMenuLUA.xml") --show GUI
          GUI:SetCursorVisible(true) --make the mouse cursor visible
        end
      
    end

function OnThink(self)
  --if(self.inputMap:GetTrigger("Enter")~=0) then
   -- G.enemyFactory:LoadLevelFilename("Scenes/Scene_Splash")
  --end
end

function OnBeforeSceneUnloaded()

  local mode = Application:GetEditorMode()

  if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then

     GUI:SetCursorVisible(false)

     GUI:CloseDialog( G.myDialog )

  end

end




