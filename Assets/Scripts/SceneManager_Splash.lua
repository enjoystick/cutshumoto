-- new script file
function OnAfterSceneLoaded(self)
      G.enemyFactory = Game:CreateEntity( Vision.hkvVec3(0,0,0), "EnemyFactoryLUA" )
      
      local width, height = Screen:GetViewportSize()

      -- create an input map with specific dimensions
      numTriggers = 1 --the max number of triggers we can use with this map
      numAlternatives = 2 --we are going to map 5 controls to the trigger "Jump"

      self.inputMap = Input:CreateMap("Player", numTriggers, numAlternatives)
      
      -- map a trigger to the touch area 
      self.inputMap:MapTrigger("Enter", {0,0, width,height},  "CT_TOUCH_ANY", {once = true})
      
      --map a trigger to mouse button
      self.inputMap:MapTrigger("Enter", "MOUSE", "CT_MOUSE_LEFT_BUTTON", {once = true})


      
    end

function OnThink(self)
  if(self.inputMap:GetTrigger("Enter")~=0) then
    G.enemyFactory:LoadLevelFilename("Scenes/Scene_MainMenu")
  end
end