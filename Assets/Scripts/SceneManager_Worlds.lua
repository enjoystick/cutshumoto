-- new script file
-- new script file
function OnAfterSceneLoaded(self)
     G.enemyFactory = Game:CreateEntity( Vision.hkvVec3(0,0,0), "EnemyFactoryLUA" )
      
      local width, height = Screen:GetViewportSize()

      -- create an input map with specific dimensions
     --numTriggers = 1 --the max number of triggers we can use with this map
     --numAlternatives = 2 --we are going to map 5 controls to the trigger "Jump"

      --self.inputMap = Input:CreateMap("Player", numTriggers, numAlternatives)
      
      -- map a trigger to the touch area 
      --self.inputMap:MapTrigger("Enter", {0,0, width,height},  "CT_TOUCH_ANY", {once = true})
      
      --map a trigger to mouse button
     -- self.inputMap:MapTrigger("Enter", "MOUSE", "CT_MOUSE_LEFT_BUTTON", {once = true})


        GUI:LoadResourceFile("Dialogs/MenuSystem.xml") --load resources

        if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
          G.myDialog = GUI:ShowDialog("Dialogs/WorldsMenuLUA.xml") --show GUI
          GUI:SetCursorVisible(true) --make the mouse cursor visible
        end
        
        G.enemyFactory = Game:CreateEntity( Vision.hkvVec3(0,0,0), "EnemyFactoryLUA" )
        Game:InstantiatePrefab(Vision.hkvVec3(-1829.31,38.61007,0),"Prefabs/World1.vprefab",self)
        PersistentData:Load("levelFinish")
        PersistentData:Load("starMax")
        --Temporal desbloqueo total de niveles
        --[[
        PersistentData:SetBoolean("levelFinish","1",true)
        PersistentData:SetBoolean("levelFinish","2",true)
        PersistentData:SetBoolean("levelFinish","3",true)
        PersistentData:SetBoolean("levelFinish","4",true)
        PersistentData:SetBoolean("levelFinish","5",true)
        PersistentData:SetBoolean("levelFinish","6",true)
        PersistentData:SetBoolean("levelFinish","7",true)
        PersistentData:SetBoolean("levelFinish","8",true)
        PersistentData:SetBoolean("levelFinish","9",true)
        PersistentData:SetBoolean("levelFinish","10",true)
        PersistentData:SetBoolean("levelFinish","11",true)
        PersistentData:SetBoolean("levelFinish","12",true)
        PersistentData:SetBoolean("levelFinish","13",true)
        PersistentData:SetBoolean("levelFinish","14",true)
        PersistentData:SetBoolean("levelFinish","15",true)
        ]]

        G.lvl2=PersistentData:GetBoolean("levelFinish","1",false)
        G.lvl3=PersistentData:GetBoolean("levelFinish","2",false)
        G.lvl4=PersistentData:GetBoolean("levelFinish","3",false)
        G.lvl5=PersistentData:GetBoolean("levelFinish","4",false)
        G.CheatLevelAct=0
        if(G.lvl2==true) then
          StarReplace("1","DeadStar1_1","DeadStar1_2","DeadStar1_3")
          lock2=Game:GetEntity("lock2")
          lock2:Remove()
          G.CheatLevelAct=1
        end
        if(G.lvl3==true) then
          StarReplace("2","DeadStar2_1","DeadStar2_2","DeadStar2_3")
          lock3=Game:GetEntity("lock3")
          lock3:Remove()
          G.CheatLevelAct=2
        end
        if(G.lvl4==true) then
          StarReplace("3","DeadStar3_1","DeadStar3_2","DeadStar3_3")
          lock4=Game:GetEntity("lock4")
          lock4:Remove()
          G.CheatLevelAct=3
        end
        if(G.lvl5==true) then
        StarReplace("4","DeadStar4_1","DeadStar4_2","DeadStar4_3")
          lock5=Game:GetEntity("lock5")
          lock5:Remove()
          G.CheatLevelAct=4
        end
        local WorldFinish1=PersistentData:GetBoolean("levelFinish","5",false)
        if WorldFinish1==false then
          Game:InstantiatePrefab(Vision.hkvVec3(0,0,0),"Prefabs/Locked.vprefab",self)
        end
        if WorldFinish1==true then
        
          G.CheatLevelAct=5
          Game:InstantiatePrefab(Vision.hkvVec3(0,0,0),"Prefabs/World2.vprefab",self)
          G.lvl7=PersistentData:GetBoolean("levelFinish","6",false)
          G.lvl8=PersistentData:GetBoolean("levelFinish","7",false)
          G.lvl9=PersistentData:GetBoolean("levelFinish","8",false)
          G.lvl10=PersistentData:GetBoolean("levelFinish","9",false)
          StarReplace("5","DeadStar5_1","DeadStar5_2","DeadStar5_3")
          if(G.lvl7==true) then
          StarReplace("6","DeadStar6_1","DeadStar6_2","DeadStar6_3")
          lock7=Game:GetEntity("lock7")
          lock7:Remove()
          G.CheatLevelAct=6
          end
          if(G.lvl8==true) then
          StarReplace("7","DeadStar7_1","DeadStar7_2","DeadStar7_3")
          lock8=Game:GetEntity("lock8")
          lock8:Remove()
          G.CheatLevelAct=7
          end
          if(G.lvl9==true) then
          StarReplace("8","DeadStar8_1","DeadStar8_2","DeadStar8_3")
          lock9=Game:GetEntity("lock9")
          lock9:Remove()
          G.CheatLevelAct=8
          end
          if(G.lvl10==true) then
          StarReplace("9","DeadStar9_1","DeadStar9_2","DeadStar9_3")
          lock10=Game:GetEntity("lock10")
          lock10:Remove()
          G.CheatLevelAct=9
          end
        end
        local WorldFinish2=PersistentData:GetBoolean("levelFinish","10",false)
        if WorldFinish2==false then
          Game:InstantiatePrefab(Vision.hkvVec3(1829.31,0,0),"Prefabs/Locked.vprefab",self)
        end

        if WorldFinish2==true then
          G.CheatLevelAct=10
          G.lvl12=PersistentData:GetBoolean("levelFinish","11",false)
          G.lvl13=PersistentData:GetBoolean("levelFinish","12",false)
          G.lvl14=PersistentData:GetBoolean("levelFinish","13",false)
          G.lvl15=PersistentData:GetBoolean("levelFinish","14",false)
          Game:InstantiatePrefab(Vision.hkvVec3(1829.31,0,0),"Prefabs/World3.vprefab",self)
          StarReplace("10","DeadStar10_1","DeadStar10_2","DeadStar10_3")
        if(G.lvl12==true) then
          StarReplace("11","DeadStar11_1","DeadStar11_2","DeadStar11_3")
          lock12=Game:GetEntity("lock12")
          lock12:Remove()
          G.CheatLevelAct=11
        end
        if(G.lvl13==true) then
          StarReplace("12","DeadStar12_1","DeadStar12_2","DeadStar12_3")
          lock13=Game:GetEntity("lock13")
          lock13:Remove()
          G.CheatLevelAct=12
        end
        if(G.lvl14==true) then
          StarReplace("13","DeadStar13_1","DeadStar13_2","DeadStar13_3")
          lock14=Game:GetEntity("lock14")
          lock14:Remove()
          G.CheatLevelAct=13
        end
        if(G.lvl15==true) then
          StarReplace("14","DeadStar14_1","DeadStar14_2","DeadStar14_3")
          lock15=Game:GetEntity("lock15")
          lock15:Remove()
          G.CheatLevelAct=14
        end
        StarReplace("15","DeadStar15_1","DeadStar15_2","DeadStar15_3")
        end
        
        
        G.CheatLevelDes=G.CheatLevelAct
        
        
      
end

function OnThink(self)
  --if(self.inputMap:GetTrigger("Enter")~=0) then
   -- G.enemyFactory:LoadLevelFilename("Scenes/Scene_Splash")
  --end
  if  G.CheatLevelAct~=G.CheatLevelDes then
      G.enemyFactory:LoadLevelFilename("Scenes/Scene_Mundos")
  end
  
  
end

function OnBeforeSceneUnloaded()

  --G.myDialog:SetVisible(false)
  local mode = Application:GetEditorMode()

  if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then

     GUI:SetCursorVisible(false)

     --GUI:CloseDialog( G.myDialog )

  end

end


function StarReplace(numLevel, stringStar1,stringStar2,stringStar3)
  local stars1 =PersistentData:GetNumber("starMax",numLevel,0)
        if(stars1>0)then
        
          star1=Game:GetEntity(stringStar1)
          local star1Pos =star1:GetPosition()
          star1:Remove()
          Game:InstantiatePrefab(star1Pos,"Prefabs/Star.vprefab",self)
          if(stars1>1) then
            star2=Game:GetEntity(stringStar2)
            local star2Pos =star2:GetPosition()
            star2:Remove()
            Game:InstantiatePrefab(star2Pos,"Prefabs/Star.vprefab",self)
            if(stars1>2) then
              star3=Game:GetEntity(stringStar3)
              local star3Pos =star3:GetPosition()
              star3:Remove()
              Game:InstantiatePrefab(star3Pos,"Prefabs/Star.vprefab",self)
            end
          end
 end
end