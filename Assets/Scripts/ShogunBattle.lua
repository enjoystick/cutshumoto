-- new script file

function OnAfterSceneLoaded()

  local mode = Application:GetEditorMode()
  if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then
  
    --Camera set up-------------------------------
    local camPos = Game:GetEntity( "camPos" )
    local cam = Game:GetCamera()
    cam:SetUseEulerAngles( true )
    cam:SetPosition( camPos:GetPosition() )
    cam:SetOrientation( camPos:GetOrientation() )
    ----------------------------------------------
    
    --GUI init------------------------------------
    --GUI:LoadResourceFile( "Dialogs/MenuSystem.xml" ) --load resources
    --GUI:SetCursorVisible( true ) --show cursor
    ----------------------------------------------
  
  end

  G.mapSpeed = 0
  G.currentLevel = 0
  
  Debug:Enable( true )
  G.enemyFactory = Game:CreateEntity( Vision.hkvVec3(0,0,0), "EnemyFactoryLUA" )
  
  G.enemyFactory:PreloadEnemy(11, 0 ) -- spiderling
  G.enemyFactory:PreloadEnemy(41, 1 ) -- shuriken
  
  G.enemyFactory:ResetStats()
  G.currentLevel = 15
  G.enemyFactory:SetCurrentLevel(15)
  
  
end
