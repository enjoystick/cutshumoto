-- new script file

function OnObjectEnter(self, object)
	  
  if  G.enemyFactory:GetKeyLua(object) == "Boss" then
            
    local dragonpos = object:GetPosition()
    local trigerpos = self:GetPosition()
  
    local pos = Vision.hkvVec3(dragonpos.x,dragonpos.y,trigerpos.z)
    local effect = Game:GetEffect("cloudfx")
    
    if effect ~= nil then
      local zinc = 300
      if Timer:GetTime() > 15 then
        zinc = 0
      end
      
      effect:SetPosition(Vision.hkvVec3(0,dragonpos.y,trigerpos.z + zinc))    
      if Timer:GetTime() < 20 or Timer:GetTime() > 40 then
        effect:Restart()
      end
    end
    
  end    

end
