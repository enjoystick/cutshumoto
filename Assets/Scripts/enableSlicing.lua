
function OnObjectEnter(self, object)

  if object == nil then
  	if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
    	Debug:PrintLine( "enableSlicing OnObjectEnter object: es nil" )
	end
  else
	local properties = object:GetComponentOfType("CmpEnemyProperties")

	if properties == nil then
		if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
      		Debug:PrintLine("enableSlicing cmpEnemyProperties nil")  
  		end
	else  
	  properties.SetCanBeSliced(true)  
	end
  end
	
end
