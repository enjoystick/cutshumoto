	

    --G.useRemoteInput = false and (Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11")
     
     
    local cam
    local camPos
    local pathstate
    local timeToWait
    local secsAnim
     
     
    function InitInput(self)
      --self.map = Input:CreateMap("InputMap")
      --local w, h = Screen:GetViewportSize()
     
      -- Put the camera control button in the top right corner
      --self.screenMask = Game:CreateScreenMask(16, 16, "Textures\\GUI\\freeCamera.dds")
      --self.screenMask:SetBlending(Vision.BLEND_ALPHA)
      --local smw, smh = self.screenMask:GetTextureSize()
      --self.screenMask:SetPos( w - smw - 16, 16 );
     
      -- Touch input
      --self.map:MapTrigger("TOUCH_ANY", { w - smw - 16, 16, w - 16, smh + 16 }, "CT_TOUCH_ANY", {once = true})
     
      -- Mouse
      --self.map:MapTrigger("TOUCH_ANY", "MOUSE", "CT_MOUSE_LEFT_BUTTON", {once = true})
    end
     
    function Init(self)
      InitInput(self)
     
      cam = Game:GetCamera()
      camPos = Game:GetEntity( "camPos" )
      cam:SetUseEulerAngles( true )
      self.pathcam = Game:GetEntity("PathCamera0_0")
      cam:SetPosition( camPos:GetPosition() )
      cam:SetOrientation( camPos:GetOrientation() )
      self.showInfo = false
     
      G.pathstate=0
      timeToWait=0
      secsAnim=0.8
     
      G.XSwipe=0
      G.XSwiping =false
      G.XSwipingOnce= false
     
      SetPathCamera(self)
    end
     
    function OnAfterSceneLoaded(self)
      Init(self);
       
      self.map = Input:CreateMap("CameraMap")
      local w, h = Screen:GetViewportSize()
     
      -- touch triggers
      if (Application:GetPlatformName() == "ANDROID") or (Application:GetPlatformName() == "IOS") or (Application:GetPlatformName() == "TIZEN") then
        self.map:MapTrigger("Touch", {0, 0, w, h}, "CT_TOUCH_ANY", {once = false})
        self.map:MapTrigger("X", {0, 0, w, h}, "CT_TOUCH_ABS_X")
        self.map:MapTrigger("Y", {0, 0, w, h}, "CT_TOUCH_ABS_Y")
      else
        self.map:MapTrigger("X", "MOUSE", "CT_MOUSE_ABS_X")
        self.map:MapTrigger("Y", "MOUSE", "CT_MOUSE_ABS_Y")
        --self.map:MapTrigger("XNORM", "MOUSE", "CT_MOUSE_NORM_X")
        --self.map:MapTrigger("YNORM", "MOUSE", "CT_MOUSE_NORM_Y")
        self.map:MapTrigger("Left", "MOUSE", "CT_MOUSE_LEFT_BUTTON")
        self.map:MapTrigger("Right", "MOUSE", "CT_MOUSE_RIGHT_BUTTON")
      end
         self.pathcam = Game:GetEntity("PathCamera0_0")
         SetPathCamera(self)
    end
     
    function OnBeforeSceneUnloaded(self)
      if (self.map ~= nil) then
        Input:DestroyMap(self.map)
        self.map = nil
      end
      Game:DeleteAllUnrefScreenMasks()
      cam:AttachToEntity(nil);
    end
     
     
    function SetPathCamera(self)
      if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
        Debug:PrintLine("Activating path camera")
      end
     
      self.pathcam:Start();
      cam:AttachToEntity(self.pathcam);
      cam:Set(self.pathcam:GetRotationMatrix(), self.pathcam:GetPosition());
     
    end
    function RightOrLeft(self, entity)
            local pathstateAux
            local  pathDone=false
           
            if (G.XSwipe-G.x>20) and G.pathstate== 1 and pathDone==false and G.XSwiping==true then
               self.pathcam = Game:GetEntity("PathCamera1_2")
               G.pathstate=2
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
     
            end
           if (G.x-G.XSwipe>20)  and G.pathstate== 2 and pathDone==false and G.XSwiping==true then
               self.pathcam = Game:GetEntity("PathCamera2_1")
               G.pathstate=1
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
            end
            if (G.XSwipe-G.x>20) and G.pathstate== 2 and pathDone==false and G.XSwiping==true then
               self.pathcam = Game:GetEntity("PathCamera2_3")
               G.pathstate=3
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
            end
            if (G.x-G.XSwipe>20) and G.pathstate== 3  and pathDone==false and G.XSwiping==true then
               self.pathcam = Game:GetEntity("PathCamera3_2")
               G.pathstate=2
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
            end
           
             --[[if entity:GetKey() == "LeftRight1" and pathstate== 1 and pathDone==false then
               self.pathcam = Game:GetEntity("PathCamera1_2")
               pathstateAux=2
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               --return
            end
            if entity:GetKey() == "LeftRight1" and pathstate== 2 and pathDone==false  then
               self.pathcam = Game:GetEntity("PathCamera2_1")
               pathstateAux=1
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               --return
            end
            if entity:GetKey() == "LeftRight2" and pathstate== 2 and pathDone==false then
               self.pathcam = Game:GetEntity("PathCamera2_3")
               pathstateAux=3
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               --return
            end
             if entity:GetKey() == "LeftRight2" and pathstate== 3 and pathDone==false then
               self.pathcam = Game:GetEntity("PathCamera3_2")
               pathstateAux=2
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               --return
            end]]--
            self.picked=nil
            pathstate=pathstateAux
    end
     
    function SwipeInit()
      if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
        Debug.Draw:Line2D(G.x,G.y,G.x+10,G.y+5, Vision.V_RGBA_PURPLE)
        Debug.Draw:Line2D(G.x,G.y,G.x+5,G.y+10, Vision.V_RGBA_PURPLE)
        Debug.Draw:Line2D(G.x+10,G.y+5,G.x+5,G.y+10, Vision.V_RGBA_PURPLE)
      end
      if(G.XSwipingOnce==false) then
        G.XSwiping=true
        G.XSwipingOnce=true
        G.XSwipe=G.x
      end
    end
     
    function SwipeDeInit()
      if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
        Debug.Draw:Line2D(G.x,G.y,G.x+10,G.y+5, Vision.V_RGBA_BLUE)
        Debug.Draw:Line2D(G.x,G.y,G.x+5,G.y+10, Vision.V_RGBA_BLUE)
        Debug.Draw:Line2D(G.x+10,G.y+5,G.x+5,G.y+10, Vision.V_RGBA_BLUE)
       end
      if(G.XSwipingOnce==true) then
        G.XSwiping=false
        G.XSwipingOnce=false
        G.XSwipe=0
      end
    end
    function OnThink(self)
          local x = self.map:GetTrigger("X")
          local y = self.map:GetTrigger("Y")
          G.x = self.map:GetTrigger("X")
          G.y =self.map:GetTrigger("Y")
          if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
            Debug.Draw:Line2D(x,y,x+10,y+5, Vision.V_RGBA_GREEN)
            Debug.Draw:Line2D(x,y,x+5,y+10, Vision.V_RGBA_GREEN)
            Debug.Draw:Line2D(x+10,y+5,x+5,y+10, Vision.V_RGBA_GREEN)
          end
     
        local isPickingEnabled = G.enemyFactory:IsPickingEnabled()

        if (self.map:GetTrigger("Left")>0 or self.map:GetTrigger("Touch")>0) and timeToWait<Timer:GetTime() and isPickingEnabled == true then
     
          local entity = Screen:PickEntity(x,y)
          
     
          if entity~=nil --[[and entity ~= self.picked]] then
            Debug:PrintLine("printado")  
            self.picked=entity
             --
            if entity:GetKey() == "world1" and G.pathstate== 0 then
               self.pathcam = Game:GetEntity("PathCamera0_1")
               G.pathstate=1
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               return
            end
            if entity:GetKey() == "world2" and G.pathstate== 0 then
               self.pathcam = Game:GetEntity("PathCamera0_2")
               G.pathstate=2
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               return
            end
            if entity:GetKey() == "world3" and G.pathstate== 0 then
               self.pathcam = Game:GetEntity("PathCamera0_3")
               G.pathstate=3
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               return
            end
            if entity:GetKey() == "back1" and G.pathstate== 1 then
               self.pathcam = Game:GetEntity("PathCamera1_0")
               G.pathstate=0
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               return
            end
            if entity:GetKey() == "back2" and G.pathstate== 2 then
               self.pathcam = Game:GetEntity("PathCamera2_0")
               G.pathstate=0
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               return
            end
             if entity:GetKey() == "back3" and G.pathstate== 3 then
               self.pathcam = Game:GetEntity("PathCamera3_0")
               G.pathstate=0
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               return
            end
            if entity:GetKey() == "level1" and G.pathstate== 1 then
               G.enemyFactory:LoadLevelSwig(1)
               return
            end
            if entity:GetKey() == "level2" and G.pathstate== 1 and G.lvl2==true then
               G.enemyFactory:LoadLevelSwig(2)
               return
            end
            if entity:GetKey() == "level3" and G.pathstate== 1 and G.lvl3==true then
               G.enemyFactory:LoadLevelSwig(3)
               return
            end
            if entity:GetKey() == "level4" and G.pathstate== 1 and G.lvl4==true then
               G.enemyFactory:LoadLevelSwig(4)
               return
            end
            if entity:GetKey() == "level5" and G.pathstate== 1 and G.lvl5==true then
               G.enemyFactory:LoadLevelSwig(5)
               return
            end
            if entity:GetKey() == "level6" and G.pathstate== 2 then
               G.enemyFactory:LoadLevelSwig(6)
               return
            end
            if entity:GetKey() == "level7" and G.pathstate== 2 and G.lvl7==true then
               G.enemyFactory:LoadLevelSwig(7)
               return
            end
            if entity:GetKey() == "level8" and G.pathstate== 2 and G.lvl8==true then
               G.enemyFactory:LoadLevelSwig(8)
               return
            end
            if entity:GetKey() == "level9" and G.pathstate== 2 and G.lvl9==true then
               G.enemyFactory:LoadLevelSwig(9)
               return
            end
             if entity:GetKey() == "level10" and G.pathstate== 2 and G.lvl10==true then
               G.enemyFactory:LoadLevelSwig(10)
               return
            end
            if entity:GetKey() == "level11" and G.pathstate== 3  then
               G.enemyFactory:LoadLevelSwig(11)
               return
            end
            if entity:GetKey() == "level12" and G.pathstate== 3 and G.lvl12==true then
               G.enemyFactory:LoadLevelSwig(12)
               return
            end
            if entity:GetKey() == "level13" and G.pathstate== 3 and G.lvl13==true then
               G.enemyFactory:LoadLevelSwig(13)
               return
            end
            if entity:GetKey() == "level14" and G.pathstate== 3 and G.lvl14==true then
               G.enemyFactory:LoadLevelSwig(14)
               return
            end
            if entity:GetKey() == "level15" and G.pathstate== 3 and G.lvl15==true then
               G.enemyFactory:LoadLevelSwig(15)
               return
            end
            if entity:GetKey() == "level16" then
              Debug:PrintLine("WEEEEEEEEEEE")
               G.enemyFactory:LoadLevelFilename("Scene_Endless.vscene")
               return
            end            
            pathDone=false
           --[[ if entity:GetKey() == "LeftRight1" and pathstate== 1 and pathDone==false then
               self.pathcam = Game:GetEntity("PathCamera1_2")
               pathstate=2
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               --return
            end
            if entity:GetKey() == "LeftRight1" and pathstate== 2 and pathDone==false  then
               self.pathcam = Game:GetEntity("PathCamera2_1")
               pathstate=1
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               --return
            end
            if entity:GetKey() == "LeftRight2" and pathstate== 2 and pathDone==false then
               self.pathcam = Game:GetEntity("PathCamera2_3")
               pathstate=3
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               --return
            end
             if entity:GetKey() == "LeftRight2" and pathstate== 3 and pathDone==false then
               self.pathcam = Game:GetEntity("PathCamera3_2")
               pathstate=2
               pathDone=true
               SetPathCamera(self)
               timeToWait=Timer:GetTime()+secsAnim
               --return
            end]]--
           
     
            --SwipeCont()
          end
          RightOrLeft(self, entity)
          SwipeInit()
     
        elseif (self.map:GetTrigger("Left")==0 and self.map:GetTrigger("Touch")==0) then
            SwipeDeInit()
          end
     
     
    end

