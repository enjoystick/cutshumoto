-- new script file

local cam
local camPos
local rails
  
function OnAfterSceneLoaded(self)

	camPos = Game:GetEntity( "camPos" )
  cam = Game:GetCamera()
  cam:SetUseEulerAngles( true )
  G.levelReach = false;

  GUI:LoadResourceFile("Dialogs/MenuSystem.xml") --load resources

  if Application:GetPlatformName() == "WIN32DX9" or Application:GetPlatformName() == "WIN32DX11" then
    G.myDialog = GUI:ShowDialog("Dialogs/IngameMenuLUA.xml") --show GUI
    GUI:SetCursorVisible(true) --make the mouse cursor visible
  end

end

function OnThink(self)

  --local speed = Vision.hkvVec3(0,300 * Timer:GetTimeDiff(),0)
  --self:SetPosition( self:GetPosition() + speed )
  --Game:GetCamera():IncPosition(speed)
    
  --Camera set up-------------------------------
  if cam ~= nil and camPos ~= nil then
    cam:SetPosition( camPos:GetPosition() )
    cam:SetOrientation( camPos:GetOrientation() )
  end
  ----------------------------------------------
  
  
  local lifes = G.enemyFactory:GetPlayerLifes()
  local score = G.enemyFactory:GetScore()
  local bossend = G.enemyFactory:GetBossFinish()
  --GUI:LoadResourceFile("Dialogs/MenuSystem.xml") --load resources

 --G.myDialog = GUI:ShowDialog("Dialogs/IngameMenuLUA.xml") --show GUI

  --GUI:SetCursorVisible(true) --make the mouse cursor visible
  
  --PersistentData:Load("scoreMax")
  -- PersistentData:Load("levelFinish")
  if lifes > 0 then
    --Debug:PrintAt(50, 50, "Points: "..score, Vision.V_RGBA_PURPLE, "Fonts/eras36")
    --Debug:PrintAt(50, 90, "Lives: "..lifes, Vision.V_RGBA_GREEN, "Fonts/eras36")
    --Debug:PrintAt(50, 140, "MaxScore: "..PersistentData:GetNumber("scoreMax",G.CurrentLevelString,0), Vision.V_RGBA_GREEN, "Fonts/eras36")
   -- pruebaBool=PersistentData:GetBoolean("levelFinish",G.CurrentLevelString,false)
    --if pruebaBool==false then
    --  Debug:PrintAt(50,180, "LevelFinish:".."Sin Acabar",Vision.V_RGBA_GREEN, "Fonts/eras36")
    -- end
    -- if pruebaBool==true then
    --  Debug:PrintAt(50,180, "LevelFinish:".."Acabada",Vision.V_RGBA_GREEN, "Fonts/eras36")
    -- end
  else
    --Debug:PrintAt(400, 200, "G A M E   O V E R", Vision.V_RGBA_RED, "Fonts/eras36")
    --G.enemyFactory:LoadLevelFilename("Scenes/Scene_Mundos")
    --G.enemyFactory:LoadLevelFilename("Scenes/Scene_GameOver")
  end
  
  if G.levelFinish==true or bossend == true then
    --Debug:PrintAt(400, 200, "Y O U W I N", Vision.V_RGBA_RED, "Fonts/eras36")
    --PersistentData:Load("scoreMax")
   -- maxScore=PersistentData:GetNumber("scoreMax",G.CurrentLevelString,0)
    --if(  G.enemyFactory:GetScoreMax()>maxScore) then
    --  PersistentData:SetNumber("scoreMax",G.CurrentLevelString,G.enemyFactory:GetScoreMax())
    --end
    
    if G.levelReach == false then

      PersistentData:SetBoolean("levelFinish",G.CurrentLevelString,true)
      -- PersistentData:Save("scoreMax")
      PersistentData:Save("levelFinish")
      --G.enemyFactory:LoadLevelFilename("Scenes/Scene_GameOver")
      G.enemyFactory:SetLevelFinish(true)

      
      G.Path=0
      G.starMaxAct=0
      G.starMax=0
      G.LevelFinish=false
      local numLevel =G.enemyFactory:GetCurrentLevel()
      local numLevelString = ""
      numLevelString= numLevelString.. numLevel
      PersistentData:Load("levelFinish")
      PersistentData:Load("starMax")
      if(numLevel ~= 5 and numLevel ~= 10 and numLevel ~= 15) then
        local scoreMax =G.enemyFactory:GetScoreMax()
        local score= G.enemyFactory:GetScore()
        G.levelFinish=G.enemyFactory:GetLevelFinish()
        G.starMax =PersistentData:GetNumber("starMax",numLevelString,0)
        G.starMaxAct =G.enemyFactory:CalculateStars(score,scoreMax,G.levelFinish)
        PersistentData:SetBoolean("levelFinish",numLevelString,G.levelFinish)
        --PersistentData:Save("levelFinish")
      else
      G.levelFinish=G.enemyFactory:GetLevelFinish()
      G.starMax =PersistentData:GetNumber("starMax",numLevelString,0)
      if(G.levelFinish==false) then
          G.starMaxAct=0
      else
          PersistentData:SetBoolean("levelFinish",numLevelString,true)
          --PersistentData:Save("levelFinish")
          G.starMaxAct =G.enemyFactory:GetPlayerLifes()
          G.Path=1
      end
      if(G.starMaxAct>3) then
           G.Path=2
          G.starMaxAct=3
      end
      end
      if(G.starMaxAct>G.starMax) then
        G.Path=2
        G.starMax=G.starMaxAct
        PersistentData:SetNumber("starMax",numLevelString,starMax)
      end
      
      PersistentData:Save("levelFinish")
      PersistentData:Save("starMax")
    

      G.enemyFactory:OnEndOfLevelReached()
      G.levelFinish = false
      G.levelReach = true
    end
  end
  
end



function OnBeforeSceneUnloaded()

  local mode = Application:GetEditorMode()
  
  --GUI:CloseDialog( G.myDialog )

  if mode == Vision.EDITOR_PLAY or mode == Vision.EDITOR_RUN then

     GUI:SetCursorVisible(false)
     
     GUI:CloseDialog( G.myDialog )
     
  end

end


