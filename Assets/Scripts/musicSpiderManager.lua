-- new script file
local volNum
local music
local music2
local music3
local trigger1
local trigger2
local trigger3
function OnAfterSceneLoaded(self)
  music=Fmod:GetSound("MusicWorld1")
  music2=Fmod:GetSound("IntroSpiderMusic")
  music3=Fmod:GetSound("LoopSpiderMusic")
  --self:SetVolume(0)
  
  volNum=1
  music:Play(0,true)
  trigger1=true
  trigger2=true
  trigger3=true
  
end

function OnThink(self)
  if(trigger1==true and 10<Timer:GetTime()) then
    music:FadeOut(5)
    trigger1=false
  end
  if(15<Timer:GetTime() and trigger2==true) then
      music:Stop()
     music2:Play(0,true)
     trigger2=true
  end
  if(20<Timer:GetTime() and trigger3==true) then
      music2:Stop()
      music3:Play(0,true)
      trigger3=true
  end
  if(G.enemyFactory:GetSoundFinish()==true) then
    volNum=volNum-0.005
    music3:SetVolume(volNum)
  end
end