-- new script file

function OnThink(self)

  local combo = G.enemyFactory:GetCurrentCombo()
  
  if combo == 1 and G.lastCombo ~= 1 then
    Debug:PrintAt(500, 100, "Combo X" .. G.lastCombo, Vision.V_RGBA_PURPLE, "Fonts/eras36")    
  end
  
  G.lastCombo = combo
  
end
