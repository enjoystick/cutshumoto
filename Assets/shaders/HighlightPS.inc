//
#include <Shaders/VisionCommon.inc>
#include <Shaders/SkinningVS.inc>

struct PS_IN
{
  float4 ProjPos  : SV_Position;
  float4 Color    : COLOR;
};

float4 ps_main( PS_IN In ) : SV_Target
{
  return In.Color;
}