//
#include <Shaders/VisionCommon.inc>
#include <Shaders/SkinningVS.inc>

#ifdef _VISION_DX10
cbuffer g_GlobalConstantBufferObject : register (b1)
{
  float4x4 matMV   : packoffset(c0);   // model view matrix
  float4x4 matMVP  : packoffset(c4);   // model view projection matrix
}
#else
  float4x4 matMV   : register(c0);
  float4x4 matMVP  : register(c8);
#endif

#if defined(_VISION_DX10)
cbuffer g_GlobalConstantBufferUser : register (b2)
{
  float4 HighlightColor : packoffset(c0);
}
#elif defined(_VISION_GLES2)
  float4 HighlightColor : register(c64);
#else
  float4 HighlightColor;
#endif

struct VS_IN
{
  float4 ObjPos   : POSITION;
  float3 Normal   : NORMAL;
  SKINNING_VERTEX_STREAMS
};

struct VS_OUT
{
  float4 ProjPos  : SV_Position;
  float4 Color    : COLOR;
};

#ifndef USE_SKINNING
VS_OUT vs_main( VS_IN In )
#else
VS_OUT vs_main_skinning( VS_IN In )
#endif
{
  VS_OUT Out;
   
  #ifndef USE_SKINNING
    float4 ObjPos = In.ObjPos;
  #else
    float4 ObjPos;
    PREPARE_SKINNING(In.BoneIndices);
    TRANSFORM_OBJECT_POS(ObjPos, In.ObjPos, In.BoneWeight);
  #endif
   
   Out.ProjPos = mul( matMVP, ObjPos );
   
  // Output normal, tangent and bi-tangent in worldspace
  float3 Normal;
   
  #ifndef USE_SKINNING
  	Normal = In.Normal;
  #else
  	TRANSFORM_OBJECT_NORMAL(Normal, In.Normal, In.BoneWeight);
  #endif
   
   // Normal and directionto eyespace
   float3 vNormal = normalize( mul( (float3x3)matMV, Normal ));
   float3 vVertexDir = -normalize( mul( matMV, ObjPos ).xyz);
   
   // Direction in eyespace
   Out.Color = HighlightColor;
   Out.Color.a *= (1-dot(vNormal,vVertexDir));
   return Out;
}