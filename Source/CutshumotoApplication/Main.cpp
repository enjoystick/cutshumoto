/*
 *
 * Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
 * prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
 * Product and Trade Secret source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2014 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
 *
 */

#include "CutshumotoApplicationPCH.h"
#include <Vision/Runtime/Framework/VisionApp/VAppImpl.hpp>
#include <Vision/Runtime/Framework/VisionApp/Modules/VHelp.hpp>
#include <Vision/Runtime/Framework/VisionApp/Modules/VLoadingScreen.hpp>

#include <string>
#include <sstream>


#define LS_LOGO 15
#define LS_BIOMBO 16

char* g_pcLoadingScreens[17] = 
{
	"Textures\\Tip_cut_the_enemies.png", 
	"Textures\\Tip_cut_range_area.png", 
	"Textures\\Tip_armored.png", 
	"Textures\\Tip_get_life.png", 
	"Textures\\Tip_Spider.png", 
	"Textures\\Tip_tap_shuriken.png", 
	"Textures\\Tip_stamina.png", 
	"Textures\\Tip_cut_range_area.png", 
	"Textures\\Tip_stars.png", 
	"Textures\\Tip_Dragon.png", 
	"Textures\\Tip_citizen.png", 
	"Textures\\Tip_armored.png", 
	"Textures\\Tip_get_life.png", 
	"Textures\\Tip_cut_the_enemies.png", 
	"Textures\\Tip_Shogun.png", 
	"Textures\\Gui_enjoystick.png", 
	"Textures\\Tip_biombo.png"
};

// Use the following line to initialize a plugin that is statically linked.
// Note that only Windows platform links plugins dynamically (on Windows you can comment out this line).
VIMPORT IVisPlugin_cl* GetEnginePlugin_CutshumotoPlugin();

class CutshumotoApplicationClass : public VAppImpl, public IVisCallbackHandler_cl
{
public:
	CutshumotoApplicationClass() {}
	virtual ~CutshumotoApplicationClass() {}

	virtual void OnHandleCallback( IVisCallbackDataObject_cl* pData ) HKV_OVERRIDE;

	//callback funtionality
	void OnBeforeSceneLoaded( VisSceneLoadedDataObject_cl* pSceneData );

	virtual void SetupAppConfig(VisAppConfig_cl& config) HKV_OVERRIDE;
	virtual void PreloadPlugins() HKV_OVERRIDE;

	virtual void Init() HKV_OVERRIDE;
	virtual void AfterSceneLoaded(bool bLoadingSuccessful) HKV_OVERRIDE;
	virtual void AfterEngineInit();
	virtual bool Run() HKV_OVERRIDE;
	virtual void DeInit() HKV_OVERRIDE;

private:
	bool m_bFirstLoadingScreenTriggered;
};

VAPP_IMPLEMENT_SAMPLE(CutshumotoApplicationClass);

void CutshumotoApplicationClass::OnHandleCallback( IVisCallbackDataObject_cl* pData )
{
	if ( pData->m_pSender == &Vision::Callbacks.OnBeforeSceneLoaded ) 
		OnBeforeSceneLoaded( static_cast<VisSceneLoadedDataObject_cl*>(pData) );
}

void CutshumotoApplicationClass::OnBeforeSceneLoaded( VisSceneLoadedDataObject_cl* pSceneData ) 
{
	int iNextLoadingScreen = -1;
	int iDotPos = std::string( pSceneData->m_szSceneFileName ).find( "." );
	char cLastChar = pSceneData->m_szSceneFileName[ iDotPos - 1 ];
	if ( isdigit( cLastChar ) ) 
	{ // Playable scene
		std::ostringstream ss;
		char cPenultimateChar = pSceneData->m_szSceneFileName[ iDotPos - 2 ];
		if ( isdigit( cPenultimateChar ) ) 
		{
			ss << (cPenultimateChar - '0');
		}
		ss << (cLastChar - '0');
		iNextLoadingScreen = atoi( ss.str().c_str() ) - 1;
	} 
	else 
	{ // Main_menu/Selection_level scene
		if ( !m_bFirstLoadingScreenTriggered ) iNextLoadingScreen = LS_BIOMBO;
		else m_bFirstLoadingScreenTriggered = false;
	}
	VLoadingScreen* pLoadingScreen = GetAppModule<VLoadingScreen>();
	if ( pLoadingScreen && iNextLoadingScreen >= 0 ) 
	{
		VLoadingScreenBase::Settings tRefSettings = pLoadingScreen->GetSettings();
		tRefSettings.m_sImagePath = g_pcLoadingScreens[iNextLoadingScreen];
		pLoadingScreen->SetSettings( tRefSettings );
	}
}

void CutshumotoApplicationClass::SetupAppConfig(VisAppConfig_cl& config)
{
  // Set custom file system root name ("havok_sdk" by default)
  config.m_sFileSystemRootName = "template_root";

  // Set the initial starting position of our game window and other properties
  // if not in fullscreen. This is only relevant on windows
  config.m_videoConfig.m_iXRes = 1280; // Set the Window size X if not in fullscreen.
  config.m_videoConfig.m_iYRes = 720;  // Set the Window size Y if not in fullscreen.
  config.m_videoConfig.m_iXPos = 0;   // Set the Window position X if not in fullscreen.
  config.m_videoConfig.m_iYPos = 0;   // Set the Window position Y if not in fullscreen.

  // Name to be displayed in the windows title bar.
  config.m_videoConfig.m_szWindowTitle = "Cutshumoto!";

  config.m_videoConfig.m_bWaitVRetrace = true;

  // Fullscreen mode with current desktop resolution
  
#if defined(WIN32)
  /*
  DEVMODEA deviceMode;
  deviceMode = Vision::Video.GetAdapterMode(config.m_videoConfig.m_iAdapter);
  config.m_videoConfig.m_iXRes = deviceMode.dmPelsWidth;
  config.m_videoConfig.m_iYRes = deviceMode.dmPelsHeight;
  config.m_videoConfig.m_bFullScreen = true;
  */
#endif
}

void CutshumotoApplicationClass::AfterEngineInit()
{
	const float fLoadingSplashScreenTexWidth = 512; // Loading splash screen width

	Vision::Callbacks.OnBeforeSceneLoaded += this;

	VLoadingScreenBase::Settings setup;
	setup.m_sImagePath = g_pcLoadingScreens[LS_LOGO];
	setup.m_eAspectRatioAlignment = VLoadingScreenBase::ALIGN_NONE;
	setup.m_backgroundColor = VColorRef(0,0,0);
	setup.m_fFadeOutTime = 0.001f;
	setup.m_progressBarColor = VColorRef(255,0,0);
	setup.m_progressBarBackgroundColor = VColorRef(0,0,0,0);
	setup.m_progressBarRect = VRectanglef( 0, 0, fLoadingSplashScreenTexWidth, 0 );

	m_bFirstLoadingScreenTriggered = true;

	RegisterAppModule(new VLoadingScreen(setup));
}

void CutshumotoApplicationClass::PreloadPlugins()
{
  // Use the following line to load a plugin. Remember that, except on Windows platform, in addition
  // you still need to statically link your plugin library (e.g. on mobile platforms) through project
  // Properties, Linker, Additional Dependencies.
  VISION_PLUGIN_ENSURE_LOADED(CutshumotoPlugin);
}

//---------------------------------------------------------------------------------------------------------
// Init function. Here we trigger loading our scene
//---------------------------------------------------------------------------------------------------------
void CutshumotoApplicationClass::Init()
{
#if (defined(_DEBUG))
	// Disabling profiler in release builds for avoid performance cost associated
	Vision::Profiling.SetMethod( VIS_PROFILINGMETHOD_DISABLED );
#endif

	// Set filename and paths to our stand alone version.
	// Note: "/Data/Vision/Base" is always added by the sample framework
	VisAppLoadSettings settings("Scenes/Scene_Mundos.vscene");
	//VisAppLoadSettings settings("Scenes//Level_8.vscene");
	//VisAppLoadSettings settings("Scenes//Level_5.vscene");
	//VisAppLoadSettings settings("Scenes//Level_2.vscene");
	settings.m_customSearchPaths.Append(":template_root/Assets");
	//LoadScene(settings); 
	Vision::GetApplication()->RequestLoadScene(settings);
}

//---------------------------------------------------------------------------------------------------------
// Gets called after the scene has been loaded
//---------------------------------------------------------------------------------------------------------
void CutshumotoApplicationClass::AfterSceneLoaded(bool bLoadingSuccessful)
{
	// Define some help text
	VArray<const char*> help;
	help.Append("How to use this demo...");
	help.Append("");
	//RegisterAppModule(new VHelp(help));

	// Create a mouse controlled camera (set above the ground so that we can see the ground)
	//Vision::Game.CreateEntity("VisMouseCamera_cl", hkvVec3(0.0f, 0.0f, 170.0f));
}

//---------------------------------------------------------------------------------------------------------
// Main Loop of the application until we quit
//---------------------------------------------------------------------------------------------------------
bool CutshumotoApplicationClass::Run()
{
	return true;
}

void CutshumotoApplicationClass::DeInit()
{
  // De-Initialization
  // [...]

	Vision::Callbacks.OnBeforeSceneLoaded -= this;
}

/*
 * Havok SDK - Base file, BUILD(#20140328)
 * 
 * Confidential Information of Havok.  (C) Copyright 1999-2014
 * Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
 * Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
 * rights, and intellectual property rights in the Havok software remain in
 * Havok and/or its suppliers.
 * 
 * Use of this software for evaluation purposes is subject to and indicates
 * acceptance of the End User licence Agreement for this product. A copy of
 * the license is included with this software and is also available from salesteam@havok.com.
 * 
 */
