//============================================================================================================
//  CmpAnchorBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================

#include "CutshumotoPluginPCH.h"
#include "CmpAnchorBehaviour.h"

//  Register the class in the engine module so it is available for RTTI
V_IMPLEMENT_SERIAL( CmpAnchorBehaviour, IVObjectComponent, 0, &g_myComponentModule);


CmpAnchorBehaviour_ComponentManager CmpAnchorBehaviour_ComponentManager::g_GlobalManager;

//============================================================================================================
//  CmpAnchorBehaviour Body
//============================================================================================================

void CmpAnchorBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{  
	mEnemyInstance=false;
	hasEnemy=false;
}

void CmpAnchorBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{
}

void CmpAnchorBehaviour::onFrameUpdate()
{
	if (!GetOwner())return;

	hkvVec3 vPos = ((VisObject3D_cl *)GetOwner())->GetPosition();
	float elapsed=Vision::GetTimer()->GetTimeDifference();

	((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x,vPos.y-speed*elapsed,vPos.z);
	
	//Mirar tiempo
	#ifdef _DEBUG
		if(hasEnemy==true)
		{
			char prueba[((((sizeof contDebug)+(sizeof contDebug2)) * CHAR_BIT) + 2)/3 + 2];
			sprintf(prueba, "%d :: %d", contDebug, contDebug2);		
			contDebugString= prueba;
			Vision::Message.SetTextColor(V_RGBA_PURPLE);
			Vision::Message.DrawMessage3D(contDebugString,vPos);
			Vision::Message.SetTextColor(V_RGBA_WHITE);
		}
	#endif

	if(vPos.y<mRangeEnemyCreation && mEnemyInstance==false)	
	{
		if (anchorType != ENone)
		{	mEnemyInstance=true;		
			instanceGenerate->CreateEnemy(anchorType,vPos, hkvVec3(0, speed, 0));
		}
	}

	if(vPos.y<mRangeAnchorRemove)
	{
		//���Como eliminar objetos Owner de forma segura???
		//Ver fallo de Destruccion
		//VisTypedEngineObject_cl* pruebaDispose = this->GetOwner();
		VisBaseEntity_cl* pruebaDispose = (VisBaseEntity_cl*)this->GetOwner();
		pruebaDispose->Remove();
		//pruebaDispose->DisposeObject();
		//pruebaDispose=NULL;*/			
	}  
}

//============================================================================================================
//  CmpAnchorBehaviour Overrides
//============================================================================================================
void CmpAnchorBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	if (pOwner!=NULL)
	{
		CmpAnchorBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);    
		onStartup( pOwner );
	}
	else
	{
	    onRemove( pOwner );    
	    CmpAnchorBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpAnchorBehaviour::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPANCHORBEHAVIOUR_VERSION_CURRENT;
	IVObjectComponent::Serialize(ar);
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPANCHORBEHAVIOUR_VERSION_CURRENT , "Invalid local version.");

		//  add your property variables here
	} 
	else
	{
	    ar << iLocalVersion;

	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpAnchorBehaviour,IVObjectComponent,    "Anchor Behaviour", 
                          VVARIABLELIST_FLAGS_NONE, 
                          "AnchorBehaviour" )
 

END_VAR_TABLE
