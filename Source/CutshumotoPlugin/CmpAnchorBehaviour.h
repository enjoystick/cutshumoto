#ifndef CMPANCHORBEHAVIOUR_H_INCLUDED
#define CMPANCHORBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyGenerator.h"

// Versions
#define CMPANCHORBEHAVIOUR_VERSION_0          0     // Initial version
#define CMPANCHORBEHAVIOUR_VERSION_CURRENT    1     // Current version

//============================================================================================================
//  CmpAnchorBehaviour Class
//============================================================================================================
class CmpAnchorBehaviour : public IVObjectComponent
{
	public:
		V_DECLARE_SERIAL  ( CmpAnchorBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpAnchorBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpAnchorBehaviour()
		{
			Vision::Message.reset();    
		}

		SAMPLEPLUGIN_IMPEXP ~CmpAnchorBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE BOOL CanAttachToObject( VisTypedEngineObject_cl *pObject, VString &sErrorMsgOut )
		{
			if ( !IVObjectComponent::CanAttachToObject( pObject, sErrorMsgOut ))return FALSE;  

			if (!pObject->IsOfType( V_RUNTIME_CLASS( VisObject3D_cl )))
			{
				sErrorMsgOut = "Component can only be added to instances of VisObject3D_cl or derived classes.";
				return FALSE;
			}
			
			return TRUE;
		}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void OnVariableValueChanged(VisVariable_cl *pVar, const char * value) {}
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );
		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );

		void SetAnchorType( eEnemyType in){anchorType=in;}
		void SetHasEnemy(bool in){ hasEnemy=in;}
		void SetContDebug(int in){contDebug=in;}
		void SetContDebug2(int in){contDebug2=in;}
		void SetGenerateEnemiesInstance(CmpEnemyGenerator* in){instanceGenerate=in;}
		void SetRangeEnemyCreation(float in){mRangeEnemyCreation=in;}
		void SetRangeAnchorRemove(float in){mRangeAnchorRemove=in;}
		void SetAnchorSpeed(float in){speed=in;}
				
	protected:
		CmpEnemyGenerator* instanceGenerate;
		int mEnemyType;
		float speed;
		eEnemyType anchorType;
		const char* contDebugString;		
		bool mEnemyInstance;
		bool hasEnemy;
		int contDebug;
		int contDebug2;



		//Enemigos (Normal, Lado Normal, Armored, Normal Rapido)
		float mDefaultSpeed;
		float mDefaultPosInit;
		float mArmoredPosInit;
		float mArmoredSpeed;
		float mNormalFastPosInit;
		float mNormalFastSpeed;
		float mFatuoPosInitX;
		float mFatuoPosInitY;
		float mFatuoSpeed;
		float mProjectilPosInit;
		float mProjectilSpeed;
		float mGhostPosInit;
		float mGhostSpeed;

		float mRangeEnemyCreation;
		float mRangeAnchorRemove;
		
		float mAnchorSpeed;
	
};


//  Collection for handling playable character component
class CmpAnchorBehaviour_Collection : public VRefCountedCollection<CmpAnchorBehaviour> {};

//============================================================================================================
//  CmpAnchorBehaviour_ComponentManager Class
//============================================================================================================
class CmpAnchorBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:

		static CmpAnchorBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }
		void OneTimeInit(){  Vision::Callbacks.OnUpdateSceneFinished += this;} // listen to this callback 
		void OneTimeDeInit(){ Vision::Callbacks.OnUpdateSceneFinished -= this;} // de-register

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			VASSERT( pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished );
			// call update function on every component
			const int iCount = m_Components.Count();

			for (int i=0;i<iCount;i++){ m_Components.GetAt(i)->onFrameUpdate(); }
		}
		
		inline CmpAnchorBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpAnchorBehaviour_Collection m_Components;		
		static CmpAnchorBehaviour_ComponentManager g_GlobalManager;
};


#endif  
