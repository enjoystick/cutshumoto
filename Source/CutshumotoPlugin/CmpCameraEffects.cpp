//============================================================================================================
//  CmpCameraEffects Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpCameraEffects.h"
#include "GameManager.h"

V_IMPLEMENT_SERIAL( CmpCameraEffects, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpCameraEffects_ComponentManager CmpCameraEffects_ComponentManager::g_GlobalManager;

void CmpCameraEffects::onStartup( VisTypedEngineObject_cl *pOwner )
{
	m_pOwner = static_cast<VisBaseEntity_cl*> (pOwner);
	m_pDamageScreen = NULL;
}

void CmpCameraEffects::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
	if (m_pDamageScreen)
		m_pDamageScreen->DisposeObject();
}

void CmpCameraEffects::onFrameUpdate()
{
	float elapsedTime = Vision::GetTimer()->GetTimeDifference();
	DynArray_cl<int> effectsToRemove;

	for (unsigned int i = 0; i < m_aCurrentEffects.GetValidSize(); i++)
	{		
		switch (m_aCurrentEffects[i]->effect)
		{
			case eShakeFX:	UpdateCameraShake(elapsedTime, *m_aCurrentEffects[i]);	break;
			case eDamageFX:	UpdateDamageEffect(elapsedTime, *m_aCurrentEffects[i]); break;
			case eHealFX:	break;
		}

		if (m_aCurrentEffects[i]->elapsedTime >= m_aCurrentEffects[i]->duration)	effectsToRemove[effectsToRemove.GetFreePos()] = i;
	}

	for (unsigned int i = 0; i < effectsToRemove.GetValidSize(); i++)	
		m_aCurrentEffects.Remove(i);
	
	m_aCurrentEffects.Pack();

}

void CmpCameraEffects::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpCameraEffects_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpCameraEffects_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpCameraEffects::CameraShake ()
{
	CameraFX* effect;
	
	int effectIndex = SearchEffect(eShakeFX);

	if (effectIndex > -1)
		effect = m_aCurrentEffects[effectIndex];
	else
		effect = new CameraFX();

	effect->effect = eShakeFX;
	effect->amplitude = ShakeAmplitude;
	effect->duration = ShakeDuration;
	effect->elapsedTime = 0;

	if (effectIndex == -1)
		m_aCurrentEffects[m_aCurrentEffects.GetFreePos()] = effect;

	VisContextCamera_cl* camera = VisRenderContext_cl::GetCurrentContext()->GetCamera();	
	m_vLastCameraPos = camera->GetPosition();	
}

int CmpCameraEffects::SearchEffect(eCameraEffect effectType)
{
	for (unsigned int i = 0; i < m_aCurrentEffects.GetValidSize(); i++)
	{
		if (m_aCurrentEffects[i]->effect == effectType)
			return i;
	}

	return -1;
}

void CmpCameraEffects::DamageEffect()
{
	CameraFX* effect;
	
	int effectIndex = SearchEffect(eDamageFX);

	if (effectIndex > -1)
		effect = m_aCurrentEffects[effectIndex];
	else
		effect = new CameraFX();

	effect->effect = eDamageFX;	
	effect->duration = DamageDuration;
	effect->color = DamageScreenColor;
	effect->elapsedTime = 0;
	effect->alphaFactor = static_cast<float>(DamageScreenColor.a) / DamageDuration;

	if (effectIndex == -1)
		m_aCurrentEffects[m_aCurrentEffects.GetFreePos()] = effect;

	if (!m_pDamageScreen)	LoadDamageScreen();

	m_pDamageScreen->SetVisible(true);
}

void CmpCameraEffects::HealEffect ()
{
}

void CmpCameraEffects::UpdateDamageEffect(float const elapsedTime, CameraFX& effect)
{
	effect.elapsedTime+= elapsedTime;

	unsigned char newAlpha = effect.color.a - (int)(effect.alphaFactor * elapsedTime);
	
	effect.color = VColorRef(effect.color.r, effect.color.g, effect.color.b, newAlpha);
	if (m_pDamageScreen)
		m_pDamageScreen->SetColor(effect.color);

	if (effect.elapsedTime >= effect.duration)
	{
		if (m_pDamageScreen)	m_pDamageScreen->SetVisible(false);
		return;
	}
}

void CmpCameraEffects::UpdateCameraShake(float const elapsedTime, CameraFX& effect)
{  
	effect.elapsedTime+= elapsedTime;

	//we can get the current camera or get entity owner... we have to think about it
	VisContextCamera_cl* camera = VisRenderContext_cl::GetCurrentContext()->GetCamera();		
	
	hkvVec3 camPosition = camera->GetPosition();
	
	//reset transformation
	camera->SetPosition(m_vLastCameraPos.x, camPosition.y, m_vLastCameraPos.z);	
	
	// apply noise to the camera's position (not Y Axis)
	camPosition.x += GetCameraShakeNoise(camPosition.x, effect);	
	camPosition.z += GetCameraShakeNoise(camPosition.z, effect);
	camera->SetPosition(camPosition);	

	//effect time finished
	if (effect.elapsedTime >= effect.duration)
	{
		hkvVec3 camPosition = camera->GetPosition();		
		camera->SetPosition(m_vLastCameraPos.x, camPosition.y, m_vLastCameraPos.z);							
		return;
	}			
}

float CmpCameraEffects::GetCameraShakeNoise(float const arg, CameraFX const effect) const
{  
	float const shakeTime = effect.duration - effect.elapsedTime;
	float const shakeScalar = (effect.duration - shakeTime) / effect.duration;

	float noise = 0;
	
	if (shakeTime > 0)		
		noise = ((Vision::Game.GetFloatRand() * effect.amplitude) - (effect.amplitude / 2)) * shakeScalar;

	return noise;
}

void CmpCameraEffects::LoadDamageScreen()
{
	m_pDamageScreen = new VisScreenMask_cl();
    m_pDamageScreen->SetTransparency(VIS_TRANSP_ADDITIVE); 
	m_pDamageScreen->LoadFromFile(DamageScreenFilename); 
    m_pDamageScreen->SetPos(0,0);
    m_pDamageScreen->SetTargetSize((float)Vision::Video.GetXRes(),(float)Vision::Video.GetYRes()); 
    m_pDamageScreen->SetZVal(0.0f);
	m_pDamageScreen->SetOrder( GAO_BACK + 1000 );
	m_pDamageScreen->SetVisible(false);
}

void CmpCameraEffects::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPCAMERAEFFECTS_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPCAMERAEFFECTS_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> ShakeDuration;
		ar >> ShakeAmplitude;
		ar >> DamageScreenFilename;
		ar >> DamageScreenColor;
		ar >> DamageDuration;
	} 
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << ShakeDuration;
		ar << ShakeAmplitude;
		ar << DamageScreenFilename;
		ar << DamageScreenColor;
		ar << DamageDuration;
	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpCameraEffects,IVObjectComponent, "Camera Effects", VVARIABLELIST_FLAGS_NONE, "CmpCameraEffects" )

	DEFINE_VAR_FLOAT	(CmpCameraEffects, ShakeDuration, "camera shake duration", "0.5",0,0);
	DEFINE_VAR_FLOAT	(CmpCameraEffects, ShakeAmplitude, "camera shake amplitude", "25",0,0);
	DEFINE_VAR_VSTRING	(CmpCameraEffects, DamageScreenFilename, "screen damage filename","Textures\\damageScreen.dds",100,0,0);
	DEFINE_VAR_COLORREF	(CmpCameraEffects, DamageScreenColor, "screen damage color", "255/0/0/255",0,0);
	DEFINE_VAR_FLOAT	(CmpCameraEffects, DamageDuration, "damage effect duration", "0.5",0,0);

END_VAR_TABLE