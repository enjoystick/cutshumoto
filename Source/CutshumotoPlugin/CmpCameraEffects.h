//============================================================================================================
//  CmpCameraEffects Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPCAMERAEFFECTS_H_INCLUDED
#define CMPCAMERAEFFECTSs_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "GlobalTypes.h"

// Versions
#define  CMPCAMERAEFFECTS_VERSION_0		0     // Initial version
#define  CMPCAMERAEFFECTS_CURRENT		1     // Current version

enum eCameraEffect
{
	eShakeFX,
	eDamageFX,
	eHealFX
};

struct CameraFX
{
	eCameraEffect effect;
	float duration;
	float amplitude;
	VColorRef color;
	float alphaFactor;
	float elapsedTime;
};

//============================================================================================================
//  CmpCameraEffects Class
//============================================================================================================
class CmpCameraEffects :  public IVObjectComponent
{
	public:
		V_DECLARE_SERIAL  ( CmpCameraEffects, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpCameraEffects, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpCameraEffects(){}
		SAMPLEPLUGIN_IMPEXP ~CmpCameraEffects(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

		void CameraShake();
		void DamageEffect();
		void HealEffect();

	private:		
		void	UpdateCameraShake(float elapsedTime, CameraFX& effect);
		void	UpdateDamageEffect(float elapsedTime, CameraFX& effect);
		float	GetCameraShakeNoise(float const arg, CameraFX const effect) const;
		int		SearchEffect(eCameraEffect effect);
		void	LoadDamageScreen();
		
		DynArray_cl<CameraFX*>	m_aCurrentEffects;
		VisScreenMask_cl*		m_pDamageScreen;
		VisBaseEntity_cl*		m_pOwner;
		hkvVec3					m_vLastCameraPos;		

		//vForge vars
		float		ShakeDuration;
		float		ShakeAmplitude;
		float		DamageDuration;
		VString		DamageScreenFilename;
		VColorRef	DamageScreenColor;
		
};


//  Collection for handling playable character component
class CmpCameraEffects_Collection : public VRefCountedCollection<CmpCameraEffects> {};

//============================================================================================================
//  CmpCameraEffects_ComponentManager Class
//============================================================================================================
class CmpCameraEffects_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpCameraEffects_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpCameraEffects_Collection &Instances() { return m_Components; }

	protected:
		CmpCameraEffects_Collection m_Components;		
		static CmpCameraEffects_ComponentManager g_GlobalManager;

};


#endif  
