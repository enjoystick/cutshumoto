//============================================================================================================
//  CmpCivilBehaviour Component
//	Author: Sergio Gomez
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpCivilBehaviour.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"

V_IMPLEMENT_SERIAL( CmpCivilBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpCivilBehaviour_ComponentManager CmpCivilBehaviour_ComponentManager::g_GlobalManager;

void CmpCivilBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);

	m_eCurrentState = eCreating;	
}

void CmpCivilBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
    
}

void CmpCivilBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;			
			case eRunning:		OnExitRun();		break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;		
		case eRunning:		OnEnterRun();			break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpCivilBehaviour::onFrameUpdate()
{		
	CmpEnemyBehaviour::onFrameUpdate();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;		
		case eRunning:		OnRun();		break;
	}
	
}

void CmpCivilBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpCivilBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpCivilBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpCivilBehaviour::OnEnterCreate()
{
	CmpEnemyBehaviour::OnEnterCreate();
}

void CmpCivilBehaviour::OnCreate()
{
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//we need to change this to wait creating animation time, but we have not creating animation yet
	if (m_fStateTimer >= 1)
		ChangeState(eRunning);
	
}

void CmpCivilBehaviour::OnExitCreate()
{
}

void CmpCivilBehaviour::OnEnterRun()
{
	CmpEnemyBehaviour::OnEnterRun();	
}

void CmpCivilBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;	
	
	m_pOwner->SetPosition(m_pOwner->GetPosition() + (m_pCmpProperties->GetSpeed() * Vision::GetTimer()->GetTimeDifference()));

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();
}

void CmpCivilBehaviour::OnExitRun()
{
}