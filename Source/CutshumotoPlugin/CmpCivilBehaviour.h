//============================================================================================================
//  CmpCivilBehaviour Component
//	Author: Sergio Gomez
//============================================================================================================
#ifndef CMPCIVILBEHAVIOUR_H_INCLUDED
#define CMPCIVILEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPCIVILBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPCIVILBEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpGhostBehaviour Class
//============================================================================================================
class CmpCivilBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpCivilBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpCivilBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP	CmpCivilBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpCivilBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void ChangeState(eEnemyState newState);		

		virtual void OnEnterCreate();		virtual void OnCreate();		virtual void OnExitCreate();
		virtual void OnEnterRun();			virtual void OnRun();			virtual void OnExitRun();
		
};


//  Collection for handling playable character component
class CmpCivilBehaviour_Collection : public VRefCountedCollection<CmpCivilBehaviour> {};

//============================================================================================================
//  CmpCivilBehaviour_ComponentManager Class
//============================================================================================================
class CmpCivilBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpCivilBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpCivilBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpCivilBehaviour_Collection m_Components;		
		static CmpCivilBehaviour_ComponentManager g_GlobalManager;

};


#endif  
