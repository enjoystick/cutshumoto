//============================================================================================================
//  CmpCometBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpCometBehaviour.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"

V_IMPLEMENT_SERIAL( CmpCometBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpCometBehaviour_ComponentManager CmpCometBehaviour_ComponentManager::g_GlobalManager;

void CmpCometBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);

	m_eCurrentState = eCreating;	
}

void CmpCometBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
    
}

void CmpCometBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;			
			case eRunning:		OnExitRun();		break;
			case eAttacking:	OnExitAttack();		break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;		
		case eRunning:		OnEnterRun();			break;
		case eAttacking:	OnEnterAttack();		break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpCometBehaviour::onFrameUpdate()
{
	CmpEnemyBehaviour::onFrameUpdate();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;		
		case eRunning:		OnRun();		break;
		case eAttacking:	OnAttack();		break;
	}
	
}

void CmpCometBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpCometBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpCometBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpCometBehaviour::OnEnterCreate()
{
	CmpEnemyBehaviour::OnEnterCreate();
}

void CmpCometBehaviour::OnCreate()
{
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();
	tempoAux=0;
	//OnCreate
	if(rand()% 2)
		DerechaOIzquierda=true;
	else
		DerechaOIzquierda=false;
	vecAux= m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->GetRelativeDistance();
	hkvVec3	 vecActual= m_pOwner->GetPosition();
	if(DerechaOIzquierda==true)
		m_pOwner->SetPosition(+vecAux.x,vecActual.y,vecAux.z);
	else
		m_pOwner->SetPosition(-vecAux.x,vecActual.y,vecAux.z);
		
	//Fatuo doesnt have animation-creation
	if (m_fStateTimer >= 0)
		ChangeState(eRunning);
}

void CmpCometBehaviour::OnExitCreate()
{
}


void CmpCometBehaviour::OnEnterRun()
{
	CmpEnemyBehaviour::OnEnterRun();	
}

void CmpCometBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;	
	if(DerechaOIzquierda==true)
		m_pOwner->SetPosition(m_pOwner->GetPosition() + hkvVec3(-m_pCmpProperties->GetSpeed().x,m_pCmpProperties->GetSpeed().y,m_pCmpProperties->GetSpeed().z )* Vision::GetTimer()->GetTimeDifference());
	else
		m_pOwner->SetPosition(m_pOwner->GetPosition() + hkvVec3(m_pCmpProperties->GetSpeed().x,m_pCmpProperties->GetSpeed().y,m_pCmpProperties->GetSpeed().z )* Vision::GetTimer()->GetTimeDifference());

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	if(DerechaOIzquierda==true)
	{
		if(m_pOwner->GetPosition().x<=-vecAux.x)
		{
			EnemiesFactory::Instance()->RemoveFromSlicedEntities(GetOwner());
			m_pOwner->Remove();
		}
	}
	else
	{
		if(m_pOwner->GetPosition().x>=vecAux.x)
		{
			EnemiesFactory::Instance()->RemoveFromSlicedEntities(GetOwner());
			m_pOwner->Remove();
		}
	}
	if (m_fStateTimer >= (m_pCmpProperties->GetTempo()+tempoAux))
	{
		
		//EnemiesFactory::Instance()->CreateEnemy(EShuriken, m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->GetNumLevel(), m_pOwner->GetPosition() + hkvVec3(0,20,50));
		ChangeState(eAttacking);
	}
}

void CmpCometBehaviour::OnExitRun()
{
}

void CmpCometBehaviour::OnEnterAttack()
{
	CmpEnemyBehaviour::OnEnterAttack();

	if (!HasAnimationSM()) 
	{
		m_fAttackTime = 0.1f;
		return;
	}
	m_bHasShoot=false;
	m_fShootingTime= m_fStateTimer+0.2f;
	//m_fAttackTime = m_pAnimSM->GetActiveState()->GetLength();
	m_fAttackTime = m_fShootingTime+m_pCmpProperties->GetCooldown();
}

void CmpCometBehaviour::OnAttack()
{
	if (!HasEnemyProperties())	return;

	
	if(DerechaOIzquierda==true)
		m_pOwner->SetPosition(m_pOwner->GetPosition() + hkvVec3(-m_pCmpProperties->GetSpeed().x,m_pCmpProperties->GetSpeed().y,m_pCmpProperties->GetSpeed().z) * Vision::GetTimer()->GetTimeDifference());
	else
		m_pOwner->SetPosition(m_pOwner->GetPosition() + hkvVec3(m_pCmpProperties->GetSpeed().x,m_pCmpProperties->GetSpeed().y,m_pCmpProperties->GetSpeed().z) * Vision::GetTimer()->GetTimeDifference());

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	if(m_fStateTimer >=m_fShootingTime && m_bHasShoot==false)
	{
		VisBaseEntity_cl* pShuriken = EnemiesFactory::Instance()->CreateEnemy(EShuriken, m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->GetNumLevel(), m_pOwner->GetPosition() + hkvVec3(60,20,100));
		pShuriken->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced( false );
		m_bHasShoot=true;
	}

	if (m_fStateTimer >= m_fAttackTime)
	{
		tempoAux=m_fStateTimer;
		m_bHasShoot=false;
		ChangeState(eRunning);
	}
}

void CmpCometBehaviour::OnExitAttack()
{
	//only shoot one time
	//m_bHasShoot = true;

	//create shuriken


	//reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( pShuriken );
}