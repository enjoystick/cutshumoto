//============================================================================================================
//  CmpCometBehaviour Component
//	Author: Sergio Gomez
//============================================================================================================
#ifndef CMPCOMETBEHAVIOUR_H_INCLUDED
#define CMPCOMETBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPCOMETBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPCOMETBEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpCometBehaviour Class
//============================================================================================================
class CmpCometBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpCometBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpCometBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP	CmpCometBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpCometBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void ChangeState(eEnemyState newState);		
		bool DerechaOIzquierda;
		hkvVec3 vecAux;
		float tempoAux;
		float m_fAttackTime;
		bool m_bHasShoot;
		float m_fShootingTime;


		virtual void OnEnterCreate();		virtual void OnCreate();		virtual void OnExitCreate();
		virtual void OnEnterRun();			virtual void OnRun();			virtual void OnExitRun();
		virtual void OnEnterAttack();		virtual void OnAttack();		virtual void OnExitAttack();
		
};


//  Collection for handling playable character component
class CmpCometBehaviour_Collection : public VRefCountedCollection<CmpCometBehaviour> {};

//============================================================================================================
//  CmpCometBehaviour_ComponentManager Class
//============================================================================================================
class CmpCometBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpCometBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpCometBehaviour_Collection &Instances() { return m_Components; }



	protected:
		CmpCometBehaviour_Collection m_Components;		
		static CmpCometBehaviour_ComponentManager g_GlobalManager;



};


#endif  
