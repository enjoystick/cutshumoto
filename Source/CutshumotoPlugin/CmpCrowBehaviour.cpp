//============================================================================================================
//  CmpCrowBehaviour Component
//	Author: David Escalona
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpCrowBehaviour.h"
#include "CmpEnemyProperties.h"
#include "CutshumotoUtilities.h"
#include "CmpSlicing.h"
#include "CmpShurikenBehaviour.h"
#include "GameManager.h"
#include <sstream>

const int STAGES_NUMBER = 4;
const int PASS_NUMBER = 4;
const int INITIAL_Run_TIME = 2;
const int WAIT_TIME = 1;
const float IMMUNE_TIME = 0.2f;

const float LIFEUNIT_WIDTH = 10;
const float LIFEUNIT_HEIGHT = 40;
const float BAR_Y = 40;

V_IMPLEMENT_SERIAL( CmpCrowBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpCrowBehaviour_ComponentManager CmpCrowBehaviour_ComponentManager::g_GlobalManager;

void CmpCrowBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);
	
	m_fElapsedProjectiles = 0;
	m_bAlreadyDead = false;
}

void CmpCrowBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
    
}

void CmpCrowBehaviour::onAfterSceneLoaded()
{
	InitCrowBehaviour();
	
	m_ptrLifeBarLeft = new VisScreenMask_cl(); 
	m_ptrLifeBar = new VisScreenMask_cl(); 

	//center spider life bar
	m_fGUI_XPos = ((float)Vision::Video.GetXRes() / 2) - (m_pCmpProperties->GetLifePoints() * LIFEUNIT_WIDTH / 2);		
	
	//draw initial lifepoints
	LoadImg(m_ptrLifeBarLeft,"Textures\\lifeleft.jpg", m_fGUI_XPos,BAR_Y, 1, VColorRef(255,255,255,180),m_pCmpProperties->GetLifePoints() * LIFEUNIT_WIDTH,LIFEUNIT_HEIGHT);

	//draw paper below lifepoints
	LoadImg(m_ptrLifeBar,"Textures\\lifebarcrow.png", m_fGUI_XPos - 6, BAR_Y - 7, 2, VColorRef(255,255,255),418,57);

	ChangeState(eCreating);
}

int CmpCrowBehaviour::GetProjectile(int Stage, int Pass)
{
	return m_pProjectiles[Stage * STAGES_NUMBER + Pass];
}

void CmpCrowBehaviour::SetProjectile(int Stage, int Pass, int value)
{
	m_pProjectiles[Stage * STAGES_NUMBER + Pass] = value;
}


void CmpCrowBehaviour::FollowPath()
{
	//if we have not path, move it using properties speed
	if (m_pCurrentPath)
	{			
		// Calculate current relative parameter value [0..1]
		float fCurrentParam = m_fStateTimer / m_fPathTime;

		if (fCurrentParam < 1)
		{
			// Evaluate current position on path
			hkvVec3 vPos;
			hkvVec3 vDir;

			m_pCurrentPath->EvalPointSmooth(fCurrentParam, vPos, &vDir, NULL);

			m_pOwner->SetUseEulerAngles(true); 
			m_pOwner->SetPosition(vPos);    		
		}
	}
}

void CmpCrowBehaviour::InitCrowBehaviour()
{		
	//parse exposed variables
	m_pProjectiles = new int[STAGES_NUMBER * PASS_NUMBER];
	ParseProjectiles(Stage1_Projectiles, 0);
	ParseProjectiles(Stage2_Projectiles, 1);
	ParseProjectiles(Stage3_Projectiles, 2);
	ParseProjectiles(Stage4_Projectiles, 3);	

	//search for paths	
	Vision::Game.SearchPath(firstPassPathKey, &m_aFirstPassPaths);
	Vision::Game.SearchPath(secondPassPathKey, &m_aSecondPassPaths);
	Vision::Game.SearchPath(thirdPassPathKey, &m_aThirdPassPaths);
	Vision::Game.SearchPath(fourthPassPathKey, &m_aFourthPassPaths);

	//create transition state machine
	VTransitionTable *pTable = VTransitionManager::GlobalManager().CreateDefaultTransitionTable(m_pOwner->GetMesh() );	

	if (pTable)
	{
		m_pAnimSM = new VTransitionStateMachine();	
		m_pAnimSM->Init(pTable,true);	
		m_pOwner->AddComponent(m_pAnimSM);
		m_pAnimSM = m_pOwner->Components().GetComponentOfType<VTransitionStateMachine>();
	}
	
	//create enemy properties
	m_pOwner->AddComponent(new CmpEnemyProperties());
	m_pCmpProperties = m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>();

	if (m_pCmpProperties)
	{
		m_pCmpProperties->SetCanBeSliced(false);
		m_pCmpProperties->SetLifePoints(crowLifes);
		m_pCmpProperties->SetEnemyType(EBoss);
	}

	m_pCmpHighlight = NULL;

	//create highlight component
	m_pCmpHighlight = new CmpHighlight();
	m_pOwner->AddComponent(m_pCmpHighlight);	
	m_pCmpHighlight = m_pOwner->Components().GetComponentOfType<CmpHighlight>();

	//add crow has sliceable
	reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( m_pOwner );

	m_bCrowIn = false;
	m_bImmune = false;
	m_fCurrentSpeed = initialSpeed;
	m_iCurrentCrowLifes = crowLifes;
	
}

void CmpCrowBehaviour::GetPath(int Stage, int pass)
{
	int randomPath;

	switch (pass)
	{
		case 0 :	randomPath = CutshumotoUtilities::GetRandomInt(0, m_aFirstPassPaths.GetValidSize() - 1);	
					m_pCurrentPath = m_aFirstPassPaths[randomPath];
					break;

		case 1 :	randomPath = CutshumotoUtilities::GetRandomInt(0, m_aSecondPassPaths.GetValidSize() - 1);	
					m_pCurrentPath = m_aSecondPassPaths[randomPath];
					break;

		case 2 :	randomPath = CutshumotoUtilities::GetRandomInt(0, m_aThirdPassPaths.GetValidSize() - 1);	
					m_pCurrentPath = m_aThirdPassPaths[randomPath];
					break;

		case 3 :	randomPath = CutshumotoUtilities::GetRandomInt(0, m_aFourthPassPaths.GetValidSize() - 1);	
					m_pCurrentPath = m_aFourthPassPaths[randomPath];
					break;
	}

}


void CmpCrowBehaviour::CheckSpeedIncrement()
{
	int secondStageLifes = 2 * crowLifes / 3;
	int thirdStageLifes = crowLifes / 3;
	
	if (m_iCurrentCrowLifes > secondStageLifes)
		m_fCurrentSpeed = initialSpeed;	
	else if (m_iCurrentCrowLifes <= secondStageLifes && m_iCurrentCrowLifes > thirdStageLifes)			
		m_fCurrentSpeed = initialSpeed + (initialSpeed * firstSpeedInc / 100);
	else 
		m_fCurrentSpeed = initialSpeed + (initialSpeed * secondSpeedInc / 100);	

}

void CmpCrowBehaviour::ParseProjectiles (VString stageProjectiles, int StageNumber)
{	
	int numEle = 0;
	int passNumber = 0;
	char* elements = strtok(stageProjectiles.GetChar(),",");

	while (elements)
	{
		int projectiles = atoi(elements);
		SetProjectile(StageNumber, passNumber, projectiles);
		elements = strtok(NULL,",");
		passNumber++;
	}		
	
}

void CmpCrowBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();	break;			
			case eRunning:		OnExitRun();	break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();	break;		
		case eRunning:		OnEnterRun();		break;
		case eDying:		OnEnterDying();		break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpCrowBehaviour::LoadImg(VisScreenMaskPtr ptrImg, char* filename, float xpos, float ypos, float zpos, VColorRef color, float width, float height)
{	
	ptrImg->LoadFromFile(filename);
	ptrImg->SetTransparency(VIS_TRANSP_ALPHA);	
	ptrImg->SetPos(xpos, ypos);
	ptrImg->SetTargetSize(width,height);	
	ptrImg->SetZVal(0.0f);
	ptrImg->SetOrder(static_cast<int>(zpos));
	ptrImg->SetVisible(true);
}

void CmpCrowBehaviour::DebugInfo()
{
	std::ostringstream pass, bosslifes, projectiles, sliceable;
	pass << "Pass / Stage: " << m_iCurrentPass << " / " << m_iCurrentStage;
	projectiles << "Projectiles: " << m_iCurrentProjectiles;
	bosslifes << "Crow Lifes: " << m_iCurrentCrowLifes;
	if (HasEnemyProperties())
		sliceable << "Sliceable: " << m_pCmpProperties->CanBeSliced();

	Vision::Message.Print(1, 950, 50, pass.str().c_str());
	Vision::Message.Print(1, 950, 100, projectiles.str().c_str());
	Vision::Message.Print(1, 950, 150, bosslifes.str().c_str());
	Vision::Message.Print(1, 950, 20, sliceable.str().c_str());
}

void CmpCrowBehaviour::CheckImmunity()
{
	//if crow has immunity, check immune time
	if (m_bImmune)
	{
		m_pCmpProperties->SetCanBeSliced(false);	
		if (m_fImmuneTime <= 0 )
		{	
			m_bImmune = false;
			m_fImmuneTime = IMMUNE_TIME;	
			m_pCmpProperties->SetCanBeSliced(false);	
		}

		m_fImmuneTime-= Vision::GetTimer()->GetTimeDifference();
	}

	if (m_iCurrentPass > 1 && !m_bImmune && m_eCurrentState != eDying)			
		m_pCmpProperties->SetCanBeSliced(true);			
	else	
		m_pCmpProperties->SetCanBeSliced(false);

}

void CmpCrowBehaviour::CheckCrowLifes()
{
	if (!HasEnemyProperties()) return;

	//is crow dead?
	if (m_pCmpProperties->GetLifePoints() <= 0 && m_iCurrentCrowLifes > 0)
	{
		//update crow life bar
		if (m_ptrLifeBarLeft)	m_ptrLifeBarLeft->SetTargetSize(m_pCmpProperties->GetLifePoints() * LIFEUNIT_WIDTH,LIFEUNIT_HEIGHT);
		m_pCmpProperties->SetCanBeSliced(false);
		
		//only die one time ;)
		m_iCurrentCrowLifes = 0;

		ChangeState(eDying);
		return;
	}

	//crow hit... check speed increment, show damage and check death
	if (m_iCurrentCrowLifes != m_pCmpProperties->GetLifePoints())
	{
		//make it immune for a while
		m_iCurrentCrowLifes = m_pCmpProperties->GetLifePoints();
		m_bImmune = true;
		m_fImmuneTime = IMMUNE_TIME;

		CheckSpeedIncrement();

		//damage effect
		m_pCmpHighlight->Flash(VColorRef(255,0,0), 0.5f);

		//update crow life bar
		if (m_ptrLifeBarLeft)	m_ptrLifeBarLeft->SetTargetSize(m_pCmpProperties->GetLifePoints() * LIFEUNIT_WIDTH,LIFEUNIT_HEIGHT);
	}
}

void CmpCrowBehaviour::onFrameUpdate()
{
	CmpEnemyBehaviour::onFrameUpdate();

	if (!MyGameManager::GlobalManager().IsPlayingTheGame())	return;

	DebugInfo();

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	CheckCrowLifes();
	CheckImmunity();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;		
		case eRunning:		OnRun();		break;
		case eDying:		OnDying();		break;
	}
	
}

void CmpCrowBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpCrowBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpCrowBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpCrowBehaviour::OnEnterCreate()
{
	Vision::GetTimer()->SetSlowMotion(true);
		CutshumotoUtilities::SetMainCameraPosAs("cut2");

	m_pCurrentPath = Vision::Game.SearchPath(InPathKey);	
	m_fPathTime = InPathTime;

	if (HasAnimationSM())
		m_pAnimSM->SetState("Run");
}

void CmpCrowBehaviour::OnCreate()
{	
	FollowPath();	

	if (m_fStateTimer > 5 )	
		CutshumotoUtilities::SetMainCameraPosAs("cut1");

	if (m_fStateTimer > 10)
		CutshumotoUtilities::SetMainCameraPosAs("camPos");


	if (m_fStateTimer > m_fPathTime + INITIAL_Run_TIME)
	{
		if (!m_bCrowIn)
		{
			m_pCurrentPath = Vision::Game.SearchPath(OutPathKey);	
			m_fPathTime = OutPathTime;
			m_fStateTimer = 0;
			m_bCrowIn = true;			
		}
		else
		{
			m_iCurrentStage = 0;
			m_iCurrentPass = 0;
		
			ChangeState(eRunning);
		}
	}
}

void CmpCrowBehaviour::OnExitCreate()
{
	Vision::GetTimer()->SetSlowMotion(false);
}

void CmpCrowBehaviour::OnEnterRun()
{
	//get the correct path
	GetPath(m_iCurrentStage, m_iCurrentPass);	
	//get projectiles for this pass
	m_iCurrentProjectiles = GetProjectile(m_iCurrentStage, m_iCurrentPass);
	//calculate path time 
	m_fPathTime = m_pCurrentPath->GetLen() / m_fCurrentSpeed;
	//calculate time between projectiles
	m_fTimeBetweenProjectiles = m_fPathTime / m_iCurrentProjectiles;
}

void CmpCrowBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;	
	
	FollowPath();
	ShootProjectiles();
	
	if (ManageStagesPass())	
	{	
		if (m_iCurrentPass == 2 || m_iCurrentPass == 3)
			if (HasEnemyProperties())	m_pCmpProperties->SetCanBeSliced(true);
		else
			if (HasEnemyProperties())	m_pCmpProperties->SetCanBeSliced(false);

		ChangeState(eRunning);
	}

}


bool CmpCrowBehaviour::ManageStagesPass()
{
	//finish pass
	if (m_fStateTimer > m_fPathTime)
	{
		//assign new pass / stage
		m_iCurrentPass++;
		
		if (m_iCurrentPass >= PASS_NUMBER)
		{
			m_iCurrentPass = 0;
			m_iCurrentStage++;

			if (m_iCurrentStage >= STAGES_NUMBER)
				m_iCurrentStage = 0;
		}

		return true;		
	}

	return false;
}

void CmpCrowBehaviour::ShootProjectiles()
{
	m_fElapsedProjectiles+=Vision::GetTimer()->GetTimeDifference();

	//time to shoot
	if (m_fElapsedProjectiles > m_fTimeBetweenProjectiles)
	{
		VisBaseEntity_cl* pShuriken = EnemiesFactory::Instance()->CreateEnemy(EShuriken, 1, m_pOwner->GetPosition());
		pShuriken->Components().GetComponentOfType<CmpShurikenBehaviour>()->SetEndPoint(hkvVec3(0,0,Vision::Camera.GetMainCamera()->GetPosition().z));
		//pShuriken->Components().GetComponentOfType<CmpShurikenBehaviour>()->SetOutlineWidth(3.0f);
		//pShuriken->Components().GetComponentOfType<CmpShurikenBehaviour>()->DisablePathFollow(Vision::Camera.GetMainCamera()->GetPosition());
		pShuriken->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced( true );

		m_fElapsedProjectiles = 0;
	}

}

void CmpCrowBehaviour::OnEnterDying()
{
	//get death path
	m_pCurrentPath = Vision::Game.SearchPath(DeathPathKey);	
	m_fPathTime = DeathPathTime;

	//set first node as actual position
	if (m_pCurrentPath)	m_pCurrentPath->GetPathNode(0,true)->SetPosition(m_pOwner->GetPosition());
	
	if (HasAnimationSM())
		m_pAnimSM->SetState("Run");
}

void CmpCrowBehaviour::OnDying()
{	
	if (!m_bAlreadyDead)
	{
		FollowPath();

		//is death path finished
		if (m_fStateTimer > m_fPathTime)
		{
			if (HasAnimationSM())
			{				
				//play death animation
				m_pAnimSM->SetState("Screaming");
				m_bAlreadyDead = true;
				m_fStateTimer = 0;
			}
		}
	}
	else
	{
		//when death animation finished stop it, no loop.
		if (HasAnimationSM())
		{
			if (m_fStateTimer > m_pAnimSM->GetActiveState()->GetLength())	m_pAnimSM->GetActiveControl()->Pause();			
		}
		
	}
}

void CmpCrowBehaviour::OnExitRun()
{

}

void CmpCrowBehaviour::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPCROWBEHAVIOUR_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPCROWBEHAVIOUR_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> crowScore;
		ar >> crowLifes;
		ar >> initialSpeed;
		ar >> firstSpeedInc;
		ar >> secondSpeedInc;
		ar >> thirdSpeedInc;
		ar >> InPathKey;
		ar >> InPathTime;
		ar >> OutPathKey;
		ar >> OutPathTime;
		ar >> DeathPathKey;
		ar >> DeathPathTime;
		ar >> firstPassPathKey;
		ar >> secondPassPathKey;
		ar >> thirdPassPathKey;
		ar >> fourthPassPathKey;
		ar >> Stage1_Projectiles;
		ar >> Stage2_Projectiles;
		ar >> Stage3_Projectiles;
		ar >> Stage4_Projectiles;

	} 
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << crowScore;
		ar << crowLifes;
		ar << initialSpeed;
		ar << firstSpeedInc;
		ar << secondSpeedInc;
		ar << thirdSpeedInc;
		ar << InPathKey;
		ar << InPathTime;
		ar << OutPathKey;
		ar << OutPathTime;
		ar << DeathPathKey;
		ar << DeathPathTime;
		ar << firstPassPathKey;
		ar << secondPassPathKey;
		ar << thirdPassPathKey;
		ar << fourthPassPathKey;
		ar << Stage1_Projectiles;
		ar << Stage2_Projectiles;
		ar << Stage3_Projectiles;
		ar << Stage4_Projectiles;

	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpCrowBehaviour,IVObjectComponent, "Crow Boss Behaviour", VVARIABLELIST_FLAGS_NONE, "Crow Boss Behaviour" )

	DEFINE_VAR_INT		(CmpCrowBehaviour, crowScore, "Crow Score", "5000", 0, 0);
	DEFINE_VAR_INT		(CmpCrowBehaviour, crowLifes, "Crow Lifes", "40", 0, 0);
	DEFINE_VAR_FLOAT	(CmpCrowBehaviour, initialSpeed, "Initial Speed", "300",0,0);
	DEFINE_VAR_FLOAT	(CmpCrowBehaviour, firstSpeedInc, "first Speed Increment (% value)", "15",0,0);	
	DEFINE_VAR_FLOAT	(CmpCrowBehaviour, secondSpeedInc, "second Speed Increment (% value)", "15",0,0);	
	DEFINE_VAR_FLOAT	(CmpCrowBehaviour, thirdSpeedInc, "third Speed Increment (% value)", "15",0,0);	
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, InPathKey, "Entrance Path Key","",100,0,0);
	DEFINE_VAR_FLOAT	(CmpCrowBehaviour, InPathTime, "Entrance Path Time", "10",0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, OutPathKey, "Exit Path Key","",100,0,0);
	DEFINE_VAR_FLOAT	(CmpCrowBehaviour, OutPathTime, "Exit Path Time", "10",0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, DeathPathKey, "Death Path Key","",100,0,0);
	DEFINE_VAR_FLOAT	(CmpCrowBehaviour, DeathPathTime, "Death Path Time", "10",0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, firstPassPathKey, "First Pass Path Key","",100,0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, secondPassPathKey, "Second Pass Path Key","",100,0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, thirdPassPathKey, "Third Pass Path Key","",100,0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, fourthPassPathKey, "Fourth Pass Path Key","",100,0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, Stage1_Projectiles, "Stage 1 Projectiles (separated by commas)","5,6,4,3",100,0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, Stage2_Projectiles, "Stage 2 Projectiles (separated by commas)","5,6,4,3",100,0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, Stage3_Projectiles, "Stage 3 Projectiles (separated by commas)","5,6,4,3",100,0,0);
	DEFINE_VAR_VSTRING	(CmpCrowBehaviour, Stage4_Projectiles, "Stage 4 Projectiles (separated by commas)","5,6,4,3",100,0,0);
	
	

END_VAR_TABLE