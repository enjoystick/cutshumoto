//============================================================================================================
//  CmpDragonBehaviour Component
//	Author: David Escalona
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpDragonBehaviour.h"
#include "CmpEnemyProperties.h"
#include "CutshumotoUtilities.h"
#include "SoundManager.h"
#include "CmpSlicing.h"
#include "CmpShurikenBehaviour.h"
#include "GameManager.h"
#include <sstream>

const int STAGES_NUMBER = 4;
const int PASS_NUMBER = 4;

const float ANG_SPEED = 22.0f;
const float WAIT_TIME = 6.0f;
const float IMMUNE_TIME = 0.2f;

const float BAR_INIHEIGHT = 20;
const float BAR_WIDTH = 61;
const float BAR_HEIGHT = 367;
const float BAR_OFFSET = 20;

const char*	DRAGON_RUN = "entry";
const char*	DRAGON_IDLE = "run";

const char* FIRETRAIL_KEY = "firetrail";

V_IMPLEMENT_SERIAL( CmpDragonBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpDragonBehaviour_ComponentManager CmpDragonBehaviour_ComponentManager::g_GlobalManager;

void CmpDragonBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);
	
	
	m_fElapsedProjectiles = 0;
	m_bAlreadyDead = false;
	m_bBossSuccess = false;
}

void CmpDragonBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
	m_ptrBossHead = NULL;
	m_ptrLifeBar = NULL;
	m_ptrLifeBarLeft = NULL;
}

void CmpDragonBehaviour::InitGUI()
{
	m_ptrLifeBarLeft = new VisScreenMask_cl(); 
	m_ptrLifeBar = new VisScreenMask_cl(); 
	m_ptrBossHead = new VisScreenMask_cl(); 

	float yBar = (Vision::Video.GetYRes() / 2) - (BAR_HEIGHT / 2);
	float xBar = Vision::Video.GetXRes() - BAR_WIDTH - BAR_OFFSET;
	
	m_fLifeUnitHeight = (BAR_HEIGHT - (BAR_OFFSET * 2)) / m_pCmpProperties->GetLifePoints(); //get lifeleft heigth

	LoadImg(m_ptrBossHead,"Textures\\GUIBosses\\Boss_dragonhead.png", Vision::Video.GetXRes() - 120, yBar - 75, 1000, VColorRef(255,255,255,255), 120,90);
	LoadImg(m_ptrLifeBarLeft,"Textures\\GUIBosses\\Boss_hd_grey.png", xBar, yBar + BAR_INIHEIGHT, 1001, VColorRef(255,0,0,175), BAR_WIDTH, m_fLifeUnitHeight *  m_pCmpProperties->GetLifePoints() );
	LoadImg(m_ptrLifeBar,"Textures\\GUIBosses\\Boss_hd.png", xBar, yBar, 1002, VColorRef(255,255,255),BAR_WIDTH, BAR_HEIGHT);

	m_ptrLifeBar->SetVisible(false);
	m_ptrLifeBarLeft->SetVisible(false);
	m_ptrBossHead->SetVisible(false);
}

void CmpDragonBehaviour::UpdateGUI()
{	
	if (m_ptrLifeBarLeft)
		m_ptrLifeBarLeft->SetTargetSize(BAR_WIDTH, m_fLifeUnitHeight * m_pCmpProperties->GetLifePoints());
}


void CmpDragonBehaviour::onAfterSceneLoaded()
{
	InitDragonBehaviour();
		
	InitGUI();	
	
	ChangeState(eResting);

	PlayerStats::Instance()->AddScoreMax(20000);
}


int CmpDragonBehaviour::GetProjectile(int Stage, int Pass)
{
	return m_pProjectiles[Stage * STAGES_NUMBER + Pass];
}

void CmpDragonBehaviour::SetProjectile(int Stage, int Pass, int value)
{
	m_pProjectiles[Stage * STAGES_NUMBER + Pass] = value;
}


void CmpDragonBehaviour::FollowPath()
{
	//if we have not path, move it using properties speed
	if (m_pCurrentPath)
	{			
		// Calculate current relative parameter value [0..1]
		float fCurrentParam = m_fStateTimer / m_fPathTime;

		if (fCurrentParam < 1)
		{
			// Evaluate current position on path
			hkvVec3 vPos;
			hkvVec3 vDir;

			m_pCurrentPath->EvalPointSmooth(fCurrentParam, vPos, &vDir, NULL);

			m_pOwner->SetUseEulerAngles(true); 
			m_pOwner->SetPosition(vPos); 
			//m_pOwner->SetDirection(vDir);
		}
	}
}

void CmpDragonBehaviour::InitDragonBehaviour()
{		
	//parse exposed variables
	m_pProjectiles = new int[STAGES_NUMBER * PASS_NUMBER];
	ParseProjectiles(Stage1_Projectiles, 0);
	ParseProjectiles(Stage2_Projectiles, 1);
	ParseProjectiles(Stage3_Projectiles, 2);
	ParseProjectiles(Stage4_Projectiles, 3);	

	//search for paths	
	Vision::Game.SearchPath(firstPassPathKey, &m_aFirstPassPaths);
	Vision::Game.SearchPath(secondPassPathKey, &m_aSecondPassPaths);
	Vision::Game.SearchPath(thirdPassPathKey, &m_aThirdPassPaths);
	Vision::Game.SearchPath(fourthPassPathKey, &m_aFourthPassPaths);

	//create transition state machine
	VTransitionTable *pTable = VTransitionManager::GlobalManager().LoadTransitionTable(m_pOwner->GetMesh(),"Models\\Ch_Ryu.vTransition" );

	if (pTable)
	{
		m_pAnimSM = new VTransitionStateMachine();	
		m_pAnimSM->Init(pTable,true);	
		m_pOwner->AddComponent(m_pAnimSM);
		m_pAnimSM = m_pOwner->Components().GetComponentOfType<VTransitionStateMachine>();
	}
	
	//create enemy properties
	m_pOwner->AddComponent(new CmpEnemyProperties());
	m_pCmpProperties = m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>();

	if (m_pCmpProperties)
	{
		m_pCmpProperties->SetCanBeSliced(false);
		m_pCmpProperties->SetLifePoints(DragonLifes);				
		m_pCmpProperties->SetEnemyType(EBoss);
	}

	m_pCmpHighlight = NULL;

	//create highlight component
	m_pCmpHighlight = new CmpHighlight();
	m_pOwner->AddComponent(m_pCmpHighlight);	
	m_pCmpHighlight = m_pOwner->Components().GetComponentOfType<CmpHighlight>();

	//search effect
	m_pEffect = NULL;
	m_pEffect = VisParticleGroupManager_cl::GlobalManager().Instances().FindByKey("dragonShoot");
	if (m_pEffect)	
	{	m_pEffect->SetRemoveWhenFinished(false);
		m_pEffect->SetPause(true);
	}
		
	//add Dragon as sliceable
	reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( m_pOwner );

	m_vCameraPos = hkvVec3(0,22000,100);

	m_pFXResource = NULL;
	m_bDragonIn = false;
	m_bImmune = false;
	m_fCurrentSpeed = initialSpeed;
	m_iCurrentDragonLifes = DragonLifes;
	m_pFXResConfeti = NULL;
	
}

void CmpDragonBehaviour::GetPath(int Stage, int pass)
{
	int randomPath;

	switch (pass)
	{
		case 0 :	randomPath = CutshumotoUtilities::GetRandomInt(0, m_aFirstPassPaths.GetValidSize() - 1);	
					m_pCurrentPath = m_aFirstPassPaths[randomPath];
					break;

		case 1 :	randomPath = CutshumotoUtilities::GetRandomInt(0, m_aSecondPassPaths.GetValidSize() - 1);	
					m_pCurrentPath = m_aSecondPassPaths[randomPath];
					break;

		case 2 :	randomPath = CutshumotoUtilities::GetRandomInt(0, m_aThirdPassPaths.GetValidSize() - 1);	
					m_pCurrentPath = m_aThirdPassPaths[randomPath];
					break;

		case 3 :	randomPath = CutshumotoUtilities::GetRandomInt(0, m_aFourthPassPaths.GetValidSize() - 1);	
					m_pCurrentPath = m_aFourthPassPaths[randomPath];
					break;
	}
}

void CmpDragonBehaviour::CheckSpeedIncrement()
{
	int secondStageLifes = 2 * DragonLifes / 3;
	int thirdStageLifes = DragonLifes / 3;
	
	if (m_iCurrentDragonLifes > secondStageLifes)
		m_fCurrentSpeed = initialSpeed;	
	else if (m_iCurrentDragonLifes <= secondStageLifes && m_iCurrentDragonLifes > thirdStageLifes)			
		m_fCurrentSpeed = initialSpeed + (initialSpeed * firstSpeedInc / 100);
	else 
		m_fCurrentSpeed = initialSpeed + (initialSpeed * secondSpeedInc / 100);	

}

void CmpDragonBehaviour::ParseProjectiles (VString stageProjectiles, int StageNumber)
{	
	int numEle = 0;
	int passNumber = 0;
	char* elements = strtok(stageProjectiles.GetChar(),",");

	while (elements)
	{
		int projectiles = atoi(elements) + 1;
		SetProjectile(StageNumber, passNumber, projectiles);
		elements = strtok(NULL,",");
		passNumber++;
	}		
	
}

void CmpDragonBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();	break;			
			case eRunning:		OnExitRun();	break;			
			case eRunningBack:	OnExitRunback();break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();	break;		
		case eRunning:		OnEnterRun();		break;
		case eDying:		OnEnterDying();		break;		
		case eRunningBack:	OnEnterRunback();	break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpDragonBehaviour::LoadImg(VisScreenMaskPtr ptrImg, char* filename, float xpos, float ypos, float zpos, VColorRef color, float width, float height)
{	
	ptrImg->LoadFromFile(filename);
	ptrImg->SetTransparency(VIS_TRANSP_ALPHA);	
	ptrImg->SetPos(xpos, ypos);
	ptrImg->SetTargetSize(width,height);	
	ptrImg->SetZVal(0.0f);
	ptrImg->SetOrder(static_cast<int>(zpos));
	ptrImg->SetVisible(true);
	ptrImg->SetColor(color);
}

void CmpDragonBehaviour::DebugInfo()
{/*
	std::ostringstream pass, bosslifes, projectiles, sliceable;
	pass << "Pass / Stage: " << m_iCurrentPass << " / " << m_iCurrentStage;
	projectiles << "Projectiles: " << m_iCurrentProjectiles;
	bosslifes << "Dragon Lifes: " << m_iCurrentDragonLifes;
	if (HasEnemyProperties())
		sliceable << "Dragon X: " << m_pOwner->GetPosition().x;

	Vision::Message.Print(1, 950, 50, pass.str().c_str());
	Vision::Message.Print(1, 950, 100, projectiles.str().c_str());
	Vision::Message.Print(1, 950, 150, bosslifes.str().c_str());
	Vision::Message.Print(1, 950, 20, sliceable.str().c_str());*/
}

void CmpDragonBehaviour::CheckImmunity()
{
	//if Dragon has immunity, check immune time
	if (m_bImmune)
	{
		m_pCmpProperties->SetCanBeSliced(false);	
		if (m_fImmuneTime <= 0 )
		{	
			m_bImmune = false;
			m_fImmuneTime = IMMUNE_TIME;	
			m_pCmpProperties->SetCanBeSliced(false);	
		}

		m_fImmuneTime-= Vision::GetTimer()->GetTimeDifference();
	}

	if (m_iCurrentPass > 1 && !m_bImmune && m_eCurrentState != eDying)			
		m_pCmpProperties->SetCanBeSliced(true);			
	else	
		m_pCmpProperties->SetCanBeSliced(false);

}

void CmpDragonBehaviour::CheckDragonLifes()
{
	if (!HasEnemyProperties()) return;

	//is Dragon dead?
	if (m_pCmpProperties->GetLifePoints() <= 0 && m_iCurrentDragonLifes > 0)
	{
		m_pCmpProperties->SetCanBeSliced(false);
		
		//only die one time ;)
		m_iCurrentDragonLifes = 0;

		ChangeState(eDying);
		return;
	}

	//Dragon hit... check speed increment, show damage and check death
	if (m_iCurrentDragonLifes != m_pCmpProperties->GetLifePoints())
	{
		//make it immune for a while
		m_iCurrentDragonLifes = m_pCmpProperties->GetLifePoints();
		m_bImmune = true;
		m_fImmuneTime = IMMUNE_TIME;

		CheckSpeedIncrement();

		//damage effect
		m_pCmpHighlight->Flash(VColorRef(255,0,0), 0.5f);

		//trigger confeti	
		float increment = (m_pOwner->GetMesh()->GetCollisionBoundingBox().getSizeZ() * m_pOwner->GetScaling().z) / 2;

		if (!m_pFXResConfeti)
			m_pFXResConfeti = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//cut_confetti.xml");
	
		VisParticleEffectPtr pConfetiFX = m_pFXResConfeti->CreateParticleEffectInstance(m_pOwner->GetPosition() /*+ hkvVec3(0,0,increment)*/,m_pOwner->GetOrientation(),3,0);	
		pConfetiFX->SetRemoveWhenFinished(true);
		
	}
}

void CmpDragonBehaviour::onFrameUpdate()
{			
	if (!MyGameManager::GlobalManager().IsPlayingTheGame())	return;

	if (PlayerStats::Instance()->GetPlayerLifes() <= 0 && !m_bBossSuccess)	
	{	
		m_bBossSuccess = true;
		DisableFireballs();
		m_pMusic->Stop();
		m_pMusic->DisposeObject();		
	}

	if (m_bBossSuccess)	return;

	//DebugInfo();

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	CheckDragonLifes();
	CheckImmunity();
	UpdateGUI();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;		
		case eRunning:		OnRun();		break;
		case eDying:		OnDying();		break;
		case eResting:		OnRest();		break;
		case eRunningBack:	OnRunBack();	break;
	}
	
}

void CmpDragonBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpDragonBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpDragonBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpDragonBehaviour::OnEnterRunback()
{	
	m_pCurrentPath = Vision::Game.SearchPath(OutPath2Key);
	m_fPathTime = OutPath2Time;	
	//m_pAnimSM->GetActiveControl()->SetSpeed(1);

}				
void CmpDragonBehaviour::OnRunBack()
{		
	FollowPath();

	if (m_fStateTimer > m_fPathTime)
	{
		ChangeState(eRunning);
	}
}

void CmpDragonBehaviour::OnExitRunback()
{
	m_ptrLifeBar->SetVisible(true);
	m_ptrLifeBarLeft->SetVisible(true);
	m_ptrBossHead->SetVisible(true);
	m_pMusic = SoundManager::Instance()->PlaySoundReturn(eDragonMusic,m_vCameraPos,true);		
}

void CmpDragonBehaviour::OnRest()
{
	if (m_fStateTimer > InitialOffset)
	{
		ChangeState(eCreating);
	}
}


void CmpDragonBehaviour::OnEnterCreate()
{
	SoundManager::Instance()->PlaySound(eDragonRoar,hkvVec3(0,15000,100),false);

	m_pCurrentPath = Vision::Game.SearchPath(InPathKey);	
	m_fPathTime = InPathTime;
	m_pOwner->SetUseEulerAngles(true);
	m_pOwner->IncOrientation(-45,0,-90);

	if (HasAnimationSM())
		m_pAnimSM->SetState(DRAGON_RUN);		

}

void CmpDragonBehaviour::OnCreate()
{			
	FollowPath();	
		

	if (m_fStateTimer > m_fPathTime)
	{
		//change path, dragon now go to clouds
		if (!m_bDragonIn)
		{						
			m_pCurrentPath = Vision::Game.SearchPath(OutPathKey);	
			m_fPathTime = OutPathTime;
			m_fStateTimer = 0;
			m_pOwner->IncOrientation(45,0,140);			
			m_bDragonIn = true;
			
		}
		else
		{
			m_pAnimSM->SetState(DRAGON_IDLE);			
			m_pAnimSM->GetActiveControl()->SetSpeed(0.5f);
			
			//wait for a while
			if (m_fStateTimer > m_fPathTime + WAIT_TIME)
			{								
				m_iCurrentStage = 0;
				m_iCurrentPass = 0;
		
				//exit from screen in order to start game sequence
				ChangeState(eRunningBack);
			}
		}
	}
	else if (m_bDragonIn && m_fStateTimer > m_fPathTime / 2)
	{
			//now exit from clouds
			if (m_pOwner->GetOrientation().z > 0)
				m_pOwner->IncOrientation(0,0,-ANG_SPEED * Vision::GetTimer()->GetTimeDifference());			
			else if (m_pOwner->GetOrientation().z < 0)
			{	
				SoundManager::Instance()->PlaySound(eDragonVoice,m_vCameraPos,false);							
				m_pOwner->SetOrientation(0,0,0);
			}

	}


}

void CmpDragonBehaviour::OnExitCreate()
{

}

void CmpDragonBehaviour::OnEnterRun()
{	
	VTextureObjectPtr tex;

	if (m_iCurrentPass == 0)
	{ 	tex = Vision::TextureManager.Load2DTexture("Textures\\Tx_Ryu_Dragon_diffuse.tga");	
		m_pOwner->GetMesh()->GetSurface(0)->SetBaseTexture(tex);
	}
	else if (m_iCurrentPass == 2)
	{	tex = Vision::TextureManager.Load2DTexture("Textures\\Tx_Ryu_Dragon_diffuse2.dds");	
		m_pOwner->GetMesh()->GetSurface(0)->SetBaseTexture(tex);
	}		

	m_pAnimSM->SetState(DRAGON_IDLE);

	float currentDistance = 0;

	switch (m_iCurrentPass)
	{
		case 0: currentDistance = path1_offsetToShoot;	break;
		case 1: currentDistance = path2_offsetToShoot;	break;
		case 2: currentDistance = path3_offsetToShoot;	break;
		case 3: currentDistance = path4_offsetToShoot;	break;
	}

	//dragon is not shooting along the path, because we have offsets and begining and end of the path
	currentDistance *= 2;

	if (m_iCurrentPass > 1)
	{
		m_fCurrentSpeed -= 200;
	}

	//get the correct path
	GetPath(m_iCurrentStage, m_iCurrentPass);	
	//get projectiles for this pass
	m_iCurrentProjectiles = GetProjectile(m_iCurrentStage, m_iCurrentPass);
	//calculate path time 
	m_fPathTime = (m_pCurrentPath->GetLen() - currentDistance) / m_fCurrentSpeed;
	//calculate time between projectiles
	m_fTimeBetweenProjectiles = m_fPathTime / m_iCurrentProjectiles;
}

void CmpDragonBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;	
	
	FollowPath();
	ShootProjectiles();
	
	if (ManageStagesPass())	
	{	
		if (m_iCurrentPass == 2 || m_iCurrentPass == 3)
			if (HasEnemyProperties())	m_pCmpProperties->SetCanBeSliced(true);
		else
			if (HasEnemyProperties())	m_pCmpProperties->SetCanBeSliced(false);

		ChangeState(eRunning);
	}

}


bool CmpDragonBehaviour::ManageStagesPass()
{
	//finish pass
	if (m_fStateTimer > m_fPathTime)
	{
		//assign new pass / stage
		m_iCurrentPass++;
		
		if (m_iCurrentPass >= PASS_NUMBER)
		{
			m_iCurrentPass = 0;
			m_iCurrentStage++;

			if (m_iCurrentStage >= STAGES_NUMBER)
				m_iCurrentStage = 0;
		}

		return true;		
	}

	return false;
}

VisParticleEffectPtr CmpDragonBehaviour::CreateFireball(hkvVec3 vPos)
{
	VisParticleEffectPtr fireball;	

	//load particles
	if (!m_pFXResource)
		m_pFXResource = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//Kitsunebi_particles.xml");		

	if (!m_pFXResource) return NULL;

	//create new effect
	fireball = m_pFXResource->CreateParticleEffectInstance(vPos, m_pOwner->GetOrientation());	
	fireball->SetEffectKey(FIRETRAIL_KEY);
	fireball->SetScaling(3.0f);
	fireball->SetRemoveWhenFinished(true);		

	return fireball;

}

void CmpDragonBehaviour::ShootProjectiles()
{
	m_fElapsedProjectiles+=Vision::GetTimer()->GetTimeDifference();

	//time to shoot
	if (m_fElapsedProjectiles > m_fTimeBetweenProjectiles)
	{
		//if dragon is in shoot range
		if (CanShoot())
		{	
			hkvVec3 pos;

			VisBaseEntity_cl* point = Vision::Game.SearchEntity("fireballpoint");

			if (point)			
				pos = point->GetPosition();
			else
			{
				pos = m_pOwner->GetPosition();
			}
			
			//ShowExplosion();

			//trigger ssund
			SoundManager::Instance()->PlaySound(eDragonShoot,m_vCameraPos,false);
			
			//create fireball
			VisBaseEntity_cl* pFireball = EnemiesFactory::Instance()->CreateEnemy(EFireball, 10, pos);			
			VisParticleEffectPtr effect = CreateFireball(pos);

			//attack effect trail
			if (effect)
			{	effect->AttachToParent(pFireball);
				effect->ResetLocalTransformation();
				effect->SetPosition(effect->GetPosition().x , effect->GetPosition().y, effect->GetPosition().z);
			}
						
			//setup
			pFireball->Components().GetComponentOfType<CmpShurikenBehaviour>()->SetEndPoint(hkvVec3(0,10000,100));			
			pFireball->Components().GetComponentOfType<CmpShurikenBehaviour>()->SetRotate(false);
			pFireball->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced( false );
		}

		//reset elapsed time
		m_fElapsedProjectiles = 0;
	}

}

void CmpDragonBehaviour::ShowExplosion()
{
	//show explosion
	if (m_pEffect)
	{	m_pEffect->SetVisible(true);
		m_pEffect->Restart();
	}
}

bool CmpDragonBehaviour::CanShoot()
{
	if (m_pCurrentPath)
	{
		float distanceToIni = 0;
		float distanceToEnd = 0;
		float currentDistance = 0;
		hkvVec3 posIni = m_pCurrentPath->GetPathNode(0)->GetPosition();
		hkvVec3 posEnd = m_pCurrentPath->GetPathNode(m_pCurrentPath->GetPathNodeCount() - 1)->GetPosition();

		//get distance from dragon to end point
		distanceToEnd = hkvMath::Abs( m_pOwner->GetPosition().x - posEnd.x);
		distanceToIni = hkvMath::Abs( m_pOwner->GetPosition().x - posIni.x);

		switch (m_iCurrentPass)
		{
			case 0: currentDistance = path1_offsetToShoot;	break;
			case 1: currentDistance = path2_offsetToShoot;	break;
			case 2: currentDistance = path3_offsetToShoot;	break;
			case 3: currentDistance = path4_offsetToShoot;	break;
		}

		//near to path begin
		if (distanceToEnd > distanceToIni)
		{
			if (distanceToIni > currentDistance)
				return true;
			else
				return false;
		}
		else //near to end
		{
			if (distanceToEnd > currentDistance)
				return true;
			else
				return false;
		}
	}

	return false;
}

void CmpDragonBehaviour::OnEnterDying()
{
	PlayerStats::Instance()->AddScore(DragonScore);	
	SoundManager::Instance()->PlaySound(eDragonDeath, m_vCameraPos,false);

	if (m_pMusic)
	{
		m_pMusic->Stop();
		m_pMusic->DisposeObject();
	}

	//get death path
	m_pCurrentPath = Vision::Game.SearchPath(DeathPathKey);	
	m_fPathTime = DeathPathTime;

	//set first node as actual position
	if (m_pCurrentPath)	m_pCurrentPath->GetPathNode(0,true)->SetPosition(m_pOwner->GetPosition());

	DisableFireballs();	
}

void CmpDragonBehaviour::DisableFireballs()
{
	//search current fireballs and set them as no enemies
	DynArray_cl<VisBaseEntity_cl *> fireballs;
	Vision::Game.SearchEntity(ENEMY_ENT_KEY, &fireballs);
	
	for (unsigned int i=0; i < fireballs.GetValidSize(); i++)
	{		
		fireballs[i]->Components().GetComponentOfType<CmpEnemyProperties>()->Damage(eAll);
		fireballs[i]->SetEntityKey("puturrudefua");
		fireballs[i]->SetVisibleBitmask(0);
	}

	DynArray_cl<VisParticleEffect_cl*> firetrails;
	VisParticleGroupManager_cl::GlobalManager().Instances().FindByKey(FIRETRAIL_KEY, &firetrails);

	for (unsigned int i=0; i < firetrails.GetValidSize(); i++)
	{		
		firetrails[i]->SetFinished();
	}
}

void CmpDragonBehaviour::OnDying()
{	
	if (!m_bAlreadyDead)
	{
		FollowPath();

		//is death path finished
		if (m_fStateTimer > m_fPathTime)
		{
			if (HasAnimationSM())
			{				
				m_bAlreadyDead = true;
				m_fStateTimer = 0;
			}
		}
	}
	else
	{
		//when death animation finished stop it, no loop.
		if (HasAnimationSM())
		{
			if (m_fStateTimer > m_pAnimSM->GetActiveState()->GetLength())	
				m_pAnimSM->GetActiveControl()->Pause();			
		}

		if (m_fStateTimer > 3.0f)
		{			
			if ( !m_bBossSuccess ) 
			{
				m_bBossSuccess = true;
				PlayerStats::Instance()->SetBossFinish(true);
				//MyGameManager::GlobalManager().OnEndOfLevelReached();
			}
		}		
	}
}

void CmpDragonBehaviour::OnExitRun()
{

}

void CmpDragonBehaviour::Serialize( VArchive &ar )
{
	char iLocalVersion = CmpDragonBehaviour_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CmpDragonBehaviour_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> InitialOffset;
		ar >> DragonScore;
		ar >> DragonLifes;
		ar >> initialSpeed;
		ar >> firstSpeedInc;
		ar >> secondSpeedInc;
		ar >> thirdSpeedInc;
		ar >> InPathKey;
		ar >> InPathTime;
		ar >> OutPathKey;
		ar >> OutPathTime;
		ar >> OutPath2Key;
		ar >> OutPath2Time;
		ar >> DeathPathKey;
		ar >> DeathPathTime;
		ar >> firstPassPathKey;
		ar >> secondPassPathKey;
		ar >> thirdPassPathKey;
		ar >> fourthPassPathKey;
		ar >> Stage1_Projectiles;
		ar >> Stage2_Projectiles;
		ar >> Stage3_Projectiles;
		ar >> Stage4_Projectiles;
		ar >> path1_offsetToShoot;
		ar >> path2_offsetToShoot;
		ar >> path3_offsetToShoot;
		ar >> path4_offsetToShoot;

	} 
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << InitialOffset;
		ar << DragonScore;
		ar << DragonLifes;
		ar << initialSpeed;
		ar << firstSpeedInc;
		ar << secondSpeedInc;
		ar << thirdSpeedInc;
		ar << InPathKey;
		ar << InPathTime;
		ar << OutPathKey;
		ar << OutPathTime;
		ar << OutPath2Key;
		ar << OutPath2Time;
		ar << DeathPathKey;
		ar << DeathPathTime;
		ar << firstPassPathKey;
		ar << secondPassPathKey;
		ar << thirdPassPathKey;
		ar << fourthPassPathKey;
		ar << Stage1_Projectiles;
		ar << Stage2_Projectiles;
		ar << Stage3_Projectiles;
		ar << Stage4_Projectiles;
		ar << path1_offsetToShoot;
		ar << path2_offsetToShoot;
		ar << path3_offsetToShoot;
		ar << path4_offsetToShoot;

	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpDragonBehaviour,IVObjectComponent, "Dragon Boss Behaviour", VVARIABLELIST_FLAGS_NONE, "Dragon Boss Behaviour" )

	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, InitialOffset, "Initial Offset time", "5",0,0);
	DEFINE_VAR_INT		(CmpDragonBehaviour, DragonScore, "Dragon Score", "5000", 0, 0);
	DEFINE_VAR_INT		(CmpDragonBehaviour, DragonLifes, "Dragon Lifes", "40", 0, 0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, initialSpeed, "Initial Speed", "300",0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, firstSpeedInc, "first Speed Increment (% value)", "15",0,0);	
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, secondSpeedInc, "second Speed Increment (% value)", "15",0,0);	
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, thirdSpeedInc, "third Speed Increment (% value)", "15",0,0);	
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, InPathKey, "Entrance Path Key","",100,0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, InPathTime, "Entrance Path Time", "10",0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, OutPathKey, "Exit Path Key","",100,0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, OutPathTime, "Exit Path Time", "10",0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, OutPath2Key, "Exit Path 2 Key","",100,0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, OutPath2Time, "Exit Path 2 Time", "10",0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, DeathPathKey, "Death Path Key","",100,0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, DeathPathTime, "Death Path Time", "10",0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, firstPassPathKey, "First Pass Path Key","",100,0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, secondPassPathKey, "Second Pass Path Key","",100,0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, thirdPassPathKey, "Third Pass Path Key","",100,0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, fourthPassPathKey, "Fourth Pass Path Key","",100,0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, Stage1_Projectiles, "Stage 1 Projectiles (separated by commas)","5,6,4,3",100,0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, Stage2_Projectiles, "Stage 2 Projectiles (separated by commas)","5,6,4,3",100,0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, Stage3_Projectiles, "Stage 3 Projectiles (separated by commas)","5,6,4,3",100,0,0);
	DEFINE_VAR_VSTRING	(CmpDragonBehaviour, Stage4_Projectiles, "Stage 4 Projectiles (separated by commas)","5,6,4,3",100,0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, path1_offsetToShoot, "first path shoot distance offset", "300",0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, path2_offsetToShoot, "second path shoot distance offset", "300",0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, path3_offsetToShoot, "third path shoot distance offset", "300",0,0);
	DEFINE_VAR_FLOAT	(CmpDragonBehaviour, path4_offsetToShoot, "fourth path shoot distance offset", "300",0,0);		

END_VAR_TABLE