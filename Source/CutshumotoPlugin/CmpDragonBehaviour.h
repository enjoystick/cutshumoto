//============================================================================================================
//  CmpDragonBehaviour Component
//	Author: David Escalona
//============================================================================================================
#ifndef CmpDragonBehaviour_H_INCLUDED
#define CmpDragonBehaviour_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "CmpHighlight.h"
#include "GlobalTypes.h"

// Versions
#define  CmpDragonBehaviour_VERSION_0	0     // Initial version
#define  CmpDragonBehaviour_CURRENT		1     // Current version

//============================================================================================================
//  CmpGhostBehaviour Class
//============================================================================================================
class CmpDragonBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpDragonBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpDragonBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP	CmpDragonBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpDragonBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onAfterSceneLoaded();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void	ChangeState(eEnemyState newState);
		void	ParseProjectiles (VString stageProjectiles, int StageNumber);
		int		GetProjectile(int Stage, int Pass);
		void	SetProjectile(int Stage, int Pass, int value);
		void	InitDragonBehaviour();
		bool	CanShoot();
		void	FollowPath();
		void	GetPath(int Stage, int pass);
		bool	ManageStagesPass();
		void	CheckSpeedIncrement();
		void	CheckDragonLifes();
		void	CheckImmunity();
		void	ShootProjectiles();		
		void	ShowExplosion();
		void	DebugInfo();
		void	InitGUI();
		void	UpdateGUI();
		void	DisableFireballs();
		void	LoadImg(VisScreenMaskPtr ptrImg, char* filename, float xpos, float ypos, float zpos, VColorRef color, float width, float height);
		VisParticleEffectPtr	CreateFireball(hkvVec3 vPos);

		virtual void OnEnterCreate();		virtual void OnCreate();		virtual void OnExitCreate();
		virtual void OnEnterRun();			virtual void OnRun();			virtual void OnExitRun();				
				void OnEnterRunback();				void OnRunBack();				void OnExitRunback();
				void OnEnterDying();				void OnDying();
				void OnRest();
				
	private:

		//exposed VForge vars (yes, its insane... designers orders...)
		float	InitialOffset;
		VString InPathKey;
		float	InPathTime;
		VString OutPathKey;
		float	OutPathTime;
		VString OutPath2Key;
		float	OutPath2Time;
		VString	DeathPathKey;
		float	DeathPathTime;
		VString firstPassPathKey;
		VString secondPassPathKey;
		VString thirdPassPathKey;
		VString fourthPassPathKey;
		int		DragonScore;
		int		DragonLifes;
		float	initialSpeed;
		float	firstSpeedInc;
		float	secondSpeedInc;
		float	thirdSpeedInc;
		VString	Stage1_Projectiles;
		VString	Stage2_Projectiles;
		VString	Stage3_Projectiles;
		VString	Stage4_Projectiles;
		float	path1_offsetToShoot;
		float	path2_offsetToShoot;
		float	path3_offsetToShoot;
		float	path4_offsetToShoot;

		//----------- member vars ----------------
		VisScreenMaskPtr			m_ptrLifeBar;
		VisScreenMaskPtr			m_ptrLifeBarLeft;
		VisScreenMaskPtr			m_ptrBossHead;
		DynArray_cl<VisPath_cl*>	m_aFirstPassPaths;
		DynArray_cl<VisPath_cl*>	m_aSecondPassPaths;
		DynArray_cl<VisPath_cl*>	m_aThirdPassPaths;
		DynArray_cl<VisPath_cl*>	m_aFourthPassPaths;
		VisPath_cl*					m_pCurrentPath;
		CmpHighlight*				m_pCmpHighlight;
		VisParticleEffectFile_cl*	m_pFXResource;
		VisParticleEffectFile_cl*	m_pFXResConfeti;
		VisParticleEffect_cl*		m_pEffect;
		float						m_fPathTime;
		float						m_fCurrentSpeed;
		int*						m_pProjectiles;
		float						m_fTimeBetweenProjectiles;
		float						m_fElapsedProjectiles;
		int							m_iCurrentProjectiles;
		int							m_iCurrentStage;
		int							m_iCurrentPass;
		int							m_iCurrentDragonLifes;
		bool						m_bDragonIn;
		bool						m_bImmune;
		float						m_fImmuneTime;
		float						m_fGUI_XPos;
		bool						m_bAlreadyDead;
		hkvVec3						m_vCameraPos;
		bool						m_bBossSuccess;
		VFmodSoundObject*			m_pMusic;		
		float						m_fLifeUnitHeight;
};


//  Collection for handling playable character component
class CmpDragonBehaviour_Collection : public VRefCountedCollection<CmpDragonBehaviour> {};

//============================================================================================================
//  CmpDragonBehaviour_ComponentManager Class
//============================================================================================================
class CmpDragonBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpDragonBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	Vision::Callbacks.OnAfterSceneLoaded += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	Vision::Callbacks.OnAfterSceneLoaded -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}

			else if (pData->m_pSender==&Vision::Callbacks.OnAfterSceneLoaded )	
			{
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onAfterSceneLoaded();

			}
		}
		
		inline CmpDragonBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpDragonBehaviour_Collection m_Components;		
		static CmpDragonBehaviour_ComponentManager g_GlobalManager;

};


#endif  
