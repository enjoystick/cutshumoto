//============================================================================================================
//  CmpEnemyBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpEnemyBehaviour.h"

#define CREATION_ANIMNAME	"Birth"
#define ATTACK_ANIMNAME		"Attack"
#define RUN_ANIMNAME		"Run"
#define SHOOT_ANIMNAME		"Shoot"
#define RUNBACK_ANIMNAME	"RunBack"

//  Register the class in the engine module so it is available for RTTI
V_IMPLEMENT_SERIAL( CmpEnemyBehaviour, IVObjectComponent, 0, &g_myComponentModule);


CmpEnemyBehaviour_ComponentManager CmpEnemyBehaviour_ComponentManager::g_GlobalManager;

//============================================================================================================
//  CmpEnemyBehaviour Body
//============================================================================================================
void CmpEnemyBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	m_pOwner = static_cast<VisBaseEntity_cl*>(pOwner);

	m_fStateTimer = 0;
	m_pAnimSM = NULL;
	m_pCmpProperties = NULL;
	m_eCurrentState = eInitalState;


}

void CmpEnemyBehaviour::onFrameUpdate()
{
	if ( !m_pOwner ) return;
	if ( Vision::GetTimer()->GetFrozen() ) return; // Do nothing if game is paused

#if (_DEBUG)
	VColorRef bbColor;
	if ( !m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->CanBeSliced() )
		bbColor = VColorRef( 255, 0, 0 );
	else
		bbColor = VColorRef( 0, 255, 0 );
	m_pOwner->DrawBoundingBox( true, bbColor );
#endif
}

bool CmpEnemyBehaviour::HasAnimationSM()
{
	if (!m_pAnimSM)
	{	
		m_pAnimSM = m_pOwner->Components().GetComponentOfType<VTransitionStateMachine>();

		if (m_pAnimSM)	
			return true;
		else
			return false;			
	}
	else
		return true;
}

bool CmpEnemyBehaviour::HasEnemyProperties()
{
	if (!m_pCmpProperties)
	{			
		m_pCmpProperties = m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>();

		if (m_pCmpProperties)	
			return true;
		else
			return false;			
	}
	else
		return true;
}


void CmpEnemyBehaviour::OnEnterCreate()
{
	if (HasAnimationSM())
		m_pAnimSM->SetState(CREATION_ANIMNAME);
}

void CmpEnemyBehaviour::OnEnterAttack()
{
	if (HasAnimationSM())
		m_pAnimSM->SetState(ATTACK_ANIMNAME);

}

void CmpEnemyBehaviour::OnEnterRun()
{
	if (HasAnimationSM())
		m_pAnimSM->SetState(RUN_ANIMNAME);
}

void CmpEnemyBehaviour::onRemove( VisTypedEngineObject_cl *pOwner )
{
}

