//============================================================================================================
//  CmpEnemyBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPENEMYBEHAVIOUR_H_INCLUDED
#define CMPENEMYBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyProperties.h"
#include "GlobalTypes.h"

// Versions
#define  CMPENEMYBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPENEMYBEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpEnemyBehaviour Class
//============================================================================================================
class CmpEnemyBehaviour : public IVObjectComponent
{
	public:
		V_DECLARE_SERIAL  ( CmpEnemyBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpEnemyBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpEnemyBehaviour(){}
		virtual SAMPLEPLUGIN_IMPEXP ~CmpEnemyBehaviour(){}

		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP VOVERRIDE BOOL CanAttachToObject( VisTypedEngineObject_cl *pObject, VString &sErrorMsgOut )
		{
			if ( !IVObjectComponent::CanAttachToObject( pObject, sErrorMsgOut ))return FALSE;  

			if (!pObject->IsOfType( V_RUNTIME_CLASS( VisObject3D_cl )))
			{
				sErrorMsgOut = "Component can only be added to instances of VisObject3D_cl or derived classes.";
				return FALSE;
			}
			
			return TRUE;
		}

		void ChangeState(eEnemyState newState); //public because we probably want to change state from LUA

	protected:	
		virtual void OnEnterCreate();	virtual void OnCreate(){}	virtual void OnExitCreate(){}	
		virtual void OnEnterAttack();	virtual void OnAttack(){}	virtual void OnExitAttack(){}	
		virtual void OnEnterRun();		virtual void OnRun(){}		virtual void OnExitRun(){}	
		
		bool	HasAnimationSM(); 
		bool	HasEnemyProperties();
				
		float						m_fStateTimer;
		bool						m_bIsOK;		
		eEnemyState					m_eCurrentState;
		VisBaseEntity_cl*			m_pOwner;
		VTransitionStateMachine*	m_pAnimSM;
		CmpEnemyProperties*			m_pCmpProperties;
		
};


//  Collection for handling playable character component
class CmpEnemyBehaviour_Collection : public VRefCountedCollection<CmpEnemyBehaviour> {};

//============================================================================================================
//  CmpEnemyBehaviour_ComponentManager Class
//============================================================================================================
class CmpEnemyBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpEnemyBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }
		void OneTimeInit()	
		{  
			Vision::Callbacks.OnUpdateSceneFinished += this;			
		} 

		void OneTimeDeInit()
		{ 
			Vision::Callbacks.OnUpdateSceneFinished -= this;			
		} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
		}
		
		inline CmpEnemyBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpEnemyBehaviour_Collection m_Components;		
		static CmpEnemyBehaviour_ComponentManager g_GlobalManager;

};


#endif  
