//============================================================================================================
//  CmpEnemyGenerator Component
//	Author: David Escalona Rocha
//============================================================================================================

#include "CutshumotoPluginPCH.h"
#include "CmpEnemyGenerator.h"
#include "GameManager.h"
#include "EnemiesFactory.h"
#include "CutshumotoUtilities.h"
#include "rapidjson\document.h"

using namespace rapidjson;

//  Register the class in the engine module so it is available for RTTI
V_IMPLEMENT_SERIAL( CmpEnemyGenerator, IVObjectComponent, 0, &g_myComponentModule);


CmpEnemyGenerator_ComponentManager CmpEnemyGenerator_ComponentManager::g_GlobalManager;

//============================================================================================================
//  CmpEnemyGenerator Body
//============================================================================================================
CmpEnemyGenerator::CmpEnemyGenerator(){}
CmpEnemyGenerator::~CmpEnemyGenerator(){}

void CmpEnemyGenerator::onStartup( VisTypedEngineObject_cl *pOwner )
{  
	m_fTimeElapsed = 0;
	m_fTimeToNextBlock = 0;
	m_iBlockIndex = 0;
	
	//get level information for enemy generation
	m_isOK = LoadLevelInfo(true);

	if (!m_isOK) 		
	{
		printf("json - fallo al cargar los datos del fichero\n");
		return;
	}

	//calculate estimated level time
	if (m_pLevelInfo)	
		m_fEstimatedTotalTime = m_pLevelInfo->aBlocks.GetValidSize() * m_pLevelInfo->fBlockSpawnTime;	

}

void CmpEnemyGenerator::onRemove(  VisTypedEngineObject_cl *pOwner )
{
	delete m_pLevelInfo;
}

void CmpEnemyGenerator::onFrameUpdate()
{
	VisBaseEntity_cl* pOwner = static_cast<VisBaseEntity_cl*>(this->GetOwner());

	if (!pOwner)	return;	
	if (!m_isOK)	return;

	if (!MyGameManager::GlobalManager().IsPlayingTheGame())	return;
	
	//get total level elapsed time and time to next block
	m_fTimeElapsed += Vision::GetTimer()->GetTimeDifference();
	m_fTimeToNextBlock += Vision::GetTimer()->GetTimeDifference();

	//end of level... do something		
	if (m_iBlockIndex >=m_pLevelInfo->aBlocks.GetValidSize())			
		Vision::Message.Add("Level Ends");	
	else
		//check if have to create new block
		CheckBlockGeneration();	
	
}


//===========================================================================
//  Function GetLevelPercent
//	Returns level gamed percent
//===========================================================================
float CmpEnemyGenerator::GetLevelPercent()
{
	if (m_fEstimatedTotalTime != 0)
		return (m_fTimeElapsed / m_fEstimatedTotalTime) * 100;

	return 0;
}

//===========================================================================
//  Function CheckBlockGeneration
//	Checks if its time to create a new block and reset time counter if it's
//===========================================================================
void CmpEnemyGenerator::CheckBlockGeneration()
{
	if (m_pLevelInfo)
		//if its time to next block
		if (m_fTimeToNextBlock >= m_pLevelInfo->fBlockSpawnTime)
		{
			//reset timer and create new block
			m_fTimeToNextBlock = 0;
			GenerateEnemyBlock();
		}
}

//===========================================================================
//  Function GenerateEnemyBlock
//	Create block of anchors and assign their enemy types 
//===========================================================================
void CmpEnemyGenerator::GenerateEnemyBlock()
{
	BlockSpawnInfo* block = m_pLevelInfo->aBlocks[m_iBlockIndex];

	for (unsigned int row = 0; row < BLOCKROWS; row++)
		for (unsigned int col = 0; col < BLOCKCOLS; col++)
		{			
			//calculate anchor position
			hkvVec3 vAnchorPos = hkvVec3(float(ANCHOR_X_INIT) + (col * float(ANCHOR_X_DISTANCE)), row * float(ANCHOR_Y_DISTANCE), float(FLOOR_Z));			

			//create anchor with the enemy asociated
			eEnemyType eType = GetEnemyAtPosition(*block, col, row);
			//VisBaseEntity_cl* pAnchor = EnemiesFactory::Instance()->CreateAnchor(eType, vAnchorPos, hkvVec3(0, ANCHOR_Y_SPEED, 0), RANGE_CREATION, this);			
		}	

	m_iBlockIndex++;
}

//===========================================================================
//  Function CreateEnemy
//	Create and Enemy from EnemiesFactory and set some values (speed, animation...)
//===========================================================================
void CmpEnemyGenerator::CreateEnemy(eEnemyType eType, hkvVec3 vPos, hkvVec3 vAnchorSpeed)
{
	VisBaseEntity_cl* pEntity;
	CmpEnemyProperties* pCmpProp;	
	
	pEntity = EnemiesFactory::Instance()->CreateEnemy(eType, vPos);

	pCmpProp = pEntity->Components().GetComponentOfType<CmpEnemyProperties>();
	//pCmpBehaviour = pEntity->Components().GetComponentOfType<EnemyBehaviour>();
	
	pCmpProp->SetSpeed( pCmpProp->GetSpeed() + vAnchorSpeed ); //add the anchor speed to enemy speed
	//pCmpBehaviour->SetSpeed( pCmpProp->GetSpeed().y ); //behaviour should get speed from properties component. Sergio please check this...
	//pCmpBehaviour->SetGenerateEnemiesInstance(this); // and this... ummm... 

	if (pCmpProp->GetAnimated())
		pEntity->Components().GetComponentOfType<VTransitionStateMachine>()->SetState("Run"); //ini run animation			
	
}

//===========================================================================
//  Function GetEnemyAtPosition
//	return the enemy type in specific row and col for the block passed
//===========================================================================
eEnemyType CmpEnemyGenerator::GetEnemyAtPosition (BlockSpawnInfo& block, unsigned char iCol, unsigned char iRow)
{
	for (unsigned char i = 0; i < block.aEnemies.GetValidSize(); i++)
	{
		if (block.aEnemies[i]->iCol == iCol && block.aEnemies[i]->iRow == iRow)
			return block.aEnemies[i]->eType;
	}

	return ENone;
}

//===========================================================================
//  Function LoadLevelInfo
//	read json level file and save the data into a member var m_pLevelInfo
//  get level info file from component property is design time (vForge)
//===========================================================================
bool CmpEnemyGenerator::LoadLevelInfo(bool bPreloadEnemies)
{
	Document jsonDoc;
	char* fileContent;	

	m_pLevelInfo = new LevelInfo();		

	printf("leyendo fichero: ");
	printf(m_sLevelInfoFile.AsChar());
	printf("\n");

	fileContent = CutshumotoUtilities::ReadFile(m_sLevelInfoFile.AsChar(), "LevelInfo");

	if (!fileContent) return false;

	printf("parseando json\n");
	jsonDoc.Parse<0>(fileContent);
	
	if (!jsonDoc.IsObject())	return false;

	printf("json parseado\n");
	printf("json - leyendo levelinfo\n");
	
	Value& levelInfo = jsonDoc["levelInfo"];		

	//if (!levelInfo.IsObject())	return false;
	SizeType pos = 0;

	//load level info details
	
	printf("json - leyendo name\n");
	if (levelInfo[pos].HasMember("name"))			m_pLevelInfo->sLevelName = levelInfo[pos]["name"].GetString();
	printf("json - leyendo successLevel\n");
	if (levelInfo[pos].HasMember("successLevel"))	m_pLevelInfo->sSuccessLevel = levelInfo[pos]["successLevel"].GetString();
	printf("json - leyendo successLevel\n");
	if (levelInfo[pos].HasMember("failLevel"))		m_pLevelInfo->sFailLevel = levelInfo[pos]["failLevel"].GetString();
	printf("json - leyendo successLevel\n");
	if (levelInfo[pos].HasMember("blockSpawnTime"))	m_pLevelInfo->fBlockSpawnTime = static_cast<float>(levelInfo[pos]["blockSpawnTime"].GetDouble());

	printf("json - leyendo successLevel\n");
	Value& blocks = jsonDoc["chunks"];

	//if (!blocks.IsObject())	return false;

	//load chunks
	for (SizeType i = 0; i < blocks.Size(); i++)
	{
		BlockSpawnInfo* pBlock = new BlockSpawnInfo();
		
		printf("json - leyendo successLevel\n");
		if (blocks[i].HasMember("type"))	pBlock->iChunkType = blocks[i]["type"].GetInt();				
		if (!blocks[i].HasMember("enemies")) continue;
		
		printf("json - leyendo successLevel\n");
		Value& enemies = blocks[i]["enemies"];

		m_pLevelInfo->aBlocks[i] = pBlock;

		//load enemies
		for (SizeType j = 0; j < enemies.Size(); j++)
		{
			EnemySpawnInfo* pEnemy = new EnemySpawnInfo();
			if (enemies[j].HasMember("type"))	pEnemy->eType = static_cast<eEnemyType>(enemies[j]["type"].GetInt());
			if (enemies[j].HasMember("row"))	pEnemy->iRow = enemies[j]["row"].GetInt();
			if (enemies[j].HasMember("col"))	pEnemy->iCol = enemies[j]["col"].GetInt();

			//if (bPreloadEnemies)
				//EnemiesFactory::Instance()->PreloadEnemy(pEnemy->eType);
			
			m_pLevelInfo->aBlocks[i]->aEnemies[j] = pEnemy;
		}
	}

	printf("json - fin fichero\n");
	return true;

}

//============================================================================================================
//  CmpEnemyGenerator Overrides
//============================================================================================================
void CmpEnemyGenerator::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	if (pOwner!=NULL)
	{
		CmpEnemyGenerator_ComponentManager::GlobalManager().Instances().AddUnique(this);    
		onStartup( pOwner );
	}
	else
	{
	    onRemove( pOwner );    
	    CmpEnemyGenerator_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpEnemyGenerator::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPENEMYGENERATOR_VERSION_CURRENT;
	IVObjectComponent::Serialize(ar);
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPENEMYGENERATOR_VERSION_CURRENT , "Invalid local version.");

		ar >> m_sLevelInfoFile;
	} 
	else
	{
	    ar << iLocalVersion;
		ar << m_sLevelInfoFile;

	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpEnemyGenerator,IVObjectComponent,    "Enemy Generator", VVARIABLELIST_FLAGS_NONE, "EnemyGenerator" )
	
	DEFINE_VAR_VSTRING	(CmpEnemyGenerator, m_sLevelInfoFile,   "Level Info filename", "", 50, 0,0); 

END_VAR_TABLE
