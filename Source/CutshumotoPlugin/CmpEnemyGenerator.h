//============================================================================================================
//  CmpEnemyGenerator Component
//	Author: David Escalona Rocha
//============================================================================================================

#ifndef CMPENEMYGENERATOR_H_INCLUDED
#define CMPENEMYGENERATOR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "GlobalTypes.h"
#include "LevelInfo.h"
// Versions
#define CMPENEMYGENERATOR_VERSION_0          0     // Initial version
#define CMPENEMYGENERATOR_VERSION_CURRENT    1     // Current version

//============================================================================================================
//  CmpEnemyGenerator Class
//============================================================================================================
class CmpEnemyGenerator : public IVObjectComponent
{

	public:
		V_DECLARE_SERIAL  ( CmpEnemyGenerator, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpEnemyGenerator, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpEnemyGenerator(); 		
		SAMPLEPLUGIN_IMPEXP ~CmpEnemyGenerator();  
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE BOOL CanAttachToObject( VisTypedEngineObject_cl *pObject, VString &sErrorMsgOut )
		{
			if ( !IVObjectComponent::CanAttachToObject( pObject, sErrorMsgOut ))return FALSE;  

			if (!pObject->IsOfType( V_RUNTIME_CLASS( VisObject3D_cl )))
			{
				sErrorMsgOut = "Component can only be added to instances of VisObject3D_cl or derived classes.";
				return FALSE;
			}
			return TRUE;
		}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void OnVariableValueChanged(VisVariable_cl *pVar, const char * value) {}
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );
		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove ( VisTypedEngineObject_cl *pOwner );
		
		float GetEstimatedLevelTime()	{ return m_fEstimatedTotalTime; }
		float GetElapsedLevelTime()		{ return m_fTimeElapsed; }
		float GetLevelPercent();
		void CreateEnemy(eEnemyType eType, hkvVec3 vPos, hkvVec3 vAnchorSpeed);
	
	protected:	

	private:

		void CheckBlockGeneration();
		void GenerateEnemyBlock();
		
		eEnemyType GetEnemyAtPosition (BlockSpawnInfo& block, unsigned char iCol, unsigned char iRow);
		bool LoadLevelInfo(bool bPreloadEnemies);

		LevelInfo*		m_pLevelInfo;
		float			m_fTimeElapsed;
		float			m_fTimeToNextBlock;
		float			m_fEstimatedTotalTime;
		unsigned int	m_iBlockIndex;
		VString			m_sLevelInfoFile;
		bool			m_isOK;

};


//  Collection for handling playable character component
class CmpEnemyGenerator_Collection : public VRefCountedCollection<CmpEnemyGenerator> {};

//============================================================================================================
//  CmpEnemyGenerator_ComponentManager Class
//============================================================================================================
/// This manager class has a list of all available CmpEnemyGenerator instances
/// and takes care of calling their CmpEnemyGenerator::PerFrameUpdate function
/// on each frame.

class CmpEnemyGenerator_ComponentManager : public IVisCallbackHandler_cl
{
	public:

		//   Gets the singleton of the manager
		static CmpEnemyGenerator_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		//   Should be called at plugin initialization time.
		void OneTimeInit(){  Vision::Callbacks.OnUpdateSceneFinished += this;} // listen to this callback
  
		//   Should be called at plugin de-initialization time
		void OneTimeDeInit(){ Vision::Callbacks.OnUpdateSceneFinished -= this;} // de-register

		//   Callback method that takes care of updating the managed instances each frame
		
		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			VASSERT( pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished );
			// call update function on every component
			const int iCount = m_Components.Count();
			for (int i=0;i<iCount;i++){ m_Components.GetAt(i)->onFrameUpdate(); }
		}

		//   Gets all VPlayableCharacterComponent instances this manager holds
		inline CmpEnemyGenerator_Collection &Instances() { return m_Components; }

	protected:

		/// Holds the collection of all instances of CmpEnemyGenerator
		CmpEnemyGenerator_Collection m_Components;

		/// One global instance of our manager
		static CmpEnemyGenerator_ComponentManager g_GlobalManager;
};

#endif