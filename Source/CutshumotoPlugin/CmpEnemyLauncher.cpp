//============================================================================================================
//  CmpEnemyLauncher Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpEnemyLauncher.h"
#include "EnemiesFactory.h"
#include "GlobalTypes.h"
#include "CutshumotoUtilities.h"
#include "GameManager.h"

const int COLLISION_GROUPS = 20;
const int COMBOHEIGHT = 97;
const int COMBOWIDTH = 329;
const int COMBONUMWIDTH = 78;
const float COMBOMSGTIME = 0.75f;
const float INITIALWAIT = 5.0f;

const float YCOMBOPOS = 100;

V_IMPLEMENT_SERIAL( CmpEnemyLauncher, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpEnemyLauncher_ComponentManager CmpEnemyLauncher_ComponentManager::g_GlobalManager;

void CmpEnemyLauncher::onStartup( VisTypedEngineObject_cl *pOwner )
{
	m_pOwner = static_cast<VisBaseEntity_cl*> (pOwner);

	m_ptrComboNum = new VisScreenMask_cl();
	m_ptrCombo = new VisScreenMask_cl();	

	float x = static_cast<float>(Vision::Video.GetXRes() / 2) - static_cast<float>((COMBOWIDTH / 2));	

	LoadImg(m_ptrCombo,"Textures\\Combo.png",x,YCOMBOPOS,1000,VColorRef(255,255,255), COMBOWIDTH, COMBOHEIGHT);
	m_ptrCombo->SetVisible(false);

	m_fTimer = 0;	
	m_bSpecial = false;
	m_fSpecialTimer = 0;
	m_iLastcombo = 1;
	m_fComboMsgTime = 0;
	m_fCurrentTimeBE = InitialTimeBetweenEnemies;
	m_iRelativeScore = scoreRangeIncrement;
	m_iCounter = 1;
}

void CmpEnemyLauncher::LoadImg(VisScreenMaskPtr ptrImg, char* filename, float xpos, float ypos, float zpos, VColorRef color, int width, int height)
{	
	ptrImg->LoadFromFile(filename);
	ptrImg->SetTransparency(VIS_TRANSP_ALPHA);	
	ptrImg->SetPos(xpos, ypos);
	ptrImg->SetTargetSize((float)width,(float)height);	
	ptrImg->SetZVal(0.0f);
	ptrImg->SetOrder(static_cast<int>(zpos));
	ptrImg->SetVisible(true);
	ptrImg->SetColor(color);
}

void CmpEnemyLauncher::CheckCombo()
{
	int combo = PlayerStats::Instance()->GetCurrentCombo();

	if (combo > 1)
	{
		m_fComboMsgTime = COMBOMSGTIME;	
		float x = (float)(Vision::Video.GetXRes() / 2) + (float)(COMBOWIDTH / 2);	
		VColorRef color = m_ptrComboNum->GetColor();
		
		switch (m_iLastcombo)
		{
			case 2: LoadImg(m_ptrComboNum,"Textures//cx2.png",x,YCOMBOPOS,1001,color,COMBONUMWIDTH,COMBOHEIGHT); break;
			case 3: LoadImg(m_ptrComboNum,"Textures//cx3.png",x,YCOMBOPOS,1001,color,COMBONUMWIDTH,COMBOHEIGHT); break;
			case 4: LoadImg(m_ptrComboNum,"Textures//cx4.png",x,YCOMBOPOS,1001,color,COMBONUMWIDTH,COMBOHEIGHT); break;
		}		
	}

	m_iLastcombo = combo;

}

void CmpEnemyLauncher::ShowComboInfo()
{
	float elapsed = Vision::GetTimer()->GetTimeDifference();

	if (m_fComboMsgTime > 0)
	{		
		m_ptrCombo->SetVisible(true);		
		m_ptrComboNum->SetVisible(true);		
		m_fComboMsgTime-=elapsed;

		ImageEffect(m_ptrComboNum, elapsed);		
		ImageEffect(m_ptrCombo, elapsed);

	}
	else
	{
		m_ptrCombo->SetVisible(false);
		m_ptrCombo->SetColor(VColorRef(255,255,255,255));
		//m_ptrCombo->SetTargetSize(COMBOWIDTH, COMBOHEIGHT);
		m_ptrComboNum->SetVisible(false);
		m_ptrComboNum->SetColor(VColorRef(255,255,255,255));
		//m_ptrComboNum->SetTargetSize(COMBONUMWIDTH, COMBOHEIGHT);

	}

}

void CmpEnemyLauncher::ImageEffect(VisScreenMaskPtr ptrImg, float elapsed)
{
	float x,y;
	
	ptrImg->GetTargetSize(x,y);

	x+=30 * elapsed;
	y+=30 * elapsed;

	//ptrImg->SetTargetSize(x,y);

	VColorRef color = ptrImg->GetColor();

	color.a-= (UBYTE)80 * elapsed;
	
	color.a = hkvMath::clamp<UBYTE>(color.a,0,255);

	ptrImg->SetColor(color);

}

void CmpEnemyLauncher::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
}

void CmpEnemyLauncher::onAfterSceneLoaded()
{
	PlayerStats::Instance()->SetGameMode(PlayerStats::eEndless);	
	PlayerStats::Instance()->AddScoreMax(1000000);
}

void CmpEnemyLauncher::onFrameUpdate()
{
	if (!m_pOwner || !MyGameManager::GlobalManager().IsPlayingTheGame())	return;
	
	if (Vision::GetTimer()->GetTime() < INITIALWAIT)	return;

	CheckCombo();
	ShowComboInfo();
	CheckTimeBeetweenEnemies();
	CheckSpecialThrow();

	m_fTimer+= Vision::GetTimer()->GetTimeDifference();

	if (m_fTimer >= m_fCurrentTimeBE)
	{	
		ThrowEnemy();
		m_fTimer = 0;
	}	
}

void CmpEnemyLauncher::CheckSpecialThrow()
{
	m_fSpecialTimer+=Vision::GetTimer()->GetTimeDifference();

	if (m_fSpecialTimer < timeForSpecial)
	{
		if (!m_bSpecial &&  m_fSpecialTimer > timeForSpecial / 2)
		{
			float special = Vision::Game.GetFloatRand();

			if (special > specialRate)
			{
				TriggerSpecialEvent();
				m_bSpecial = true;
			}		
		}
	}
	else
	{
		m_fSpecialTimer = 0;
		m_bSpecial = false;
	}
	
}

void CmpEnemyLauncher::TriggerSpecialEvent()
{
	int numOfEnemies = CutshumotoUtilities::GetRandomInt(2,4);

	for (int i=0; i < numOfEnemies; i++)
	{
		ThrowEnemy(true);
	}

}

void CmpEnemyLauncher::CheckTimeBeetweenEnemies()
{
	if (PlayerStats::Instance()->GetScore() > scoreRangeIncrement * m_iCounter)
	{		
		float reduction = InitialTimeBetweenEnemies * timeReduction / 100;

		m_fCurrentTimeBE-=reduction;

		if (m_fCurrentTimeBE < 0.5f )
		{
			m_fCurrentTimeBE = InitialTimeBetweenEnemies - (InitialTimeBetweenEnemies * 15 / 100);
		}
		m_iCounter++;
	}
}

void CmpEnemyLauncher::ThrowEnemy(bool noRepeat)
{  	
	float xAng, yAng, zAng;
	int type = CutshumotoUtilities::GetRandomInt(0,7);
	hkvVec3 vPos = m_pOwner->GetPosition();
	hkvVec3 vLinVel;

	GetThrowData(vPos.x, vLinVel);
	
	if (noRepeat)
	{
		if (type == m_iLastEnemy)
		{
			if (Vision::Game.GetFloatRand() > 0.5f)
				type = hkvMath::clamp(type + 1,0,7);
			else
				type = hkvMath::clamp(type - 1,0,7);
		}
		
	}
	
	VisBaseEntity_cl* pEntity = EnemiesFactory::Instance()->CreateEnemy(ParseEnemyType(type),16,vPos,true);

	if (!pEntity->HasMesh() || !pEntity->GetMesh()->IsLoaded())
		return;

	pEntity->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(true);
	pEntity->SetCastShadows(true);
	
	vHavokRigidBody* pRigidBody = new vHavokRigidBody();	
	pRigidBody->Shape_Type = ShapeType_CONVEX; 	
	pEntity->AddComponent(pRigidBody);
	pRigidBody->GetHkRigidBody()->setCollisionFilterInfo(COLLISION_FILTER_GROUP);	

	pRigidBody->SetLinearVelocity(vLinVel);	

	xAng = (float)CutshumotoUtilities::GetRandomInt(50,150);
	yAng = (float)CutshumotoUtilities::GetRandomInt(30,150);
	zAng = (float)CutshumotoUtilities::GetRandomInt(50,120);

	hkvVec3 vAngVel(xAng, yAng, zAng);
	pRigidBody->SetAngularVelocity(vAngVel);

	m_iLastEnemy = type;	
	
}


void CmpEnemyLauncher::GetThrowData(float& x, hkvVec3& vDir)
{	
	hkvVec3 dir;
	x = (float)CutshumotoUtilities::GetRandomInt((int)IniRangeX,(int)EndRangeX);

	if (hkvMath::Abs(m_fLastPos - x) < 100)
	{
		m_fLastPos < 0 ? x+=300 : x-=300;
	}

	dir = hkvVec3(0,0,1);

	if ((hkvMath::Abs(IniRangeX - x) < 75))
	{
		dir =  hkvVec3(0.2,0,1);
	}

	if ((hkvMath::Abs(EndRangeX - x) < 75))
	{
		dir =  hkvVec3(-0.2,0,1);
	}

	float vel = (float)CutshumotoUtilities::GetRandomInt(600,700);

	vDir = dir * vel;

	m_fLastPos = x;
}

eEnemyType CmpEnemyLauncher::ParseEnemyType(int number)
{
	switch (number)
	{
		case 0:		return ENormal;
		case 1:		return EArmored;
		case 2:		return EComet;
		case 3:		return EGhost;
		case 4:		return EArmoredPlus;
		//case 5:		return ESpiderling;
		//case 6:		return EKappa;
		case 7:		return ELauncher;
		default:	return ENormal;
	}
}

void CmpEnemyLauncher::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpEnemyLauncher_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpEnemyLauncher_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}



void CmpEnemyLauncher::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPENEMYLAUNCHER_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPENEMYLAUNCHER_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> IniRangeX;
		ar >> EndRangeX;
		ar >> InitialTimeBetweenEnemies;
		ar >> scoreRangeIncrement;
		ar >> timeReduction;
		ar >> scoreToNewLife;
		ar >> timeForSpecial;
		ar >> specialRate;
	} 
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << IniRangeX;
		ar << EndRangeX;
		ar << InitialTimeBetweenEnemies;
		ar << scoreRangeIncrement;
		ar << timeReduction;
		ar << scoreToNewLife;
		ar << timeForSpecial;
		ar << specialRate;
	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpEnemyLauncher,IVObjectComponent, "Enemy Launcher", VVARIABLELIST_FLAGS_NONE, "Enemy Launcher" )

	DEFINE_VAR_FLOAT	(CmpEnemyLauncher, IniRangeX, "X Initial range", "-500",0,0);
	DEFINE_VAR_FLOAT	(CmpEnemyLauncher, EndRangeX, "X End Range", "500",0,0);
	DEFINE_VAR_FLOAT	(CmpEnemyLauncher, InitialTimeBetweenEnemies, "time between enemies", "2",0,0);
	DEFINE_VAR_FLOAT	(CmpEnemyLauncher, timeReduction, "Time reduction %", "15",0,0);
	DEFINE_VAR_INT		(CmpEnemyLauncher, scoreRangeIncrement, "Score cap to reduce time", "20", 0, 0);
	DEFINE_VAR_INT		(CmpEnemyLauncher, scoreToNewLife, "new live at...", "100", 0, 0);
	DEFINE_VAR_FLOAT	(CmpEnemyLauncher, timeForSpecial, "time for trigger special throw", "10",0,0);
	DEFINE_VAR_FLOAT	(CmpEnemyLauncher, specialRate, "possibility rate (0..1)", "0.8",0,0);

END_VAR_TABLE