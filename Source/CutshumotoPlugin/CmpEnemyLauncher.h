//============================================================================================================
//  CmpEnemyLauncher Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPENEMYLAUNCHER_H_INCLUDED
#define CMPENEMYLAUNCHERs_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "GlobalTypes.h"

// Versions
#define  CMPENEMYLAUNCHER_VERSION_0		0     // Initial version
#define  CMPENEMYLAUNCHER_CURRENT		1     // Current version

//============================================================================================================
//  CmpEnemyLauncher Class
//============================================================================================================
class CmpEnemyLauncher :  public IVObjectComponent
{
	public:
		V_DECLARE_SERIAL  ( CmpEnemyLauncher, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpEnemyLauncher, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpEnemyLauncher(){}
		SAMPLEPLUGIN_IMPEXP ~CmpEnemyLauncher(){}


		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onAfterSceneLoaded();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:
		void ThrowEnemy(bool noRepeat = false);
		void CheckTimeBeetweenEnemies();
		void ImageEffect(VisScreenMaskPtr ptrImg, float time);
		void CheckSpecialThrow();
		void TriggerSpecialEvent();
		void CheckCombo();
		void ShowComboInfo();
		void GetThrowData(float& x, hkvVec3& vDir);
		eEnemyType ParseEnemyType(int number);
		void LoadImg(VisScreenMaskPtr ptrImg, char* filename, float xpos, float ypos, float zpos, VColorRef color, int width, int height);

		//--- member vars ---
		float				m_fCurrentSpeed;
		float				m_fCurrentTimeBE;
		float				m_fTimer;
		float				m_fSpecialTimer;
		VisBaseEntity_cl*	m_pOwner;
		float				m_fLastPos;
		float				m_fComboMsgTime;
		int					m_iLastcombo;
		VisScreenMaskPtr	m_ptrCombo;
		VisScreenMaskPtr	m_ptrComboNum;
		int					m_iRelativeScore;
		int					m_iCounter;
		int					m_iLastEnemy;
		bool				m_bSpecial;
		
		
		// --- exposed vars ---
		float			IniRangeX;
		float			EndRangeX;
		float			InitialTimeBetweenEnemies;
		unsigned int	scoreRangeIncrement;
		float			timeReduction;
		int				scoreToNewLife;
		float			timeForSpecial;
		float			specialRate;
		
};


//  Collection for handling playable character component
class CmpEnemyLauncher_Collection : public VRefCountedCollection<CmpEnemyLauncher> {};

//============================================================================================================
//  CmpEnemyLauncher_ComponentManager Class
//============================================================================================================
class CmpEnemyLauncher_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpEnemyLauncher_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	Vision::Callbacks.OnAfterSceneLoaded += this; } 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	Vision::Callbacks.OnAfterSceneLoaded -= this;} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
			else if (pData->m_pSender==&Vision::Callbacks.OnAfterSceneLoaded )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onAfterSceneLoaded();
			}
		}
		
		inline CmpEnemyLauncher_Collection &Instances() { return m_Components; }

	protected:
		CmpEnemyLauncher_Collection m_Components;		
		static CmpEnemyLauncher_ComponentManager g_GlobalManager;

};


#endif  
