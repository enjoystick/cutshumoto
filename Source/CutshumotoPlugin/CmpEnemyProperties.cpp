//============================================================================================================
//  CmpEnemyProperties Component
//	Author: David Escalona Rocha
//============================================================================================================

#include "CutshumotoPluginPCH.h"
#include "CmpEnemyProperties.h"
#include "EnemiesFactory.h"
#include "GlobalTypes.h"
#include "GameManager.h"
#include "PlayerStats.h"
#include "CmpSlicing.h"
#include "SoundManager.h"
//#include <conio.h>
//#include <windows.h>



const char* ARMORED_HIT_TEXTURE_2 = "Textures\\Tx_oni_Cut_RIGHT.tga";
const char* ARMORED_HIT_TEXTURE_1 = "Textures\\Tx_oni_Cut_LEFT.tga";
const char* TENGU_HIT_TEXTURE_1 = "Textures\\Tx_tengu_Cut_Vertical.tga";
const char* TENGU_HIT_TEXTURE_2 = "Textures\\Tx_tengu_Cut_Horizontal.tga";


//  Register the class in the engine module so it is available for RTTI
V_IMPLEMENT_SERIAL( CmpEnemyProperties, IVObjectComponent, 0, &g_myComponentModule);


CmpEnemyProperties_ComponentManager CmpEnemyProperties_ComponentManager::g_GlobalManager;

//============================================================================================================
//  CmpEnemyProperties Body
//============================================================================================================

void CmpEnemyProperties::onStartup( VisTypedEngineObject_cl *pOwner )
{ 
	VisBaseEntity_cl* m_pOwner = static_cast<VisBaseEntity_cl*>( this->GetOwner());
}

void CmpEnemyProperties::onRemove(  VisTypedEngineObject_cl *pOwner )
{
	
}

CmpEnemyProperties::CmpEnemyProperties()
{
	//default values
	m_bCanBeSliced = false;
	m_bAlive = true;	
	m_eSliceDir = eAll;
	m_fScale = 1;
	m_iLevel = 0;

	//m_Rand.Reset(150);
}

CmpEnemyProperties::~CmpEnemyProperties()
{

}

void CmpEnemyProperties::onFrameUpdate()
{	
	if (!m_pOwner)	return;
}

void CmpEnemyProperties::GetSliceForces(eSliceDirection eSliceDir, hkvVec3& topLinear, hkvVec3& TopAngular, hkvVec3& bottomLinear, hkvVec3& bottomAngular)
{	
	float attenuation = 0.7f;
	float power = 1.3f;
	float linear = m_Rand.GetFloat() * 600 + 200;
	float angular = m_Rand.GetFloat() * 600 * power;	
	
	topLinear.x = bottomLinear.z = -linear;
	topLinear.z = bottomLinear.x = linear;

	TopAngular.x = -angular;
	bottomAngular.x =  angular;

	if (eSliceDir == eVerticalToDown || eSliceDir == eVerticalToUp)
	{	
		TopAngular.z = -angular;
		TopAngular.x = angular;
		bottomAngular.z = angular;
		bottomAngular.x = -angular;
	}
}

void CmpEnemyProperties::GetLowSliceForces(eSliceDirection eSliceDir, hkvVec3& topLinear, hkvVec3& TopAngular, hkvVec3& bottomLinear, hkvVec3& bottomAngular)
{	
	float attenuation = 0.7f;
	float power = 1.3f;
	float linear = m_Rand.GetFloat() * 300 + 200;
	float angular = m_Rand.GetFloat() * 200 * power;	
	
	topLinear.x = bottomLinear.z = -linear;
	topLinear.z = bottomLinear.x = linear;

	TopAngular.x = -angular;
	bottomAngular.x =  angular;

	if (eSliceDir == eVerticalToDown || eSliceDir == eVerticalToUp)
	{	
		TopAngular.z = -angular;
		TopAngular.x = angular;
		bottomAngular.z = angular;
		bottomAngular.x = -angular;
	}
}

void CmpEnemyProperties::StaticSlice(eSliceDirection eSliceDir)
{	
	VisBaseEntity_cl * pTopPiece;
	VisBaseEntity_cl * pBottomPiece;
	VString sTopPiece, sBottomPiece;
	hkvVec3 vTopPieceLinear;
	hkvVec3 vTopPieceAngular;
	hkvVec3 vBottomPieceLinear;
	hkvVec3 vBottomPieceAngular;

	if (!this->GetOwner()) return;

	//set pieces position from enemy position
	hkvVec3 vInitialPos = static_cast<VisBaseEntity_cl*>( this->GetOwner() )->GetPosition();
	hkvVec3 vInitialPosConf=vInitialPos;
	hkvVec3 vInitialOri = static_cast<VisBaseEntity_cl*>( this->GetOwner() )->GetOrientation();
	//cc->Components().GetComponentOfBaseType<>
	if(m_eEnemyType==EKappa)
	{
		vInitialPosConf.z=vInitialPosConf.z+120;
	}

	VisBaseEntity_cl* owner = static_cast<VisBaseEntity_cl*>( this->GetOwner() );	
	
	float increment = 100; //sergio's values
	
	if (owner->GetMesh())
		increment = (owner->GetMesh()->GetCollisionBoundingBox().getSizeZ() * owner->GetScaling().z) / 2;

	//david vInitialPosConf.z=+100;
	vInitialPosConf.z += increment;

	//Sergio 10_09_14 Confeti!!!
	if(m_eEnemyType!=EShuriken)
	{
	//hkvLog::Error("Antes del confeti");	
	//Sleep(100);
	m_pFXResource = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//cut_confetti.xml");
	//hkvLog::Error("Despues del confeti");	
	VisParticleEffectPtr m_pConfetiFX = m_pFXResource->CreateParticleEffectInstance(vInitialPosConf, vInitialOri);	
	m_pConfetiFX->SetRemoveWhenFinished(true);
	}

	//get the correct pieces...

	//horizonal slice
	if (eSliceDir == eHorizontalToLeft || eSliceDir == eHorizontalToRight)
	{	
		sTopPiece = m_sModelPieces[eTopSide];
		sBottomPiece = m_sModelPieces[eBottomSide];
	}
	//vertical slice
	if (eSliceDir == eVerticalToDown || eSliceDir == eVerticalToUp)
	{	
		sTopPiece = m_sModelPieces[eLeftSide];		
		sBottomPiece = m_sModelPieces[eRightSide];
	}
	//diagonal slice
	if (eSliceDir == eDiagonalToRightDown || eSliceDir == eDiagonalToLeftUp)
	{	
		sTopPiece = m_sModelPieces[eUpRightSide];	
		sBottomPiece = m_sModelPieces[eDownLeftSide];
	}
	//diagonal slice
	if (eSliceDir == eDiagonalToLeftDown || eSliceDir == eDiagonalToRightUp)
	{	
		sTopPiece = m_sModelPieces[eUpLeftSide];	
		sBottomPiece = m_sModelPieces[eDownRightSide];
	}

	//if there are no pieces then exit
	if (sTopPiece == "" && sBottomPiece == "") return;

	bool collide = false;

	if (PlayerStats::Instance()->GetGameMode() == PlayerStats::eEndless)
	{	
		collide = true;	
		GetLowSliceForces(eSliceDir, vTopPieceLinear, vTopPieceAngular, vBottomPieceLinear, vBottomPieceAngular);
	}
	else
	{
		GetSliceForces(eSliceDir, vTopPieceLinear, vTopPieceAngular, vBottomPieceLinear, vBottomPieceAngular);
	}

	//create entities with model asociated and dynamic rigid body
	pTopPiece = EnemiesFactory::Instance()->CreateEnemyPiece(sTopPiece.AsChar(), vInitialPos, m_fScale, collide );
	pTopPiece->SetOrientation(owner->GetOrientation());
	pBottomPiece = EnemiesFactory::Instance()->CreateEnemyPiece(sBottomPiece.AsChar(), vInitialPos, m_fScale, collide );			
	pBottomPiece->SetOrientation(owner->GetOrientation());

	//apply forces, temporal values
	pTopPiece->Components().GetComponentOfType<vHavokRigidBody>()->ApplyAngularImpulse(vTopPieceAngular);
	pBottomPiece->Components().GetComponentOfType<vHavokRigidBody>()->ApplyAngularImpulse(vBottomPieceAngular);

	pTopPiece->Components().GetComponentOfType<vHavokRigidBody>()->ApplyLinearImpulse(vTopPieceLinear);
	pBottomPiece->Components().GetComponentOfType<vHavokRigidBody>()->ApplyLinearImpulse(vTopPieceAngular);

}


// Return true just when enemy is dead
bool CmpEnemyProperties::Damage(eSliceDirection eSwipeDir)
{
	VisBaseEntity_cl* pOwner = static_cast<VisBaseEntity_cl*>( this->GetOwner());

	//only damage enemy if the slice direction is avaliable for this entity
	if (CorrectSliceDirection(eSwipeDir))
	{
		SoundHit();
		m_iLifePoints--;		

		//remove enemy if it lifes going to zero
		if (m_iLifePoints <= 0)
		{
			SoundDeath();
			m_bAlive = false;

			//cut enemy
			StaticSlice(eSwipeDir);			
			//custom enemy actions
			DamageActions(m_eEnemyType);
			
			//boss is not a normal enemy, we want to control it death
			if (m_eEnemyType == EBoss)
				return false;
			else
				return true;						
		}
		else
			ShowTextureHit(eSwipeDir);
	}
	return false;
}


void CmpEnemyProperties::ShowTextureHit(eSliceDirection eDir)
{
	if (m_eEnemyType == EArmored)
	{
		if (eDir == eDiagonalToLeftDown || eDir == eDiagonalToRightUp)
			ReplaceEntityTexture(ARMORED_HIT_TEXTURE_2);

		if (eDir == eDiagonalToLeftUp || eDir == eDiagonalToRightDown)
			ReplaceEntityTexture(ARMORED_HIT_TEXTURE_1);			

	}
	else if (m_eEnemyType == EArmoredPlus)
	{	
		if (eDir == eVerticalToDown || eDir == eVerticalToUp)
			ReplaceEntityTexture(TENGU_HIT_TEXTURE_1);

		if (eDir == eHorizontalToLeft || eDir == eHorizontalToRight)
			ReplaceEntityTexture(TENGU_HIT_TEXTURE_2);			
	}

}

void CmpEnemyProperties::ReplaceEntityTexture(const char *szNewTexture)
{
	VisBaseEntity_cl* pOwner = static_cast<VisBaseEntity_cl*>( this->GetOwner());
	VisSurfaceTextureSet_cl *pSet = pOwner->GetCustomTextureSet();

	if (!pSet)
	{
		pSet = pOwner->CreateCustomTextureSet();
		pOwner->SetCustomTextureSet(pSet);
	}

	pSet->GetTextures(0)->m_spDiffuseTexture = Vision::TextureManager.Load2DTexture(szNewTexture);
}

void CmpEnemyProperties::DamageActions (eEnemyType eType)
{
	

	PlayerStats::Instance()->AddScore(m_iScore);

	switch (eType)
	{
		case ECivilian:	PlayerStats::Instance()->DamagePlayer();	break;
		case EFatuo:	PlayerStats::Instance()->HealPlayer();		break;
	}
}



bool CmpEnemyProperties::CorrectSliceDirection(eSliceDirection eSliceDir)
{
	if (m_eSliceDir == eAll)	return true;

	if ( (eSliceDir == eHorizontalToLeft || eSliceDir == eHorizontalToRight) && (m_eSliceDir == eHorizontalToLeft || m_eSliceDir == eHorizontalToRight) )		return true;
	if ( (eSliceDir == eVerticalToDown || eSliceDir == eVerticalToUp) && (m_eSliceDir == eVerticalToDown || m_eSliceDir == eVerticalToUp) )						return true;
	if ( (eSliceDir == eDiagonalToLeftDown || eSliceDir == eDiagonalToRightUp) && (m_eSliceDir == eDiagonalToLeftDown || m_eSliceDir == eDiagonalToRightUp) )	return true;
	if ( (eSliceDir == eDiagonalToLeftUp || eSliceDir == eDiagonalToRightDown) && (m_eSliceDir == eDiagonalToLeftUp || m_eSliceDir == eDiagonalToRightDown) )	return true;

	//Sergio 08/08/14: Modificado para eX y ePlus
	if( m_eSliceDir== ePlus)
	{
		if ( eSliceDir == eVerticalToDown || eSliceDir == eVerticalToUp )
		{
			m_eSliceDir=eHorizontalToLeft;
			return true;
		}
		if ( eSliceDir == eHorizontalToLeft || eSliceDir == eHorizontalToRight )
		{
			m_eSliceDir=eVerticalToDown;
			return true;
		}
	}

	if( m_eSliceDir== eX)
	{
		if ( eSliceDir == eDiagonalToLeftDown || eSliceDir == eDiagonalToRightUp )
		{
			m_eSliceDir=eDiagonalToLeftUp;
			return true;
		}
		if ( eSliceDir == eDiagonalToLeftUp || eSliceDir == eDiagonalToRightDown )
		{
			m_eSliceDir=eDiagonalToLeftDown;
			return true;
		}
	}

	return false;
}

//cutre cutre cutre... jodidas entregas express que solo hacen joder la calidad...
bool CmpEnemyProperties::CheckDistance(hkvVec3 entityPos)
{
	hkvVec3 vFromEntityToCam;	

	vFromEntityToCam = Vision::Camera.GetMainCamera()->GetPosition() - entityPos;

	float distance = vFromEntityToCam.getLengthSquared();

	//noooo numeros magicos nooooo!!
	if (distance < 350000)
		m_bCanBeSliced = true;

	return false;
}

void CmpEnemyProperties::SoundHit()
{
	if(m_eEnemyType == EShuriken || m_eEnemyType == EFireball)
		SoundManager::Instance()->PlaySound(eParry,Vision::Camera.GetMainCamera()->GetPosition(),false);		
	else
		SoundManager::Instance()->PlaySound(eHit,Vision::Camera.GetMainCamera()->GetPosition(),false);
}

void CmpEnemyProperties::SoundDeath()
{
	switch (m_eEnemyType)
	{
	case ENormal: SoundManager::Instance()->PlaySound(eKitsuneDeath,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case EKappa: SoundManager::Instance()->PlaySound(eKappaDeath,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case EArmored: SoundManager::Instance()->PlaySound(eOniDeath,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case EArmoredPlus: SoundManager::Instance()->PlaySound(eTenguDeath,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case ELauncher: SoundManager::Instance()->PlaySound(eNinjaDeath,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case EComet: SoundManager::Instance()->PlaySound(eKiteDeath,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case EGhost: SoundManager::Instance()->PlaySound(eGhostDeath,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case EFatuo: SoundManager::Instance()->PlaySound(eFatuoDeath,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case ECivilian: SoundManager::Instance()->PlaySound(eCivilDeath, Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	}
}

void CmpEnemyProperties::SoundCry()
{
	switch (m_eEnemyType)
	{
	case ENormal: SoundManager::Instance()->PlaySound(eKitsuneCry,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case EKappa: SoundManager::Instance()->PlaySound(eKappaCry,Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	case EArmored: SoundManager::Instance()->PlaySound(eOniCry,Vision::Camera.GetMainCamera()->GetPosition(),false);break;
	case EArmoredPlus: SoundManager::Instance()->PlaySound(eTenguCry,Vision::Camera.GetMainCamera()->GetPosition(),false);break;
	case ELauncher: SoundManager::Instance()->PlaySound(eNinjaCry,Vision::Camera.GetMainCamera()->GetPosition(),false);break;
	case EComet: SoundManager::Instance()->PlaySound(eKiteCry,Vision::Camera.GetMainCamera()->GetPosition(),false);break;
	case EGhost: SoundManager::Instance()->PlaySound(eGhostCry,Vision::Camera.GetMainCamera()->GetPosition(),false);break;
	case EFatuo: SoundManager::Instance()->PlaySound(eFatuoCry,Vision::Camera.GetMainCamera()->GetPosition(),false);break;
	case ECivilian: SoundManager::Instance()->PlaySound(eCivilCry, Vision::Camera.GetMainCamera()->GetPosition(),false); break;
	}
}


void CmpEnemyProperties::SetCanBeSliced (bool slice)
{
	m_bCanBeSliced = slice; 
	/*if(slice==true)
		SoundCry();*/
}
//============================================================================================================
//  CmpEnemyProperties Overrides
//============================================================================================================
void CmpEnemyProperties::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	if (pOwner!=NULL)
	{
		CmpEnemyProperties_ComponentManager::GlobalManager().Instances().AddUnique(this);    
		onStartup( pOwner );
	}
	else
	{
	    onRemove( pOwner );    
	    CmpEnemyProperties_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

