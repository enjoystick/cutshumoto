//============================================================================================================
//  CmpEnemyProperties Component
//	Author: David Escalona Rocha
//============================================================================================================

#ifndef CMPENEMYPROPERTIES_H_INCLUDED
#define CMPENEMYPROPERTIES_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "GlobalTypes.h"

// Versions
#define CMPENEMYPROPERTIES_VERSION_0          0     // Initial version
#define CMPENEMYPROPERTIES_VERSION_CURRENT    1     // Current version

//============================================================================================================
//  CmpEnemyProperties Class
//============================================================================================================
class CmpEnemyProperties : public IVObjectComponent
{

	public:
		V_DECLARE_SERIAL  ( CmpEnemyProperties, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpEnemyProperties, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpEnemyProperties(); 
		SAMPLEPLUGIN_IMPEXP ~CmpEnemyProperties();  
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE BOOL CanAttachToObject( VisTypedEngineObject_cl *pObject, VString &sErrorMsgOut )
		{
			if ( !IVObjectComponent::CanAttachToObject( pObject, sErrorMsgOut ))return FALSE;  

			if (!pObject->IsOfType( V_RUNTIME_CLASS( VisObject3D_cl )))
			{
				sErrorMsgOut = "Component can only be added to instances of VisObject3D_cl or derived classes.";
				return FALSE;
			}
			return TRUE;
		}

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove( VisTypedEngineObject_cl *pOwner );
		
		void SetCanBeSliced (bool slice);
		void SetSpeed (float x, float y, float z)			{ m_vSpeed.x = x;	m_vSpeed.y = y;	m_vSpeed.z = z; }
		void SetSpeed (hkvVec3 vSpeed)						{ m_vSpeed = vSpeed; }
		void SetRelativeDistance(float x, float y, float z) { m_vRelDistance.x = x;	m_vRelDistance.y = y;	m_vRelDistance.z = z; }	
		void SetRelativeDistance (hkvVec3 vRelDist)			{ m_vRelDistance = vRelDist; }
		void SetSliceDir (eSliceDirection eDir)				{ m_eSliceDir = eDir; }		
		void SetLifePoints (unsigned int lifes)				{ m_iLifePoints = lifes; }
		void SetEnemyType (eEnemyType eType)				{ m_eEnemyType = eType; }
		void SetScale (float scale)							{ m_fScale = scale; }
		void SetBaseModel(const char* baseModel)			{ m_sBaseModel = baseModel; }
		void SetFilename(const char* file)					{ m_sFilename = file; }
		void SetScore(unsigned int score)							{ m_iScore = score; }
		void SetAnimated (bool animated)					{ m_bAnimated = animated; }
		void SetTempo (float tempo)							{ m_fTempo = tempo; }
		void SetCooldown(float cooldown)					{ m_fCooldown = cooldown; }
		void SetNumLevel(unsigned int iLevel)				{ m_iLevel=iLevel; }

		bool CanBeSliced()									{ return m_bCanBeSliced; }
		bool IsAlive()										{ return m_bAlive; }
		int GetLifePoints()									{ return m_iLifePoints; }
		eEnemyType GetEnemyType()							{ return m_eEnemyType; }
		eSliceDirection GetSliceDir()						{ return m_eSliceDir; }
		hkvVec3 GetSpeed()									{ return m_vSpeed; }
		hkvVec3 GetRelativeDistance()						{ return m_vRelDistance; }
		float GetScale()									{ return m_fScale; }
		const char* GetBaseModel()							{ return m_sBaseModel.AsChar(); }
		VString* GetModelPieces()							{ return m_sModelPieces; }
		const char* GetFilename()							{ return m_sFilename; }
		unsigned int GetScore()								{ return m_iScore; }
		bool GetAnimated()									{ return m_bAnimated; }
		float GetTempo()									{ return m_fTempo; }
		float GetCooldown()									{ return m_fCooldown; }
		int GetNumLevel()									{ return m_iLevel; }
		void SoundHit();
		void SoundDeath();
		void SoundCry();

		bool Damage(eSliceDirection eSwipeDir); 
		bool CheckDistance(hkvVec3 entityPos);
		void StaticSlice(eSliceDirection eSliceDir);				

	private:
		bool CorrectSliceDirection(eSliceDirection eSliceDir);
		void GetSliceForces(eSliceDirection eSliceDir, hkvVec3& topLinear, hkvVec3& TopAngular, hkvVec3& bottomLinear, hkvVec3& bottomAngular);	
		void GetLowSliceForces(eSliceDirection eSliceDir, hkvVec3& topLinear, hkvVec3& TopAngular, hkvVec3& bottomLinear, hkvVec3& bottomAngular);	
		void DamageActions (eEnemyType eType);
		void ReplaceEntityTexture(const char *szNewTexture);
		void ShowTextureHit(eSliceDirection eSliceDir);
		
		VRandom m_Rand;

		bool					m_bAlive;	
		bool					m_bCanBeSliced;
		bool					m_bAnimated;
		hkvVec3					m_vSpeed;
		hkvVec3					m_vRelDistance;
		unsigned int			m_iLifePoints;
		eEnemyType				m_eEnemyType;
		VString					m_sModelPieces[8];
		eSliceDirection			m_eSliceDir;
		float					m_fScale;
		VString					m_sBaseModel;
		VString					m_sFilename;
		unsigned int			m_iScore;
		float					m_fTempo;
		float					m_fCooldown;
		VisParticleEffectPtr	m_pFX;
		VisBaseEntity_cl*		m_pOwner;
		unsigned int			m_iLevel;
		VisParticleEffectFile_cl* m_pFXResource;

};


//  Collection for handling playable character component
class CmpEnemyProperties_Collection : public VRefCountedCollection<CmpEnemyProperties> {};

//============================================================================================================
//  CmpEnemyProperties_ComponentManager Class
//============================================================================================================
class CmpEnemyProperties_ComponentManager : public IVisCallbackHandler_cl
{
	public:

		//   Gets the singleton of the manager
		static CmpEnemyProperties_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		//   Should be called at plugin initialization time.
		void OneTimeInit(){  Vision::Callbacks.OnUpdateSceneFinished += this;} // listen to this callback
  
		//   Should be called at plugin de-initialization time
		void OneTimeDeInit(){ Vision::Callbacks.OnUpdateSceneFinished -= this;} // de-register

		//   Callback method that takes care of updating the managed instances each frame
		
		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			VASSERT( pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished );
			// call update function on every component
			const int iCount = m_Components.Count();
			for (int i=0;i<iCount;i++){ m_Components.GetAt(i)->onFrameUpdate(); }
		}

		//   Gets all VPlayableCharacterComponent instances this manager holds
		inline CmpEnemyProperties_Collection &Instances() { return m_Components; }

	protected:

		/// Holds the collection of all instances of CmpEnemyProperties
		CmpEnemyProperties_Collection m_Components;
		/// One global instance of our manager
		static CmpEnemyProperties_ComponentManager g_GlobalManager;
};

#endif