//============================================================================================================
//  CmpGhostBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpFatuoBehaviour.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"

V_IMPLEMENT_SERIAL( CmpFatuoBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpFatuoBehaviour_ComponentManager CmpFatuoBehaviour_ComponentManager::g_GlobalManager;

void CmpFatuoBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);

	m_eCurrentState = eCreating;	
	m_pFXResource = NULL;
}

void CmpFatuoBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
    
}

void CmpFatuoBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;			
			case eRunning:		OnExitRun();		break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;		
		case eRunning:		OnEnterRun();			break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpFatuoBehaviour::onFrameUpdate()
{		
	CmpEnemyBehaviour::onFrameUpdate();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;		
		case eRunning:		OnRun();		break;
	}
	
}

void CmpFatuoBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpFatuoBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpFatuoBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpFatuoBehaviour::OnEnterCreate()
{
	CmpEnemyBehaviour::OnEnterCreate();
	
}

void CmpFatuoBehaviour::OnCreate()
{
	m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced( true );
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();
	//OnCreate
	int prueba=rand()% 2;
	if(prueba)
		DerechaOIzquierda=true;
	else
		DerechaOIzquierda=false;
	vecAux= m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->GetRelativeDistance();
	hkvVec3	 vecActual= m_pOwner->GetPosition();
	if(DerechaOIzquierda==true)
		m_pOwner->SetPosition(+vecAux.x,vecActual.y,vecAux.z);
	else
		m_pOwner->SetPosition(-vecAux.x,vecActual.y,vecAux.z);
	
	if (!m_pFXResource)
	{
		hkvVec3 pos = m_pOwner->GetPosition();
		pos.z=pos.z+145;
		//m_pFXResource = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//DavidExplosion.xml");
		m_pFXResource = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//Kitsunebi_particles.xml");
		m_pFlameFX = m_pFXResource->CreateParticleEffectInstance(pos, m_pOwner->GetOrientation());	
		m_pFlameFX->SetRemoveWhenFinished(false);
		m_pFlameFX->AttachToParent(m_pOwner);
		//m_pFXResource->
	}
	//Fatuo doesnt have animation-creation
	if (m_fStateTimer >= 0)
		ChangeState(eRunning);
	
}

void CmpFatuoBehaviour::OnExitCreate()
{
}


void CmpFatuoBehaviour::OnEnterRun()
{
	CmpEnemyBehaviour::OnEnterRun();	
}

void CmpFatuoBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;		
	if(DerechaOIzquierda==true)
		m_pOwner->SetPosition(m_pOwner->GetPosition() + hkvVec3(-m_pCmpProperties->GetSpeed().x,m_pCmpProperties->GetSpeed().y,m_pCmpProperties->GetSpeed().z )* Vision::GetTimer()->GetTimeDifference());
	else
		m_pOwner->SetPosition(m_pOwner->GetPosition() + hkvVec3(m_pCmpProperties->GetSpeed().x,m_pCmpProperties->GetSpeed().y,m_pCmpProperties->GetSpeed().z )* Vision::GetTimer()->GetTimeDifference());

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	if(DerechaOIzquierda==true)
	{
		if(m_pOwner->GetPosition().x<=-vecAux.x)
		{
			EnemiesFactory::Instance()->RemoveFromSlicedEntities(GetOwner());
			m_pOwner->Remove();
		}
	}
	else
	{
		if(m_pOwner->GetPosition().x>=vecAux.x)
		{
			EnemiesFactory::Instance()->RemoveFromSlicedEntities(GetOwner());
			m_pOwner->Remove();
		}
	}

	/*if(m_fStateTimer>m_pCmpProperties->GetTempo())
		m_pOwner->Remove();*/
}

void CmpFatuoBehaviour::OnExitRun()
{
}


