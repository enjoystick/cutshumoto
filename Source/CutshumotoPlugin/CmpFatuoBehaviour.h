//============================================================================================================
//  CmpGhostBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPFATUOBEHAVIOUR_H_INCLUDED
#define CMPFATUOEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPFATUOBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPFATUOBEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpGhostBehaviour Class
//============================================================================================================
class CmpFatuoBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpFatuoBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpFatuoBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP	CmpFatuoBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpFatuoBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void ChangeState(eEnemyState newState);		

		virtual void OnEnterCreate();		virtual void OnCreate();		virtual void OnExitCreate();		
		virtual void OnEnterRun();			virtual void OnRun();			virtual void OnExitRun();
		bool DerechaOIzquierda;
		hkvVec3 vecAux;

		VisParticleEffectFile_cl*	m_pFXResource;
		VisParticleEffectPtr		m_pFlameFX;
		
};


//  Collection for handling playable character component
class CmpFatuoBehaviour_Collection : public VRefCountedCollection<CmpFatuoBehaviour> {};

//============================================================================================================
//  CmpGhostBehaviour_ComponentManager Class
//============================================================================================================
class CmpFatuoBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpFatuoBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpFatuoBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpFatuoBehaviour_Collection m_Components;		
		static CmpFatuoBehaviour_ComponentManager g_GlobalManager;

};


#endif  
