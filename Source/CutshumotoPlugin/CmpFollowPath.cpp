//============================================================================================================
//  CmpFollowPath Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpFollowPath.h"
#include "GameManager.h"

V_IMPLEMENT_SERIAL( CmpFollowPath, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpFollowPath_ComponentManager CmpFollowPath_ComponentManager::g_GlobalManager;

void CmpFollowPath::onStartup( VisTypedEngineObject_cl *pOwner )
{
	m_pOwner = static_cast<VisBaseEntity_cl*> (pOwner);
	m_pPath = NULL;
	m_fStateTimer = 0;

	m_bLoopPath = static_cast<bool>(loop);
	m_bFollowPath = static_cast<bool>(enabled);
	m_fPathTime = pathTime;

}

void CmpFollowPath::onRemove(  VisTypedEngineObject_cl *pOwner )
{

}

void CmpFollowPath::onFrameUpdate()
{
	if (!m_pOwner || !MyGameManager::GlobalManager().IsPlayingTheGame())	return;
	
	if (m_bFollowPath)	FollowPath();
}

void CmpFollowPath::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpFollowPath_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpFollowPath_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

bool CmpFollowPath::IniPath()
{
	if (!m_pPath)
	{
		m_pPath = Vision::Game.SearchPath(pathKey);

		if (m_pPath)	
			return true;
		else
			return false;
	}
	else
		return true;
}

void CmpFollowPath::FollowPath()
{
	//there are no path 
	if (!m_pPath) 
	{	
		//try to load it
		if (!IniPath())
			return;
	}
		
	// Get time since last frame
	m_fStateTimer += Vision::GetTimer()->GetTimeDifference();
	
	//if time has gone, start again (or not)
	if (m_fStateTimer > m_fPathTime)
	{
		if (m_bLoopPath)		
			m_fStateTimer = 0.0f;		
		else
		{
			m_bFollowPath = false;
			return;
		}
	}

	// Calculate current relative parameter value [0..1]	
	float fCurrentParam = m_fStateTimer / m_fPathTime;

	// Evaluate current position on path
	hkvVec3 vPos;
	hkvVec3 vDir;

	m_pPath->EvalPointSmooth(fCurrentParam, vPos, &vDir, NULL);

    m_pOwner->SetUseEulerAngles(true); 
    m_pOwner->SetPosition(vPos);    
	m_pOwner->SetDirection(vDir);


	if (fCurrentParam >= changeOffset)
		m_pOwner->IncOrientation(endOrientationOffset * fCurrentParam);
	else
		m_pOwner->IncOrientation(iniOrientationOffset);
	
}

void CmpFollowPath::Reset()
{
	m_fStateTimer = 0;
}

void CmpFollowPath::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPFOLLOWPATH_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPFOLLOWPATH_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> pathKey;
		ar >> pathTime;
		ar >> loop;
		ar >> enabled;		
		ar >> changeOffset;
		iniOrientationOffset.SerializeAsVisVector (ar);
		endOrientationOffset.SerializeAsVisVector (ar);		
	} 
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << pathKey;
		ar << pathTime;
		ar << loop;
		ar << enabled;
		ar << changeOffset;
		iniOrientationOffset.SerializeAsVisVector (ar);
		endOrientationOffset.SerializeAsVisVector (ar);
	}

}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpFollowPath,IVObjectComponent, "CmpFollowPath", VVARIABLELIST_FLAGS_NONE, "CmpFollowPath" )
	
	DEFINE_VAR_VSTRING		(CmpFollowPath, pathKey, "path key","",100,0,0);
	DEFINE_VAR_FLOAT		(CmpFollowPath, pathTime, "path time", "10",0,0);	
	DEFINE_VAR_BOOL			(CmpFollowPath, loop, "looped path", "1", 0, 0);
	DEFINE_VAR_BOOL			(CmpFollowPath, enabled, "enable path following", "0", 0, 0);
	DEFINE_VAR_VECTOR_FLOAT	(CmpFollowPath, iniOrientationOffset, " initial Orientation offset", "0.0/0.0/0.0", 0, 0); 
	DEFINE_VAR_VECTOR_FLOAT	(CmpFollowPath, endOrientationOffset, "final Orientation offset", "0.0/0.0/0.0", 0, 0); 
	DEFINE_VAR_FLOAT		(CmpFollowPath, changeOffset, "point to change orientation (0...1)", "0.7",0,0);	

END_VAR_TABLE