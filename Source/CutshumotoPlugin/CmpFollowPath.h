//============================================================================================================
//  CmpFollowPath Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPFOLLOWPATH_H_INCLUDED
#define CMPFOLLOWPATH_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "GlobalTypes.h"

// Versions
#define  CMPFOLLOWPATH_VERSION_0		0     // Initial version
#define  CMPFOLLOWPATH_CURRENT		1     // Current version



//============================================================================================================
//  CmpFollowPath Class
//============================================================================================================
class CmpFollowPath :  public IVObjectComponent
{
	public:
		V_DECLARE_SERIAL  ( CmpFollowPath, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpFollowPath, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpFollowPath(){}
		SAMPLEPLUGIN_IMPEXP ~CmpFollowPath(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

		void SetPathToFollow(VisPath_cl* pPath, float pathTime, bool loop)	{ m_pPath = pPath;	m_fPathTime = pathTime; m_bLoopPath = loop; }
		void SetFollowPath(bool follow)										{ m_bFollowPath = follow; }
		void Reset();

	private:				
		void FollowPath();
		bool IniPath();
		
		VisBaseEntity_cl*	m_pOwner;		
		bool				m_bFollowPath;
		VisPath_cl*			m_pPath;
		float				m_fPathTime;
		float				m_fStateTimer;
		bool				m_bLoopPath;

		//vforge
		VString pathKey;
		float	pathTime;
		int		loop;
		int		enabled;
		hkvVec3 iniOrientationOffset;
		hkvVec3 endOrientationOffset;
		float	changeOffset;
		
};


//  Collection for handling playable character component
class CmpFollowPath_Collection : public VRefCountedCollection<CmpFollowPath> {};

//============================================================================================================
//  CmpFollowPath_ComponentManager Class
//============================================================================================================
class CmpFollowPath_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpFollowPath_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpFollowPath_Collection &Instances() { return m_Components; }

	protected:
		CmpFollowPath_Collection m_Components;		
		static CmpFollowPath_ComponentManager g_GlobalManager;

};


#endif  
