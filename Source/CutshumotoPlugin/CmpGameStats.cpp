//============================================================================================================
//  CmpGameStats Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpGameStats.h"

//  Register the class in the engine module so it is available for RTTI
V_IMPLEMENT_SERIAL( CmpGameStats, IVObjectComponent, 0, &g_myComponentModule);


CmpGameStats_ComponentManager CmpGameStats_ComponentManager::g_GlobalManager;

//============================================================================================================
//  CmpGameStats Body
//============================================================================================================
void CmpGameStats::onStartup( VisTypedEngineObject_cl *pOwner )
{
	m_iScore = 0;
	m_iSwipeCount = 0;
	m_iSwipeSucess = 0;
	//load effect for cut mark
	m_pFXResCut = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//cutfx.xml");	
}

void CmpGameStats::onRemove(  VisTypedEngineObject_cl *pOwner )
{

}

void CmpGameStats::onFrameUpdate()
{
	if (!GetOwner()) return;

	//if (m_iPlayerLifes <= 0)
		//MyGameManager::GlobalManager().SetPlayTheGame(false);

					
}

void CmpGameStats::AddScore(unsigned int score)
{
	m_iScore += score;
}

void CmpGameStats::DamagePlayer()
{
	m_iPlayerLifes--;
}

void CmpGameStats::CountSwipe (bool success)
{
	m_iSwipeCount++;

	if (success)
		m_iSwipeSucess++;
}

//============================================================================================================
//  CmpGameStats Overrides
//============================================================================================================
void CmpGameStats::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	if (pOwner!=NULL)
	{
		CmpGameStats_ComponentManager::GlobalManager().Instances().AddUnique(this);    
		onStartup( pOwner );
	}
	else
	{
	    onRemove( pOwner );    
	    CmpGameStats_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}


//----------------------------------------------------------------------------------------------------------  
//  CmpGameStats::Serialize(..)
//----------------------------------------------------------------------------------------------------------  
//  component's properties are automatically saved and restored on the vForge 
//  side via property reflection, custom code has to be implemented to 
//  serialize the component's properties into the exported vscene file. 
//
//  You should only save what is absolutely necessary for restoring the object's 
//  state and that cannot be recalculated at loading time. 
//----------------------------------------------------------------------------------------------------------
void CmpGameStats::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPGAMESTATS_VERSION_CURRENT;
	IVObjectComponent::Serialize(ar);
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPGAMESTATS_VERSION_CURRENT , "Invalid local version.");

		//  add your property variables here
		//  Load Data
		ar >> m_iPlayerLifes;

	} 
	else
	{
	    ar << iLocalVersion;    
	    //  Save Data
	    ar << m_iPlayerLifes;
	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpGameStats,IVObjectComponent,    "Game Stats", 
                          VVARIABLELIST_FLAGS_NONE, 
                          "GameStats" )

	DEFINE_VAR_INT	(CmpGameStats, m_iPlayerLifes,   "Initial Player's Lifes", "3", 0, 0);	


END_VAR_TABLE
