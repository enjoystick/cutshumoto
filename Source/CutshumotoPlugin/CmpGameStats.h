//============================================================================================================
//  CmpGameStats Component
//	Author: David Escalona Rocha
//============================================================================================================

#ifndef CMPGAMESTATS_H_INCLUDED
#define CMPGAMESTATS_H_INCLUDED

#include "CutshumotoPluginModule.h"

// Versions
#define CMPGAMESTATS_VERSION_0          0     // Initial version
#define CMPGAMESTATS_VERSION_CURRENT    1     // Current version

//============================================================================================================
//  CmpGameStats Class
//============================================================================================================
class CmpGameStats : public IVObjectComponent
{

	public:
		V_DECLARE_SERIAL  ( CmpGameStats, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpGameStats, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpGameStats() {}
		SAMPLEPLUGIN_IMPEXP ~CmpGameStats(){}  
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE BOOL CanAttachToObject( VisTypedEngineObject_cl *pObject, VString &sErrorMsgOut )
		{
			if ( !IVObjectComponent::CanAttachToObject( pObject, sErrorMsgOut ))return FALSE;  

			if (!pObject->IsOfType( V_RUNTIME_CLASS( VisObject3D_cl )))
			{
				sErrorMsgOut = "Component can only be added to instances of VisObject3D_cl or derived classes.";
				return FALSE;
			}
			return TRUE;
		}
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void OnVariableValueChanged(VisVariable_cl *pVar, const char * value) {}
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );
		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );

		
		signed char GetPlayerLifes()	{ return m_iPlayerLifes; }
		unsigned int GetScore()		{ return m_iScore; }		

		void AddScore(unsigned int score);
		void DamagePlayer();
		void CountSwipe (bool Success);
		VisParticleEffectFile_cl* GetFxResource() { return m_pFXResCut; }

		

	protected:	
		signed char m_iPlayerLifes;
		unsigned int m_iSwipeCount;
		unsigned int m_iSwipeSucess;
		unsigned int m_iScore;
		VisParticleEffectFile_cl*	m_pFXResCut;


	private:		
	

};

//  Collection for handling playable character component
class CmpGameStats_Collection : public VRefCountedCollection<CmpGameStats> {};

//============================================================================================================
//  CmpGameStats_ComponentManager Class
//============================================================================================================
/// This manager class has a list of all available CmpGameStats instances
/// and takes care of calling their CmpGameStats::PerFrameUpdate function
/// on each frame.

class CmpGameStats_ComponentManager : public IVisCallbackHandler_cl
{
	public:

		//   Gets the singleton of the manager
		static CmpGameStats_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		//   Should be called at plugin initialization time.
		void OneTimeInit(){  Vision::Callbacks.OnUpdateSceneFinished += this;} // listen to this callback
  
		//   Should be called at plugin de-initialization time
		void OneTimeDeInit(){ Vision::Callbacks.OnUpdateSceneFinished -= this;} // de-register

		//   Callback method that takes care of updating the managed instances each frame
		
		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			VASSERT( pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished );
			// call update function on every component
			const int iCount = m_Components.Count();
			for (int i=0;i<iCount;i++){ m_Components.GetAt(i)->onFrameUpdate(); }
		}

		//   Gets all VPlayableCharacterComponent instances this manager holds
		inline CmpGameStats_Collection &Instances() { return m_Components; }

	protected:

		/// Holds the collection of all instances of CmpGameStats
		CmpGameStats_Collection m_Components;

		/// One global instance of our manager
		static CmpGameStats_ComponentManager g_GlobalManager;
};

#endif