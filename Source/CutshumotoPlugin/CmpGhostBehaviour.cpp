//============================================================================================================
//  CmpGhostBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpGhostBehaviour.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"

V_IMPLEMENT_SERIAL( CmpGhostBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpGhostBehaviour_ComponentManager CmpGhostBehaviour_ComponentManager::g_GlobalManager;

void CmpGhostBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);
	m_pFXResource = NULL;
	m_pExplosionFX = NULL;
	m_eCurrentState = eCreating;	
}

void CmpGhostBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
	CmpEnemyBehaviour::onRemove( pOwner );
}

void CmpGhostBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;
			case eAttacking:	OnExitAttack();		break;
			case eRunning:		OnExitRun();		break;
			case eDisappeared:	OnExitDisappear();	break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;
		case eAttacking:	OnEnterAttack();		break;
		case eRunning:		OnEnterRun();			break;
		case eDisappeared:	OnEnterDisappear();		break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpGhostBehaviour::onFrameUpdate()
{		
	CmpEnemyBehaviour::onFrameUpdate();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;
		case eAttacking:	OnAttack();		break;
		case eRunning:		OnRun();		break;
		case eDisappeared:	OnDisappear();	break;
	}
	
}

void CmpGhostBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpGhostBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpGhostBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpGhostBehaviour::ChangePosition(hkvVec3& ghostPos)
{			
	VRandom rand;		
	int seconds = VDateTime::Now().GetSecond (VDateTime::LOCAL);
	
	rand.Reset(seconds);	

	float value = rand.GetFloat() * 4;
	int newRail = static_cast<int>(hkvMath::ceil(value));

	switch (newRail)
	{
		case 1:	ghostPos.x = X_FIRST_RAIL;	break;
		case 2:	ghostPos.x = X_SECOND_RAIL;	break;
		case 3:	ghostPos.x = X_THIRD_RAIL;	break;
		case 4:	ghostPos.x = X_FOURTH_RAIL;	break;
	}

}

void CmpGhostBehaviour::OnEnterCreate()
{
	CmpEnemyBehaviour::OnEnterCreate();

}

void CmpGhostBehaviour::TriggerExplosion(bool forward)
{
	hkvVec3 pos = m_pOwner->GetPosition() + hkvVec3(0,0,100);

	if (forward)
		pos+=m_pCmpProperties->GetSpeed();

	if (!m_pFXResource)
		m_pFXResource = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//DavidExplosion.xml");		

	if (!m_pFXResource) return;

	if (!m_pExplosionFX)
	{	
		m_pExplosionFX = m_pFXResource->CreateParticleEffectInstance(pos, m_pOwner->GetOrientation());	
		m_pExplosionFX->SetRemoveWhenFinished(false);		

	}
	else
	{	
		m_pExplosionFX->SetPosition(pos);
		m_pExplosionFX->Restart();
	}

}

void CmpGhostBehaviour::OnCreate()
{
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//we need to change this to wait creating animation time, but we have not creating animation yet
	if (m_fStateTimer >= 0.1f)
		ChangeState(eRunning);
	
}

void CmpGhostBehaviour::OnExitCreate()
{
}

void CmpGhostBehaviour::OnEnterAttack()
{
	CmpEnemyBehaviour::OnEnterAttack();
}

void CmpGhostBehaviour::OnAttack()
{
	if (!HasEnemyProperties())	return;

	m_pOwner->SetPosition(m_pOwner->GetPosition() + (m_pCmpProperties->GetSpeed() * Vision::GetTimer()->GetTimeDifference()));
}

void CmpGhostBehaviour::OnExitAttack()
{

}

void CmpGhostBehaviour::OnEnterRun()
{
	CmpEnemyBehaviour::OnEnterRun();	
}

void CmpGhostBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;	

	m_pOwner->SetPosition(m_pOwner->GetPosition() + (m_pCmpProperties->GetSpeed() * Vision::GetTimer()->GetTimeDifference()));

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();
	
	if (m_fStateTimer >= m_pCmpProperties->GetTempo())			
		ChangeState(eDisappeared);	
}

void CmpGhostBehaviour::OnExitRun()
{
}

void CmpGhostBehaviour::OnEnterDisappear()
{
	//particle effects or animation
	TriggerExplosion(false);

	//not visible
	m_pOwner->SetVisibleBitmask(0);

	hkvVec3 newPos = m_pOwner->GetPosition();
	
	ChangePosition(newPos); //change ghost position

	m_pOwner->SetPosition(newPos);	
	
}

void CmpGhostBehaviour::OnDisappear()
{
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	if (m_pExplosionFX)
	{
		if (m_pExplosionFX->IsLifeTimeOver())	
			m_pExplosionFX->SetPause(true);
	}

	if (m_fStateTimer > (2 * m_pCmpProperties->GetCooldown() / 3))
	{
		if (m_pExplosionFX->IsLifeTimeOver())
			TriggerExplosion(true);
	}
	
	if (m_fStateTimer >= m_pCmpProperties->GetCooldown())			
		ChangeState(eRunning);		
}

void CmpGhostBehaviour::OnExitDisappear()
{
	m_pOwner->SetVisibleBitmask(1);
}


