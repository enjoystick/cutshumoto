//============================================================================================================
//  CmpGhostBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPGHOSTBEHAVIOUR_H_INCLUDED
#define CMPGHOSTBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPGHOSTBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPGHOSTBEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpGhostBehaviour Class
//============================================================================================================
class CmpGhostBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpGhostBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpGhostBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpGhostBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpGhostBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void ChangeState(eEnemyState newState);
		void ChangePosition(hkvVec3& ghostPos);
		void TriggerExplosion(bool forward);

		virtual void OnEnterCreate();		virtual void OnCreate();		virtual void OnExitCreate();
		virtual void OnEnterAttack();		virtual void OnAttack();		virtual void OnExitAttack();
		virtual void OnEnterRun();			virtual void OnRun();			virtual void OnExitRun();
		virtual void OnEnterDisappear();	virtual void OnDisappear();		virtual void OnExitDisappear();

	private:
		VisParticleEffectFile_cl*	m_pFXResource;
		VisParticleEffectPtr		m_pExplosionFX;
		
};


//  Collection for handling playable character component
class CmpGhostBehaviour_Collection : public VRefCountedCollection<CmpGhostBehaviour> {};

//============================================================================================================
//  CmpGhostBehaviour_ComponentManager Class
//============================================================================================================
class CmpGhostBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpGhostBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpGhostBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpGhostBehaviour_Collection m_Components;		
		static CmpGhostBehaviour_ComponentManager g_GlobalManager;

};


#endif  
