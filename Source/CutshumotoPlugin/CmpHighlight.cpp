#include "CutshumotoPluginPCH.h"
#include "CmpHighlight.h"
#include "CmpHighlightManager.h"

V_IMPLEMENT_SERIAL(CmpHighlight, IVObjectComponent, 0, &g_myComponentModule);

CmpHighlight::CmpHighlight() 
{
	m_color = VColorRef(255, 255, 255,0);
	m_flashColor = VColorRef(0, 0, 0);
	m_flashStartTime = -1.f;
	m_flashDuration = 1.0f;
	m_flashing = false;
}

void CmpHighlight::SetColor(VColorRef const& color)
{
	m_color = color;
}

VColorRef const& CmpHighlight::GetColor() const
{
	if (m_flashing)
		return m_flashColor;

	return m_color;
}

void CmpHighlight::Flash(VColorRef const& color, float const duration /*= 1.0f*/)
{
	m_flashStartTime = Vision::GetTimer()->GetTime();
	m_flashDuration = duration;
	m_flashColor = color;
}

void CmpHighlight::Update(float time)
{
	float timediff = time - m_flashStartTime;
	
	m_flashing = timediff < m_flashDuration;

	if (m_flashing)
	{
	    float intensity = (timediff / m_flashDuration);
	    m_flashColor.Lerp(m_flashColor, m_color, intensity);
	}
}

bool CmpHighlight::IsFlashing() const
{	
	return m_flashing;
}

void CmpHighlight::SetOwner(VisTypedEngineObject_cl *owner)
{  
	IVObjectComponent::SetOwner( owner );

	if(owner)
	{
		CmpHighlightComponentManager::s_instance.GetHighlightables().AddUnique(this);

		static_cast<VisBaseEntity_cl *>(owner)->SetTraceAccuracy(VIS_TRACEACC_AABOX);
		static_cast<VisBaseEntity_cl *>(owner)->SetTracelineBitmask(CmpHighlightComponentManager::s_instance.GetTraceBitmask());
	}
	else
	{
		CmpHighlightComponentManager::s_instance.GetHighlightables().Remove(this);
	}
}



