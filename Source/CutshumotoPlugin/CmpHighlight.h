#ifndef CMPHIGHLIGHT_H
#define CMPHIGHLIGHT_H

#include "CutshumotoPluginPCH.h"

class CmpHighlight : public IVObjectComponent
{
	public:

		V_DECLARE_SERIAL  ( CmpHighlight, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpHighlight, SAMPLEPLUGIN_IMPEXP );

		CmpHighlight();

		void SetColor(VColorRef const& color);
		VColorRef const& GetColor() const;
	    
		void Flash(VColorRef const& color, float const duration = 1.0f);
		void Update(float time);
		bool IsFlashing() const;

	protected:
  		
		void SetOwner(VisTypedEngineObject_cl *owner) HKV_OVERRIDE;

	private:
		VColorRef m_color;          /// Highlight color
		VColorRef m_flashColor;     /// Separate flash color
		float m_flashStartTime;     /// Time where flash fadeout effect started
		float m_flashDuration;      /// Duration of flash fadeout effect for this instance
		bool m_flashing;            /// Flashing state

};

typedef VSmartPtr<CmpHighlight> CmpHighlightPtr;
typedef VRefCountedCollection<CmpHighlight> CmpHighlightCollection;

#endif

