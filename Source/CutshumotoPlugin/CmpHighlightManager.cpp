#include "CutshumotoPluginPCH.h"
#include "CmpHighlightManager.h"

int const kHighlightableTraceBitmask = 2;

void CmpHighlightComponentManager::OneTimeInit()
{
	Vision::Callbacks.OnWorldInit += this;
	Vision::Callbacks.OnWorldDeInit += this;
	Vision::Callbacks.OnRenderHook += this;
	Vision::Callbacks.OnReassignShaders += this;
	Vision::Callbacks.OnUpdateSceneFinished += this;
}

void CmpHighlightComponentManager::OneTimeDeInit()
{
	Vision::Callbacks.OnRenderHook -= this;
	Vision::Callbacks.OnWorldDeInit -= this;
	Vision::Callbacks.OnWorldInit -= this;
	Vision::Callbacks.OnReassignShaders -= this;
	Vision::Callbacks.OnUpdateSceneFinished -= this;

	VASSERT(GetHighlightables().Count() == 0);
}

CmpHighlightCollection& CmpHighlightComponentManager::GetHighlightables()
{
	return m_highlightableComponents;
}

CmpHighlightCollection const& CmpHighlightComponentManager::GetHighlightables() const
{
	return m_highlightableComponents;
}

void CmpHighlightComponentManager::InitShaders()
{
	if(Vision::Shaders.LoadShaderLibrary("\\Shaders\\Cutshumoto.ShaderLib"))
	{
		m_xrayTechnique = Vision::Shaders.CreateTechnique("EntityXRay", NULL);
		if(m_xrayTechnique)
		{
			InitShader(m_xrayTechnique, m_xrayPassColorRegisters);
		}

		m_highlightTechnique = Vision::Shaders.CreateTechnique("EntityHighlight", NULL);
		if(m_highlightTechnique)
		{
			InitShader(m_highlightTechnique, m_highlightPassColorRegisters);
		}
	}
}

void CmpHighlightComponentManager::InitShader(VCompiledTechnique *shader, DynArray_cl<int>& regsOut)
{
	regsOut.Resize(shader->GetShaderCount());
	
	for(int i = 0; i < shader->GetShaderCount(); i++)
	{
		VCompiledShaderPass *const shaderPass = shader->GetShader(i);
		regsOut[i] = shader->GetShader(i)->GetConstantBuffer(VSS_VertexShader)->GetRegisterByName("HighlightColor");
	}
}

void CmpHighlightComponentManager::UpdateHighlightableEntities(CmpHighlightCollection& highlightables)
{
	float time = Vision::GetTimer()->GetTime();

	for(int i = 0; i < highlightables.Count(); i++)
	{
		CmpHighlight *comp = highlightables.GetAt(i);    
		comp->Update(time);  
	}
}

void CmpHighlightComponentManager::RenderHighlightableEntities(VCompiledTechnique *shader, DynArray_cl<int> const& regs, CmpHighlightCollection const& highlightables)
{
	for(int i = 0; i < highlightables.Count(); i++)
	{
		CmpHighlight const *const comp = highlightables.GetAt(i);

		VisBaseEntity_cl *const ent = static_cast<VisBaseEntity_cl *>(comp->GetOwner());

		// KGC demo hack (?)
		if(!ent->GetAnimConfig() || ent->GetAnimConfig()->GetSkinningMeshBuffer() == NULL)
		continue;
		
		if (!comp->IsFlashing())
			continue;

		VColorRef color = comp->GetColor();
		hkvVec4 const color4 = color.getAsVec4(); //(0.15f, 0.15f, 0.15f, 1.0f);

		for(int j = 0; j < shader->GetShaderCount(); j++)
		{
			VCompiledShaderPass *const shaderPass = shader->GetShader(j);
		
			if(regs[j] >= 0)
			{
				shaderPass->GetConstantBuffer(VSS_VertexShader)->SetSingleRegisterF(regs[j], color4.data);
				shaderPass->m_bModified = true;
			}
		}

		Vision::RenderLoopHelper.RenderEntityWithShaders(ent, shader->GetShaderCount(), shader->GetShaderList());
	}
}

void CmpHighlightComponentManager::OnHandleCallback(IVisCallbackDataObject_cl *data)
{
	if(data->m_pSender == &Vision::Callbacks.OnWorldInit)
	{
		InitShaders();
	}
	else if(data->m_pSender == &Vision::Callbacks.OnWorldDeInit)
	{
		m_xrayTechnique = NULL;
		m_highlightTechnique = NULL;
	}
	else if(data->m_pSender == &Vision::Callbacks.OnReassignShaders)
	{
		InitShaders();
	}
	else if(data->m_pSender == &Vision::Callbacks.OnRenderHook)
	{
		VisRenderHookDataObject_cl const *const renderHookData = static_cast<VisRenderHookDataObject_cl *>(data);
		switch(renderHookData->m_iEntryConst)
		{
			// XRay renders before opaque entities
			case VRH_PRE_PRIMARY_OPAQUE_PASS_ENTITIES:
			{
				if(m_xrayTechnique)
				{
					RenderHighlightableEntities(m_xrayTechnique, m_xrayPassColorRegisters, GetHighlightables());
				}
			}
			break;

		// Highlight renders after opaque entities
		case VRH_PRE_OCCLUSION_TESTS:
		{
			if(m_highlightTechnique)
			{
				RenderHighlightableEntities(m_highlightTechnique, m_highlightPassColorRegisters, GetHighlightables());
			}
		}
		break;

		default:
			break;
		}
	}
	else if (data->m_pSender == &Vision::Callbacks.OnUpdateSceneFinished)
	{
		UpdateHighlightableEntities(GetHighlightables());
	}
}

int CmpHighlightComponentManager::GetTraceBitmask() const
{
	return kHighlightableTraceBitmask;
}

CmpHighlightComponentManager CmpHighlightComponentManager::s_instance;
