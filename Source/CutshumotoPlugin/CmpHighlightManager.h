#ifndef CMPHIGHLIGHT_COMPONENTMANAGER_H_
#define CMPHIGHLIGHT_COMPONENTMANAGER_H_

#include "CutshumotoPluginPCH.h"
#include "CmpHighlight.h"

class CmpHighlightComponentManager : public IVisCallbackHandler_cl
{
	public:
		void OneTimeInit();
		void OneTimeDeInit();

		CmpHighlightCollection& GetHighlightables();
		CmpHighlightCollection const& GetHighlightables() const;

		void SetHighlighted(CmpHighlight *highlighted);

		static CmpHighlightComponentManager s_instance;

		int GetTraceBitmask() const;

	private:
		void InitShaders();

		static void InitShader(VCompiledTechnique *shader, DynArray_cl<int>& regsOut);
		static void UpdateHighlightableEntities(CmpHighlightCollection& highlightables);
		static void RenderHighlightableEntities(VCompiledTechnique *shader, DynArray_cl<int> const& regs, CmpHighlightCollection const& highlightables);
		
		void OnHandleCallback(IVisCallbackDataObject_cl *data) HKV_OVERRIDE;
	
		VCompiledTechniquePtr m_xrayTechnique;
		DynArray_cl<int> m_xrayPassColorRegisters;
	
		VCompiledTechniquePtr m_highlightTechnique;
		DynArray_cl<int> m_highlightPassColorRegisters;

		CmpHighlightCollection m_highlightableComponents;
		CmpHighlightPtr m_highlightedComponent;
};

#endif


