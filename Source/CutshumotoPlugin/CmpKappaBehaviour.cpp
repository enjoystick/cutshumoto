//============================================================================================================
//  CmpKappaehaviour Component
//	Author: Sergio Gomez
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpKappaBehaviour.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"

V_IMPLEMENT_SERIAL( CmpKappaBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpKappaBehaviour_ComponentManager CmpKappaBehaviour_ComponentManager::g_GlobalManager;

void CmpKappaBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);

	m_eCurrentState = eCreating;	
	m_vJumpSpeed = hkvVec3(0,400,1500); 
	m_fJumpTime = 0.75f;	
	m_vGravity = hkvVec3(0,0,-4000);
	m_bHasShoot = false;
}

void CmpKappaBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
    
}

void CmpKappaBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;
			case eAttacking:	OnExitAttack();		break;
			case eRunning:		OnExitRun();		break;
			case eJumping:		OnExitJumping();	break;
			case eFalling:		OnExitFalling();	break;
			case eRelaxing:		OnExitRelaxing();	break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;
		case eAttacking:	OnEnterAttack();		break;
		case eRunning:		OnEnterRun();			break;
		case eJumping:		OnEnterJumping();		break;
		case eFalling:		OnEnterFalling();		break;
		case eRelaxing:		OnEnterRelaxing();		break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpKappaBehaviour::onFrameUpdate()
{		
	CmpEnemyBehaviour::onFrameUpdate();

	//Sergio 28_08_14 Rana Friend if underground

	if(m_pOwner->GetPosition().z<=0 && m_pOwner->GetEntityKey()==ENEMY_ENT_KEY)
		m_pOwner->SetEntityKey(FRIEND_ENT_KEY);
	if(m_pOwner->GetPosition().z>0 && m_pOwner->GetEntityKey()==FRIEND_ENT_KEY)
		m_pOwner->SetEntityKey(ENEMY_ENT_KEY);

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;
		case eAttacking:	OnAttack();		break;
		case eRunning:		OnRun();		break;
		case eJumping:		OnJumping();	break;
		case eFalling:		OnFalling();	break;
		case eRelaxing:		OnRelaxing();	break;
	}

}

void CmpKappaBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpKappaBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpKappaBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}


//state creating
void CmpKappaBehaviour::OnEnterCreate()
{
	//CmpEnemyBehaviour::OnEnterCreate();
}

void CmpKappaBehaviour::OnCreate()
{
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//we need to change this to wait creating animation time, but we have not creating animation yet
	if (m_fStateTimer >= 1)			
		ChangeState(eRunning);	
}

void CmpKappaBehaviour::OnExitCreate()
{
}

//state attacking
void CmpKappaBehaviour::OnEnterAttack()
{
	//CmpEnemyBehaviour::OnEnterAttack();

	m_fAttackTime = 0.0f;
	/*if (!HasAnimationSM()) 
	{
		m_fAttackTime = 1.0f;
		return;
	}

	m_fAttackTime = m_pAnimSM->GetActiveState()->GetLength();*/
}

void CmpKappaBehaviour::OnAttack()
{
	if (!HasEnemyProperties())	return;

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//if (m_fStateTimer >= m_fAttackTime)
		ChangeState(eFalling);
}

void CmpKappaBehaviour::OnExitAttack()
{
	//only shoot one time
	

	//create shuriken
	if(!m_bHasShoot)
	{
		VisBaseEntity_cl* stone=EnemiesFactory::Instance()->CreateEnemy(EShuriken, m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->GetNumLevel(), m_pOwner->GetPosition() + hkvVec3(0,120,300));
		if (stone)
		{	stone->SetMesh("Models//Ch_Projectile.model");
			stone->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(false);
		}
		m_fAttackTime=0.0;
	}
	m_bHasShoot = true;

}

//state running
void CmpKappaBehaviour::OnEnterRun()
{
	CmpEnemyBehaviour::OnEnterRun();
	m_pAnimSM->SetEnabled(false);
}

void CmpKappaBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;

	hkvVec3 speedY(0,m_pCmpProperties->GetSpeed().y,0);
	m_pOwner->SetPosition(m_pOwner->GetPosition() + (speedY * Vision::GetTimer()->GetTimeDifference()));

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	if (m_fStateTimer >= m_pCmpProperties->GetTempo())			
		ChangeState(eJumping);
	
}

void CmpKappaBehaviour::OnExitRun()
{
}

//state jumping
void CmpKappaBehaviour::OnEnterJumping()
{
	if (!HasAnimationSM()) return;	
	
	m_pAnimSM->SetEnabled(true);

	m_fAttackTime = m_pAnimSM->GetActiveState()->GetLength()/2;

}

void CmpKappaBehaviour::OnJumping()
{
	if (!HasEnemyProperties())	return;

	float elapsed = Vision::GetTimer()->GetTimeDifference();

	//JumpKappaJump
	/*hkvVec3 speedZ(0,m_pCmpProperties->GetSpeed().y,m_pCmpProperties->GetSpeed().z);
	m_pOwner->SetPosition(m_pOwner->GetPosition() + (speedZ * Vision::GetTimer()->GetTimeDifference()));*/
	hkvVec3 speedY(0,m_pCmpProperties->GetSpeed().y,0);
	m_pOwner->SetPosition(m_pOwner->GetPosition() + (speedY * Vision::GetTimer()->GetTimeDifference()));


	m_fStateTimer+= elapsed;

	if (m_fStateTimer >= m_fAttackTime)
		ChangeState(eAttacking);	

}

void CmpKappaBehaviour::OnExitJumping()
{
	/*if (!HasAnimationSM()) return;

	m_pAnimSM->SetEnabled(true);*/

}

void CmpKappaBehaviour::OnEnterFalling()
{
		m_fAttackTime = m_pAnimSM->GetActiveState()->GetLength()/2;
}

void CmpKappaBehaviour::OnExitFalling()
{
}

void CmpKappaBehaviour::OnFalling()
{
	if (!HasEnemyProperties())	return;

	float elapsed = Vision::GetTimer()->GetTimeDifference();

	//FallKappaFall
	//hkvVec3 speedZ(0,m_pCmpProperties->GetSpeed().y,-m_pCmpProperties->GetSpeed().z);
	//m_pOwner->SetPosition(m_pOwner->GetPosition() + (speedZ * Vision::GetTimer()->GetTimeDifference()));
	hkvVec3 speedY(0,m_pCmpProperties->GetSpeed().y,0);
	m_pOwner->SetPosition(m_pOwner->GetPosition() + (speedY * Vision::GetTimer()->GetTimeDifference()));


	m_fStateTimer+= elapsed;

	if (m_fStateTimer >= m_fAttackTime)
		ChangeState(eRelaxing);	
}

void CmpKappaBehaviour::OnEnterRelaxing()
{
	m_pAnimSM->SetEnabled(false);
}

void CmpKappaBehaviour::OnExitRelaxing()
{
}

void CmpKappaBehaviour::OnRelaxing()
{
	if (!HasEnemyProperties())	return;

	//hkvVec3 speedY(0,m_pCmpProperties->GetSpeed().y,0);
	//m_pOwner->SetPosition(m_pOwner->GetPosition() + (speedY * Vision::GetTimer()->GetTimeDifference()));

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	if (m_fStateTimer >= m_pCmpProperties->GetCooldown())			
		ChangeState(eRunning);
}