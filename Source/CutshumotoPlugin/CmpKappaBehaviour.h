//============================================================================================================
//  CmpKappaBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPKAPPABEHAVIOUR_H_INCLUDED
#define CMPKAPPABEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPKAPPABEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPKAPPABEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpKappaBehaviour Class
//============================================================================================================
class CmpKappaBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpKappaBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpKappaBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpKappaBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpKappaBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void ChangeState(eEnemyState newState);
		
		virtual void OnEnterCreate();	virtual void OnCreate();	virtual void OnExitCreate();
		virtual void OnEnterAttack();	virtual void OnAttack();	virtual void OnExitAttack();
		virtual void OnEnterRun();		virtual void OnRun();		virtual void OnExitRun();
		virtual void OnEnterJumping();	virtual void OnJumping();	virtual void OnExitJumping();
		virtual void OnEnterFalling();	virtual void OnFalling();	virtual void OnExitFalling();
		virtual void OnEnterRelaxing();	virtual void OnRelaxing();	virtual void OnExitRelaxing();
			
		hkvVec3 m_vJumpSpeed;
		hkvVec3 m_vGravity;
		float m_fJumpTime;		
		float m_fAttackTime;
		bool m_bHasShoot;
		
		
};


//  Collection for handling playable character component
class CmpKappaBehaviour_Collection : public VRefCountedCollection<CmpKappaBehaviour> {};

//============================================================================================================
//  CmpKappaBehaviour_ComponentManager Class
//===========================================================================================================
class CmpKappaBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpKappaBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpKappaBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpKappaBehaviour_Collection m_Components;		
		static CmpKappaBehaviour_ComponentManager g_GlobalManager;

};


#endif  
