//============================================================================================================
//  CmpNinjaBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpNinjaBehaviour.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"
#include "CmpSlicing.h"

V_IMPLEMENT_SERIAL( CmpNinjaBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpNinjaBehaviour_ComponentManager CmpNinjaBehaviour_ComponentManager::g_GlobalManager;

void CmpNinjaBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);

	m_eCurrentState = eCreating;	
	m_vJumpSpeed = hkvVec3(0,400,1500); 
	m_fJumpTime = 0.0f;	
	m_vGravity = hkvVec3(0,0,-4000);
	m_bHasShoot = false;
}

void CmpNinjaBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
    
}

void CmpNinjaBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;
			case eAttacking:	OnExitAttack();		break;
			case eRunning:		OnExitRun();		break;
			case eJumping:		OnExitJumping();	break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;
		case eAttacking:	OnEnterAttack();		break;
		case eRunning:		OnEnterRun();			break;
		case eJumping:		OnEnterJumping();		break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpNinjaBehaviour::onFrameUpdate()
{		
	CmpEnemyBehaviour::onFrameUpdate();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;
		case eAttacking:	OnAttack();		break;
		case eRunning:		OnRun();		break;
		case eJumping:		OnJumping();	break;
	}

}

void CmpNinjaBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpNinjaBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpNinjaBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}


//state creating
void CmpNinjaBehaviour::OnEnterCreate()
{
	CmpEnemyBehaviour::OnEnterCreate();
}

void CmpNinjaBehaviour::OnCreate()
{
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//we need to change this to wait creating animation time, but we have not creating animation yet
	if (m_fStateTimer >= 1)			
		ChangeState(eRunning);	
}

void CmpNinjaBehaviour::OnExitCreate()
{
}

//state attacking
void CmpNinjaBehaviour::OnEnterAttack()
{
	CmpEnemyBehaviour::OnEnterAttack();

	if (!HasAnimationSM()) 
	{
		m_fAttackTime = 0.1f;
		return;
	}

	m_fAttackTime = m_pAnimSM->GetActiveState()->GetLength();


	//reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( pShuriken );
}

void CmpNinjaBehaviour::OnAttack()
{
	if (!HasEnemyProperties())	return;

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	if (m_fStateTimer > 0.4f && !m_bHasShoot)
	{
		//only shoot one time
		m_bHasShoot = true;

		//create shuriken
		VisBaseEntity_cl* pShuriken = EnemiesFactory::Instance()->CreateEnemy(EShuriken, m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->GetNumLevel(), m_pOwner->GetPosition() + hkvVec3(-60,20,100));
		pShuriken->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced( false );
	}

	if (m_fStateTimer >= m_fAttackTime)
		ChangeState(eRunning);
}

void CmpNinjaBehaviour::OnExitAttack()
{

}

//state running
void CmpNinjaBehaviour::OnEnterRun()
{
	CmpEnemyBehaviour::OnEnterRun();
}

void CmpNinjaBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;

	m_pOwner->SetPosition(m_pOwner->GetPosition() + (m_pCmpProperties->GetSpeed() * Vision::GetTimer()->GetTimeDifference()));

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	if (m_fStateTimer >= m_pCmpProperties->GetTempo() && !m_bHasShoot)			
		ChangeState(eJumping);
	
}

void CmpNinjaBehaviour::OnExitRun()
{
}

//state jumping
void CmpNinjaBehaviour::OnEnterJumping()
{
	/*if (!HasAnimationSM()) return;	
	
	m_pAnimSM->SetEnabled(false);*/
}

void CmpNinjaBehaviour::OnJumping()
{
	if (!HasEnemyProperties())	return;

	float elapsed = Vision::GetTimer()->GetTimeDifference();	
	float angSpeed = 360 / m_fJumpTime;

	//rotate ninja
	//hkvVec3 orientation = m_pOwner->GetOrientation();
	//orientation.z -= angSpeed * elapsed;
	//m_pOwner->SetOrientation(orientation);

	//move ninja
	//m_vJumpSpeed+= (m_vGravity * elapsed);
	//m_pOwner->SetPosition(m_pOwner->GetPosition() + (m_vJumpSpeed * elapsed));	

	//snap to floor
	//if (m_pOwner->GetPosition().z <= 0)
		//m_pOwner->SetPosition(m_pOwner->GetPosition().x, m_pOwner->GetPosition().y,0);

	m_fStateTimer+= elapsed;

	if (m_fStateTimer >= m_fJumpTime)
		ChangeState(eAttacking);	

}

void CmpNinjaBehaviour::OnExitJumping()
{
	if ( HasAnimationSM() )
		m_pAnimSM->SetEnabled(true);
}


