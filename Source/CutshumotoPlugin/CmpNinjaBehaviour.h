//============================================================================================================
//  CmpNinjaBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPNINJABEHAVIOUR_H_INCLUDED
#define CMPNINJABEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPNINJABEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPNINJABEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpNinjaBehaviour Class
//============================================================================================================
class CmpNinjaBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpNinjaBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpNinjaBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpNinjaBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpNinjaBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void ChangeState(eEnemyState newState);
		
		virtual void OnEnterCreate();	virtual void OnCreate();	virtual void OnExitCreate();
		virtual void OnEnterAttack();	virtual void OnAttack();	virtual void OnExitAttack();
		virtual void OnEnterRun();		virtual void OnRun();		virtual void OnExitRun();
		virtual void OnEnterJumping();	virtual void OnJumping();	virtual void OnExitJumping();

		hkvVec3 m_vJumpSpeed;
		hkvVec3 m_vGravity;
		float m_fJumpTime;		
		float m_fAttackTime;
		bool m_bHasShoot;
		
		
};


//  Collection for handling playable character component
class CmpNinjaBehaviour_Collection : public VRefCountedCollection<CmpNinjaBehaviour> {};

//============================================================================================================
//  CmpNinjaBehaviour_ComponentManager Class
//============================================================================================================
class CmpNinjaBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpNinjaBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpNinjaBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpNinjaBehaviour_Collection m_Components;		
		static CmpNinjaBehaviour_ComponentManager g_GlobalManager;

};


#endif  
