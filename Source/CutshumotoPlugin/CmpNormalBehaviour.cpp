//============================================================================================================
//  CmpNormalBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpNormalBehaviour.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"

V_IMPLEMENT_SERIAL( CmpNormalBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpNormalBehaviour_ComponentManager CmpNormalBehaviour_ComponentManager::g_GlobalManager;

void CmpNormalBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);

	m_eCurrentState = eCreating;	
	m_fTempoAttack=m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>()->GetTempo();
}

void CmpNormalBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
	CmpEnemyBehaviour::onRemove( pOwner );
}

void CmpNormalBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;
			case eAttacking:	OnExitAttack();		break;
			case eRunning:		OnExitRun();		break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;
		case eAttacking:	OnEnterAttack();		break;
		case eRunning:		OnEnterRun();		break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpNormalBehaviour::onFrameUpdate()
{
	CmpEnemyBehaviour::onFrameUpdate();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;
		case eAttacking:	OnAttack();		break;
		case eRunning:		OnRun();		break;
	}

}

void CmpNormalBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpNormalBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpNormalBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

//State Creating
void CmpNormalBehaviour::OnEnterCreate()
{
	CmpEnemyBehaviour::OnEnterCreate();
}

void CmpNormalBehaviour::OnCreate()
{
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//we need to change this to wait creating animation time, but we have not creating animation yet
	if (m_fStateTimer >= 0)
		ChangeState(eRunning);
}

void CmpNormalBehaviour::OnExitCreate()
{
}

//State Attacking
void CmpNormalBehaviour::OnEnterAttack()
{
	CmpEnemyBehaviour::OnEnterAttack();
}

void CmpNormalBehaviour::OnAttack()
{
	if (!HasEnemyProperties())	return;

	m_pOwner->SetPosition(m_pOwner->GetPosition() + (m_pCmpProperties->GetSpeed() * Vision::GetTimer()->GetTimeDifference()));
}

void CmpNormalBehaviour::OnExitAttack()
{

}

//State Running
void CmpNormalBehaviour::OnEnterRun()
{
	CmpEnemyBehaviour::OnEnterRun();
}

void CmpNormalBehaviour::OnRun()
{
	if (!HasEnemyProperties())	return;

	m_pOwner->SetPosition(m_pOwner->GetPosition() + (m_pCmpProperties->GetSpeed() * Vision::GetTimer()->GetTimeDifference()));

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//test
	if (m_fStateTimer >= m_fTempoAttack)
		ChangeState(eAttacking);

}

void CmpNormalBehaviour::OnExitRun()
{
}


