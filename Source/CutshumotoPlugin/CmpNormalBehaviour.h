//============================================================================================================
//  CmpNormalBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPNORMALBEHAVIOUR_H_INCLUDED
#define CMPNORMALBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPNORMALBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPNORMALBEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpNormalBehaviour Class
//============================================================================================================
class CmpNormalBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpNormalBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpNormalBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpNormalBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpNormalBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void ChangeState(eEnemyState newState);
		
		virtual void OnEnterCreate();	virtual void OnCreate();	virtual void OnExitCreate();
		virtual void OnEnterAttack();	virtual void OnAttack();	virtual void OnExitAttack();
		virtual void OnEnterRun();		virtual void OnRun();		virtual void OnExitRun();		

		float m_fTempoAttack;
		
};


//  Collection for handling playable character component
class CmpNormalBehaviour_Collection : public VRefCountedCollection<CmpNormalBehaviour> {};

//============================================================================================================
//  CmpNormalBehaviour_ComponentManager Class
//============================================================================================================
class CmpNormalBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpNormalBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpNormalBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpNormalBehaviour_Collection m_Components;		
		static CmpNormalBehaviour_ComponentManager g_GlobalManager;

};


#endif  
