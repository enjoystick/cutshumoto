//============================================================================================================
//  CmpShogunBehaviour Component
//	Author: David Escalona
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpShogunBehaviour.h"
#include "CmpEnemyProperties.h"
#include "CutshumotoUtilities.h"
#include "CmpSlicing.h"
#include "CmpCameraEffects.h"
#include "CutshumotoUtilities.h"
#include "SoundManager.h"
#include "CmpFollowPath.h"
#include "GameManager.h"

#include <sstream>
#include <string.h>

const char* SHOGUN_IDLE = "idle";
const char* SHOGUN_ATTACK = "attack";
const char* SHOGUN_DAMAGE1 = "damage_1";
const char* SHOGUN_DAMAGE2 = "damage_2";
const char* SHOGUN_DAMAGE3 = "damage_3";
const char* SHOGUN_WALK = "walk";
const char* SHOGUN_RUN = "run";
const char* SHOGUN_POSE = "teleport_pose";
const char* SHOGUN_POSEMIRROR = "Teleport_Mirror";
const char* SHOGUN_DEATH = "death";
const char* SHOGUN_JUMP = "Jump";
const char* CUTLINE_KEY = "cutline";

const char*	SHOGUN_LEFTHAND_BONE = "Bip001 L Finger12";
const char*	SHOGUN_RIGHTHAND_BONE = "Bip001 R Finger12";
const float	CIVIL_DISPLACEMENT = 170;
const char* CIVIL_EXITPATH = "exitCivil";

const float	INISEQ_SPEED = 100;
const int	STAGE_NUMBER = 3;

const float BAR_INIHEIGHT = 20;
const float BAR_WIDTH = 61;
const float BAR_HEIGHT = 367;
const float BAR_OFFSET = 20;

const float IDLETIME_AFTERHIT = 2.0f;
const float	TIME_AFTERDEATH = 2.5f;
const float SECURE_SQUAREDIST = 400;
const float MINSPEED_ALIGN = 10;

//como me han metido los dos ataques diferentes en la misma animacion, tengo que cortar "a mano" la animacion :/
const float ATTACK_TRANSITIONTIME = 1.5f;
const float ATTACK_IMPACTTIME = 0.35f;
const float JUMP_DOWNTIME = 3.75f;
const float JUMP_UPTIME = 2.45f;
const float FALLDOWN_TIME = 1.27f;
const float JUMPSPEED = 2800;

const int ZONEPOINTS = 5;

V_IMPLEMENT_SERIAL( CmpShogunBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpShogunBehaviour_ComponentManager CmpShogunBehaviour_ComponentManager::g_GlobalManager;

void CmpShogunBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);		
	m_bAlreadyDead = false;
	m_bBossSuccess = false;
}

void CmpShogunBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
	DynArray_cl<VisBaseEntity_cl*> civilians;	
	Vision::Game.SearchEntity("crew",&civilians);

	//remove civilians
	for (unsigned int i = 0; i < civilians.GetValidSize(); i++)
	{	
		if (civilians.IsValid(i))
			reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->UntrackSlicedEntity(civilians[i]);
	}

	//remove shogun from entities to slice
	reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->UntrackSlicedEntity(pOwner);
}

void CmpShogunBehaviour::onAfterSceneLoaded()
{
	InitShogunBehaviour();

	InitGUI();
	
	ChangeShogunState(eCreating);
}

void CmpShogunBehaviour::ShowLifeBar(bool show)
{
	m_ptrLifeBar->SetVisible(show);
	m_ptrLifeBarLeft->SetVisible(show);
	m_ptrBossHead->SetVisible(show);
}

void CmpShogunBehaviour::InitGUI()
{
	m_ptrLifeBarLeft = new VisScreenMask_cl(); 
	m_ptrLifeBar = new VisScreenMask_cl(); 
	m_ptrBossHead =new VisScreenMask_cl(); 

	float yBar = (Vision::Video.GetYRes() / 2) - (BAR_HEIGHT / 2);
	float xBar = Vision::Video.GetXRes() - BAR_WIDTH - BAR_OFFSET;
	
	m_fLifeUnitHeight = (BAR_HEIGHT - (BAR_OFFSET * 2)) / ShogunHitsRequired; //get lifeleft heigth

	LoadImg(m_ptrBossHead,"Textures\\GUIBosses\\Boss_shogunhead.png", Vision::Video.GetXRes() - 90, yBar - 80, 999, VColorRef(255,255,255,255), 90,100);
	LoadImg(m_ptrLifeBarLeft,"Textures\\GUIBosses\\Boss_hd_grey.png", xBar, yBar + BAR_INIHEIGHT, 1000, VColorRef(255,0,0,175), BAR_WIDTH, m_fLifeUnitHeight *  ShogunHitsRequired );
	LoadImg(m_ptrLifeBar,"Textures\\GUIBosses\\Boss_hd.png", xBar, yBar, 1001, VColorRef(255,255,255),BAR_WIDTH, BAR_HEIGHT);

	m_ptrLifeBar->SetVisible(false);
	m_ptrLifeBarLeft->SetVisible(false);
	m_ptrBossHead->SetVisible(false);
}

void CmpShogunBehaviour::UpdateGUI()
{	
	if (m_ptrLifeBarLeft)
		m_ptrLifeBarLeft->SetTargetSize(BAR_WIDTH, m_fLifeUnitHeight * m_iCurrentLifes);

	VColorRef barColor;
	int alpha = 255;

	if (m_eCurrentStage == eTranslationSimple)
	{	
		switch (m_iCurrentStageHits)
		{
			case 0: barColor = VColorRef(71,131,45,alpha);	break; //verde
			case 1: barColor = VColorRef(64,96,148,alpha);	break; //azul
			case 2: barColor = VColorRef(255,255,63,alpha);	break; //amarilla
		}
	}

	if (m_eCurrentStage == eTranslationCivil)
	{
		barColor = VColorRef(255,221,63,alpha); //naranja
	}

	if (m_eCurrentStage == eTranslationCrew)
	{
		switch (m_iCurrentStageHits)
		{
			case 0: barColor = VColorRef(220,157,80,alpha); 	break; //naranja mas fuerte
			case 1: barColor = VColorRef(202,121,22,alpha);	break; //naranja - rojo
			case 2: barColor = VColorRef(211,65,0,alpha);	break; // rojo
		}
	}

	m_ptrLifeBarLeft->SetColor(barColor);
}

void CmpShogunBehaviour::ParseData (VString data, float* ptr)
{	
	int numEle = 0;
	char* elements = strtok(data.GetChar(),",");

	while (elements)
	{
		ptr[numEle] = static_cast<float>(atof(elements));
		numEle++;
		elements = strtok(NULL,",");
	}	
}

void CmpShogunBehaviour::ChangeStaticCiviliansAnimSpeed(const char* objectKey)
{
	DynArray_cl<VisBaseEntity_cl*> civilians;

	//get civilians
	Vision::Game.SearchEntity(objectKey,&civilians);

	for (unsigned int i = 0; i < civilians.GetValidSize(); i++)
	{
		VisBaseEntity_cl* civil = civilians[i];
		if (civil->Components().GetComponentOfType<VTransitionStateMachine>())
		{
			float rand = Vision::Game.GetFloatRand();			

			if (rand < 0.33f)
				civil->Components().GetComponentOfType<VTransitionStateMachine>()->GetActiveControl()->SetSpeed(0.5f);
			else if (rand > 0.33f && rand < 0.66f)
				civil->Components().GetComponentOfType<VTransitionStateMachine>()->GetActiveControl()->SetSpeed(0.75f);
			else
				civil->Components().GetComponentOfType<VTransitionStateMachine>()->GetActiveControl()->SetSpeed(1);
		}

	}
}

void CmpShogunBehaviour::InitShogunBehaviour()
{			
	m_pTransNumber = new float[STAGE_NUMBER];
	m_pTransTime = new float[STAGE_NUMBER];
	m_pVisibleTime = new float[STAGE_NUMBER];

	//parse variables from vForge data
	ParseData(TranslationNumber, m_pTransNumber);
	ParseData(TranslationSpeed, m_pTransTime);
	ParseData(VisibleTime, m_pVisibleTime);

	//create transition state machine	
	VTransitionTable *pTable = VTransitionManager::GlobalManager().LoadTransitionTable(m_pOwner->GetMesh(),"Models\\Ch_Shogun.vTransition" );

	if (pTable)
	{
		m_pAnimSM = new VTransitionStateMachine();	
		m_pAnimSM->Init(pTable,true);	
		m_pOwner->AddComponent(m_pAnimSM);
		m_pAnimSM = m_pOwner->Components().GetComponentOfType<VTransitionStateMachine>();
	}
	
	//create enemy properties
	m_pOwner->AddComponent(new CmpEnemyProperties());
	m_pCmpProperties = m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>();

	if (m_pCmpProperties)
	{
		m_pCmpProperties->SetCanBeSliced(false);
		m_pCmpProperties->SetLifePoints(ShogunHitsRequired);
		m_pCmpProperties->SetEnemyType(EBoss);
		m_pCmpProperties->SetScore(0);
	}

	m_pCmpHighlight = NULL;

	//create highlight component
	m_pCmpHighlight = new CmpHighlight();
	m_pOwner->AddComponent(m_pCmpHighlight);	
	m_pCmpHighlight = m_pOwner->Components().GetComponentOfType<CmpHighlight>();

	//get the bone proxies to attach civil
	m_pProxyLefthand = new VSkeletalBoneProxyObject();
	m_pProxyRigthhand = new VSkeletalBoneProxyObject();
	
	m_pProxyLefthand->AttachToEntityBone(m_pOwner,SHOGUN_LEFTHAND_BONE);
	m_pProxyRigthhand->AttachToEntityBone(m_pOwner,SHOGUN_RIGHTHAND_BONE);

	//search scene effect
	m_pEffect = NULL;
	m_pEffect = VisParticleGroupManager_cl::GlobalManager().Instances().FindByKey("explosion");
	if (m_pEffect)	
	{	m_pEffect->SetRemoveWhenFinished(false);
		m_pEffect->SetPause(true);
	}
	
	//add shogun as sliceable
	reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( m_pOwner );

	//change anim speed randomly
	ChangeStaticCiviliansAnimSpeed("crew");
	ChangeStaticCiviliansAnimSpeed("crewstatic");

	//ini vars
	m_pCurrentPath = NULL;
	m_bPlayerDamaged = false;
	m_bSimpleAttack = false;
	m_bAlreadyDead = false;
	m_iCurrentStageHits = 0;
	m_iCurrentLifes = ShogunHitsRequired;
	m_iCurrentPass = 0;	

	m_pCivil = NULL;
	m_pFXResource = NULL;
	m_pExplosionFX = NULL;

	VisBaseEntity_cl* pCutline = Vision::Game.SearchEntity(CUTLINE_KEY);
	VisBaseEntity_cl* pCamera = Vision::Game.SearchEntity(CAMPOS_ENT_KEY);

	m_bCivilRunningOut = false;

	if (pCutline && pCamera)
	{
		m_fCutLineDistance = pCutline->GetPosition().getDistanceToSquared(pCamera->GetPosition());
	}
	else
		m_fCutLineDistance = 1000000;

}


void CmpShogunBehaviour::ChangeShogunState(eEnemyState newState, bool DoExitActions)
{	
	if (m_eCurrentState != eInitalState && DoExitActions)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();			break;		
			case eTranslating:	OnExitTranslation();	break;
			case eResting:		OnExitResting();		break;
			case eAttacking:	OnExitAttack();			break;		
			case eRunningBack:	OnExitRunback();		break;
			case eDamage:		OnExitDamaged();		break;
			case eJumping:		OnExitJumping();		break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;		
		case eDamage:		OnEnterDamaged();		break;
		case eTranslating:	OnEnterTranslation();	break;
		case eResting:		OnEnterResting();		break;
		case eAttacking:	OnEnterAttack();		break;		
		case eDying:		OnEnterDying();			break;
		case eRunningBack:	OnEnterRunBack();		break;
		case eJumping:		OnEnterJumping();		break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpShogunBehaviour::LoadImg(VisScreenMaskPtr ptrImg, char* filename, float xpos, float ypos, int zpos, VColorRef color, float width, float height)
{	
	ptrImg->LoadFromFile(filename);
	ptrImg->SetTransparency(VIS_TRANSP_ALPHA);	
	ptrImg->SetPos(xpos, ypos);
	ptrImg->SetTargetSize(width,height);	
	ptrImg->SetZVal(0.0f);
	ptrImg->SetOrder(zpos);
	ptrImg->SetVisible(true);
}

bool CmpShogunBehaviour::PerformAttack(float animTimeFinish, float animTimeDamage)
{
	//damage player before ends attack animation (looks better)
	if (m_fStateTimer >= animTimeDamage && !m_bPlayerDamaged)	
	{	
		SoundManager::Instance()->PlaySound(eShogunPunch,m_pOwner->GetPosition(),false);
		PlayerStats::Instance()->DamagePlayer();
		m_bPlayerDamaged =	true;
	}

	if (m_fStateTimer > animTimeFinish)	
		return true;
	else
		return false;
}


void CmpShogunBehaviour::EnableShogunAndCivilian(bool visible, bool sliceable)
{
	bool sliceShogun, sliceCivil;

	sliceShogun = sliceCivil = sliceable;

	VisBaseEntity_cl* pCamera = Vision::Game.SearchEntity(CAMPOS_ENT_KEY);
	float distance = m_pOwner->GetPosition().getDistanceToSquared(pCamera->GetPosition());

	if (distance > m_fCutLineDistance)
	{	
		sliceShogun = false;

		if (m_eCurrentStage == eTranslationCivil && sliceable)	sliceCivil = true;
	}
	
	//set shogun and civilian visibles and attackables	
	m_pOwner->SetVisibleBitmask(static_cast<int>(visible));
	m_pCmpProperties->SetCanBeSliced(sliceShogun);

	//enable/disable shadow
	if (m_pOwner->Components().GetComponentOfType<VBlobShadow>())
		m_pOwner->Components().GetComponentOfType<VBlobShadow>()->SetEnabled(visible);

	//special civil casses 
	if (m_eCurrentStage != eTranslationSimple)
	{
		if (m_pCivil)	
		{	
			if (m_bCivilDeadCurrentStage)
			{	m_pCivil->SetVisibleBitmask(0);				
				m_pCivil->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(false);
			}
			else
			{	m_pCivil->SetVisibleBitmask(static_cast<int>(visible));				
				m_pCivil->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(sliceCivil);
			}

			if (m_bCivilRunningOut)
				m_pCivil->SetVisibleBitmask(1);
		}	
	}

}

void CmpShogunBehaviour::onFrameUpdate()
{			
	if (!MyGameManager::GlobalManager().IsPlayingTheGame())	return;
	if (!HasEnemyProperties() || !HasAnimationSM())	return;

	if (PlayerStats::Instance()->GetPlayerLifes() <= 0 && !m_bBossSuccess)	
	{	
		m_bBossSuccess = true;		
		m_pMusic->Stop();
		m_pMusic->DisposeObject();		
	}

	if (m_bBossSuccess)	return;

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();
	
	CheckShogunLifes();	
	CheckCivilHits();	
	UpdateGUI();

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();			break;		
		case eTranslating:	OnTranslating();	break;
		case eResting:		OnResting();		break;
		case eAttacking:	OnAttack();			break;
		case eDying:		OnDying();			break;
		case eRunningBack:	OnRunBack();		break;
		case eDamage:		OnDamaged();		break;
		case eJumping:		OnJumping();		break;		
	}
	
}


void CmpShogunBehaviour::ManageStageChange()
{
	//shogun has been damaged required times in the current stage so go to the next stage
	if (m_iCurrentStageHits == ShogunStageLifes )
	{	
		//next stage
		m_eCurrentStage = static_cast<eShogunStages>(m_eCurrentStage + 1);

		//reset translations, hits and life values
		m_iCurrentPass = 0;
		m_iCurrentStageHits = 0;
		m_pCmpProperties->SetLifePoints(ShogunHitsRequired);
		m_iCurrentLifes = ShogunHitsRequired;

		//events post stage changing
		if (m_eCurrentStage == eTranslationCivil)	EnterCivilStage();

		if (m_eCurrentStage == eTranslationCrew)	EnterCrewStage();
		
	}
}

void CmpShogunBehaviour::ConfigureCivilProperties(VisBaseEntity_cl* pCivil, int lifes)
{
	if (pCivil)
	{
		CmpEnemyProperties* pCmp = pCivil->Components().GetComponentOfType<CmpEnemyProperties>();
		
		if (!pCmp)
		{	pCivil->AddComponent(new CmpEnemyProperties());
			pCmp = pCivil->Components().GetComponentOfType<CmpEnemyProperties>();

			pCmp->SetEnemyType(ECivilian);
			pCmp->SetCanBeSliced(true);
			pCmp->SetLifePoints(lifes);
			pCmp->GetModelPieces()[eTopSide] = "Models//Ch_civil_up.model";
			pCmp->GetModelPieces()[eBottomSide] = "Models//Ch_civil_down.model";
			pCmp->GetModelPieces()[eLeftSide] = "Models//Ch_civil_left.model";
			pCmp->GetModelPieces()[eRightSide] = "Models//Ch_civil_right.model";
			pCmp->GetModelPieces()[eUpRightSide] = "Models//Ch_civil_upright.model";
			pCmp->GetModelPieces()[eUpLeftSide] = "Models//Ch_civil_upleft.model";
			pCmp->GetModelPieces()[eDownLeftSide] = "Models//Ch_civil_downleft.model";
			pCmp->GetModelPieces()[eDownRightSide] = "Models//Ch_civil_downright.model";
			pCmp->SetScore(0);

		}
	}
}

void CmpShogunBehaviour::EnterCivilStage()
{
	//search civil on scene
	m_pCivil = Vision::Game.SearchEntity(civilKey);

	//yes, it's to bad but we have no time
	ShogunStageLifes = 1;
	
	if (m_pCivil)
	{
		reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( m_pCivil );
		
		//set civil prop
		ConfigureCivilProperties(m_pCivil, 100);
		m_iCivilLifes = 100;			

		//position it
		PositionCivil(m_vTargetPoint);
		
	}	
}

void CmpShogunBehaviour::EnterCrewStage()
{
	//yes, it's to bad but we have no time
	ShogunStageLifes = 3;

	if (m_pCivil)
		reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->UntrackSlicedEntity ( m_pCivil );

	DynArray_cl<VisBaseEntity_cl*> civilians;

	//get civilians
	Vision::Game.SearchEntity("crew",&civilians);

	for (unsigned int i = 0; i < civilians.GetValidSize(); i++)
	{
		VisBaseEntity_cl* civil = civilians[i];

		//enable path follow
		if (civil->Components().GetComponentOfType<CmpFollowPath>())
			civil->Components().GetComponentOfType<CmpFollowPath>()->SetFollowPath(true);
		
		//configure prop
		ConfigureCivilProperties(civil, 1);
		reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( civil );
	}

	EnemiesFactory::Instance()->RemoveFromSlicedEntities(m_pCivil);
	m_pCivil = NULL;		

}

void CmpShogunBehaviour::CheckCivilHits()
{
	if (m_eCurrentStage != eTranslationSimple)
	{
		if (m_pCivil)
		{
			//civilian hit
			CmpEnemyProperties* pProp = m_pCivil->Components().GetComponentOfType<CmpEnemyProperties>();
			int propLifes = pProp->GetLifePoints();

			if (propLifes != m_iCivilLifes)
			{
				m_iCivilLifes = propLifes;
				m_pCivil->SetVisibleBitmask(0);
				pProp->StaticSlice(eHorizontalToLeft);
				PlayerStats::Instance()->DamagePlayer();
				pProp->SetCanBeSliced(false);				
				m_pCmpProperties->SetCanBeSliced(false); //dont want to hit shogun too
				m_bCivilDeadCurrentStage = true;				
			}
		}
	}

}

void CmpShogunBehaviour::DebugInfo()
{/*
	std::ostringstream pass, bosslifes, projectiles, sliceable;
	pass << "Pass / Stage: " << m_iCurrentPass << " / " << m_eCurrentStage;	
	bosslifes << "Current Lifes: " << m_iCurrentStageHits;
	if (HasEnemyProperties())
	{	
		sliceable << "Sliceable: " << m_pCmpProperties->CanBeSliced();
		projectiles << "Prop Lifes: " << m_pCmpProperties->GetLifePoints();
	}

	Vision::Message.Print(1, 950, 50, pass.str().c_str());
	Vision::Message.Print(1, 950, 100, projectiles.str().c_str());
	Vision::Message.Print(1, 950, 150, bosslifes.str().c_str());
	Vision::Message.Print(1, 950, 20, sliceable.str().c_str());*/
}

void CmpShogunBehaviour::CheckShogunLifes()
{
	//shogun has been hit
	if (m_pCmpProperties->GetLifePoints() != m_iCurrentLifes)
	{
		//show damage effect
		if (m_pCmpHighlight)
			m_pCmpHighlight->Flash(VColorRef(255,0,0), 1.0f);

		//update lifes
		m_iCurrentLifes = m_pCmpProperties->GetLifePoints();

		//we can hit one time only
		m_pCmpProperties->SetCanBeSliced(false);
		
		if (m_iCurrentLifes != 0)
			ChangeShogunState(eTranslating);

	}

	//shogut has been damaged
	if (m_iCurrentLifes == 0)
	{										
		//true death
		if (m_eCurrentStage == eTranslationCrew && m_iCurrentStageHits == ShogunHitsRequired - 1)
		{	
			if (!m_bAlreadyDead)	
				ChangeShogunState(eDying, false);		
		}
		else
			ChangeShogunState(eDamage, false);
	}
	
	
}

void CmpShogunBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpShogunBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpShogunBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

// --------- CREATION STATE MANAGEMENT ----------------
void CmpShogunBehaviour::OnEnterCreate()
{
	m_eCurrentStage = eTranslationSimple;
	//m_eCurrentStage = eTranslationCrew;
	
	m_pAnimSM->SetState(SHOGUN_WALK);
}

void CmpShogunBehaviour::OnCreate()
{	
	if (m_fStateTimer < 2 * InitialSequenceTime / 3)
	{
		//walk forward
		m_pOwner->IncPosition(hkvVec3(0,-INISEQ_SPEED,0) * Vision::GetTimer()->GetTimeDifference());		
	}
	else if (m_fStateTimer < InitialSequenceTime)
	{
		//idle for a while
		m_pAnimSM->SetState(SHOGUN_IDLE);		
	}
	else
		ChangeShogunState(eJumping);		
}

void CmpShogunBehaviour::OnExitCreate()
{

} 

// --------- DAMAGE STATE MANAGEMENT ----------------
void CmpShogunBehaviour::OnEnterDamaged()
{	
	if (m_eCurrentStage == eTranslationCivil)
	{
		//civil still alive, he goes along the path
		if (!m_bCivilDeadCurrentStage)
		{
			m_pCivil->DetachFromParent();
			m_pCivil->AddComponent( new CmpFollowPath());

			CmpFollowPath* cmpPath = m_pCivil->Components().GetComponentOfType<CmpFollowPath>();
			VisPath_cl* path = Vision::Game.SearchPath(CIVIL_EXITPATH);

			if (cmpPath && path)
			{
				VisPathNode_cl* node = path->GetPathNode(0,true);
				node->SetPosition(m_pCivil->GetPosition());
				cmpPath->SetPathToFollow(path, 2, false);	
				m_bCivilRunningOut = true;
			}
		}
	}
	
	switch (m_iCurrentStageHits)	
	{
		case 0:	m_pAnimSM->SetState(SHOGUN_DAMAGE1);	break;
		case 1:	m_pAnimSM->SetState(SHOGUN_DAMAGE2);	break;
		case 2:	m_pAnimSM->SetState(SHOGUN_DAMAGE3);	break;
	}
	
	m_fCurrentAnimLength = m_fCurrentAnimLength = m_pAnimSM->GetActiveState()->GetLength();	

	SoundManager::Instance()->PlaySound(eShogunHit,m_pOwner->GetPosition(),false);

	//reset lifes	
	m_pCmpProperties->SetLifePoints(ShogunHitsRequired);
	m_iCurrentLifes = ShogunHitsRequired;
	m_iCurrentPass = 0;
	m_iCurrentStageHits++;

}

void CmpShogunBehaviour::OnDamaged()
{	
	EnableShogunAndCivilian(true, false);

	//when attack anim finish, wait half a second before change state
	if (m_fStateTimer > m_fCurrentAnimLength)
	{
		m_pAnimSM->SetState(SHOGUN_IDLE);				
	}

	if (m_fStateTimer > m_fCurrentAnimLength + 0.5f)
	{		
		ChangeShogunState(eRunningBack);
	}
	
}

void CmpShogunBehaviour::OnExitDamaged()
{
	ShowExplosion();
}

// --------- DEATH STATE MANAGEMENT ----------------
void CmpShogunBehaviour::OnEnterDying()
{
	//add score
	PlayerStats::Instance()->AddScore(ShogunScore);

	//stop music
	if (m_pMusic)
	{	
		m_pMusic->Stop();
		m_pMusic->DisposeObject();		
	}

	//enable slow motion
	m_pAnimSM->SetState(SHOGUN_DEATH);

	//get state time and down speed
	m_fCurrentAnimLength = (m_pAnimSM->GetActiveState()->GetLength()) + TIME_AFTERDEATH;	

	if (m_pOwner->Components().GetComponentOfType<VBlobShadow>())
		m_pOwner->Components().GetComponentOfType<VBlobShadow>()->SetEnabled(false);

	m_bAlreadyDead = true;

	//trigger shogun sound
	SoundManager::Instance()->PlaySound(eShogunDeath,m_pOwner->GetPosition(),false);

}

void CmpShogunBehaviour::OnDying()
{	
	
	//trigger fall sound (x2 because anim speed is 0.5)
	if (m_fStateTimer > (FALLDOWN_TIME))
	{
		if (!m_bPlayerDamaged)
		{	SoundManager::Instance()->PlaySound(eShogunFall,m_pOwner->GetPosition(),false);
			m_bPlayerDamaged = true;
		}
	}
	
	if (m_fStateTimer > (m_pAnimSM->GetActiveState()->GetLength() )  && m_fStateTimer < m_fCurrentAnimLength)
	{
		m_pAnimSM->GetActiveControl()->Pause();		
	}
	
	//wait few seconds before change scene
	if (m_fStateTimer > m_fCurrentAnimLength)
	{
		if ( !m_bBossSuccess )
		{
			m_bBossSuccess = true;
			PlayerStats::Instance()->SetBossFinish(true);
			//MyGameManager::GlobalManager().OnEndOfLevelReached();
		}
	}
}

// --------- ATTACK STATE MANAGEMENT ----------------
void CmpShogunBehaviour::OnEnterAttack()
{
	EnableShogunAndCivilian(true, false);	
	
	if (m_pCivil)	m_pCivil->SetVisibleBitmask(0);

	m_bSimpleAttack ? m_bSimpleAttack = false : m_bSimpleAttack = true;
		
	m_pAnimSM->SetState(SHOGUN_ATTACK);

	if (!m_bSimpleAttack)
		m_pAnimSM->GetActiveControl()->SetCurrentSequenceTime(ATTACK_TRANSITIONTIME);
	
}

void CmpShogunBehaviour::OnAttack()
{
	bool changeToIdle = false;
	float animTimeFinish, animTimeDamage;
	float animLength = m_pAnimSM->GetActiveState()->GetLength();
	VString stateName = m_pAnimSM->GetActiveState()->GetName();

	//get correct animation time depending on attack type
	if (m_bSimpleAttack)
	{
		animTimeDamage = ATTACK_TRANSITIONTIME - (ATTACK_IMPACTTIME * 2);
		animTimeFinish = ATTACK_TRANSITIONTIME;
	}
	else
	{
		animTimeDamage = animLength - ATTACK_TRANSITIONTIME - ATTACK_IMPACTTIME;
		animTimeFinish = animLength - ATTACK_TRANSITIONTIME;
	}

	//attack and check if shogun has to rest
	if (PerformAttack(animTimeFinish, animTimeDamage))
	{	
		m_pAnimSM->SetState(SHOGUN_IDLE);
	}

	//if shogun is resting, wait 1 sec to run back
	if (stateName == SHOGUN_IDLE)
	{
		if (m_bSimpleAttack)
		{
			if (m_fStateTimer >= ATTACK_TRANSITIONTIME + 1.0f)	ChangeShogunState(eRunningBack);
		}
		else
		{
			if (m_fStateTimer >= animLength - ATTACK_TRANSITIONTIME + 1.0f)	ChangeShogunState(eRunningBack);
		}
	}	
}

void CmpShogunBehaviour::OnExitAttack()
{
	m_bPlayerDamaged = false;
	ShowExplosion();
}

// --------- REST STATE MANAGEMENT ----------------
void CmpShogunBehaviour::OnEnterResting()
{
	//get visible time for current stage	
	float time = Vision::Game.GetFloatRandNeg();

	if (time < -hkvMath::Abs(RandomTimeRange))	time = -RandomTimeRange;
	if (time > hkvMath::Abs(RandomTimeRange))	time = hkvMath::Abs(RandomTimeRange);

	m_fCurrentVisibleTime = m_pVisibleTime[m_eCurrentStage] + time;	
	
	if (m_iCurrentPass == 0)	
	{	
		m_fCurrentVisibleTime = IDLETIME_AFTERHIT;
		//enable animation	
		m_pAnimSM->SetState(SHOGUN_IDLE);	
	}
	
	bool attackable = !(m_iCurrentPass == 0 || m_iCurrentPass == m_pTransNumber[m_eCurrentStage]);

	EnableShogunAndCivilian(true,attackable);

}


void CmpShogunBehaviour::OnResting()
{	
	//at start blend to pose
	if (m_iCurrentPass == 0 && m_fStateTimer > (2 * m_fCurrentVisibleTime / 3) && m_fStateTimer < m_fCurrentVisibleTime)	
	{	
		m_pAnimSM->SetState(SHOGUN_POSE);	
	}

	//change to run logic state
	if (m_fStateTimer >= m_fCurrentVisibleTime)
	{
		m_pCmpProperties->SetCanBeSliced(false);
		ChangeShogunState(eTranslating);	
	}
}

void CmpShogunBehaviour::OnExitResting()
{	
	ShowExplosion();
	EnableShogunAndCivilian(false, false);
}

void CmpShogunBehaviour::ShowExplosion()
{
	//show explosion
	if (m_pEffect)
	{	m_pEffect->SetVisible(true);
		m_pEffect->Restart();
	}
}

void CmpShogunBehaviour::PositionCivil(hkvVec3 vTarget)
{
	if (m_pCivil)
	{		
		m_pCivil->DetachFromParent();

		if (vTarget.x > 0)
		{
			m_pCivil->AttachToParent(m_pProxyLefthand);		
			m_pCivil->ResetLocalTransformation();
			m_pCivil->SetLocalOrientation(m_pCivil->GetLocalOrientation() + hkvVec3(0,-60,40));
			m_pCivil->SetLocalPosition(m_pCivil->GetLocalPosition() + hkvVec3(100,0,-50));	
		}
		else
		{
			m_pCivil->AttachToParent(m_pProxyRigthhand);		
			m_pCivil->ResetLocalTransformation();
			m_pCivil->SetLocalOrientation(m_pCivil->GetLocalOrientation() + hkvVec3(0,-40,40));
			m_pCivil->SetLocalPosition(m_pCivil->GetLocalPosition() + hkvVec3(100,0,-50));	
		}			
		
	}
}

void CmpShogunBehaviour::TranslateShogun(hkvVec3 vTarget)
{		
	PositionCivil(vTarget);
	m_pOwner->SetPosition(vTarget);
	
}

// --------- TRANSLATION STATE MANAGEMENT ----------------
void CmpShogunBehaviour::OnEnterTranslation()
{		
	bool lastTrans = false;	

	//if its the last translation, target point is player
	if (m_pTransNumber[m_eCurrentStage] == m_iCurrentPass)	lastTrans = true;
	
	CalculateTraslationPoint(lastTrans, (m_iCurrentPass == 0));

	//get speed for this stage
	m_fTranslationTime = m_pTransTime[m_eCurrentStage];	

	float rand = Vision::Game.GetFloatRand();
			
	if (rand < 0.5f)
		SoundManager::Instance()->PlaySound(eShogunTeleport,m_pOwner->GetPosition(),false);
	else
		SoundManager::Instance()->PlaySound(eShogunTeleport_alt,m_pOwner->GetPosition(),false);
}

void CmpShogunBehaviour::OnTranslating()
{	

	if (m_fStateTimer > m_fTranslationTime)
	{
		//move shogun to new point
		TranslateShogun(m_vTargetPoint);		

		if (m_pTransNumber[m_eCurrentStage] == m_iCurrentPass)
			ChangeShogunState(eAttacking);
		else
			ChangeShogunState(eResting);
	}
}


void CmpShogunBehaviour::OnExitTranslation()
{
	m_vCurrentPoint = m_vTargetPoint;
	m_iCurrentPass++;

	//reset translations and shogun lifes
	if (m_iCurrentPass > m_pTransNumber[m_eCurrentStage])	
	{	
		m_iCurrentPass = 0;
		m_pCmpProperties->SetLifePoints(ShogunHitsRequired);
		m_iCurrentLifes = ShogunHitsRequired;
		
	}
	else
	{
		if (m_vTargetPoint.x > 0)	
			m_pAnimSM->SetState(SHOGUN_POSEMIRROR);
		else
			m_pAnimSM->SetState(SHOGUN_POSE);	
	}

}

// --------- TRANSLATION STATE MANAGEMENT ----------------
void CmpShogunBehaviour::OnEnterRunBack()
{					
	EnableShogunAndCivilian(false, false);
	TranslateShogun(m_vInitialPoint);	
	m_fTranslationTime = 2.0f;	
}

void CmpShogunBehaviour::OnRunBack()
{
	if (m_fStateTimer > m_fTranslationTime)
	{
		ChangeShogunState(eResting);
	}
}

void CmpShogunBehaviour::OnExitRunback()
{
	ManageStageChange();
	m_vCurrentPoint = m_pOwner->GetPosition();	
	
	//civil is alive again :P
	m_bCivilDeadCurrentStage = false;
	
}

// --------- JUMPING STATE MANAGEMENT ----------------
void CmpShogunBehaviour::OnEnterJumping()
{							
	m_pAnimSM->SetState(SHOGUN_JUMP);
	m_fCurrentAnimLength = m_pAnimSM->GetActiveState()->GetLength();

	//m_vCurrentPoint = m_pOwner->GetPosition();
	m_vCurrentPoint = hkvVec3(0,5650,0);
	m_vInitialPoint = m_vCurrentPoint;
}

void CmpShogunBehaviour::OnJumping()
{
	if (m_fStateTimer >JUMP_UPTIME && m_fStateTimer < JUMP_DOWNTIME)
	{
		m_pOwner->IncPosition(0,-JUMPSPEED * Vision::GetTimer()->GetTimeDifference(),0);
	}

	if (m_fStateTimer > JUMP_DOWNTIME && !m_pFXResource)
	{
		FallDownEffects();		
	}

	if (m_fStateTimer > m_fCurrentAnimLength)
	{
		ChangeShogunState(eResting);
	}
}

void CmpShogunBehaviour::FallDownEffects()
{	
	SoundManager::Instance()->PlaySound(eShogunFall,m_pOwner->GetPosition(),false,30);

	//camera shake
	VisBaseEntity_cl* pCamera = Vision::Game.SearchEntity( CAMPOS_ENT_KEY );

	if (pCamera)
	{
		CmpCameraEffects* cmpFX = pCamera->Components().GetComponentOfType<CmpCameraEffects>();

		if (cmpFX)	cmpFX->CameraShake();							
	}	
	
	//dust effect
	hkvVec3 pos = m_pOwner->GetPosition() + hkvVec3(0,-100,0);

	//load particles
	if (!m_pFXResource)
		m_pFXResource = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//DavidExplosion.xml");		

	if (!m_pFXResource) return;

	//create new effect
	if (!m_pExplosionFX)
	{	
		m_pExplosionFX = m_pFXResource->CreateParticleEffectInstance(pos, m_pOwner->GetOrientation());	
		m_pExplosionFX->SetRemoveWhenFinished(true);		
	}

}

void CmpShogunBehaviour::OnExitJumping()
{
	//ShowExplosion();	

	EnableShogunAndCivilian(false, false);
	m_pAnimSM->SetState(SHOGUN_POSE);
	
	m_pMusic = SoundManager::Instance()->PlaySoundReturn(eShogunMusic,m_pOwner->GetPosition(),true);
	ShowLifeBar(true);
	
}

void CmpShogunBehaviour::CalculateTraslationPoint(bool lastTrans, bool firstTrans)
{	
	float y,x;
		
	//if it's the last translation, the target point is in front of the camera
	if (lastTrans)
	{
		VisBaseEntity_cl* cam = Vision::Game.SearchEntity(CAMPOS_ENT_KEY);
		 
		if (cam)
		{	
			m_vTargetPoint = cam->GetPosition() + hkvVec3(0,AttackDistanceFromCamera,0);
			m_vTargetPoint.z = 0;
		}
		return;
	}

	//get x range per zone
	float XRange = (hkvMath::Abs(LeftX_TranslationRange) + hkvMath::Abs(RightX_TranslationRange)) / ZONEPOINTS;

	//get random zone
	int zone = CutshumotoUtilities::GetRandomInt(1,ZONEPOINTS);

	if (hkvMath::Abs(zone - m_iLastGeneratedZone) <= 1)
	{
		switch (zone)
		{
			case 1: zone = 5;	break;
			case 2: zone = 5;	break;
			case 3: zone = 1;	break;
			case 4: zone = 1;	break;
			case 5: zone = 1;	break;
		}
	}
	//get the correct x pos
	x = LeftX_TranslationRange + (zone * XRange);		

	float YRange = (MaxY_TranslationRange - MinY_TranslationRange) / (m_pTransNumber[m_eCurrentStage] + 1);	

	y = m_vCurrentPoint.y - static_cast<float>(CutshumotoUtilities::GetRandomInt(static_cast<int>(YRange - 50) , static_cast<int>(YRange)));

	m_vTargetPoint = hkvVec3(x,y,0);

	m_iLastGeneratedZone = zone;
}

void CmpShogunBehaviour::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPSHOGUNBEHAVIOUR_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPSHOGUNBEHAVIOUR_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> ShogunScore;
		ar >> ShogunStageLifes;
		ar >> ShogunHitsRequired;
		ar >> TranslationSpeed;
		ar >> TranslationNumber;
		ar >> VisibleTime;
		ar >> LeftX_TranslationRange;
		ar >> RightX_TranslationRange;
		ar >> MaxY_TranslationRange;
		ar >> MinY_TranslationRange;
		ar >> InitialSequenceTime;
		ar >> civilKey;
	} 
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << ShogunScore;
		ar << ShogunStageLifes;
		ar << ShogunHitsRequired;
		ar << TranslationSpeed;
		ar << TranslationNumber;
		ar << VisibleTime;
		ar << LeftX_TranslationRange;
		ar << RightX_TranslationRange;
		ar << MaxY_TranslationRange;
		ar << MinY_TranslationRange;
		ar << InitialSequenceTime;
		ar << civilKey;

	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpShogunBehaviour,IVObjectComponent, "Shogun Boss Behaviour", VVARIABLELIST_FLAGS_NONE, "Shogun Boss Behaviour" )

	DEFINE_VAR_INT		(CmpShogunBehaviour, ShogunScore, "Shogun Score", "5000", 0, 0);
	DEFINE_VAR_INT		(CmpShogunBehaviour, ShogunStageLifes, "Shogun Lifes each stage", "3", 0, 0);
	DEFINE_VAR_INT		(CmpShogunBehaviour, ShogunHitsRequired, "hits required to damage", "3", 0, 0);
	DEFINE_VAR_FLOAT	(CmpShogunBehaviour, InitialSequenceTime, "Initial Entrance Time", "5",0,0);	
	DEFINE_VAR_VSTRING	(CmpShogunBehaviour, civilKey, "Civil entity key","civil",100,0,0);
	DEFINE_VAR_VSTRING	(CmpShogunBehaviour, TranslationSpeed, "Translation Speed (stage1, stage2, stage3)","200,300,400",100,0,0);
	DEFINE_VAR_VSTRING	(CmpShogunBehaviour, TranslationNumber, "Translation Number (stage1, stage2, stage3)","4,5,6",100,0,0);
	DEFINE_VAR_VSTRING	(CmpShogunBehaviour, VisibleTime, "Visible Time (stage1, stage2, stage3)","1,0.8,0.5",100,0,0);
	DEFINE_VAR_FLOAT	(CmpShogunBehaviour, RandomTimeRange, "Random +- range added to visible time", "0.25",0,0);	
	DEFINE_VAR_FLOAT	(CmpShogunBehaviour, AttackDistanceFromCamera, "Distance from camera to perform shogun attack", "550",0,0);	
	DEFINE_VAR_FLOAT	(CmpShogunBehaviour, LeftX_TranslationRange, "left side Translation point X coord", "-570",0,0);	
	DEFINE_VAR_FLOAT	(CmpShogunBehaviour, RightX_TranslationRange, "right side Translation point X coord", "570",0,0);	
	DEFINE_VAR_FLOAT	(CmpShogunBehaviour, MaxY_TranslationRange, "Max Y coord Translation point", "2000",0,0);	
	DEFINE_VAR_FLOAT	(CmpShogunBehaviour, MinY_TranslationRange, "Min T coord Translation point", "700",0,0);	
	

	

END_VAR_TABLE

//deprecated functions 
	/*bool CmpShogunBehaviour::AlignToScreen(bool front)
{
	//yaw is x coord
	hkvVec3 currentOrient = m_pOwner->GetOrientation();		
	float sign;

	front ? sign = 1 : sign= -1;

	if (currentOrient.x >= 0)
		m_pOwner->IncOrientation(-200 * sign * Vision::GetTimer()->GetTimeDifference(),0,0);
	else if (currentOrient.x < 0)
	{	
		m_pOwner->IncOrientation(200 * sign * Vision::GetTimer()->GetTimeDifference(),0,0);
	}
	
	if (front)
	{	if (hkvMath::Abs(currentOrient.x) < 10)
		{	
			m_pOwner->SetOrientation(0,0,0);
			return true;
		}			
	}
	else
	{
		if (hkvMath::Abs(currentOrient.x) > 170)
		{
			m_pOwner->SetOrientation(180,0,0);
			return true;
		}
	}

	return false;
}

void CmpShogunBehaviour::FollowPath(bool align)
{
	//if we have not path, move it using properties speed
	if (m_pCurrentPath)
	{			
		// Calculate current relative parameter value [0..1]
		float fCurrentParam = m_fStateTimer / m_fPathTime;

		if (fCurrentParam < 1)
		{
			// Evaluate current position on path
			hkvVec3 vPos;
			hkvVec3 vDir;

			//m_pCurrentPath->EvalPointSmooth(fCurrentParam, vPos, &vDir, NULL);
			m_pCurrentPath->EvalPoint(fCurrentParam, vPos, &vDir, NULL);

			m_pOwner->SetUseEulerAngles(true); 
			m_pOwner->SetPosition(vPos); 

			if (align)	AlignToTargetPoint();
		
		}
	}
}

void CmpShogunBehaviour::CreatePath()
{
	//create path
	if (!m_pCurrentPath)
	{	
		m_pCurrentPath = new VisPath_cl(3, FALSE, "shogunPath");	
		m_pCurrentPath->AddPathNode(new VisPathNode_cl(m_vCurrentPoint)); 
		m_pCurrentPath->AddPathNode(new VisPathNode_cl(m_vTargetPoint)); 		
	}
	else //modify nodes
	{
		VisPathNode_cl* nodeini = m_pCurrentPath->GetPathNode(0,true);
		nodeini->SetPosition(m_vCurrentPoint);

		VisPathNode_cl* nodend = m_pCurrentPath->GetPathNode(1,true);
		nodend->SetPosition(m_vTargetPoint);
	}
	
	//calculate path time depending shogun speed
	//m_fPathTime = m_pCurrentPath->GetLen() / m_fTranslationTime;
	m_fPathTime = m_pCurrentPath->GetLen() / 800;
}

void CmpShogunBehaviour::CheckImmunity()
{
	//if has immunity, check immune time
	if (m_bImmune)
	{
		m_pCmpProperties->SetCanBeSliced(false);	
		if (m_fImmuneTime <= 0 )
		{	
			m_bImmune = false;
			m_fImmuneTime = IMMUNE_TIME;	
			m_pCmpProperties->SetCanBeSliced(true);	
		}

		m_fImmuneTime-= Vision::GetTimer()->GetTimeDifference();
	}

	if (!m_bImmune && m_eCurrentState != eDying && m_iCurrentPass != 0)			
		m_pCmpProperties->SetCanBeSliced(true);			
	else	
		m_pCmpProperties->SetCanBeSliced(false);

}

void CmpShogunBehaviour::AlignToTargetPoint()
{
	hkvVec3 dire = m_pOwner->GetDirection();
	hkvVec3 dest = m_vTargetPoint;
	dest.normalizeIfNotZero();
	
	float speed;

	//hkvMath::Abs(dire.Dot(dest)) < 0.5f ? speed = FAST_ALIGN : speed = NORMAL_ALIGN;
	speed = (1 / hkvMath::Abs(dire.dot(dest))) * 7;		

	if (m_vTargetPoint.x < 0)
		dire-=dest * speed * Vision::GetTimer()->GetTimeDifference();					
	else		
		dire+=dest * speed * Vision::GetTimer()->GetTimeDifference();					

	dire.normalizeIfNotZero();
	m_pOwner->SetDirection(dire);
} */