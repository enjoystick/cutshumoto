//============================================================================================================
//  CmpShogunBehaviour Component
//	Author: David Escalona
//============================================================================================================
#ifndef CMPSHOGUNBEHAVIOUR_H_INCLUDED
#define CMPSHOGUNBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "CmpHighlight.h"
#include "GlobalTypes.h"
#include "Vision\Runtime\EnginePlugins\VisionEnginePlugin\Components\VSkeletalBoneProxy.hpp"
#include "Vision\Runtime\EnginePlugins\VisionEnginePlugin\Components\VFollowPathComponent.hpp"

// Versions
#define  CMPSHOGUNBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPSHOGUNBEHAVIOUR_CURRENT		1     // Current version


enum eShogunStages
{
	eTranslationSimple = 0,
	eTranslationCivil = 1,
	eTranslationCrew = 2
};

//============================================================================================================
//  CmpGhostBehaviour Class
//============================================================================================================
class CmpShogunBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpShogunBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpShogunBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP	CmpShogunBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpShogunBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onAfterSceneLoaded();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

	private:		
		void	InitShogunBehaviour();
		void	ChangeState(eEnemyState newState);
		void	CalculateTranslationsNumber();	
		void	ParseData (VString data, float* ptr);
		void	CalculateTraslationPoint(bool lastTrans = false, bool firstTrans = false);
		//void	FollowPath(bool align = true);
		//void	AlignToTargetPoint();
		//bool	AlignToScreen(bool front = true);
		//void	CreatePath();
		//void	CheckImmunity();
		void	ChangeStaticCiviliansAnimSpeed(const char* objectKey);
		bool	PerformAttack(float animTimeFinish, float animTimeDamage);
		void	TranslateShogun(hkvVec3 vTarget);
		void	CheckStageChange();
		void	EnableShogunAndCivilian(bool visible, bool sliceable);
		void	DebugInfo();
		void	ConfigureCivilProperties(VisBaseEntity_cl* pCivil, int lifes);
		void	EnterCivilStage();
		void	EnterCrewStage();
		void	CheckShogunLifes();
		void	CheckCivilHits();
		void	PositionCivil(hkvVec3 vTarget);
		void	ShowExplosion();
		void	FallDownEffects();
		void	InitGUI();
		void	UpdateGUI();
		void	ShowLifeBar(bool show);
		void	ManageStageChange();
		void	LoadImg(VisScreenMaskPtr ptrImg, char* filename, float xpos, float ypos, int zpos, VColorRef color, float width, float height);		
		void	ChangeShogunState(eEnemyState newState, bool DoExitActions = true);

		virtual void OnEnterCreate();		virtual void OnCreate();		virtual void OnExitCreate();
		virtual	void OnEnterAttack();		virtual	void OnAttack();		virtual void OnExitAttack();
				void OnEnterJumping();				void OnJumping();				void OnExitJumping();	
				void OnEnterRunBack();				void OnRunBack();				void OnExitRunback();
				void OnEnterDamaged();				void OnDamaged();				void OnExitDamaged();
				void OnEnterTranslation();			void OnTranslating();			void OnExitTranslation();
				void OnEnterResting();				void OnResting();				void OnExitResting();
				void OnEnterDying();				void OnDying();

	private:

		//----------- exposed vforge vars --------
		int		ShogunScore; 
		int		ShogunStageLifes;
		int		ShogunHitsRequired;
		VString	civilKey;
		VString	TranslationSpeed;
		VString	TranslationNumber;
		VString	VisibleTime;
		float	LeftX_TranslationRange;
		float	RightX_TranslationRange;
		float	MaxY_TranslationRange;
		float	MinY_TranslationRange;
		float	InitialSequenceTime;
		float	AttackDistanceFromCamera;
		float	RandomTimeRange;
		
		//----------- member vars ----------------
		VisScreenMaskPtr			m_ptrLifeBar;
		VisScreenMaskPtr			m_ptrBossHead;
		VisScreenMaskPtr			m_ptrLifeBarLeft;
		float*						m_pTransTime;
		float*						m_pTransNumber;
		float*						m_pVisibleTime;		
		VisPath_cl*					m_pCurrentPath;
		VisParticleEffect_cl*		m_pEffect;
		CmpHighlight*				m_pCmpHighlight;		
		float						m_fGUI_XPos;
		float						m_fPathTime;
		bool						m_bAlreadyDead;
		eShogunStages				m_eCurrentStage;
		int							m_iCurrentPass;				
		hkvVec3						m_vCurrentPoint;
		hkvVec3						m_vTargetPoint;
		hkvVec3						m_vInitialPoint;
		float						m_fTranslationTime;
		float						m_fCurrentTranslations;
		float						m_fCurrentVisibleTime;
		int							m_iCurrentLifes;
		float						m_fAlignSpeed;
		bool						m_bPlayerDamaged;
		bool						m_bSimpleAttack;
		int							m_iCurrentStageHits;
		float						m_fCurrentAnimLength;
		VisBaseEntity_cl*			m_pCivil;
		int							m_iCivilLifes;
		bool						m_bCivilDeadCurrentStage;
		VSkeletalBoneProxyObject*	m_pProxyLefthand;
		VSkeletalBoneProxyObject*	m_pProxyRigthhand;
		VisParticleEffectFile_cl*	m_pFXResource;
		VisParticleEffectPtr		m_pExplosionFX;
		float						m_fLifeUnitHeight;
		int							m_iLastGeneratedZone;
		float						m_fCutLineDistance;
		bool						m_bBossSuccess;
		bool						m_bCivilRunningOut;
		VFmodSoundObject*			m_pMusic;
		
};


//  Collection for handling playable character component
class CmpShogunBehaviour_Collection : public VRefCountedCollection<CmpShogunBehaviour> {};

//============================================================================================================
//  CmpShogunBehaviour_ComponentManager Class
//============================================================================================================
class CmpShogunBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpShogunBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	Vision::Callbacks.OnAfterSceneLoaded += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	Vision::Callbacks.OnAfterSceneLoaded -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}

			else if (pData->m_pSender==&Vision::Callbacks.OnAfterSceneLoaded )	
			{
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onAfterSceneLoaded();

			}
		}
		
		inline CmpShogunBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpShogunBehaviour_Collection m_Components;		
		static CmpShogunBehaviour_ComponentManager g_GlobalManager;

};


#endif  
