//============================================================================================================
//  CmpShurikenBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpShurikenBehaviour.h"
#include "CutshumotoUtilities.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"

#define WIDTH_PARAM 0
#define COLOR_PARAM 1
const char*	SHURIKENPATH = "shuriken";

V_IMPLEMENT_SERIAL( CmpShurikenBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpShurikenBehaviour_ComponentManager CmpShurikenBehaviour_ComponentManager::g_GlobalManager;


//===========================================================================
//  Function InitShader
//	load a shader lib and init outlineffect technique
//===========================================================================
void CmpShurikenBehaviour::InitShader()
{	
	m_vColor = VColorRef(255,0,0,255);
	m_fOutlineWidth = 0.2f;

	//load shader outline library
	if(Vision::Shaders.LoadShaderLibrary("shaders\\Outline.ShaderLib"))	
	{	//create outline effect shader technique	
		m_pOutlineTech = Vision::Shaders.CreateTechnique("OutlineEffect", NULL);

		//init shader params
		if(m_pOutlineTech)		
			InitShader(m_pOutlineTech, m_pOutlinePassColorReg);		
	}
}

//===========================================================================
//  Function InitShader
//	get params location from shader and store them in an array
//===========================================================================
void CmpShurikenBehaviour::InitShader(VCompiledTechnique *shader, DynArray_cl<int>& regsOut)
{	
	//need only two parameters of the first vertex shader pass
	regsOut.Resize(2);
	
	//get vertex shader params
	regsOut[WIDTH_PARAM] = shader->GetShader(0)->GetConstantBuffer(VSS_VertexShader)->GetRegisterByName("OutlineWidth");
	regsOut[COLOR_PARAM] = shader->GetShader(0)->GetConstantBuffer(VSS_VertexShader)->GetRegisterByName("OutlineColor");

	ApplyShaderParams();
	
}

void CmpShurikenBehaviour::ApplyShaderParams()
{
	VCompiledShaderPass *const shaderPass = m_pOutlineTech->GetShader(0);
	const float outlineAttackWidth = 0.2f;
	       
	hkvVec4 const color4 = m_vColor.getAsVec4();

	if (m_pOutlinePassColorReg[WIDTH_PARAM] > 0 &&  m_pOutlinePassColorReg[COLOR_PARAM] > 0 )
    {
        shaderPass->GetConstantBuffer(VSS_VertexShader)->SetSingleRegisterF(m_pOutlinePassColorReg[WIDTH_PARAM], &m_fOutlineWidth);
		shaderPass->GetConstantBuffer(VSS_VertexShader)->SetSingleRegisterF(m_pOutlinePassColorReg[COLOR_PARAM], color4.data);
        shaderPass->m_bModified = true;     
    }
}

//===========================================================================
//  Function RenderOutlineShader
//	send color and width parameters to spider shader
//===========================================================================
void CmpShurikenBehaviour::RenderOutlineShader()
{				      
	if(!m_pOwner->GetAnimConfig() || m_pOwner->GetAnimConfig()->GetSkinningMeshBuffer() == NULL) return;
	if (!HasEnemyProperties())	return;
	if (!m_pCmpProperties->CanBeSliced()) return;	
	
	Vision::RenderLoopHelper.RenderEntityWithShaders(m_pOwner, m_pOutlineTech->GetShaderCount(), m_pOutlineTech->GetShaderList());
}

void CmpShurikenBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);
	m_bRotate = true;
	//InitShader();
	
	if(HasEnemyProperties())
	{
		m_eCurrentState = eRunning;	
		m_fTimeToImpact = m_pCmpProperties->GetTempo(); 	
		prueba= true;
		m_bDisablePath = false;
	}
	
	CreatePath();
}

void CmpShurikenBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
	CmpEnemyBehaviour::onRemove( pOwner );
}

void CmpShurikenBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;			
			case eRunning:		OnExitRun();		break;
		}
	}

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;		
		case eRunning:		OnEnterRun();		break;
	}
	
	//change state
	m_eCurrentState = newState;

	m_fStateTimer = 0;
}

void CmpShurikenBehaviour::onFrameUpdate()
{		
	CmpEnemyBehaviour::onFrameUpdate();

	if (HasEnemyProperties())
	{
		if (m_pCmpProperties->GetEnemyType() == EFireball)
		{	
			ChangeFireballTexture();
		}
		else if (m_pCmpProperties->GetEnemyType() == EShuriken)
		{
			VString model = m_pOwner->GetMesh()->GetFilename();

			if (model == "Models//Ch_Projectile.model")
				ChangeKappaProjectileTexture();
			else
				ChangeShurikenTexture();
		}
	}
		
	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;		
		case eRunning:		OnRun();		break;
	}

}

void CmpShurikenBehaviour::ReplaceEntityTexture(const char *szNewTexture)
{
	VisBaseEntity_cl* pOwner = static_cast<VisBaseEntity_cl*>( this->GetOwner());
	VisSurfaceTextureSet_cl *pSet = pOwner->GetCustomTextureSet();

	if (!pSet)
	{
		pSet = pOwner->CreateCustomTextureSet();
		pOwner->SetCustomTextureSet(pSet);
	}

	pSet->GetTextures(0)->m_spDiffuseTexture = Vision::TextureManager.Load2DTexture(szNewTexture);
}

void CmpShurikenBehaviour::ChangeFireballTexture()
{
	if (m_pCmpProperties->CanBeSliced())
	{
		m_bRotate = true;

		VString tex = m_pOwner->GetMesh()->GetSurface(0)->GetBaseTexture();			

		if (tex != VString("Textures\\Ch_Fireball2.dds"))
			ReplaceEntityTexture("Textures\\Ch_Fireball2.dds");
	}
}

void CmpShurikenBehaviour::ChangeShurikenTexture()
{
	if (m_pCmpProperties->CanBeSliced())	
	{			
		VString tex = m_pOwner->GetMesh()->GetSurface(0)->GetBaseTexture();			

		if (tex != VString("Textures\\tx_ninja_diffuse2.dds"))
			ReplaceEntityTexture("Textures\\tx_ninja_diffuse2.dds");
	}
}

void CmpShurikenBehaviour::ChangeKappaProjectileTexture()
{
	if (m_pCmpProperties->CanBeSliced())	
	{			
		VString tex = m_pOwner->GetMesh()->GetSurface(0)->GetBaseTexture();			

		if (tex != VString("Textures\\Tx_Paper_Ball2.tga"))
			ReplaceEntityTexture("Textures\\Tx_Paper_Ball2.tga");		
	}
}

void CmpShurikenBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpShurikenBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpShurikenBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

//try to get a path from scene and modify points 
bool CmpShurikenBehaviour::GetPathFromScene()
{
	//not working
	return false;
	
	VisPath_cl* scenePath = Vision::Game.SearchPath(SHURIKENPATH);

	if (scenePath)
	{
		m_pPath = scenePath;
		return true;
	}

	return false;	
}

//test function
void CmpShurikenBehaviour::SetEndPoint(hkvVec3 position, bool onlyZ)
{
	if (m_pPath)
	{	
		VisPathNode_cl *node = m_pPath->GetPathNode(1, true);
		//VisPathNode_cl *node2 = m_pPath->GetPathNode(1, true);

		if (!onlyZ)
			node->SetPosition(position);
		else
		{
			hkvVec3 pos = node->GetPosition();
			//hkvVec3 pos2 = node2->GetPosition();
			pos.z = position.z;
			//pos2.z = position.z;
			node->SetPosition(pos);
			//node2->SetPosition(pos2);
		}
	}

}

void CmpShurikenBehaviour::CreatePath()
{
	hkvVec3 endPoint; 
	hkvVec3 midPoint;
	float displacement = 0;
	float yDistance = 0;

	VisBaseEntity_cl* pCamera = Vision::Game.SearchEntity( CAMPOS_ENT_KEY );
	if (pCamera)
		endPoint = pCamera->GetPosition();
	else
		endPoint = Vision::Camera.GetMainCamera()->GetPosition();	

	//int xRange = CutshumotoUtilities::GetRandomInt(0,250);	
	int xRange=0;
	//if (m_pOwner->GetPosition().x > 0)	xRange = -xRange;

	int yRange = CutshumotoUtilities::GetRandomInt(0,50);
	yRange -= CutshumotoUtilities::GetRandomInt(0,50);

	// End point, calculate random x and y
	endPoint.set(endPoint.x + xRange /*+ (rand() % 10) - 5.f*/, endPoint.y - 100.f, endPoint.z/*+ (rand() % 10) - 5.f*/);
	//m_pOwner->GetPosition().x < 0 ? displacement=200 : displacement=-200; //2 - middle point to camera (displaced a bit)				
	yDistance =  abs(m_pOwner->GetPosition().y - endPoint.y);	
	midPoint = hkvVec3(m_pOwner->GetPosition().x + (endPoint.x-m_pOwner->GetPosition().x),  endPoint.y + (yDistance / 2), m_pOwner->GetPosition().z + (endPoint.z-m_pOwner->GetPosition().z));

	if (GetPathFromScene())
	{
		m_pPath->SetClosed(false);
		m_pPath->GetPathNode(0,true)->SetPosition(m_pOwner->GetPosition());
		m_pPath->GetPathNode(1,true)->SetPosition(midPoint);
		m_pPath->GetPathNode(2,true)->SetPosition(endPoint);
	}
	else
	{
		m_pPath = new VisPath_cl(3, FALSE, "shurikenPath");

		//calculate our path points...
		m_pPath->AddPathNode(CreatePathNode(m_pOwner->GetPosition())); //1 - player position	
		if (!m_bFireball)	
			m_pPath->AddPathNode(CreatePathNode(midPoint)); //2 - middle point to camera (displaced a bit)	
		m_pPath->AddPathNode(CreatePathNode(endPoint)); //3 - Camera Position
	}		
}

VisPathNode_cl* CmpShurikenBehaviour::CreatePathNode (hkvVec3 vPos)
{
	VisPathNode_cl *node = new VisPathNode_cl(vPos);	
	//node->SetTypeIn(VisPathNode_cl::BEZIER);	
	//node->SetTypeOut(VisPathNode_cl::BEZIER);		
	node->SetControlVertices(hkvVec3(0,-66.6f,0),hkvVec3(0,66.6f,0));

	return node;
}

//State Running
void CmpShurikenBehaviour::OnEnterRun()
{		
	//CmpEnemyBehaviour::OnEnterRun();
}


void CmpShurikenBehaviour::LinearMovement()
{
	hkvVec3 dir = m_pOwner->GetPosition() - m_vTarget;
	dir.normalizeIfNotZero();

	if (HasEnemyProperties())
		m_pOwner->SetPosition(m_pOwner->GetPosition() + (dir * m_pCmpProperties->GetSpeed().y * Vision::GetTimer()->GetTimeDifference()));

	m_pOwner->IncOrientation(360 * Vision::GetTimer()->GetTimeDifference(),5 * Vision::GetTimer()->GetTimeDifference(),0);

}

void CmpShurikenBehaviour::FollowPath()
{
	if (!m_pPath) return;

	if(prueba)
		CmpEnemyBehaviour::OnEnterRun();
	prueba=false;
	
	// Get time since last frame
	m_fStateTimer += Vision::GetTimer()->GetTimeDifference();
	
	//if time has gone, destroy the path
	if (m_fStateTimer > m_fTimeToImpact)
	{
		m_pPath->DisposeObject();
		m_pPath=NULL;
		return;
	}

	// Calculate current relative parameter value [0..1]
	//float fCurrentParam = m_fStateTimer / m_fTimeToImpact;
	float fCurrentParam = m_fStateTimer / m_fTimeToImpact;

	// Evaluate current position on path
	hkvVec3 vPos;
	hkvVec3 vDir;

	m_pPath->EvalPointSmooth(fCurrentParam, vPos, &vDir, NULL);

    m_pOwner->SetUseEulerAngles(true); 
    m_pOwner->SetPosition(vPos);    
	if (m_bRotate)
		m_pOwner->IncOrientation(360 * Vision::GetTimer()->GetTimeDifference(),5 * Vision::GetTimer()->GetTimeDifference(),0);
}

void CmpShurikenBehaviour::OnRun()
{
	m_bDisablePath ? LinearMovement() : FollowPath();
}

void CmpShurikenBehaviour::OnExitRun()
{
}


