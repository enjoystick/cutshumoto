//============================================================================================================
//  CmpShurikenBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPSHURIKENBEHAVIOUR_H_INCLUDED
#define CMPSHURIKENBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPSHURIKENBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPSHURIKENBEHAVIOUR_CURRENT	1     // Current version

//============================================================================================================
//  CmpShurikenBehaviour Class
//============================================================================================================
class CmpShurikenBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpShurikenBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpShurikenBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpShurikenBehaviour(bool fireball = false){ m_bFireball = fireball; }
		SAMPLEPLUGIN_IMPEXP ~CmpShurikenBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

		void DisablePathFollow(hkvVec3 target) { m_bDisablePath = true; m_vTarget = target; }		
		void SetEndPoint(hkvVec3 position, bool onlyZ = true);
		void RenderOutlineShader();
		void SetOutlineColor(VColorRef color) {	m_vColor = color; ApplyShaderParams();}
		void SetOutlineWidth(float width) {	m_fOutlineWidth = width; ApplyShaderParams(); }
		void SetRotate(bool rot) { m_bRotate = rot; }

	private:		
		void ChangeState(eEnemyState newState);
		bool GetPathFromScene();
		void ApplyShaderParams();
		
		void CreatePath();
		void ChangeFireballTexture();
		void ChangeShurikenTexture();
		void ChangeKappaProjectileTexture();

		void ReplaceEntityTexture(const char *szNewTexture);

		void FollowPath();
		void LinearMovement();
		void InitShader(VCompiledTechnique *shader, DynArray_cl<int>& regsOut);
		void InitShader();
		VisPathNode_cl* CreatePathNode (hkvVec3 vPos);
				
		virtual void OnEnterRun();		virtual void OnRun();		virtual void OnExitRun();	

		VisPath_cl				*m_pPath;
		float					m_fTimeToImpact;
		hkvVec3					m_vTarget;
		bool					m_bDisablePath;
		VCompiledTechniquePtr	m_pOutlineTech;
		DynArray_cl<int>		m_pOutlinePassColorReg;	
		VColorRef				m_vColor;
		float					m_fOutlineWidth;
		bool					m_bRotate;
		bool					prueba;
		bool					m_bFireball;
		
};


//  Collection for handling playable character component
class CmpShurikenBehaviour_Collection : public VRefCountedCollection<CmpShurikenBehaviour> {};

//============================================================================================================
//  CmpShurikenBehaviour_ComponentManager Class
//============================================================================================================
class CmpShurikenBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpShurikenBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	/*Vision::Callbacks.OnRenderHook += this;*/	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	/*Vision::Callbacks.OnRenderHook -= this;*/ } 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
			/*else if (pData->m_pSender==&Vision::Callbacks.OnRenderHook)	
			{
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->RenderOutlineShader();
			}*/
		}
		
		inline CmpShurikenBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpShurikenBehaviour_Collection m_Components;		
		static CmpShurikenBehaviour_ComponentManager g_GlobalManager;

};


#endif  
