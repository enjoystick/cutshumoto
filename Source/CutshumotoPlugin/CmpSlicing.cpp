//============================================================================================================
//  CmpSlicing Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpEnemyProperties.h"
#include "CmpSlicing.h"
#include "GameManager.h"
#include "SoundManager.h"

#include <sstream>

#include "YConstraint.h"

//angles to detect slice direction
#define H_RIGHT 15
#define D_TOPRIGHT 80
#define V_UP 110
#define D_TOPLEFT 165
#define H_LEFT -195
#define D_DOWNLEFT -110	
#define V_DOWN -80
#define D_DOWNRIGHT -15


namespace Helper
{
	bool LineIntersectsLine( const hkvVec2& l1p1, const hkvVec2& l1p2, const hkvVec2& l2p1, const hkvVec2& l2p2 )
	{
		float q = (l1p1.y - l2p1.y) * (l2p2.x - l2p1.x) - (l1p1.x - l2p1.x) * (l2p2.y - l2p1.y);
		float d = (l1p2.x - l1p1.x) * (l2p2.y - l2p1.y) - (l1p2.y - l1p1.y) * (l2p2.x - l2p1.x);

		if( d == 0 )
		{
			return false;
		}

		float r = q / d;

		q = (l1p1.y - l2p1.y) * (l1p2.x - l1p1.x) - (l1p1.x - l2p1.x) * (l1p2.y - l1p1.y);
		float s = q / d;

		if( r < 0 || r > 1 || s < 0 || s > 1 )
		{
			return false;
		}

		return true;
	}

	bool LineIntersectsRect( const hkvVec2& p1, const hkvVec2& p2, const VRectanglef& r )
	{
		return LineIntersectsLine(p1, p2, hkvVec2(r.m_vMin.x, r.m_vMin.y), hkvVec2(r.m_vMin.x + r.GetSizeX(), r.m_vMin.y)) ||
			LineIntersectsLine(p1, p2, hkvVec2(r.m_vMin.x + r.GetSizeX(), r.m_vMin.y), hkvVec2(r.m_vMin.x + r.GetSizeX(), r.m_vMin.y + r.GetSizeY())) ||
			LineIntersectsLine(p1, p2, hkvVec2(r.m_vMin.x + r.GetSizeX(), r.m_vMin.y + r.GetSizeY()), hkvVec2(r.m_vMin.x, r.m_vMin.y + r.GetSizeY())) ||
			LineIntersectsLine(p1, p2, hkvVec2(r.m_vMin.x, r.m_vMin.y + r.GetSizeY()), hkvVec2(r.m_vMin.x, r.GetSizeY())) ||
			(r.IsInside(p1) && r.IsInside(p2));
	}
};


V_IMPLEMENT_SERIAL( CmpSlicing, IVObjectComponent, 0, &g_myComponentModule);
CmpSlicing_ComponentManager CmpSlicing_ComponentManager::g_GlobalManager;

//============================================================================================================
//  CmpSlicing Body
//============================================================================================================
void CmpSlicing::onStartup( VisTypedEngineObject_cl *pOwner )
{			
	//get global vars
	m_pRenderContext = Vision::Contexts.GetMainRenderContext();
	m_pPhysicsMod = vHavokPhysicsModule::GetInstance();	

	//initialize member vars
	m_eSwipeState = eNoSwipe;
	m_vIniSwipePoint = hkvVec2::ZeroVector();	
	m_vEndSwipePoint = hkvVec2::ZeroVector();	
	m_vCurrentTouchPos = hkvVec2::ZeroVector();	
	m_vLastTouchPos = hkvVec2::ZeroVector();
	m_fTimeToEndSwipe = 0;
	m_fSliceLength = 50.f;
	staminaScale=1;
	cutSoundTreshold=50;
	cutSoundSecToWaitInit=0.3;
	cutSoundSecToWait=cutSoundSecToWaitInit;
	altSwipe=false;

	m_pRenderContext->GetSize( iScreenXRes, iScreenYRes );

	//pSoundSwipe= VFmodManager::GlobalManager().LoadSoundResource("Sounds\\sword_swipe.mp3");
	//create input mapping
	InputMapping();
	//create trail
	CreateTouchTrail();

	// Init with 0 as defaultValue
	m_aSlicedEntities.Init(0);

	onceBool=true;	
}


void CmpSlicing::onRemove(  VisTypedEngineObject_cl *pOwner )
{	
	//remove trail effect 
	m_pFXResource->Purge();
	VisParticleGroupManager_cl::GlobalManager().Instances().Purge();

	//comentado por david temporales, para poder trabajar porque esto me peta

	//Prueba Sergio 25/08
	m_aSlicedEntities.Pack();
	m_aSlicedEntities.AdjustSize();
	for ( unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++ )
	{
		if ( m_aSlicedEntities[ i ]->entity)
		{	
				delete m_aSlicedEntities[ i ];
				m_aSlicedEntities.Remove(i);
		}
	}

	m_aSlicedEntities.Reset();
	m_aSlicedEntities.Pack();
	m_aSlicedEntities.AdjustSize();

	EnemiesFactory::Instance()->ResetEnemyLoaded();
}


void CmpSlicing::onFrameUpdate()
{
	if (!GetOwner())	return;
	if ( Vision::GetTimer()->GetFrozen() ) return; // Do nothing if game is paused

	//posAnchor= Vision::Camera.GetMainCamera()->GetPosition();
	
	ReadInputs();
	UpdateTrailPosition(false);
	ManageSwipeState();

	// Save last touch pos
	m_vLastTouchPos = m_vCurrentTouchPos;
}

void CmpSlicing::CreateTouchTrail()
{
	//load resource from disk
	m_pFXResource = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//TextureTrail.xml");	
	
	//random pos
	hkvVec3 vPos(0.f,0.f,0.f);
	hkvVec3 vEulerOri(0.f,270.f,0.f);
	m_pTouchTrailFX = m_pFXResource->CreateParticleEffectInstance(vPos, vEulerOri);	
	

	YConstraint *pCons = new YConstraint();
	m_pTouchTrailFX->AddConstraint(pCons,false);
	//set pause effect until user touch screen
	m_pTouchTrailFX->SetPause(true);

}

void CmpSlicing::UpdateTrailPosition( bool bTeleport)
{
	if (m_eSwipeState == eSwipping)	
	{				
		//get direction from touch screen position
		/*hkvVec3 posAnchor= pAnchor->GetPosition();
		if(onceBool)
		{
			posAnchor.x= Vision::Camera.GetMainCamera()->GetPosition().x;
			posAnchor.z= Vision::Camera.GetMainCamera()->GetPosition().z;
			onceBool=false;
		}
		posAnchor.y= Vision::Camera.GetMainCamera()->GetPosition().y;
		pAnchor->SetPosition(posAnchor);*/
		hkvVec3 vDirection;	
		//Prueba Sergio Gomez 20_09_14
		float touchPosX=m_vCurrentTouchPos.x;
		float touchPosY=m_vCurrentTouchPos.y;
		/*float varX=200;
		float varY=200;
		if(touchPosX<varX)
			touchPosX=varX;
		if(touchPosX>iScreenXRes-varX)
			touchPosX=iScreenXRes-varX;
		if(touchPosY<varY)
			touchPosY=varY;
		if(touchPosY>iScreenYRes-varY)
			touchPosY=iScreenYRes-varY;*/
		m_pRenderContext->GetTraceDirFromScreenPos(touchPosX,touchPosY,vDirection,1.f);
		hkvVec3 vEndPos = Vision::Camera.GetMainCamera()->GetPosition() + (vDirection * 100.f);
		vEndPos.y= Vision::Camera.GetMainCamera()->GetPosition().y +100;
		/*float varX=40;
		float varY=20;
		if(vEndPos.x>varX && vEndPos.x>0)
			vEndPos.x=varX;
		if(vEndPos.x<-varX && vEndPos.x<0)
			vEndPos.x=-varX;*/
		/*if(vEndPos.y<varY)
			vEndPos.y=varY;
		if(vEndPos.y>iScreenYRes-varY)
			vEndPos.y=iScreenYRes-varY;*/
		m_pTouchTrailFX->SetPosition(vEndPos);
		m_pTouchTrailFX->SetScaling(staminaScale);
		float resultStamina=staminaScale-staminaReduction*Vision::GetTimer()->GetTimeDifference();
		if(resultStamina>0)
			staminaScale= resultStamina;
		else
			staminaScale=0;

		if (bTeleport)	
		{
				m_pTouchTrailFX->TeleportSpawnPosition();
				m_pTouchTrailFX->RespawnAllParticles(true);
		}
		SoundSwipe();
	}
}

void CmpSlicing::ManageSwipeState()
{
	static bool g_bInitColPointReseted = false;

	switch (m_eSwipeState)
	{
	case eNoSwipe: 
		// Reset initCollisionPoint in slicedEntitites
		if ( !g_bInitColPointReseted ) 
		{
			for ( unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++ ) m_aSlicedEntities[i]->stillSlicing = false;
			g_bInitColPointReseted = true;
		}

		break;

		case eSwipeFinished:
			m_eSwipeState = eNoSwipe;
			m_fTimeToEndSwipe = SWIPETIME;

			break;

		case eSwipping:
			//m_fTimeToEndSwipe -= Vision::GetTimer()->GetTimeDifference();
			g_bInitColPointReseted = false;

			if (m_fTimeToEndSwipe <= 0)
			{
				m_eSwipeState = eSwipeFinished;
				m_vEndSwipePoint = hkvVec2 (m_vCurrentTouchPos.x, m_vCurrentTouchPos.y);
			}

			CheckCollision();

			break;
	}
}

void CmpSlicing::InputMapping()
{	
	m_pInputMap = new VInputMap(INPUT_MAP_NUM_INPUTS, INPUT_MAP_NUM_ALTERNATIVES);

	#ifdef SUPPORTS_MOUSE
		m_pInputMap->MapTrigger(INPUT_PRIMARY_ACTION, V_MOUSE, CT_MOUSE_LEFT_BUTTON);
		m_pInputMap->MapTrigger(INPUT_PRIMARY_XPOS, V_MOUSE, CT_MOUSE_ABS_X);
		m_pInputMap->MapTrigger(INPUT_PRIMARY_YPOS, V_MOUSE, CT_MOUSE_ABS_Y);

	#endif

	#if !defined(WIN32)
		float const priority = -950.0f;
		int const width = Vision::Video.GetXRes();
		int const height = Vision::Video.GetYRes();

		VTouchArea *touchScreen = new VTouchArea(VInputManager::GetTouchScreen(), VRectanglef(0.0f, 0.0f, width, height), priority);
		m_pInputMap->MapTrigger(INPUT_PRIMARY_ACTION, touchScreen, CT_TOUCH_ANY);
		m_pInputMap->MapTrigger(INPUT_PRIMARY_XPOS, touchScreen, CT_TOUCH_ABS_X);
		m_pInputMap->MapTrigger(INPUT_PRIMARY_YPOS, touchScreen, CT_TOUCH_ABS_Y);
	#endif

}

void CmpSlicing::ReadInputs()
{
#if defined(WIN32)
	m_vCurrentTouchPos.x = (float) VInputManager::GetInputDevice("MOUSE").GetRawControlValue(CT_MOUSE_RAW_CURSOR_X);//m_pInputMap->GetTrigger(INPUT_PRIMARY_XPOS);
	m_vCurrentTouchPos.y = (float) VInputManager::GetInputDevice("MOUSE").GetRawControlValue(CT_MOUSE_RAW_CURSOR_Y);//m_pInputMap->GetTrigger(INPUT_PRIMARY_YPOS);
#else
	//m_vCurrentTouchPos.x = VInputManager::GetInputDevice(INPUT_DEVICE_TOUCHSCREEN).GetControlValue( CT_TOUCH_POINT_0_X, 0 );
	//m_vCurrentTouchPos.y = VInputManager::GetInputDevice(INPUT_DEVICE_TOUCHSCREEN).GetControlValue( CT_TOUCH_POINT_0_Y, 0 );
	m_vCurrentTouchPos.x = m_pInputMap->GetTrigger(INPUT_PRIMARY_XPOS);
	m_vCurrentTouchPos.y = m_pInputMap->GetTrigger(INPUT_PRIMARY_YPOS);
#endif
	
	//input action (touch or mouse input)
	if(m_pInputMap->GetTrigger(INPUT_PRIMARY_ACTION))
	{
		//ini swipe
		if (m_eSwipeState == eNoSwipe)
		{	
			m_vIniSwipePoint.x = m_vCurrentTouchPos.x;
			m_vIniSwipePoint.y = m_vCurrentTouchPos.y;
			m_vPrevSwipePoint= hkvVec2(m_vCurrentTouchPos.x,m_vCurrentTouchPos.y);
			m_eSwipeState = eSwipping;
			m_fTimeToEndSwipe = SWIPETIME;
			staminaScale=0.5;
			altSwipe=false;
			UpdateTrailPosition(true);			
			m_pTouchTrailFX->SetPause(false);			
		}
	}
	else //no input action
	{
		//end swipe
		if (m_eSwipeState == eSwipping)
		{	
			m_vEndSwipePoint.x = m_vCurrentTouchPos.x;
			m_vEndSwipePoint.y = m_vCurrentTouchPos.y;
			m_eSwipeState = eSwipeFinished;
			
			m_pTouchTrailFX->SetPause(true);
		}
	}
}



eSliceDirection CmpSlicing::SliceDirection (hkvVec3 vIniPos, hkvVec3 vEndPos )
{	
	//get vector from ini swipe point to actual position, transform Y axis and calculate vector degree abour center
	//hkvVec2 vIni = hkvVec2(m_vIniSwipePoint.x, m_vIniSwipePoint.y * -1);	
	//hkvVec2 vEnd = hkvVec2(m_vCurrentTouchPos.x, - m_vCurrentTouchPos.y);
	//hkvVec2 vTargetDir =  vEnd - vIni;  
	hkvVec3 vTargetDir =  vEndPos  - vIniPos;  

	float degrees = hkvMath::atan2Deg(vTargetDir.z, vTargetDir.x);

	//return swipe direction
	if (degrees >= D_DOWNRIGHT && degrees < H_RIGHT)
		return eHorizontalToRight;

	else if (degrees >= H_RIGHT && degrees < D_TOPRIGHT)
		return eDiagonalToRightUp;

	else if (degrees >= D_TOPRIGHT && degrees < V_UP)
		return eVerticalToUp; 

	else if (degrees >= V_UP && degrees < D_TOPLEFT)
		return eDiagonalToLeftUp;

	else if (hkvMath::Abs(degrees) >= D_TOPLEFT && hkvMath::Abs(degrees) < hkvMath::Abs(H_LEFT))
		return eHorizontalToLeft; 

	else if (degrees >= H_LEFT && degrees < D_DOWNLEFT)
		return eDiagonalToLeftDown;

	else if (degrees >= D_DOWNLEFT && degrees < V_DOWN)
		return eVerticalToDown;

	else if (degrees >= V_DOWN && degrees < D_DOWNRIGHT)
		return eDiagonalToRightDown;

	return eAll;
}

bool CmpSlicing::GetEntityPosition(VisBaseEntity_cl* pEntity, int& pos)
{
	for (unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++)
		if (m_aSlicedEntities[i]->entity == pEntity)
		{	
			pos = i;
			return true;
		}

	return false;
}

bool CmpSlicing::SlicedEntityExists( VisBaseEntity_cl* pEntity )
{
	for ( unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++ )
	{
		if ( m_aSlicedEntities[i]->entity == pEntity )
		{
			return true;
		}
	}
	return false;
}

void CmpSlicing::CheckCollision()
{
	// 13/08/14 Nacho and Sergio: New slice handling algorithm

	DynArray_cl<int> aBin;
	aBin.Init(-1);
	int iBinValidSize = 0;
	// Collision checks and slice tracking
	for ( unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++ )
	{
		if ( !EntityCanBeSliced( m_aSlicedEntities[i]->entity ) ) continue;

		SlicedEntity* pSlicedEntity = m_aSlicedEntities[i];
		// Update slicedEntity data
		ManageSliceAreaEntity( pSlicedEntity->entity );
		if ( CheckScreenSpaceCollision( m_vCurrentTouchPos.x, m_vCurrentTouchPos.y, pSlicedEntity ) )
		{
			// Collided
			if ( pSlicedEntity->stillSlicing )
			{
				// Previously frame collided, keeps tracking slice
				pSlicedEntity->lastCollisionPoint = hkvVec2( m_vCurrentTouchPos.x, m_vCurrentTouchPos.y );
				if ( (!pSlicedEntity->isProjectil && pSlicedEntity->iniCutPoint.getDistanceTo( pSlicedEntity->lastCollisionPoint ) > m_fSliceLength)  
					|| pSlicedEntity->isProjectil )
				{
					// Chopped
					pSlicedEntity->endCutPoint = pSlicedEntity->lastCollisionPoint;
					pSlicedEntity->sliceDir = ResolveSliceDir( pSlicedEntity->iniCutPoint, pSlicedEntity->endCutPoint );

					if ( HitEnemy( pSlicedEntity->entity, pSlicedEntity->sliceDir ) )
					{
						aBin[iBinValidSize] = i; //add entity to remove	
						iBinValidSize++;
					}
				} 
				else if ( pSlicedEntity->isProjectil ) 
				{
					// Tapped
					aBin[iBinValidSize] = i; //add entity to remove
					iBinValidSize++;
				}
			}
		}
		else
		{
			// Not collided
			pSlicedEntity->iniCutPoint = hkvVec2::ZeroVector();
		}
	}

	// Remove pending chopped entities
	for ( unsigned int i = 0; i < aBin.GetValidSize(); i++ )
	{
		if ( m_aSlicedEntities[ aBin[i] ] )
			delete m_aSlicedEntities[ aBin[i] ];
		m_aSlicedEntities.Remove( aBin[i] ); // Set index to defaultValue
		m_aSlicedEntities.Pack(); // Shift all defaultValues at the end
	}
	aBin.Reset();

	// Free memory used by defaultValues when array size reach a limit
	if ( m_aSlicedEntities.GetSize() >= MAX_SLICEDENTITITES_SIZE ) m_aSlicedEntities.AdjustSize();





	/*
	//�should it be public component var??
	static const int MAXSLICECOLLISIONS = 5;

	//get world camera position
	hkvVec3 vIniPos = Vision::Camera.GetMainCamera()->GetPosition();
	
	//get direction from touch screen position
	hkvVec3 vDirection;	
	m_pRenderContext->GetTraceDirFromScreenPos(m_vCurrentTouchPos.x,m_vCurrentTouchPos.y,vDirection,10000.f);	
	
	//we have our ray end point
	hkvVec3 vEndPos = vIniPos + vDirection;
	
	//setup ray settings
	VisPhysicsHit_t physicsHits[MAXSLICECOLLISIONS];
	VisPhysicsRaycastAllResults_cl result(MAXSLICECOLLISIONS, physicsHits);

	result.vRayStart = vIniPos;
	result.vRayEnd = vEndPos;		
	result.iCollisionBitmask = hkpGroupFilter::calcFilterInfo(vHavokPhysicsModule::HK_LAYER_COLLIDABLE_DYNAMIC);

	#ifdef _DEBUG
		Vision::Game.DrawSingleLine(vIniPos, vEndPos,VColorRef(255,255,0,1));
	#endif
		
	//cast the ray
	m_pPhysicsMod->PerformRaycast(&result);


	// New touch silicing recognizing
	VisRenderContext_cl* pMainRenCxt = Vision::Renderer.GetRendererNode(0)->GetReferenceContext();
	const hkvMat4 matInverseVP = pMainRenCxt->getProjectionMatrix().getInverse();
	//const hkvVec3 vTouchInWorldCoords = matInverseVP.transformPosition( hkvVec3( m_vCurrentTouchPos.x, vIniPos.y, m_vCurrentTouchPos.y ) );
	
	int iScreenXRes, iScreenYRes;
	m_pRenderContext->GetSize( iScreenXRes, iScreenYRes );
	hkvVec3 vTouchInWorldCoords = pMainRenCxt->GetWorldPosFromScreenPos( m_vCurrentTouchPos.x, m_vCurrentTouchPos.y, 0.f );
	VisTraceLineInfo_t traceInfo;
	hkvVec3 vEndPosFromTouch = vTouchInWorldCoords + pMainRenCxt->GetTraceDirFromScreenPos( m_vCurrentTouchPos.x, m_vCurrentTouchPos.y ) * 1000.f;
	
	hkvVec3 traceStart = Vision::Camera.GetMainCamera()->GetPosition();
	traceStart.y += 1.f;
    hkvVec3 traceDir;
    Vision::Contexts.GetMainRenderContext()->GetTraceDirFromScreenPos( m_vCurrentTouchPos.x, m_vCurrentTouchPos.y, traceDir, 1000.f );
    hkvVec3 traceEnd = traceStart + traceDir;

	int iEntitiesIntersected = Vision::CollisionToolkit.TraceLineEntities( traceStart, traceEnd, *(EnemiesFactory::Instance()->m_collecSliceAreaEntities), &traceInfo );
	#ifdef _DEBUG
		Vision::Game.DrawSingleLine( traceStart, traceEnd, VColorRef(255,255,0,1) );
	#endif
	std::stringstream ss;
	ss << iEntitiesIntersected << "\n";
	OutputDebugString( ss.str().c_str() );

	


	for (int i = 0; i < result.iNumResults; i++)
	{
		VisBaseEntity_cl* pEntity = (VisBaseEntity_cl*) result.pPhysicsHits[i].pHitObject;
		
		//check if enemy can be chopped
		if (!EntityCanBeSliced(pEntity))
			continue;

		//new sliced entity?
		if (!EntityAlreadyBeingSliced(pEntity))
		{
			SlicedEntity* newSlice = new SlicedEntity();
			newSlice->entity = pEntity;
			newSlice->iniCutPoint = result.pPhysicsHits[i].vImpactPoint;
			newSlice->stillSlicing = true;

			m_aSlicedEntities[m_aSlicedEntities.GetValidSize()] = newSlice;
		}
		else //entity already sliced, get the last collision point
		{	
			int entityPos;
			m_lastCollisionPos = result.pPhysicsHits[i].vImpactPoint;
			
			if (GetEntityPosition(pEntity,entityPos))
				m_aSlicedEntities[entityPos]->lastCollisionPoint = m_lastCollisionPos;
		}
	}
	
	DynArray_cl<int> bin;
	int defaultValue = -1;
	bin.Init( defaultValue );
	int numEntities = 0;


	m_aSlicedEntities.Pack();
	for ( unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++ )
	{
		std::stringstream ss;
		hkvVec3 corners[8];
		m_aSlicedEntities[i]->entity->GetBoundingBox().getCorners( corners );
		for ( unsigned int j = 0; j < 8; j++ )
		{
			hkvVec3 corner( corners[j].x, corners[j].y, corners[j].z );
			hkvMat4 cornerMatrix;
			cornerMatrix.setTranslation( corner );
			corner = pMainRenCxt->getProjectionMatrix().multiply( cornerMatrix ).getTranslation();
			ss << "BB Vertex screen coords " << j << ": x:" << corner.x << " y: " << corner.y << " z: " << corner.z << "\n";
			//OutputDebugString( ss.str().c_str() );
		}
	}


	m_aSlicedEntities.Pack();
	//set current sliced entities as no-cut in order to know entities which are not collided so... before collided not now then... enemy sliced
	for (unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++)
		m_aSlicedEntities[i]->stillSlicing = false;

	m_aSlicedEntities.Pack();
	//entities no being sliced are then cutoff and remove from array
	for (unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++)
	{
		float dist = (m_aSlicedEntities[i]->iniCutPoint - m_lastCollisionPos).Length();
		//get end point and slice entity
		if ((m_aSlicedEntities[i]->iniCutPoint - m_lastCollisionPos).Length() > 100.f)
		{							
			m_aSlicedEntities[i]->endCutPoint = m_lastCollisionPos;			
			//get slice direction and hit enemy
			m_aSlicedEntities[i]->sliceDir = SliceDirection(m_aSlicedEntities[i]->iniCutPoint, m_aSlicedEntities[i]->endCutPoint);

			HitEnemy(m_aSlicedEntities[i]->entity, m_aSlicedEntities[i]->sliceDir);
			bin[numEntities] = i; //add entity to remove	
			numEntities++;
		}
		
	}

	for (unsigned int i = 0; i < bin.GetValidSize(); i++)
	{			
		//delete	m_aSlicedEntities[bin[i]];
		//if (m_aSlicedEntities[bin[i]])
			//if (m_aSlicedEntities[bin[i]]->entity)
				//if (m_aSlicedEntities[bin[i]]->entity->GetVisibleBitmask() == 0)
					//m_aSlicedEntities[bin[i]]->entity->Remove();
					
		if ( m_aSlicedEntities[ bin[i] ]->entity ) // Remove entity to slice area collection
			EnemiesFactory::Instance()->m_collecSliceAreaEntities->Remove( m_aSlicedEntities[ bin[i] ]->entity );
		if ( m_aSlicedEntities[ bin[i] ] )
			delete m_aSlicedEntities[ bin[i] ];
		m_aSlicedEntities.Remove( bin[i] ); // Set index to defaultValue == 0
	}
	bin.Reset();

	//remove free spaces
	m_aSlicedEntities.Pack();
	m_aSlicedEntities.AdjustSize();
	*/
}

bool CmpSlicing::EntityCanBeSliced(VisBaseEntity_cl* hitEntity)
{
	//check if collided entity is in cut distance
	if (hitEntity)
	{
		CmpEnemyProperties* pCmpProp = hitEntity->Components().GetComponentOfType<CmpEnemyProperties>();
		if (pCmpProp)
			return pCmpProp->CanBeSliced();
	}

	return false;
}

//check if an enemy is already being sliced
bool CmpSlicing::EntityAlreadyBeingSliced(VisBaseEntity_cl* hitEntity)
{
	for (unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++)
		if (m_aSlicedEntities[i]->entity == hitEntity)
		{	
			m_aSlicedEntities[i]->stillSlicing = true;
			return true;
		}

	return false;
}

bool CmpSlicing::HitEnemy(VisBaseEntity_cl* pEntity, eSliceDirection eSliceDir)
{
	if (pEntity)
	{
		//get enemy properties and gamestats scene entity
		if ( pEntity->Components().Count() > 0 )
		{
			CmpEnemyProperties* pCmpProp = pEntity->Components().GetComponentOfType<CmpEnemyProperties>();
			if ( pCmpProp )
			{	
				//Sergio 28_08_14 Frog Doesnt die underground
				if(pCmpProp->GetEnemyType()==10 && pEntity->GetEntityKey()==FRIEND_ENT_KEY)
					return false;
				else
				{
					if(staminaScale>staminaTreshold)
						return pCmpProp->Damage(eSliceDir);
					else
						return false;
				}
			}
		}
	}
	return false;
}

/*
	If this entity already exists as SlicedEntity return NULL
	else return builded SlicedEntity
*/
SlicedEntity* CmpSlicing::TryCreateSlicedEntity( VisBaseEntity_cl* pEntity )
{
	if ( !SlicedEntityExists( pEntity ) )
	{
		// Create a new sliced entity
		SlicedEntity* pSlicedEntity = SlicedEntity::Create();
		pSlicedEntity->entity = pEntity;
		if ( pEntity->Components().Count() > 0 ) 
		{
			CmpEnemyProperties* pProperties = pEntity->Components().GetComponentOfType<CmpEnemyProperties>();
			pSlicedEntity->enemyType = pProperties ? pProperties->GetEnemyType() : ENone;
			if ( pSlicedEntity->enemyType == EShuriken 
				|| pSlicedEntity->enemyType == EFatuo 
				|| pSlicedEntity->enemyType == EFireball ) pSlicedEntity->isProjectil = true; // Is a projectil
		}
		// Set rect2d
		float fMinX, fMaxX, fMinY, fMaxY = 0.f;
		GetBBExtents( pSlicedEntity->entity->GetBoundingBox(), fMinX, fMaxX, fMinY, fMaxY );
		// Set
		if ( !pSlicedEntity->isProjectil ) pSlicedEntity->rect2d.Set( fMinX, fMinY, fMaxX, fMaxY );
		else pSlicedEntity->rect2d.Set( fMinX - (fMinX * 0.05f), fMinY - (fMinY * 0.05f), fMaxX * 1.05f, fMaxY * 1.05f );
		return pSlicedEntity;
	}
	return 0;
}

bool CmpSlicing::CheckScreenSpaceCollision( float fTouchX, float fTouchY, SlicedEntity* pSlicedEntity )
{
	if ( !pSlicedEntity ) return false;

	bool bCollided = false;
	// Check point is inside of rect
	if ( !pSlicedEntity->isProjectil ) 
	{ // Check intersect line in rect
		if ( pSlicedEntity->rect2d.IsValid() && Helper::LineIntersectsRect( hkvVec2(fTouchX, fTouchY), m_vLastTouchPos, pSlicedEntity->rect2d ) ) 
		{ // Collided
			bCollided = true;
		}
	}
	else if ( pSlicedEntity->isProjectil ) 
	{ // Check point over rect
		if ( pSlicedEntity->rect2d.IsValid() && pSlicedEntity->rect2d.IsInside( fTouchX, fTouchY ) ) 
		{ // Collided
			bCollided = true;
		}
	}
	if ( bCollided ) 
	{
		if ( !pSlicedEntity->stillSlicing ) // First contact, set as iniPoint
			pSlicedEntity->iniCutPoint = hkvVec2( fTouchX, fTouchY );
		pSlicedEntity->stillSlicing = true;
		return true;
	}
	else
	{
		// Not collided
		pSlicedEntity->stillSlicing = false;
		return false;
	}
}

eSliceDirection CmpSlicing::ResolveSliceDir( hkvVec2 vIniPos, hkvVec2 vEndPos )
{
	hkvVec2 vHeading = vIniPos - vEndPos;
	vHeading.x *= -1.f;
	vHeading.normalize();

	if ( vHeading.x < 0.5f && vHeading.x > -0.5f && vHeading.y > 0.5f )
		return eVerticalToUp;
	if ( vHeading.x > 0.5f && vHeading.y > 0.5f )
		return eDiagonalToRightUp;
	if ( vHeading.x > 0.5f && vHeading.y < 0.5f && vHeading.y > -0.5f )
		return eHorizontalToRight;
	if ( vHeading.x > 0.5f && vHeading.y < -0.5f )
		return eDiagonalToRightDown;
	if ( vHeading.x < 0.5f && vHeading.x > -0.5f && vHeading.y < -0.5f )
		return eVerticalToDown;
	if ( vHeading.x < -0.5f && vHeading.y < -0.5f )
		return eDiagonalToLeftDown;
	if ( vHeading.x < -0.5f && vHeading.y < 0.5f && vHeading.y > -0.5f )
		return eHorizontalToLeft;
	if ( vHeading.x < -0.5f && vHeading.y > 0.5f )
		return eDiagonalToLeftUp;

	return eAll;
}

void CmpSlicing::GetBBExtents( const hkvAlignedBBox& abbox, float& fMinX, float& fMaxX, float& fMinY, float& fMaxY )
{
	// Get bb vertices
	hkvVec3 vBBVertices[8];
	abbox.getCorners( vBBVertices );
	// Transform vertices to screen coords
	hkvVec2 vTransformedBBVertices[8];
	for ( unsigned int j = 0; j < 8; j++ )
		m_pRenderContext->Project2D( vBBVertices[j], vTransformedBBVertices[j].x, vTransformedBBVertices[j].y );
	// Get 'x' and 'y' axes extent from transformed vertices
	fMinX = fMaxX = vTransformedBBVertices[0].x; fMinY = fMaxY = vTransformedBBVertices[0].y;
	for ( unsigned int j = 1; j < 8; j++ )
	{
		// Set minX
		if ( vTransformedBBVertices[j].x < fMinX ) fMinX = vTransformedBBVertices[j].x;
		// Set maxX
		if ( vTransformedBBVertices[j].x > fMaxX ) fMaxX = vTransformedBBVertices[j].x;
		// Set minY
		if ( vTransformedBBVertices[j].y < fMinY ) fMinY = vTransformedBBVertices[j].y;
		// Set maxY
		if ( vTransformedBBVertices[j].y > fMaxY ) fMaxY = vTransformedBBVertices[j].y;
	}
}

void CmpSlicing::ManageSliceAreaEntity( VisBaseEntity_cl* pEntity )
{
	SlicedEntity* pSlicedEntity = TryCreateSlicedEntity( pEntity );
	if ( pSlicedEntity )
	{
		m_aSlicedEntities[ m_aSlicedEntities.GetFreePos() ] = pSlicedEntity;
	}
	else
	{
		// Update data from a existing sliced entity
		// rect2d
		int iEntityPos;
		if ( GetEntityPosition( pEntity, iEntityPos ) )
		{
			float fMinX, fMaxX, fMinY, fMaxY = 0.f;
			GetBBExtents( m_aSlicedEntities[iEntityPos]->entity->GetBoundingBox(), fMinX, fMaxX, fMinY, fMaxY );
			// Set
			if ( !m_aSlicedEntities[iEntityPos]->isProjectil ) m_aSlicedEntities[iEntityPos]->rect2d.Set( fMinX, fMinY, fMaxX, fMaxY );
			else m_aSlicedEntities[iEntityPos]->rect2d.Set( fMinX - (fMinX * 0.05f), fMinY - (fMinY * 0.05f), fMaxX * 1.05f, fMaxY * 1.05f );
		}
	}

#if (_DEBUG)
	// Draw screen space collision rect area
	int iEntityPos;
	if ( GetEntityPosition( pEntity, iEntityPos ) )
	{
		hkvVec2 vMin = m_aSlicedEntities[iEntityPos]->rect2d.m_vMin;
		hkvVec2 vMax = m_aSlicedEntities[iEntityPos]->rect2d.m_vMax;
		Vision::Game.DrawSingleLine2D( vMin.x, vMax.y, vMax.x, vMax.y );
		Vision::Game.DrawSingleLine2D( vMax.x, vMax.y, vMax.x, vMin.y );
		Vision::Game.DrawSingleLine2D( vMax.x, vMin.y, vMin.x, vMin.y );
		Vision::Game.DrawSingleLine2D( vMin.x, vMin.y, vMin.x, vMax.y );
	}
#endif
}

// Remove SlicedEntity from m_aSlicedEntities by entity
bool CmpSlicing::UntrackSlicedEntity( const VisTypedEngineObject_cl* pEntity )
{	
	if (m_aSlicedEntities.GetSize() > 10000) return false;

	for ( unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++ )
	{
		if ( m_aSlicedEntities[i] && m_aSlicedEntities[i]->entity == pEntity )
		{
			// Found wanted slicedEntity
			if ( m_aSlicedEntities[i] )
				delete m_aSlicedEntities[i];

			m_aSlicedEntities.Remove(i);
			m_aSlicedEntities.Pack();
			return true;
		}
	}
	return false; // Not found
}

// Remove each enemy visible in scene and managed by slice control system
void CmpSlicing::PurgeSlicedEntities() 
{
	for ( unsigned int i = 0; i < m_aSlicedEntities.GetValidSize(); i++ ) 
	{
		if ( m_aSlicedEntities[i] ) delete m_aSlicedEntities[i];
		m_aSlicedEntities.Remove(i);
	}
	m_aSlicedEntities.Pack();
	m_aSlicedEntities.AdjustSize();
}

// Remove visible sliced chunks from dead enemies
void CmpSlicing::PurgeSlicedChunks() 
{
	DynArray_cl<VisBaseEntity_cl*> aSlicedChunks(0,0);
	Vision::Game.SearchEntity( SLICEDCHUNKS_ENT_KEY, &aSlicedChunks );
	for ( unsigned int i = 0; i < aSlicedChunks.GetValidSize(); i++ ) 
	{
		aSlicedChunks[i]->Remove();
	}
}

/*
void CmpSlicing::SliceObject (SlicedEntity* entitySliced)
{
	Vision::Game.DrawSingleLine(entitySliced->iniCutPoint, entitySliced->endCutPoint,VColorRef(255,0,0,1));		

	return;

	hkvVec3 iniMeshSpace = entitySliced->iniCutPoint;
	hkvVec3 endMeshSpace = entitySliced->endCutPoint;
		

	//we need the third point to define the plane, it will ini or end swipe point changing depth value
	hkvVec3 thirdPoint = entitySliced->iniCutPoint + hkvVec3(0,10,0);

	//setup cutplane
	entitySliced->cutPlane = new hkvPlane();
	entitySliced->cutPlane->setFromPoints(entitySliced->iniCutPoint, entitySliced->endCutPoint, thirdPoint, hkvTriangleOrientation::ClockWise);	

	//CreateMeshPlane(entitySliced->iniCutPoint, entitySliced->endCutPoint, 500, entitySliced->cutPlane->GetNormal());

	
	//get mesh buffer data from entity
	VisMeshBuffer_cl *meshBuffer = entitySliced->entity->GetMesh()->GetMeshBuffer();			
			
	ModelVertex_t* pVertexBuffer = static_cast<ModelVertex_t *>(meshBuffer->LockVertices(VIS_LOCKFLAG_READONLY));
	unsigned short* pIndicesBuffer = static_cast<unsigned short*>(meshBuffer->LockIndices(VIS_LOCKFLAG_READONLY));	
	
	int iVertexCount = meshBuffer->GetVertexCount();	
	int iIndicesCount = meshBuffer->GetIndexCount();	
	
	ModelVertex_t* aTriangle[3];	
	unsigned short aTriIndex[3];

	PolygonData* pFrontPoly = new PolygonData(iVertexCount, iIndicesCount);
	PolygonData* pBackPoly = new PolygonData(iVertexCount, iIndicesCount);
	
 	for (int i= 0 ; i < iIndicesCount; i+=3 )
	{		
		//get triangle indices
		aTriIndex[0] = pIndicesBuffer[0];
		aTriIndex[1] = pIndicesBuffer[1];
		aTriIndex[2] = pIndicesBuffer[2];
		
		//get vertices 
		aTriangle[0] = &pVertexBuffer[aTriIndex[0]];	
		aTriangle[1] = &pVertexBuffer[aTriIndex[1]];	
		aTriangle[2] = &pVertexBuffer[aTriIndex[2]];	

		//transform to world position
		aTriangle[0]->pos = entitySliced->entity->GetWorldMatrix() * (&pVertexBuffer[aTriIndex[0]])->pos; 
		aTriangle[1]->pos = entitySliced->entity->GetWorldMatrix() * (&pVertexBuffer[aTriIndex[1]])->pos; 
		aTriangle[2]->pos = entitySliced->entity->GetWorldMatrix() *(&pVertexBuffer[aTriIndex[2]])->pos; 
				
		//divide triangle
		SutherlandHodgman(*entitySliced->cutPlane, *aTriangle, aTriIndex, pFrontPoly, pBackPoly);

		pIndicesBuffer+=3;		
	}
	
	meshBuffer->UnLockVertices();
	meshBuffer->UnLockIndices();	
	
}
*/

void CmpSlicing::CreateNewMesh()
{
	VisMBVertexDescriptor_t MyFormatDescriptor;

	MyFormatDescriptor.m_iStride = sizeof(ModelVertex_t);
	MyFormatDescriptor.m_iPosOfs = offsetof(ModelVertex_t, pos) | VERTEXDESC_FORMAT_FLOAT3;
	MyFormatDescriptor.m_iNormalOfs = offsetof(ModelVertex_t, normal) | VERTEXDESC_FORMAT_FLOAT3;
	MyFormatDescriptor.m_iTexCoordOfs[0] = offsetof(ModelVertex_t, texcoord) | VERTEXDESC_FORMAT_FLOAT2;		
	
	VisMeshBuffer_cl *pMesh = new VisMeshBuffer_cl();	
	pMesh->AllocateVertices(MyFormatDescriptor, 100);
	
	//...

}

//============================================================================================================
//  CmpSlicing Overrides
//============================================================================================================
void CmpSlicing::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	if (pOwner!=NULL)
	{
		CmpSlicing_ComponentManager::GlobalManager().Instances().AddUnique(this);    
		onStartup( pOwner );
	}
	else
	{
	    onRemove( pOwner );    
	    CmpSlicing_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

/*---------------------------------------------------------------------
*  get intersect between segment and plane
* --------------------------------------------------------------------*/		
bool CmpSlicing::Intersect (const ModelVertex_t* pointA, const ModelVertex_t* pointB, const hkvPlane& cuttingPlane, ModelVertex_t* intersecPoint)
{	
	hkvVec3 interPos;
	float t;

	if (cuttingPlane.getLineSegmentIntersection(pointA->pos, pointB->pos, &t, &interPos))
	{
		intersecPoint->pos = interPos;

		//get interpolated uv's and normals (if it doesnt work try interpolating single values of vectors)
		intersecPoint->texcoord = hkvMath::interpolate <hkvVec2>(pointA->texcoord,  pointB->texcoord, t);
		intersecPoint->normal = hkvMath::interpolate <hkvVec3>(pointA->normal,  pointB->normal, t);

		return true;
	}
	
	return false;
}

bool CmpSlicing::InFrontOfPlane(const char* point, float distance)
{
	if (distance > EPSILON)	return true;
	return false;
}
bool CmpSlicing::BehindPlane(const char* point, float distance)
{
	if (distance < -EPSILON)	return true;
	return false;
}
bool CmpSlicing::OnPlane (const char* point, float distance)
{
	if (!InFrontOfPlane(point,distance) && !BehindPlane(point,distance)) return true;
	return false;
}

/*---------------------------------------------------------------------
	calcula los triangulos resultantes de cortar un triangulo con un plano, inserta en las dos listas de vertices
	los vertices resultantes superiores e inferiores al corte

 	Casuistica del algoritmo: (Ref. Real-Time Collision Detection - Christer Ericson)

	A (previous v.)	B (current v.)	Front Polygon		Back Polygon
		
	InFront			InFront			B					None
	OnPlane			InFront			B					None
	Behind			InFront			I, B				I
	InFront			On				B					None
	On				On				B					None
	Behind			On				B					B
	InFront			Behind			I					I, B
	On				Behind			None				A, B
	Behind			Behind			None				B
	* --------------------------------------------------------------------*/			
bool CmpSlicing::SutherlandHodgman( const hkvPlane& plane, ModelVertex_t* pTriangle, unsigned short* pTriIndex, PolygonData* pFrontPoly, PolygonData* pBackPoly)
{
	//get last vertex
	ModelVertex_t* pPointA;
	ModelVertex_t* pPointB;
	bool bIntersection = false;
	unsigned char initialFrontVertexCount, initialBackVertexCount;

	initialFrontVertexCount = pFrontPoly->vertexCount;
	initialBackVertexCount = pBackPoly->vertexCount;
	
	pPointA = &pTriangle[2]; 

	float distancePointAtoPlane = plane.getDistanceTo(pPointA->pos); //calculamos la distancia del punto al plano
		
	//for each triangle vertex...
	for(int i = 0; i < 3; ++i)
	{
		//calculate distance to plane
		pPointB = &pTriangle[i];
		float distancePointBtoPlane = plane.getDistanceTo(pPointB->pos);

		//point B is above of the plane
		if( InFrontOfPlane("B", distancePointBtoPlane) ) 
		{
			//...and point A is behind the plane, we have and intersection
			if( BehindPlane("A", distancePointAtoPlane) )
			{
				//calculate intersection point. New point will be added to front and back new polygon 
				ModelVertex_t inter; 
				Intersect(pPointA, pPointB, plane, &inter);				

				AddVertex(pFrontPoly, inter);
				AddVertex(pBackPoly, inter);

				bIntersection = true;
			}
				
			//point B is above so it will added to front polygon 
			AddVertex(pFrontPoly, *pPointB);
		}
		//point B is behind the plane
		else if( BehindPlane("B", distancePointBtoPlane) )
		{
			//and  point A is above so... intersection!
			if(InFrontOfPlane("A", distancePointAtoPlane))
			{
				//calculate intersection point. New point will be added to front and back new polygon 
				ModelVertex_t inter; 
				Intersect(pPointA, pPointB, plane, &inter);	

				AddVertex(pFrontPoly, inter);
				AddVertex(pBackPoly, inter);

				bIntersection = true;
			}
			else if( OnPlane("A", distancePointAtoPlane) ) //if point A is on the plane add it to back polygon 
				AddVertex(pBackPoly, *pPointA);
								
			AddVertex(pBackPoly, *pPointB);; //add point B to back polygon
		}		
		//point B is on the plane
		else 
		{	//add point B to front poly			
			AddVertex(pFrontPoly, *pPointB);
				
			//add point A to back poly too only if it's on the plane
			if( OnPlane("A", distancePointAtoPlane) )
				AddVertex(pBackPoly, *pPointB);				
		}
			
		//swap point A to B and repeat with next vertex 
		pPointA = pPointB;
		distancePointAtoPlane = distancePointBtoPlane;
	}		

	//add indices
	AddIndex(pFrontPoly, initialFrontVertexCount);
	AddIndex(pBackPoly, initialBackVertexCount);

	return bIntersection;
}

void CmpSlicing::AddIndex(PolygonData* polygon, unsigned char initialVertex)
{
	for (unsigned int i = initialVertex,j = initialVertex; j < polygon->vertexCount; i++, j+=3)
	{	
		*polygon->indices = (unsigned short)initialVertex;	polygon->indices++;
		*polygon->indices = (unsigned short)i + 1;	polygon->indices++;
		*polygon->indices = (unsigned short)i + 2;	polygon->indices++;
		
		polygon->indicesCount+=3;		
	}
}

void CmpSlicing::AddVertex(PolygonData* polygon, ModelVertex_t& vertex)
{
	*polygon->vertex = vertex;
	polygon->vertex++;
	polygon->vertexCount++;
	//polygon->
}

void CmpSlicing::SoundSwipe()
{
		float condA =m_vPrevSwipePoint.x-m_vCurrentTouchPos.x;
		float condB =m_vPrevSwipePoint.y-m_vCurrentTouchPos.y;
		if( (abs(condA)>cutSoundTreshold || abs(condB)>cutSoundTreshold) && Vision::GetTimer()->GetTime()>cutSoundSecToWait && staminaScale>staminaTreshold)
		{
			if(!altSwipe)
			{
				SoundManager::Instance()->PlaySound(eSwipe1,Vision::Camera.GetMainCamera()->GetPosition(),false);
				altSwipe=true;
			}
			else
			{
				SoundManager::Instance()->PlaySound(eSwipe2,Vision::Camera.GetMainCamera()->GetPosition(),false);
				altSwipe=false;
			}
			/*VFmodSoundObject *pSoundObject = pSoundSwipe->CreateInstance(Vision::Camera.GetMainCamera()->GetPosition(),VFMOD_FLAG_NONE);
			pSoundObject->Stop();
			pSoundObject->SetVolume(0.1);
			pSoundObject->Play();*/
			cutSoundSecToWait=Vision::GetTimer()->GetTime()+cutSoundSecToWaitInit;
		}
		m_vPrevSwipePoint= hkvVec2(m_vCurrentTouchPos.x,m_vCurrentTouchPos.y);
}


void CmpSlicing::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPSLICING_VERSION_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPSLICING_VERSION_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> m_fSliceLength;
	}
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << m_fSliceLength;
	}
}




//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpSlicing,IVObjectComponent, "Slicing", VVARIABLELIST_FLAGS_NONE, "CmpSlicing" )

	DEFINE_VAR_FLOAT	(CmpSlicing, m_fSliceLength, "Min swipe length to be considered a slice", "0.0",0,0);
	DEFINE_VAR_FLOAT	(CmpSlicing,staminaReduction,"Reduction of stamina by second","0.1",0,0);
	DEFINE_VAR_FLOAT	(CmpSlicing,staminaTreshold,"Umbral of Stamina, when is reached cut doesnt work","0.1",0,0);
	

END_VAR_TABLE

