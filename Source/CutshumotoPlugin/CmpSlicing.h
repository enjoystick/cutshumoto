//============================================================================================================
//  CmpSlicing Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPSLICING_H_INCLUDED
#define CMPSLICING_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "Vision/Runtime/EnginePlugins/Havok/HavokPhysicsEnginePlugin/vHavokPhysicsModule.hpp"
#include "SlicedEntity.h"
#include "GlobalTypes.h"


// Versions
#define CMPSLICING_VERSION_0          0     // Initial version
#define CMPSLICING_VERSION_CURRENT    1     // Current version

class VertexInfo;

// Versions
#define CMPSLICING_VERSION_0			0     // Initial version
#define CMPSLICING_VERSION_CURRENT		1     // Current version

#define EPSILON							0.0001f
#define SWIPETIME						3
#define MAX_SLICEDENTITITES_SIZE		50

//============================================================================================================
//  CmpSlicing Class
//============================================================================================================
class CmpSlicing : public IVObjectComponent
{
	public:
		V_DECLARE_SERIAL  ( CmpSlicing, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpSlicing, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpSlicing()
		{
			Vision::Message.reset();    
		}

		SAMPLEPLUGIN_IMPEXP ~CmpSlicing(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE BOOL CanAttachToObject( VisTypedEngineObject_cl *pObject, VString &sErrorMsgOut )
		{
			if ( !IVObjectComponent::CanAttachToObject( pObject, sErrorMsgOut ))return FALSE;  

			if (!pObject->IsOfType( V_RUNTIME_CLASS( VisObject3D_cl )))
			{
				sErrorMsgOut = "Component can only be added to instances of VisObject3D_cl or derived classes.";
				return FALSE;
			}
			
			return TRUE;
		}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void OnVariableValueChanged(VisVariable_cl *pVar, const char * value) {}
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );
		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );

		bool							UntrackSlicedEntity( const VisTypedEngineObject_cl* pEntity );
		const float						GetSliceLength() const { return m_fSliceLength; }
		void							SetSliceLength( const float fSliceLength ) { m_fSliceLength = fSliceLength; }
		void							ManageSliceAreaEntity( VisBaseEntity_cl* pEntity );
		void							PurgeSlicedEntities();
		void							PurgeSlicedChunks();

	protected:	

	private:
	
		enum eSwipeState
		{
			eNoSwipe = 0,
			eSwipping,
			eSwipeFinished
		};

		enum MyPlayerInputs
		{
			INPUT_INVALID = -1,
			INPUT_PRIMARY_ACTION = 0,
			INPUT_PRIMARY_XPOS,
			INPUT_PRIMARY_YPOS,
			INPUT_TOTAL_INPUTS,

			INPUT_MAP_NUM_INPUTS = 64,
			INPUT_MAP_NUM_ALTERNATIVES = 20

		};

		hkvVec2		m_vCurrentTouchPos;	
		hkvVec2		m_vLastTouchPos;
		hkvVec2		m_vIniSwipePoint;
		hkvVec2		m_vEndSwipePoint;
		eSwipeState m_eSwipeState;
		float		m_fTimeToEndSwipe;
		VInputMap*	m_pInputMap;
		float		m_fSliceLength;
		bool		onceBool;
		float		staminaScale;
		float		staminaReduction;
		float		staminaTreshold;
		float		cutSoundTreshold;
		float		cutSoundSecToWait;
		float		cutSoundSecToWaitInit;
		hkvVec2		m_vPrevSwipePoint;
		bool		altSwipe;
		int		iScreenXRes;
		int		iScreenYRes;

		VisParticleEffectFile_cl*	m_pFXResource;
		VisParticleEffectPtr		m_pTouchTrailFX;

		VisRenderContext_cl*		m_pRenderContext;
		vHavokPhysicsModule*		m_pPhysicsMod;		

		DynArray_cl<SlicedEntity*>	m_aSlicedEntities;		
		hkvVec3						m_lastCollisionPos;				

		VFmodSoundResource *pSoundSwipe;

		bool HitEnemy(VisBaseEntity_cl* pEntity, eSliceDirection eSliceDir);
		void InputMapping();
		void ReadInputs();
		void CreateTouchTrail();	
		void UpdateTrailPosition(bool bTeleport);
		void ManageSwipeState();
		void CheckCollision();
		bool EntityAlreadyBeingSliced(VisBaseEntity_cl* hitEntity);
		bool EntityCanBeSliced(VisBaseEntity_cl* hitEntity);
		eSliceDirection SliceDirection (hkvVec3 vIniPos, hkvVec3 vEndPos);
		bool GetEntityPosition(VisBaseEntity_cl* pEntity, int& pos);
		bool SlicedEntityExists( VisBaseEntity_cl* pEntity );
		void SliceObject (SlicedEntity* entity);
		SlicedEntity* TryCreateSlicedEntity( VisBaseEntity_cl* pEntity );
		bool CheckScreenSpaceCollision( float fTouchX, float fTouchY, SlicedEntity* pSlicedEntity );
		eSliceDirection ResolveSliceDir( hkvVec2 vIniPos, hkvVec2 vEndPos );
		void GetBBExtents( const hkvAlignedBBox& abbox, float& fMinX, float& fMaxX, float& fMinY, float& fMaxY );

		bool Intersect (const ModelVertex_t* pointA, const ModelVertex_t* pointB, const hkvPlane& cuttingPlane, ModelVertex_t* intersecPoint);
		bool SutherlandHodgman( const hkvPlane& plane, ModelVertex_t* pTriangle, unsigned short* pTriIndex, PolygonData* pFrontPoly, PolygonData* pBackPoly);
		bool InFrontOfPlane(const char* point, float distance);
		bool BehindPlane(const char* point, float distance);
		bool OnPlane (const char* point, float distance);
		void AddVertex(PolygonData* polygon, ModelVertex_t& vertex);
		void AddIndex(PolygonData* polygon, unsigned char initialVertex);				
		void CreateNewMesh();
		void SoundSwipe();
		


};


//  Collection for handling playable character component
class CmpSlicing_Collection : public VRefCountedCollection<CmpSlicing> {};

//============================================================================================================
//  CmpSlicing_ComponentManager Class
//============================================================================================================
class CmpSlicing_ComponentManager : public IVisCallbackHandler_cl
{
	public:

		static CmpSlicing_ComponentManager &GlobalManager(){  return g_GlobalManager;  }
		void OneTimeInit(){  Vision::Callbacks.OnUpdateSceneFinished += this;} // listen to this callback 
		void OneTimeDeInit(){ Vision::Callbacks.OnUpdateSceneFinished -= this;} // de-register

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			VASSERT( pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished );
			// call update function on every component
			const int iCount = m_Components.Count();

			for (int i=0;i<iCount;i++){ m_Components.GetAt(i)->onFrameUpdate(); }
		}
		
		inline CmpSlicing_Collection &Instances() { return m_Components; }

	protected:
		CmpSlicing_Collection m_Components;		
		static CmpSlicing_ComponentManager g_GlobalManager;
};


#endif  
