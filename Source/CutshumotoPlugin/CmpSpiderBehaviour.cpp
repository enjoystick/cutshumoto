//============================================================================================================
//  CmpSpiderBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpSpiderBehaviour.h"
#include "CmpSpiderlingBehaviour.h"
#include "CmpEnemyProperties.h"
#include "CutshumotoUtilities.h"
#include "SoundManager.h"
#include "GlobalTypes.h"
#include "CmpSlicing.h"
#include "GameManager.h"
#include "EnemyFactoryLUA.h"
#include "rapidjson\document.h"
#include "CmpFollowPath.h"
#include <sstream>

using namespace rapidjson;

#define ENEMYFILES_PATH "EnemiesInfo//"
#define ALTERNATIVE_ENEMYFILES_PATH "Assets//EnemiesInfo//"
#define WIDTH_PARAM 0
#define COLOR_PARAM 1

const char* SPIDER_IDLE = "Idle";
const char* SPIDER_RUN = "Run";
const char* SPIDER_ATTACK = "Attack";
const char* SPIDER_DAMAGED = "Ouch_Hit";
const char* SPIDER_RUNBACK = "Back_Run";
const char* SPIDER_RUNDAMAGED = "Damage_Reverse";
const char* SPIDER_SCREAM = "Screaming";
const char* SPIDER_DIE = "Death";
const char* SPIDER_SPAWN = "Open";

const char* SPIDERHEAD_ENT_KEY = "SpiderHead";
const int	SPIDERHEAD_HITS = 5;
const float IMMUNE_TIME = 0.2f;
const float TIME_AFTERDEATH = 1.5f;

const float BAR_INIHEIGHT = 20;
const float BAR_WIDTH = 61;
const float BAR_HEIGHT = 367;
const float BAR_OFFSET = 20;

//  Register the class in the engine module so it is available for RTTI
V_IMPLEMENT_SERIAL( CmpSpiderBehaviour, IVObjectComponent, 0, &g_myComponentModule);
CmpSpiderBehaviour_ComponentManager CmpSpiderBehaviour_ComponentManager::g_GlobalManager;

//===========================================================================
//  Function InitShader
//	load a shader lib and init outlineffect technique
//===========================================================================
void CmpSpiderBehaviour::InitShader()
{
	return;

	//load shader outline library
	if(Vision::Shaders.LoadShaderLibrary("\\assets\\shaders\\Outline.ShaderLib"))	
	{	//create outline effect shader technique	
		m_pOutlineTech = Vision::Shaders.CreateTechnique("OutlineEffect", NULL);

		//init shader params
		if(m_pOutlineTech)		
			InitShader(m_pOutlineTech, m_pOutlinePassColorReg);		
	}
}

//===========================================================================
//  Function InitShader
//	get params location from shader and store them in an array
//===========================================================================
void CmpSpiderBehaviour::InitShader(VCompiledTechnique *shader, DynArray_cl<int>& regsOut)
{	
	//need only two parameters of the first vertex shader pass
	regsOut.Resize(2);
	
	//get vertex shader params
	regsOut[WIDTH_PARAM] = shader->GetShader(0)->GetConstantBuffer(VSS_VertexShader)->GetRegisterByName("OutlineWidth");
	regsOut[COLOR_PARAM] = shader->GetShader(0)->GetConstantBuffer(VSS_VertexShader)->GetRegisterByName("OutlineColor");
	
}

void CmpSpiderBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{			
	if (m_pSpiderHead && m_bControlInit)	
	{
			EnemiesFactory::Instance()->RemoveFromSlicedEntities(m_pSpiderHead);
	}
	
	m_aStatePlan.Reset();
	m_ptrLifeBar = NULL;
	m_ptrLifeBarLeft = NULL;
	m_ptrLifeMark = NULL;
	m_ptrBossHead = NULL;
}

//===========================================================================
//  Function RenderOutlineShader
//	send color and width parameters to spider shader
//===========================================================================
void CmpSpiderBehaviour::RenderOutlineShader()
{	
	return;
	
	if (m_pCurrentState.state != eAttack) return;
	
	VCompiledShaderPass *const shaderPass = m_pOutlineTech->GetShader(0);
	       
    hkvVec4 const color4 = shaderAttackColor.getAsVec4(); 	

	if (m_pOutlinePassColorReg[WIDTH_PARAM] > 0 &&  m_pOutlinePassColorReg[COLOR_PARAM] > 0 )
    {
        shaderPass->GetConstantBuffer(VSS_VertexShader)->SetSingleRegisterF(m_pOutlinePassColorReg[WIDTH_PARAM], &outlineAttackWidth);
		shaderPass->GetConstantBuffer(VSS_VertexShader)->SetSingleRegisterF(m_pOutlinePassColorReg[COLOR_PARAM], color4.data);
        shaderPass->m_bModified = true;     
    }

	Vision::RenderLoopHelper.RenderEntityWithShaders(m_pOwner, m_pOutlineTech->GetShaderCount(), m_pOutlineTech->GetShaderList());
}

void CmpSpiderBehaviour::InitGUI()
{
	m_ptrLifeBarLeft = new VisScreenMask_cl(); 
	m_ptrLifeBar = new VisScreenMask_cl(); 
	m_ptrLifeMark = new VisScreenMask_cl(); 
	m_ptrBossHead = new VisScreenMask_cl();

	float yBar = (Vision::Video.GetYRes() / 2) - (BAR_HEIGHT / 2);
	float xBar = Vision::Video.GetXRes() - BAR_WIDTH - BAR_OFFSET;
	
	m_fLifeUnitHeight = (BAR_HEIGHT - (BAR_OFFSET * 2)) / m_pCmpProperties->GetLifePoints(); //get lifeleft heigth

	LoadImg(m_ptrBossHead,"Textures\\GUIBosses\\Boss_spiderhead.png", Vision::Video.GetXRes() - 120, yBar - 70, 999, VColorRef(255,255,255),120,90);	
	LoadImg(m_ptrLifeMark,"Textures\\GUIBosses\\Boss_hd_mark.png",xBar, yBar, 1000, VColorRef(0,0,0,255),BAR_WIDTH, m_fLifeUnitHeight / 2);
	LoadImg(m_ptrLifeBarLeft,"Textures\\GUIBosses\\Boss_hd_grey.png", xBar, yBar + BAR_INIHEIGHT, 1001, VColorRef(255,0,0,175), BAR_WIDTH, m_fLifeUnitHeight *  m_pCmpProperties->GetLifePoints() );
	LoadImg(m_ptrLifeBar,"Textures\\GUIBosses\\Boss_hd.png", xBar, yBar, 1002, VColorRef(255,255,255),BAR_WIDTH, BAR_HEIGHT);		
	
}

void CmpSpiderBehaviour::UpdateGUI()
{	
	if (m_ptrLifeBarLeft)
		m_ptrLifeBarLeft->SetTargetSize(BAR_WIDTH, m_fLifeUnitHeight * m_pCmpProperties->GetLifePoints());
}


void CmpSpiderBehaviour::onAfterSceneLoaded()
{
	//load paths for sons	
    Vision::Game.SearchPath("sonPath", &m_aScenePaths);
	m_bControlInit=true;
		
	//search for spider head to be hit on attack state
	m_pSpiderHead = NULL;
	m_pSpiderHead = Vision::Game.SearchEntity(SPIDERHEAD_ENT_KEY);

	//if dont find head, add entire spider to be sliced
	if (m_pSpiderHead)
	{
		SetHeadProperties();
		if (reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing))
			reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity(m_pSpiderHead);		
	}
	else
	{	if (reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing))
			reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity(m_pOwner);
	}

	InitGUI();

	ShowLifeBar(false, false);
}

void CmpSpiderBehaviour::LoadImg(VisScreenMaskPtr ptrImg, char* filename, float xpos, float ypos, float zpos, VColorRef color, float width, float height)
{	
	ptrImg->LoadFromFile(filename);
	ptrImg->SetTransparency(VIS_TRANSP_ALPHA);	
	ptrImg->SetPos(xpos, ypos);
	ptrImg->SetTargetSize(width,height);	
	ptrImg->SetZVal(0.0f);
	ptrImg->SetOrder(static_cast<int>(zpos));
	ptrImg->SetVisible(true);
	ptrImg->SetColor(color);
}

void CmpSpiderBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);
	
	m_pCmpProperties = NULL;
	m_pAnimSM = NULL;
	m_pCmpHighlight = NULL;
	m_pSpiderHead = NULL;
	
	m_iStatePlanIndex = 0;
	m_SpiderHeadHitsLeft = SPIDERHEAD_HITS;
	m_bSpiderHasBeenHit = false;
	m_bNeedToReadTime = false;
	m_bControlInit=false;

	m_bBossSuccess = false;

	VString result;
	m_bIsOK = InitSpiderBehaviour(&result);

	if (!m_bIsOK)
		Vision::Message.Add(result.AsChar(),1,VColorRef(255,0,0));	
	
	//InitShader();
		
}

void CmpSpiderBehaviour::SetHeadProperties()
{
	CmpEnemyProperties* pCmpProp;
	m_pSpiderHead->AddComponent(new CmpEnemyProperties());

	pCmpProp = m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>();

	pCmpProp->SetLifePoints(SPIDERHEAD_HITS);
	pCmpProp->SetEnemyType(EBoss);
	pCmpProp->SetSliceDir(eAll);
	pCmpProp->SetCanBeSliced(false);
	pCmpProp->SetScore(0);
	
}

void CmpSpiderBehaviour::DebugInfo()
{/*
	std::ostringstream sliced, headpoints, spiderlifes;
	if(m_pSpiderHead)
	{
	sliced << "Attack Enabled: " << m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->CanBeSliced();
	}
	headpoints << "Head Hits Left: " << m_SpiderHeadHitsLeft;
	spiderlifes << "Spider Lifes: " << Vision::GetTimer()->GetTime();

	Vision::Message.Print(1, 950, 50, sliced.str().c_str());
	Vision::Message.Print(1, 950, 100, headpoints.str().c_str());
	Vision::Message.Print(1, 950, 150, spiderlifes.str().c_str());*/
}

void CmpSpiderBehaviour::onFrameUpdate()
{
	if (!m_pOwner || !m_bIsOK || !MyGameManager::GlobalManager().IsPlayingTheGame())	return;
	
	if (PlayerStats::Instance()->GetPlayerLifes() <= 0 && !m_bBossSuccess)	
	{	
		m_bBossSuccess = true;		
		m_pMusic->Stop();
		m_pMusic->DisposeObject();		
	}

	if (m_bBossSuccess)	return;

	//if spider is dead we want not state changing anymore
	if (m_pCurrentState.state != eDied)
	{	
		CheckSpiderHeadHits();
		CheckSpiderLifes();
		CheckStateTime();
	}

	CheckSoundEvents();
	UpdateGUI();
	DebugInfo();

	switch (m_pCurrentState.state)
	{
		case eIdle:			OnIdle();		break;
		case eAttack:		OnAttack();		break;
		case eSpawnSons:	OnSpawnSons();	break;
		case eScream:		OnScream();		break; 
		case eDamaged:		OnDamaged();	break;
		case eRun:			OnRun();		break;
		case eRunBack:		OnRunBack();	break;
		case eDied:			OnDied();		break;
		case eFollowPath:	OnFollowPath();	break;
	}	
}

//===========================================================================
//  Function CheckSpiderLifes
//	check if spider has been hit and inject damage state or death state
//===========================================================================
void CmpSpiderBehaviour::CheckSpiderLifes()
{
	if (!HasEnemyProperties())	return;

	//spider death
	if (m_pCmpProperties->GetLifePoints() == 0 )
	{
		//inject death state
		StateInfo deathState;

		CreateStateForInjection(eDied, deathState,-1);				
		m_iStatePlanIndex++;
		InsertStateAt(deathState, m_iStatePlanIndex+1);

		//change state
		ChangeState(m_aStatePlan[m_iStatePlanIndex],false,true);
	}

	//spider has been damaged	
	if (m_bSpiderHasBeenHit && m_pCmpProperties->GetLifePoints() > 0)
	{		
		m_ptrLifeMark->SetColor(VColorRef(255,255,0,255)); 

		m_bSpiderHasBeenHit = false;
		m_bRunDamaged = true;

		//inject new damaged state
		StateInfo damagedState;		

		CreateStateForInjection(eDamaged, damagedState, -1);		
		m_iStatePlanIndex++;
		InsertStateAt(damagedState, m_iStatePlanIndex);

		//change state
		ChangeState(m_aStatePlan[m_iStatePlanIndex],false,true);
	}
}


void CmpSpiderBehaviour::CheckSpiderHeadHits()
{
	if (!m_pSpiderHead)	return;

	int lifesLeft = m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->GetLifePoints();
	
	//head hit
	if (lifesLeft != m_SpiderHeadHitsLeft)
	{
		m_SpiderHeadHitsLeft--; //damage head
		m_pCmpProperties->Damage(eHorizontalToLeft);  //damage spider
		m_pCmpHighlight->Flash(VColorRef(255,0,0),0.2f); //start effect
		SoundManager::Instance()->PlaySound(eHitEnemy, m_pOwner->GetPosition(),false);	//play sound	

		m_bImmune = true;
		m_fImmuneTime = IMMUNE_TIME;

	}

	//spider hit
	if (m_SpiderHeadHitsLeft <= 0 && !m_bSpiderHasBeenHit)
	{				
		m_bSpiderHasBeenHit = true;

		//reset head hit points
		m_SpiderHeadHitsLeft = SPIDERHEAD_HITS;
		m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->SetLifePoints(SPIDERHEAD_HITS);
		m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(false);

		//update life mark
		if (m_pCmpProperties->GetLifePoints() <= SPIDERHEAD_HITS)
		{
			if (m_ptrLifeMark)	m_ptrLifeMark->SetVisible(false);
		}
		/*else
		{
			float xmark = m_fGUI_XPos + (m_pCmpProperties->GetLifePoints() * LIFEUNIT_WIDTH) - (LIFEUNIT_WIDTH * 5);
			if (m_ptrLifeMark)	m_ptrLifeMark->SetPos(xmark, BAR_Y);
		}*/

	}

}

//===========================================================================
//  Function CreateStateForInjection
//	create a new state not defined in file
//===========================================================================
void CmpSpiderBehaviour::CreateStateForInjection(eSpiderState eState, StateInfo& state, float time)
{
	state.oneTime = true;
	state.state = eState;
	state.time = time;
	state.alreadyShot = false;
}

//===========================================================================
//  Function InsertStateAt
//	insert an state in an specific states array position
//===========================================================================
void CmpSpiderBehaviour::InsertStateAt(StateInfo& state, int index)
{
	DynArray_cl<StateInfo> tempArray;
	
	m_aStatePlan.Resize(m_aStatePlan.GetValidSize() + 1);

	//copy element from index 
	for (unsigned int i = 0; i < m_aStatePlan.GetValidSize() - index; i++)	
		tempArray[i] = m_aStatePlan[index + i];

	//put state passed
	m_aStatePlan[index] = state;

	//copy the rest of the elements
	index++;
	for (unsigned int i = 0; i < tempArray.GetValidSize(); i++)
		m_aStatePlan[index + i] = tempArray[i];
	
	tempArray.Reset();
	
}

//===========================================================================
//  Function CheckStateTime
//	check if its time to change state and assign state time if it's not defined in file
//===========================================================================
void CmpSpiderBehaviour::CheckStateTime()
{				
	//if state has no time read animation length
	if (m_bNeedToReadTime)
	{	
		if (HasAnimationSM())
			m_pCurrentState.time = m_pAnimSM->GetActiveState()->GetLength();

		m_bNeedToReadTime = false;
	}
	
	//updating state time
	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//time to change state
	if (m_fStateTimer >= m_pCurrentState.time && m_pCurrentState.state != eDied)
	{		
		if (m_iStatePlanIndex + 1 == m_aStatePlan.GetValidSize())
			m_iStatePlanIndex = 0;
		else
			m_iStatePlanIndex++;
		
		//MANAGEMENT state transitions
		ChangeState(m_aStatePlan[m_iStatePlanIndex]);
		
	}
}

//===========================================================================
//  Function ChangeState
//	change state and perform exit and enter actions for each state
//===========================================================================
void CmpSpiderBehaviour::ChangeState(StateInfo& newState, bool dDoExitActions, bool bDoEnterActions)
{	
	//modification for differents spawn states
	if (m_pCurrentState.state == newState.state)	return;

	if (newState.oneTime)
	{	
		if (newState.alreadyShot)
		{	newState.time = 0;
			return;
		}
		else
			newState.alreadyShot = true;
	}
	
	if (dDoExitActions)
	{	//actions on exit current state
		switch (m_pCurrentState.state)
		{
			case eIdle:			OnExitIdle();		break;
			case eAttack:		OnExitAttack();		break;
			case eSpawnSons:	OnExitSpawnSons();	break;
			case eScream:		OnExitScream();		break;
			case eDamaged:		OnExitDamaged();	break;
			case eRun:			OnExitRun();		break;
			case eRunBack:		OnExitRunBack();	break;
			case eFollowPath:	OnExitFollowPAth();	break;
		}
	}

	//change state
	m_pCurrentState = newState;

	if (bDoEnterActions)
	{	//actions on enter new state
		switch (newState.state)
		{
			case eIdle:			OnEnterIdle();			break;
			case eAttack:		OnEnterAttack();		break;
			case eSpawnSons:	OnEnterSpawnSons();		break;
			case eScream:		OnEnterScream();		break;
			case eDamaged:		OnEnterDamaged();		break;
			case eRun:			OnEnterRun();			break;
			case eRunBack:		OnEnterRunBack();		break;
			case eDied:			OnEnterDied();			break;
			case eFollowPath:	OnEnterFollowPath();	break;

		}
	}
	
	//if state has no time it means that state finish when animation finish
	if (m_pCurrentState.time < 0)
		m_bNeedToReadTime = true;		

	m_fStateTimer = 0;

}

//===========================================================================
//  Function InitSpiderBehaviour
//	ini transition state machine, enemyproperties component and load states from file
//===========================================================================
bool CmpSpiderBehaviour::InitSpiderBehaviour(VString* sResult)
{
	if (!m_pOwner)
	{
		*sResult = "Entity NULL";
		return false;
	}

	//we manage states with a state machine animation component	
	//VTransitionTable *pTable = VTransitionManager::GlobalManager().CreateDefaultTransitionTable(m_pOwner->GetMesh() );
	VTransitionTable *pTable = VTransitionManager::GlobalManager().LoadTransitionTable(m_pOwner->GetMesh(),"Models\\Spider_Boss.vTransition" );

	if (!pTable)
	{
		*sResult = "This Model has not animations!!";
		return false;
	}
		
	if (!m_pAnimSM)
	{	//transition state machine component
		m_pAnimSM = new VTransitionStateMachine();	
		m_pAnimSM->Init(pTable,true);	
		m_pOwner->AddComponent(m_pAnimSM);
	}
	m_pAnimSM = m_pOwner->Components().GetComponentOfType<VTransitionStateMachine>();	
	
	
	//enemy properties component
	if (!m_pCmpProperties)
	{	m_pCmpProperties = new CmpEnemyProperties();
		m_pOwner->AddComponent(m_pCmpProperties);	
	}
	
	m_pCmpProperties = m_pOwner->Components().GetComponentOfType<CmpEnemyProperties>();

	m_pCmpProperties->SetCanBeSliced(false);
	m_pCmpProperties->SetEnemyType(EBoss);
	m_pCmpProperties->SetSpeed(hkvVec3(0,-700, 0));
	m_pCmpProperties->SetLifePoints(initialLifes);
	m_pCmpProperties->SetScore(score);
	m_pCmpProperties->SetSliceDir(eAll);

	if (!m_pCmpHighlight)
	{	m_pCmpHighlight = new CmpHighlight();
		m_pOwner->AddComponent(m_pCmpHighlight);	
	}
	m_pCmpHighlight = m_pOwner->Components().GetComponentOfType<CmpHighlight>();

	if (!m_pCmpProperties || !m_pAnimSM)
	{ 
		*sResult = "Error creating components";
		return false;
	}

	//load spider states loop
	if (!LoadBehaviourFile())
	{
		*sResult = "Error reading behaviours file";
		return false;
	}	

	m_bImmune = false;
	m_fImmuneTime = IMMUNE_TIME;
	m_bRunDamaged = false;

	//init the first state
	ChangeState(m_aStatePlan[m_iStatePlanIndex],false);

	return true;	
} 
//===========================================================================
//  Function LoadBehaviourFile
//	read json file defined in vforge expose var and load spider states
//===========================================================================
bool CmpSpiderBehaviour::LoadBehaviourFile()
{
	char fileContent[5000];
	
	//read behaviour file
	CutshumotoUtilities::ReadFile(fileContent, behaviourFilename.AsChar(), ENEMYFILES_PATH);			
	
	if(!fileContent)
	{	
		CutshumotoUtilities::ReadFile(fileContent, behaviourFilename.AsChar(), ALTERNATIVE_ENEMYFILES_PATH);
		if (!fileContent)	return false; 
	}

	Document jsonDoc;	
	jsonDoc.Parse<0>(fileContent);

	if (!jsonDoc.IsObject())	return false;

	Value& states = jsonDoc["states"];

	//load states
	for (SizeType i = 0; i < states.Size(); i++)
	{
		StateInfo state;
		
		if (states[i].HasMember("type"))				state.state = static_cast<eSpiderState>(states[i]["type"].GetInt());		
		if (states[i].HasMember("time"))				state.time =  static_cast<float>(states[i]["time"].GetDouble());
		if (states[i].HasMember("animSpeed"))			state.animSpeed = static_cast<float>(states[i]["animSpeed"].GetDouble());
		if (states[i].HasMember("OneTime"))				state.oneTime = static_cast<bool>(states[i]["OneTime"].GetInt());
		if (states[i].HasMember("sonsNumber"))			state.sonsNumber = states[i]["sonsNumber"].GetInt();
		if (states[i].HasMember("timeToReachPlayer"))	state.timeToReachPlayer = static_cast<float>(states[i]["timeToReachPlayer"].GetDouble());
		if (states[i].HasMember("timeBetweenSons"))		state.timeBetweenSons = static_cast<float>(states[i]["timeBetweenSons"].GetDouble());
		if (states[i].HasMember("triggerSound"))		state.triggerSound = states[i]["triggerSound"].GetInt();
		if (states[i].HasMember("soundTime"))			state.soundTime = static_cast<float>(states[i]["soundTime"].GetDouble());
		if (states[i].HasMember("pathKey"))				state.pathKey = new VString(states[i]["pathKey"].GetString());		
		if (states[i].HasMember("loopSound"))			state.loopSound = static_cast<bool>(states[i]["loopSound"].GetInt());

		state.alreadyShot = false;		

		m_aStatePlan[m_iStatePlanIndex] = state;
		m_iStatePlanIndex++;
	}

	m_iStatePlanIndex = 0;

	return true;
}

void CmpSpiderBehaviour::CheckSoundEvents()
{
	if (m_pCurrentState.triggerSound > 0)
	{
		if (m_fStateTimer >= m_pCurrentState.soundTime)
		{
			VisBaseEntity_cl* camera = Vision::Game.SearchEntity(CAMPOS_ENT_KEY);
			hkvVec3 pos;

			if (camera)
				pos = camera->GetPosition();
			else
				pos = m_pOwner->GetPosition();

			eSounds sound = static_cast<eSounds>(m_pCurrentState.triggerSound);
			SoundManager::Instance()->PlaySound(sound, pos, m_pCurrentState.loopSound);
			m_pCurrentState.triggerSound = -1;
		}

	}
}

void CmpSpiderBehaviour::ShowLifeBar(bool bar, bool mark)
{
	m_ptrLifeBar->SetVisible(bar);
	m_ptrLifeBarLeft->SetVisible(bar);
	m_ptrBossHead->SetVisible(bar);
	
	if (m_pCmpProperties->GetLifePoints() <= 5)
		m_ptrLifeMark->SetVisible(false);
	else
		m_ptrLifeMark->SetVisible(mark);

}

//---------- FOLLOW PATH STATE MANAGEMENT ----------
void CmpSpiderBehaviour::OnEnterFollowPath()
{
	SoundManager::Instance()->PlaySound(eSpiderAmbient, m_pOwner->GetPosition(), false);
	SoundManager::Instance()->PlaySound(eSpiderWalkLong, m_pOwner->GetPosition(), false);

	VisPath_cl* pPath = Vision::Game.SearchPath(m_pCurrentState.pathKey->AsChar());
	
	if (m_pOwner->Components().GetComponentOfType<CmpFollowPath>() && pPath)
	{
		m_pOwner->Components().GetComponentOfType<CmpFollowPath>()->SetPathToFollow(pPath, m_pCurrentState.time,false);
		m_pOwner->Components().GetComponentOfType<CmpFollowPath>()->SetFollowPath(true);

		m_pAnimSM->SetState(SPIDER_RUN);
		m_pOwner->IncOrientation(0,0,180);
	}
}
void CmpSpiderBehaviour::OnFollowPath(){}
void CmpSpiderBehaviour::OnExitFollowPAth()
{
	//music begins
	m_pOwner->SetUseEulerAngles(true);
	m_pOwner->SetOrientation(0,0,0);
	m_pMusic = SoundManager::Instance()->PlaySoundReturn(eSpiderMusic, m_pOwner->GetPosition(), true);
}


//---------- IDLE STATE MANAGEMENT ----------
void CmpSpiderBehaviour::OnIdle() {}
void CmpSpiderBehaviour::OnExitIdle() {}
void CmpSpiderBehaviour::OnEnterIdle()
{	
	if (!HasAnimationSM()) return;

	m_pAnimSM->SetState(SPIDER_IDLE);		
	  
}

//---------- ATTACK STATE MANAGEMENT ----------
void CmpSpiderBehaviour::OnAttack() 
{
	if (m_fStateTimer > 0.5f) //safe time to avoid animation errors
	{
		if (m_pSpiderHead)
		{			
			//if spider has been hit recently wait a bit for attack it again
			if (m_bImmune)
			{
				m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(false);
				
				m_fImmuneTime-= Vision::GetTimer()->GetTimeDifference();

				//yeah, now is time to set it attackable
				if (m_fImmuneTime <= 0 )
				{
					m_bImmune = false;
					m_fImmuneTime = IMMUNE_TIME;
					m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(true);
				}
			}
			else
				m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(true);
		}
		else
			m_pCmpProperties->SetCanBeSliced(true);
	}
}
void CmpSpiderBehaviour::OnExitAttack()
{
	if (!HasEnemyProperties()) return;

	VTextureObjectPtr tex = Vision::TextureManager.Load2DTexture("Textures\\Spider_Texture.dds");
	m_pOwner->GetMesh()->GetSurface(0)->SetBaseTexture(tex);


	if (m_pSpiderHead)
		m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->SetCanBeSliced(false);
	else
		m_pCmpProperties->SetCanBeSliced(false);

	//if attack is finished then player is damaged
	PlayerStats::Instance()->DamagePlayer();
	SoundManager::Instance()->PlaySound(eShogunPunch, m_pOwner->GetPosition(),false);

	//update life mark
	if (m_pCmpProperties->GetLifePoints() <= SPIDERHEAD_HITS)
	{
		if (m_ptrLifeMark)	m_ptrLifeMark->SetVisible(false);
	}
	/*else
	{
		float xmark = m_fGUI_XPos + (m_pCmpProperties->GetLifePoints() * LIFEUNIT_WIDTH) - (LIFEUNIT_WIDTH * 5);
		if (m_ptrLifeMark)	m_ptrLifeMark->SetPos(xmark, BAR_Y);
	}*/
}

void CmpSpiderBehaviour::UpdateLifeMark()
{
	float yBar = (Vision::Video.GetYRes() / 2) - (BAR_HEIGHT / 2);
	float xMark = Vision::Video.GetXRes() - BAR_WIDTH - BAR_OFFSET;
	float yMark = (yBar + BAR_INIHEIGHT) + (m_fLifeUnitHeight * m_pCmpProperties->GetLifePoints()) - (m_fLifeUnitHeight * 5);
		
	if (m_ptrLifeMark)	m_ptrLifeMark->SetPos(xMark, yMark);
}

void CmpSpiderBehaviour::OnEnterAttack()
{
	if (!HasAnimationSM() || !HasEnemyProperties()) return;
	
	ShowLifeBar(true,true);

	UpdateLifeMark();

	//SoundManager::Instance()->PlaySound(eSpiderScream, m_pOwner->GetPosition(), false);

	//set head hits on attack state
	m_SpiderHeadHitsLeft = SPIDERHEAD_HITS;
	m_bSpiderHasBeenHit = false;

	if (m_pSpiderHead)
		m_pSpiderHead->Components().GetComponentOfType<CmpEnemyProperties>()->SetLifePoints(SPIDERHEAD_HITS);

	VTextureObjectPtr tex = Vision::TextureManager.Load2DTexture("Textures\\Spider_Texture_2.dds");
	m_pOwner->GetMesh()->GetSurface(0)->SetBaseTexture(tex);

	//spider can be sliced during attack state
	m_pAnimSM->SetState(SPIDER_ATTACK);	

	
}

//---------- SPAWN SONS STATE MANAGEMENT ----------
void CmpSpiderBehaviour::OnSpawnSons() 
{
	//wait for open animation is finished
	if (HasAnimationSM())
		if (m_pAnimSM->IsBlending()) return;	
	
	//check correct sons number creation
	if (m_iSonsLeft <= 0)	return;

	m_fTimeForNextSon -= Vision::GetTimer()->GetTimeDifference();

	//time to create new son??
	if (m_fTimeForNextSon <= 0)
	{
		CreateSpiderling();

		m_iSonsLeft--;
		m_fTimeForNextSon = m_pCurrentState.timeBetweenSons;
	}
}
void CmpSpiderBehaviour::OnExitSpawnSons() {}
void CmpSpiderBehaviour::OnEnterSpawnSons()
{
	if (!HasAnimationSM()) return;

	//adjust needed state time
	m_pCurrentState.time =  m_pCurrentState.sonsNumber * m_pCurrentState.timeBetweenSons + 0.5f;	
	
	m_pAnimSM->SetState(SPIDER_SPAWN);
	m_iSonsLeft = m_pCurrentState.sonsNumber;
	m_fTimeForNextSon = 0;
}

//---------- SCREAM STATE MANAGEMENT ----------
void CmpSpiderBehaviour::OnScream() {}
void CmpSpiderBehaviour::OnExitScream() {}
void CmpSpiderBehaviour::OnEnterScream()
{
	if (!HasAnimationSM()) return;	

	m_pAnimSM->SetState(SPIDER_SCREAM);		
	SoundManager::Instance()->PlaySound(eSpiderScream, m_pOwner->GetPosition(),false);
}

void CmpSpiderBehaviour::OnDamaged() 
{
	
}
void CmpSpiderBehaviour::OnExitDamaged() {}
void CmpSpiderBehaviour::OnEnterDamaged()
{	
	SoundManager::Instance()->PlaySound(eSpiderDamaged, m_pOwner->GetPosition(), false);
	
	if (!HasAnimationSM()) return;

	VTextureObjectPtr tex = Vision::TextureManager.Load2DTexture("Textures\\Spider_Texture.dds");
	m_pOwner->GetMesh()->GetSurface(0)->SetBaseTexture(tex);

	m_pAnimSM->SetState(SPIDER_DAMAGED);		

	//position attack sequence close to the end sequence time to avoid blending
	m_pAnimSM->GetInactiveControl()->SetCurrentSequenceTime(m_pAnimSM->GetInactiveState()->GetLength() - 0.2f);
}

//---------- RUN STATE MANAGEMENT ----------
void CmpSpiderBehaviour::OnRun()
{
	if (!HasEnemyProperties()) return;

	hkvVec3 speed = m_pCmpProperties->GetSpeed();

	m_pOwner->SetPosition( m_pOwner->GetPosition() + (speed * Vision::GetTimer()->GetTimeDifference()) );

}
void CmpSpiderBehaviour::OnExitRun() {}
void CmpSpiderBehaviour::OnEnterRun()
{
	if (!HasAnimationSM()) return;

	m_pAnimSM->SetState(SPIDER_RUN);

	if (m_pCurrentState.time < 3)
		SoundManager::Instance()->PlaySound(eSpiderWalkShort, m_pOwner->GetPosition(), false);
	else if (m_pCurrentState.time > 3 && m_pCurrentState.time < 10)
		SoundManager::Instance()->PlaySound(eSpiderWalkMid, m_pOwner->GetPosition(), false);
	else
		SoundManager::Instance()->PlaySound(eSpiderWalkLong, m_pOwner->GetPosition(), false);
}

//---------- RUN BACK STATE MANAGEMENT ----------
void CmpSpiderBehaviour::OnRunBack()
{	
	VColorRef cc = m_ptrLifeMark->GetColor();
	cc.a = cc.a - 3;
	if (cc.a <= 252 )
		m_ptrLifeMark->SetColor(cc);

	if (!HasEnemyProperties()) return;

	if (m_bRunDamaged)
	{
		if (m_fStateTimer >= m_pAnimSM->GetActiveState()->GetLength())
		{	
			m_pAnimSM->SetState(SPIDER_RUNBACK);
			m_bRunDamaged = false;
		}
	}
	
	hkvVec3 speed = m_pCmpProperties->GetSpeed();

	m_pOwner->SetPosition( m_pOwner->GetPosition() - (speed * Vision::GetTimer()->GetTimeDifference()) );
}
void CmpSpiderBehaviour::OnExitRunBack() 
{	
	ShowLifeBar(false,false);
	
	UpdateLifeMark();

	m_ptrLifeMark->SetColor(VColorRef(255,255,255,255)); 
}

void CmpSpiderBehaviour:: OnEnterRunBack()
{
	if (m_pCurrentState.time < 1)
		SoundManager::Instance()->PlaySound(eSpiderWalkShort, m_pOwner->GetPosition(), false);
	else if (m_pCurrentState.time > 1 && m_pCurrentState.time <= 2)
		SoundManager::Instance()->PlaySound(eSpiderWalkMid, m_pOwner->GetPosition(), false);
	else
		SoundManager::Instance()->PlaySound(eSpiderWalkLong, m_pOwner->GetPosition(), false);

	if (!HasAnimationSM()) return;

	if (m_bRunDamaged)
		m_pAnimSM->SetState(SPIDER_RUNDAMAGED);		
	else
		m_pAnimSM->SetState(SPIDER_RUNBACK);		
	
}

//---------- DEATH STATE MANAGEMENT ----------
void CmpSpiderBehaviour::OnDied()
{
	PlayerStats::Instance()->SetSoundFinish(true);
	if (!HasAnimationSM()) return;

	VString stateName =  m_pAnimSM->GetActiveState()->GetName();

	//wait animation enter in death state and then wait animation length before stop it
	if (stateName == SPIDER_DIE)
	{
		m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

		//when animation ends, stop it (no loop)
		if (m_fStateTimer >= m_pAnimSM->GetActiveState()->GetLength() && m_pAnimSM->GetActiveControl()->IsPlaying())
		{
			m_pAnimSM->GetActiveControl()->Pause();			

		}
		else if (!m_pAnimSM->GetActiveControl()->IsPlaying()) //animation stoped
		{
			//wait some seconds before change scene
			if (m_fStateTimer >  m_pAnimSM->GetActiveState()->GetLength() + TIME_AFTERDEATH)
			{
				if ( !m_bBossSuccess ) 
				{
					m_bBossSuccess = true;
					PlayerStats::Instance()->SetBossFinish(true);
					//MyGameManager::GlobalManager().OnEndOfLevelReached();
				}
			}
		}
	}
}

void CmpSpiderBehaviour::OnEnterDied()
{
	//trigger sound
	SoundManager::Instance()->PlaySound(eSpiderDeath, m_pOwner->GetPosition(), false);
	if (m_pMusic)
	{
		m_pMusic->Stop();
		m_pMusic->DisposeObject();
	}

	//add score
	PlayerStats::Instance()->AddScore(score);

	
	if (!HasAnimationSM()) return;

	m_pAnimSM->SetState(SPIDER_DIE);
				
	//position the attack sequence close to the end sequence time to avoid blending
	m_pAnimSM->GetInactiveControl()->SetCurrentSequenceTime(m_pAnimSM->GetInactiveState()->GetLength() - 0.2f);		
	
}

//===========================================================================
//  Function CreateSpiderling
//	create an spiderling from factory an assign a random path from scene
//===========================================================================
void CmpSpiderBehaviour::CreateSpiderling()
{
	VisBaseEntity_cl* pSpider = EnemiesFactory::Instance()->CreateEnemy(ESpiderling, 5, m_pOwner->GetPosition());	
	CmpSpiderlingBehaviour* pCmp = pSpider->Components().GetComponentOfType<CmpSpiderlingBehaviour>();

	//trigger sound
	SoundManager::Instance()->PlaySound(eSpiderMinion, m_pOwner->GetPosition(), false);

	if (pCmp)
	{
		if (m_aScenePaths.GetValidSize() > 0)
		{
			int randomPath = CutshumotoUtilities::GetRandomInt(0,m_aScenePaths.GetValidSize()-1);

			//we dont want the same path two times
			if (randomPath == m_iPreviousPath)
			{
				if (randomPath == m_aScenePaths.GetValidSize() - 1)
					randomPath = 0;
				else
					randomPath++;
			}

			pCmp->SetPathData(m_aScenePaths.Get(randomPath), m_pCurrentState.timeToReachPlayer);
			m_iPreviousPath = randomPath;
		}
	}
	const char* keyTrial =pSpider->GetObjectKey();
	//hkvLog::Error(keyTrial);
}


void CmpSpiderBehaviour::BlendAnimations(const char* fromAnim, const char* toAnim, float interValue, float blendTime)
{	
	/*
	// create a new AnimConfig instance
	VDynamicMesh *pMesh = m_pOwner->GetMesh();
	VisAnimFinalSkeletalResult_cl* pFinalSkeletalResult;
	VisAnimConfig_cl* pConfig = VisAnimConfig_cl::CreateSkeletalConfig(pMesh, &pFinalSkeletalResult);

	// get skeletal animation sequence
	VisSkeletalAnimSequence_cl* pAnimFrom = static_cast<VisSkeletalAnimSequence_cl*>(pMesh->GetSequence(fromAnim, VIS_MODELANIM_SKELETAL));
	VisSkeletalAnimSequence_cl* pAnimTo = static_cast<VisSkeletalAnimSequence_cl*>(pMesh->GetSequence(toAnim, VIS_MODELANIM_SKELETAL));
 
	VSmartPtr<VisSkeletalAnimControl_cl> spFromAnimControl = VisSkeletalAnimControl_cl::Create(pMesh->GetSkeleton(), pAnimFrom, VANIMCTRL_LOOP|VSKELANIMCTRL_DEFAULTS, 1.0f, true);
	VSmartPtr<VisSkeletalAnimControl_cl> spToAnimControl = VisSkeletalAnimControl_cl::Create(pMesh->GetSkeleton(), pAnimTo, VANIMCTRL_LOOP|VSKELANIMCTRL_DEFAULTS, 1.0f, true);

	// create the mixer node that blends the two animations	
	VSmartPtr<VisAnimNormalizeMixerNode_cl> spNormalizeMixerNode = new VisAnimNormalizeMixerNode_cl(pMesh->GetSkeleton());
	int iMixInputFrom = spNormalizeMixerNode->AddMixerInput(spFromAnimControl, 1.0f);
	int iMixInputTo = spNormalizeMixerNode->AddMixerInput(spToAnimControl, 0.0f);

	// finally set the mixer as the root animation node
	pFinalSkeletalResult->SetSkeletalAnimInput(spNormalizeMixerNode);
	m_pOwner->SetAnimConfig(pConfig);

	const float fBlendTime = 1.0f;  

	spNormalizeMixerNode->EaseIn(iMixInputFrom, fBlendTime, true);
    spNormalizeMixerNode->EaseOut(iMixInputTo, fBlendTime, true);*/
}

//============================================================================================================
//  CmpSpiderBehaviour Overrides
//============================================================================================================
//  
void CmpSpiderBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpSpiderBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpSpiderBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

void CmpSpiderBehaviour::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPSPIDERBEHAVIOUR_VERSION_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPSPIDERBEHAVIOUR_VERSION_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> initialLifes;
		ar >> score;
		ar >> behaviourFilename;
		ar >> shaderAttackColor;
		ar >> outlineAttackWidth;
		ar >> numberOfSons;
		ar >> timeBetweenSons;
		ar >> timeToReachPlayer;
	} 
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << initialLifes;
		ar << score;
		ar << behaviourFilename;
		ar << shaderAttackColor;
		ar << outlineAttackWidth;
		ar << numberOfSons;
		ar << timeBetweenSons;
		ar << timeToReachPlayer;

	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpSpiderBehaviour,IVObjectComponent, "Spider Boss Behaviour", VVARIABLELIST_FLAGS_NONE, "Spider Boss Behaviour" )

	DEFINE_VAR_INT		(CmpSpiderBehaviour, initialLifes, "Spider Lifes", "25", 0, 0);
	DEFINE_VAR_INT		(CmpSpiderBehaviour, score, "Points for kill Spider Boss", "5000", 0, 0);
	DEFINE_VAR_VSTRING	(CmpSpiderBehaviour, behaviourFilename, "behaviour json file","SpiderBehaviour.json",100,0,0);
	DEFINE_VAR_INT		(CmpSpiderBehaviour, numberOfSons, "Sons will be generated (deprecated)", "3", 0, 0);
	DEFINE_VAR_FLOAT	(CmpSpiderBehaviour, timeBetweenSons, "elapsed time between sons (deprecated)", "2",0,0);
	DEFINE_VAR_FLOAT	(CmpSpiderBehaviour, timeToReachPlayer, " time to reach the player (deprecated)", "3",0,0);	
	DEFINE_VAR_COLORREF	(CmpSpiderBehaviour, shaderAttackColor, "Shader attack color", "255/0/0/255",0,0);
	DEFINE_VAR_FLOAT	(CmpSpiderBehaviour, outlineAttackWidth, "Shader outline width", "3",0,0);
	

END_VAR_TABLE
