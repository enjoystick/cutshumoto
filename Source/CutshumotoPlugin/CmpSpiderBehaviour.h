//============================================================================================================
//  CmpSpiderBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPSPIDERBEHAVIOUR_H_INCLUDED
#define CMPSPIDERBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "CmpHighlight.h"
#include "GlobalTypes.h"

// Versions
#define CMPSPIDERBEHAVIOUR_VERSION_0          0     // Initial version
#define CMPSPIDERBEHAVIOUR_VERSION_CURRENT    1     // Current version

enum eSpiderState
{
	eInitial = -1,
	eIdle = 0,
	eAttack = 1,
	eSpawnSons = 2,
	eScream = 3,
	eDamaged = 4,
	eRun = 5,
	eRunBack = 6,
	eDied = 7,
	eFollowPath = 8
};

struct StateInfo
{
	eSpiderState	state;
	float			time;
	bool			oneTime;
	bool			alreadyShot;	
	int				sonsNumber;
	float			timeToReachPlayer;
	float			timeBetweenSons;
	float			animSpeed;
	int				triggerSound;
	float			soundTime;
	bool			loopSound;
	VString*		pathKey;

	StateInfo() : state(eInitial), time(-1), oneTime(false), alreadyShot(false), sonsNumber(-1), 
		animSpeed(1), timeToReachPlayer(-1), timeBetweenSons(-1), pathKey(NULL), triggerSound(-1), soundTime (-1), loopSound(false)  {}	

	inline StateInfo& operator=(const StateInfo& other)
	{
		state = other.state;
		time = other.time;
		oneTime = other.oneTime;
		alreadyShot = other.alreadyShot;
		sonsNumber = other.sonsNumber;
		animSpeed = other.animSpeed;
		timeToReachPlayer = other.timeToReachPlayer;
		timeBetweenSons = other.timeBetweenSons;
		triggerSound = other.triggerSound;
		soundTime = other.soundTime;
		loopSound = other.loopSound;
		
		if (other.pathKey)
			pathKey = other.pathKey;

	
		return *this;
	}

	inline bool operator==(const StateInfo& other)
	{
		if (state != other.state)  
			return false;

		if (time != other.time)
			return false;

		if (oneTime != other.oneTime)
			return false;

		if (alreadyShot != other.alreadyShot)
			return false;

		if (sonsNumber != other.sonsNumber)
			return false;

		if (animSpeed != other.animSpeed)
			return false;

		if (timeToReachPlayer != other.timeToReachPlayer)
			return false;

		if (timeBetweenSons != other.timeBetweenSons)
			return false;

		return true;
	}

	inline bool operator!=(const StateInfo& other)
	{
		if (*this == other)
			return false;
		else
			return true;
	}
};


//============================================================================================================
//  CmpSpiderBehaviour Class
//============================================================================================================
class CmpSpiderBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpSpiderBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpSpiderBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpSpiderBehaviour(){}
		SAMPLEPLUGIN_IMPEXP ~CmpSpiderBehaviour(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );		
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );
		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onAfterSceneLoaded();

		void RenderOutlineShader();

	protected:	

	private:

		void OnIdle();		void OnExitIdle();		void OnEnterIdle();
		void OnAttack();	void OnExitAttack();	void OnEnterAttack();
		void OnSpawnSons();	void OnExitSpawnSons();	void OnEnterSpawnSons();
		void OnScream();	void OnExitScream();	void OnEnterScream();
		void OnDamaged();	void OnExitDamaged();	void OnEnterDamaged();
		void OnRun();		void OnExitRun();		void OnEnterRun();
		void OnRunBack();	void OnExitRunBack();	void OnEnterRunBack();
		void OnFollowPath();void OnExitFollowPAth();void OnEnterFollowPath();
		void OnDied();		void OnEnterDied();

		void ChangeState(StateInfo& newState, bool dDoExitActions = true, bool bDoEnterActions = true);
		void CheckStateTime();
		bool InitSpiderBehaviour(VString* sResult);
		bool LoadBehaviourFile();
		char* ParseAnimationName(eSpiderState eState);
		void InitShader(VCompiledTechnique *shader, DynArray_cl<int>& regsOut);
		void InitShader();
		void CheckSpiderLifes();
		void CreateStateForInjection(eSpiderState eState, StateInfo& state,float time);
		void InsertStateAt(StateInfo& state, int index);		
		void CreateSpiderling();
		void SetHeadProperties();
		void CheckSpiderHeadHits();
		void CheckCutLineDistante();
		void SetSpiderAttackable(bool attackable);
		void CheckSoundEvents();
		void ReceiveHits();
		void DebugInfo();
		void InitGUI();
		void UpdateLifeMark();
		void UpdateGUI();
		void ShowLifeBar(bool bar, bool mark);
		void BlendAnimations(const char* fromAnim, const char* toAnim, float intValue, float blendTime);
		void LoadImg(VisScreenMaskPtr ptr, char* filename, float xpos, float ypos, float zpos, VColorRef color, float width, float height);

				
		//member vars
		StateInfo					m_pCurrentState;
		DynArray_cl<StateInfo>		m_aStatePlan;
		DynArray_cl<VisPath_cl*>	m_aScenePaths;		
		VisBaseEntity_cl*			m_pSpiderHead;
		float						m_fCutlineSquareDist;
		CmpHighlight*				m_pCmpHighlight;
		VisScreenMaskPtr			m_ptrLifeBar;
		VisScreenMaskPtr			m_ptrLifeBarLeft;
		VisScreenMaskPtr			m_ptrLifeMark;
		VisScreenMaskPtr			m_ptrBossHead;
		int							m_SpiderHeadHitsLeft;
		int							m_iStatePlanIndex;
		bool						m_bIsOK;
		bool						m_bNeedToReadTime;		
		bool						m_bSpiderHasBeenHit;
		VCompiledTechniquePtr		m_pOutlineTech;
		DynArray_cl<int>			m_pOutlinePassColorReg;		
		int							m_iSonsLeft;
		float						m_fTimeForNextSon;
		int							m_iPreviousPath;
		bool						m_bImmune;
		float						m_fImmuneTime;
		float						m_fGUI_XPos;
		bool						m_bRunDamaged;
		bool						m_bControlInit;
		float						m_fLifeUnitHeight;
		float						m_fAttackableTime;
		bool						m_bSpiderAttackable;
		bool						m_bBossSuccess;
		VFmodSoundObject*			m_pMusic;	
		
		//vforge editable vars
		int			initialLifes;		
		int			score;
		int			numberOfSons;
		float		timeBetweenSons;
		float		timeToReachPlayer;
		VString		behaviourFilename;
		VColorRef	shaderAttackColor;
		float		outlineAttackWidth;
		
};


//  Collection for handling playable character component
class CmpSpiderBehaviour_Collection : public VRefCountedCollection<CmpSpiderBehaviour> {};

//============================================================================================================
//  CmpSpiderBehaviour_ComponentManager Class
//============================================================================================================
class CmpSpiderBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpSpiderBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }
		void OneTimeInit()	
		{  
			Vision::Callbacks.OnUpdateSceneFinished += this;
			Vision::Callbacks.OnAfterSceneLoaded += this;
			//Vision::Callbacks.OnRenderHook += this;
		} 

		void OneTimeDeInit()
		{ 
			Vision::Callbacks.OnUpdateSceneFinished -= this;
			Vision::Callbacks.OnAfterSceneLoaded -= this;
			//Vision::Callbacks.OnRenderHook -= this;
		} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
			else if (pData->m_pSender==&Vision::Callbacks.OnAfterSceneLoaded )
			{
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onAfterSceneLoaded();
			}
			/*else if (pData->m_pSender==&Vision::Callbacks.OnRenderHook)	
			{
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->RenderOutlineShader();
			}*/
		}
		
		inline CmpSpiderBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpSpiderBehaviour_Collection m_Components;		
		static CmpSpiderBehaviour_ComponentManager g_GlobalManager;

};


#endif  
