//============================================================================================================
//  CmpSpiderlingBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpSpiderlingBehaviour.h"
#include "CmpEnemyProperties.h"
#include "GameManager.h"

#define WIDTH_PARAM 0
#define COLOR_PARAM 1

V_IMPLEMENT_SERIAL( CmpSpiderlingBehaviour, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpSpiderlingBehaviour_ComponentManager CmpSpiderlingBehaviour_ComponentManager::g_GlobalManager;

void CmpSpiderlingBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	CmpEnemyBehaviour::onStartup(pOwner);

	m_pFXResource = NULL;
	m_pExplosionFX = NULL;
	m_pOwner->SetVisibleBitmask(0);
	m_eCurrentState = eInitalState;	
	//InitShader();
	
	ChangeState(eCreating);
}

//===========================================================================
//  Function InitShader
//	load a shader lib and init outlineffect technique
//===========================================================================
void CmpSpiderlingBehaviour::InitShader()
{	
	m_vColor = VColorRef(255,0,0,255);
	m_fOutlineWidth = 7.0f;

	//load shader outline library
	if(Vision::Shaders.LoadShaderLibrary("shaders\\Outline.ShaderLib"))	
	{	//create outline effect shader technique	
		m_pOutlineTech = Vision::Shaders.CreateTechnique("OutlineEffect", NULL);

		//init shader params
		if(m_pOutlineTech)		
			InitShader(m_pOutlineTech, m_pOutlinePassColorReg);		
	}
}

//===========================================================================
//  Function InitShader
//	get params location from shader and store them in an array
//===========================================================================
void CmpSpiderlingBehaviour::InitShader(VCompiledTechnique *shader, DynArray_cl<int>& regsOut)
{	
	//need only two parameters of the first vertex shader pass
	regsOut.Resize(2);
	
	//get vertex shader params
	regsOut[WIDTH_PARAM] = shader->GetShader(0)->GetConstantBuffer(VSS_VertexShader)->GetRegisterByName("OutlineWidth");
	regsOut[COLOR_PARAM] = shader->GetShader(0)->GetConstantBuffer(VSS_VertexShader)->GetRegisterByName("OutlineColor");

	ApplyShaderParams();
	
}

void CmpSpiderlingBehaviour::ApplyShaderParams()
{
	VCompiledShaderPass *const shaderPass = m_pOutlineTech->GetShader(0);	
	       
	hkvVec4 const color4 = m_vColor.getAsVec4();

	if (m_pOutlinePassColorReg[WIDTH_PARAM] > 0 &&  m_pOutlinePassColorReg[COLOR_PARAM] > 0 )
    {
        shaderPass->GetConstantBuffer(VSS_VertexShader)->SetSingleRegisterF(m_pOutlinePassColorReg[WIDTH_PARAM], &m_fOutlineWidth);
		shaderPass->GetConstantBuffer(VSS_VertexShader)->SetSingleRegisterF(m_pOutlinePassColorReg[COLOR_PARAM], color4.data);
        shaderPass->m_bModified = true;     
    }
}

//===========================================================================
//  Function RenderOutlineShader
//	send color and width parameters to spider shader
//===========================================================================
void CmpSpiderlingBehaviour::RenderOutlineShader()
{				      
	if(!m_pOwner->GetAnimConfig() || m_pOwner->GetAnimConfig()->GetSkinningMeshBuffer() == NULL) return;
	if (!HasEnemyProperties())	return;
	if (!m_pCmpProperties->CanBeSliced()) return;	
	
	Vision::RenderLoopHelper.RenderEntityWithShaders(m_pOwner, m_pOutlineTech->GetShaderCount(), m_pOutlineTech->GetShaderList());
}

void CmpSpiderlingBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
    //EnemiesFactory::Instance()->RemoveFromSlicedEntities(pOwner);
}

void CmpSpiderlingBehaviour::TriggerExplosion()
{
	hkvVec3 pos = m_pOwner->GetPosition() + hkvVec3(0,0,20);

	//load particles
	if (!m_pFXResource)
		m_pFXResource = VisParticleGroupManager_cl::GlobalManager().LoadFromFile("Particles//DavidExplosion.xml");		

	if (!m_pFXResource) return;

	//create new effect
	if (!m_pExplosionFX)
	{	
		m_pExplosionFX = m_pFXResource->CreateParticleEffectInstance(pos, m_pOwner->GetOrientation());	
		m_pExplosionFX->SetRemoveWhenFinished(true);		
	}

}

void CmpSpiderlingBehaviour::ChangeState(eEnemyState newState)
{	
	if (m_eCurrentState != eInitalState)
	{	//actions on exit current state
		switch (m_eCurrentState)
		{			
			case eCreating:		OnExitCreate();		break;			
			case eRunning:		OnExitRun();		break;
		}
	}

	//change state
	m_eCurrentState = newState;

	//actions on enter new state
	switch (newState)
	{		
		case eCreating:		OnEnterCreate();		break;		
		case eRunning:		OnEnterRun();		break;
	}
	
	m_fStateTimer = 0;
}

void CmpSpiderlingBehaviour::onFrameUpdate()
{			
	if (!m_pOwner || !MyGameManager::GlobalManager().IsPlayingTheGame())	return;

	switch (m_eCurrentState)
	{
		case eCreating:		OnCreate();		break;
		case eRunning:		OnRun();		break;
	}

}

void CmpSpiderlingBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpSpiderlingBehaviour_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		//Solucion provisional
		//onRemove( pOwner );    
		CmpSpiderlingBehaviour_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}

//State Creating
void CmpSpiderlingBehaviour::OnEnterCreate()
{
	//CmpEnemyBehaviour::OnEnterCreate();
	TriggerExplosion();

	if (HasAnimationSM())
		m_pAnimSM->GetActiveControl()->SetSpeed(2.0f);
	
}

void CmpSpiderlingBehaviour::OnCreate()
{
	if (HasEnemyProperties())
		m_pCmpProperties->SetCanBeSliced(false);

	m_fStateTimer+= Vision::GetTimer()->GetTimeDifference();

	//we need to change this to wait creating animation time, but we have not creating animation yet
	if (m_fStateTimer >= 0.3f)
		ChangeState(eRunning);
}

void CmpSpiderlingBehaviour::OnExitCreate()
{
	//TriggerExplosion();
}

//State Running
void CmpSpiderlingBehaviour::OnEnterRun()
{
	m_pOwner->SetVisibleBitmask(1);

	CmpEnemyBehaviour::OnEnterRun();
}

void CmpSpiderlingBehaviour::OnRun()
{

	// Get time since last frame
	m_fStateTimer += Vision::GetTimer()->GetTimeDifference();

	//if we have not path, move it using properties speed
	if (!m_pPath)
	{
		if (!HasEnemyProperties())	return;

		m_pOwner->SetPosition(m_pOwner->GetPosition() + (m_pCmpProperties->GetSpeed() * Vision::GetTimer()->GetTimeDifference()));	
	}
	else
	{			
		// Calculate current relative parameter value [0..1]
		float fCurrentParam = m_fStateTimer / m_fPathTime;

		if (fCurrentParam < 1)
		{
			// Evaluate current position on path
			hkvVec3 vPos;
			hkvVec3 vDir;

			m_pPath->EvalPointSmooth(fCurrentParam, vPos, &vDir, NULL);

			m_pOwner->SetUseEulerAngles(true); 
			m_pOwner->SetPosition(vPos);    		
		}
	}

}

void CmpSpiderlingBehaviour::OnExitRun()
{
}


