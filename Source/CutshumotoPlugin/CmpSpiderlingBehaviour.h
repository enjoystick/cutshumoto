//============================================================================================================
//  CmpSpiderlingBehaviour Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPSPIDERLINGBEHAVIOUR_H_INCLUDED
#define CMPSPIDERLINGBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyBehaviour.h"
#include "GlobalTypes.h"

// Versions
#define  CMPSPIDERLINGBEHAVIOUR_VERSION_0	0     // Initial version
#define  CMPSPIDERLINGBEHAVIOUR_CURRENT		1     // Current version

//============================================================================================================
//  CmpSpiderlingBehaviour Class
//============================================================================================================
class CmpSpiderlingBehaviour : public CmpEnemyBehaviour
{
	public:
		V_DECLARE_SERIAL  ( CmpSpiderlingBehaviour, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpSpiderlingBehaviour, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpSpiderlingBehaviour(VisPath_cl* pPath = NULL, float fPathTime = 3) : m_pPath(pPath), m_fPathTime(fPathTime) {}
		SAMPLEPLUGIN_IMPEXP ~CmpSpiderlingBehaviour() {}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );

		void SetPathData(VisPath_cl* pPath, float fPathTime) { m_pPath = pPath; m_fPathTime = fPathTime; }
		void RenderOutlineShader();

	private:		
		void ChangeState(eEnemyState newState);
		void InitShader(VCompiledTechnique *shader, DynArray_cl<int>& regsOut);
		void InitShader();
		void ApplyShaderParams();
		void TriggerExplosion();
		
		virtual void OnEnterCreate();	virtual void OnCreate();	virtual void OnExitCreate();		
		virtual void OnEnterRun();		virtual void OnRun();		virtual void OnExitRun();		

		VisPath_cl					*m_pPath;
		float						m_fPathTime;
		VisParticleEffectFile_cl*	m_pFXResource;
		VisParticleEffectPtr		m_pExplosionFX;
		VCompiledTechniquePtr		m_pOutlineTech;
		DynArray_cl<int>			m_pOutlinePassColorReg;	
		VColorRef					m_vColor;
		float						m_fOutlineWidth;
		
};


//  Collection for handling playable character component
class CmpSpiderlingBehaviour_Collection : public VRefCountedCollection<CmpSpiderlingBehaviour> {};

//============================================================================================================
//  CmpSpiderlingBehaviour_ComponentManager Class
//============================================================================================================
class CmpSpiderlingBehaviour_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpSpiderlingBehaviour_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	/*Vision::Callbacks.OnRenderHook += this;*/	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	/*Vision::Callbacks.OnRenderHook -= this;*/ } 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
			/*else if (pData->m_pSender==&Vision::Callbacks.OnRenderHook)	
			{
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->RenderOutlineShader();
			}*/
		}
		
		inline CmpSpiderlingBehaviour_Collection &Instances() { return m_Components; }

	protected:
		CmpSpiderlingBehaviour_Collection m_Components;		
		static CmpSpiderlingBehaviour_ComponentManager g_GlobalManager;

};


#endif  
