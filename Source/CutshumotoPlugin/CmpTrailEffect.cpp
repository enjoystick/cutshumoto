//============================================================================================================
//  CmpTrailEffect Component
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "CmpTrailEffect.h"
#include "GameManager.h"
#include <algorithm>
using namespace std; 


V_IMPLEMENT_SERIAL( CmpTrailEffect, CmpEnemyBehaviour, 0, &g_myComponentModule);
CmpTrailEffect_ComponentManager CmpTrailEffect_ComponentManager::g_GlobalManager;

void CmpTrailEffect::onStartup( VisTypedEngineObject_cl *pOwner )
{
	m_pOwner = static_cast<VisBaseEntity_cl*> (pOwner);

    hkvAlignedBBox bbox;
	m_pOwner->GetMesh()->GetVisibilityBoundingBox(bbox);

    VTextureObject *pTrailTex = Vision::TextureManager.Load2DTexture(textureFilename);
    bbox.m_vMin.x = bbox.m_vMin.y = bbox.m_vMax.x = bbox.m_vMax.y = 0.f;     	

	IniTrailEffect(bbox.m_vMin,bbox.m_vMax, 0.3f, pTrailTex, VColorRef(trailColor));

}

void CmpTrailEffect::onRemove(  VisTypedEngineObject_cl *pOwner )
{		
	delete[] m_History;
}

void CmpTrailEffect::onFrameUpdate()
{
	if (!MyGameManager::GlobalManager().IsPlayingTheGame())	return;

	float fTime = Vision::GetTimer()->GetTime();

	if (m_bEnabled && fTime - m_fLastTime >= 0.01f) // only update with max 100fps
	{
		UpdateHistory(fTime);
		UpdateMesh(fTime);
		m_fLastTime = fTime;
    }

}

void CmpTrailEffect::SetEnabled(bool bStatus)
{
	if (m_bEnabled == bStatus)	return;

    m_spMeshObj->SetVisibleBitmask(m_bEnabled ? 0xffffffff : 0);

    m_bEnabled = bStatus;
    ResetTrail();
}

void CmpTrailEffect::SetVisible(bool bStatus)
{
	m_bVisible = bStatus;
	m_spMeshObj->SetVisibleBitmask(m_bVisible ? 0xffffffff : 0);
}

void CmpTrailEffect::ResetTrail()
{
	m_iHistoryCount = 0;
}


void CmpTrailEffect::SetOwner( VisTypedEngineObject_cl *pOwner )
{
	IVObjectComponent::SetOwner( pOwner );
	
	if (pOwner!=NULL)
	{
		CmpTrailEffect_ComponentManager::GlobalManager().Instances().AddUnique(this);		
		onStartup( pOwner );
	}
	else
	{
		onRemove( pOwner );    
		CmpTrailEffect_ComponentManager::GlobalManager().Instances().SafeRemove(this);    
	}
}


// add a new trail position to the histroy
void CmpTrailEffect::UpdateHistory(float fTime)
{
	m_iHistoryCount = min(m_iHistoryCount+1, TRAIL_HISTORY_LENGTH);
	m_iHistoryPos = (m_iHistoryPos+1)%TRAIL_HISTORY_LENGTH;
	VTrailHistoryEntry &entry(m_History[m_iHistoryPos]);

	VisObject3D_cl *pObj = (VisObject3D_cl *)GetOwner();
	
	if (pObj)
	{
		hkvMat3 rotation = pObj->GetRotationMatrix();
		const hkvVec3 &vPos(pObj->GetPosition());

		// transform into worldspace
		entry.vStart = m_vRelStart;
		entry.vEnd = m_vRelEnd;
		entry.vStart = rotation*entry.vStart;
		entry.vEnd = rotation*entry.vEnd;
		entry.vStart += vPos;
		entry.vEnd += vPos;

		entry.fTime = fTime; // set the age of this entry

	}
  
}


// update the trail mesh using the current history
void CmpTrailEffect::UpdateMesh(float fTime)
{
	int iIndex = m_iHistoryPos + TRAIL_HISTORY_LENGTH; // go back in history
	int iPrim = -1;

	VisMBSimpleVertex_t *pV = (VisMBSimpleVertex_t *)m_spMesh->LockVertices(VIS_LOCKFLAG_DISCARDABLE);

	for (int i = 0; i < m_iHistoryCount; i++, iIndex--, pV+=2, iPrim++)
	{		
		VTrailHistoryEntry &entry(m_History[iIndex % TRAIL_HISTORY_LENGTH]);

		float fAge = (fTime - entry.fTime) * m_fTimeScale;		
		int iAlpha = 255 - (int)(fAge*255.f);

		if (iAlpha < 0)		break;

		// the v-texture coordinate on the trail texture:
		float fV = entry.fTime * m_fTimeScale; 

		// alternative
		// float fV = fAge; 

		VColorRef iColor(255,255,255,iAlpha);

		if (!m_bVisible)
			iColor.a = 0;

		iColor *= m_iColor;
		pV[0].color = iColor.GetNative();
		pV[0].pos = entry.vStart;
		pV[0].texcoord0.set(0.f,fV);
		pV[1].color = iColor.GetNative();
		pV[1].pos = entry.vEnd;
		pV[1].texcoord0.set(1.f,fV);

	}

	m_spMesh->UnLockVertices();
	m_spMesh->SetPrimitiveCount(max(iPrim,0) * 2);

}

void CmpTrailEffect::IniTrailEffect(const hkvVec3 &vRelStart, const hkvVec3 &vRelEnd, float fDuration, VTextureObject *pTrailTex, VColorRef iColor) 
{
	if (pTrailTex == NULL)	pTrailTex = Vision::TextureManager.GetPlainWhiteTexture();

    m_bEnabled = true;
    m_iColor = iColor;
	m_vRelStart = vRelStart + hkvVec3(0,0, iniOffset);
    m_vRelEnd = vRelEnd - hkvVec3(0,0,endOffset);
    m_iHistoryPos = m_iHistoryCount = 0;
    m_fLastTime = 0.f;
    m_fTimeScale = 1.f/fDuration; // maps 'age' to alpha
    
    m_spMesh = new VisMeshBuffer_cl(VisMBSimpleVertex_t::VertexDescriptor, TRAIL_HISTORY_LENGTH*2, VisMeshBuffer_cl::MB_PRIMTYPE_TRISTRIP, 0, 0, VIS_MEMUSAGE_DYNAMIC);

    m_spMesh->SetBaseTexture(pTrailTex);
    VSimpleRenderState_t iState(VIS_TRANSP_ALPHA, RENDERSTATEFLAG_DOUBLESIDED|RENDERSTATEFLAG_FILTERING);
    m_spMesh->SetDefaultRenderState(iState);

    m_spMeshObj = new VisMeshBufferObject_cl(m_spMesh);

	m_History = new VTrailHistoryEntry[segmentsNumber];
}


void CmpTrailEffect::Serialize( VArchive &ar )
{
	char iLocalVersion = CMPTRAILEFFECT_CURRENT;
	IVObjectComponent::Serialize(ar);
	
	if (ar.IsLoading())
	{
		ar >> iLocalVersion;
		VASSERT_MSG(iLocalVersion == CMPTRAILEFFECT_CURRENT , "Invalid local version.");

		//  Load Data
		ar >> segmentsNumber;
		ar >> textureFilename;
		ar >> trailColor;
		ar >> iniOffset;
		ar >> endOffset;
	} 
	else
	{
		ar << iLocalVersion;
    
		//  Save Data
		ar << segmentsNumber;
		ar << textureFilename;
		ar << trailColor;
		ar << iniOffset;
		ar << endOffset;
	}
}


//============================================================================================================
//  Variable Table - Property variables can be exposed by the programmer 
//  and edited per instance by the artist  
//============================================================================================================
//
START_VAR_TABLE(CmpTrailEffect, IVObjectComponent, "Trail Mesh Effect", VVARIABLELIST_FLAGS_NONE, "Trail Mesh Effect" )

	DEFINE_VAR_INT		(CmpTrailEffect, segmentsNumber, "Number of Trail Mesh Segments", "50", 0, 0);
	DEFINE_VAR_VSTRING	(CmpTrailEffect, textureFilename, "Texture for Trail","\\Textures\\perlinnoise.dds",100,0,0);
	DEFINE_VAR_COLORREF	(CmpTrailEffect, trailColor, "Trail color", "255/0/0/255",0,0);
	DEFINE_VAR_FLOAT	(CmpTrailEffect, iniOffset, "Initial Offset", "20",0,0);
	DEFINE_VAR_FLOAT	(CmpTrailEffect, endOffset, "End Offset", "20",0,0);


END_VAR_TABLE