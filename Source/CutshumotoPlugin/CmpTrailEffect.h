//============================================================================================================
//  CmpTrailEffect Component
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef CMPTRAILEFFECT_H_INCLUDED
#define CMPTRAILEFFECT_H_INCLUDED

#include "CutshumotoPluginModule.h"

#define TRAIL_HISTORY_LENGTH 50

// Versions
#define  CMPTRAILEFFECT_VERSION_0		0     // Initial version
#define  CMPTRAILEFFECT_CURRENT		1     // Current version

//============================================================================================================
//  CmpTrailEffect Class
//============================================================================================================
class CmpTrailEffect :  public IVObjectComponent
{
	public:
	
		struct VTrailHistoryEntry
		{
			hkvVec3 vStart,vEnd;
			float fTime;
		};

		V_DECLARE_SERIAL  ( CmpTrailEffect, SAMPLEPLUGIN_IMPEXP ); // for RTTI
		V_DECLARE_VARTABLE( CmpTrailEffect, SAMPLEPLUGIN_IMPEXP );

		SAMPLEPLUGIN_IMPEXP CmpTrailEffect(){}
		SAMPLEPLUGIN_IMPEXP ~CmpTrailEffect(){}

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP VOVERRIDE void Serialize( VArchive &ar );

		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );		

		void SetEnabled(bool bStatus);
		void SetVisible(bool bStatus);


	private:
		void IniTrailEffect(const hkvVec3 &vRelStart, const hkvVec3 &vRelEnd, float fDuration, VTextureObject *pTrailTex, VColorRef iColor);
		void UpdateHistory(float fTime);
		void UpdateMesh(float fTime);
		void ResetTrail();

	private:
		//---- vForge vars ----
		int			segmentsNumber;
		VString		textureFilename;
		VColorRef	trailColor;
		float		iniOffset;
		float		endOffset;

		
		//---- member vars ----
		bool					m_bEnabled;
		bool					m_bVisible;
		float					m_fLastTime;
		float					m_fTimeScale;
		VColorRef				m_iColor;
		hkvVec3					m_vRelStart;
		hkvVec3					m_vRelEnd;
		int						m_iHistoryPos;
		int						m_iHistoryCount;
		VTrailHistoryEntry*		m_History;
		VisMeshBufferPtr		m_spMesh;
		VisMeshBufferObjectPtr	m_spMeshObj;
		VisBaseEntity_cl*		m_pOwner;
		
};


//  Collection for handling playable character component
class CmpTrailEffect_Collection : public VRefCountedCollection<CmpTrailEffect> {};

//============================================================================================================
//  CmpTrailEffect_ComponentManager Class
//============================================================================================================
class CmpTrailEffect_ComponentManager : public IVisCallbackHandler_cl
{
	public:
		static CmpTrailEffect_ComponentManager &GlobalManager(){  return g_GlobalManager;  }

		void OneTimeInit()	{	Vision::Callbacks.OnUpdateSceneFinished += this;	} 
		void OneTimeDeInit(){	Vision::Callbacks.OnUpdateSceneFinished -= this;	} 

		VOVERRIDE void OnHandleCallback( IVisCallbackDataObject_cl *pData )
		{
			if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished )			
			{	
				const int iCount = m_Components.Count();
				for (int i=0;i<iCount;i++)
					m_Components.GetAt(i)->onFrameUpdate();
			}
		}
		
		inline CmpTrailEffect_Collection &Instances() { return m_Components; }

	protected:
		CmpTrailEffect_Collection m_Components;		
		static CmpTrailEffect_ComponentManager g_GlobalManager;

};


#endif  
