#ifndef CUTSHUMOTO_UTILITIES_H_INCLUDED
#define CUTSHUMOTO_UTILITIES_H_INCLUDED

#include "CutshumotoPluginPCH.h"

#define MAX_ENEMY_FILENAME_SIZE 60
#define MAX_TPJSONFILE_SIZE 16000 // TexturePacker json file output
#define MAX_FILE_SIZE 20000

class CutshumotoUtilities
{
	public:

	static int m_ilastSeed;

	static bool ReadFile( char* pcDestBuf, const char* filename, const char* path )
	{
		const_cast<char*>(filename)[strlen(filename)] = '\0';
		IVFileInStream* pFile = Vision::File.Open(filename, path);

		if (pFile)
		{	
			LONG size = pFile->GetSize();

			ASSERT( size < MAX_FILE_SIZE ); // Overextended max file size

			pFile->Read(pcDestBuf, size);
			pFile->Close();
				
			pcDestBuf[size] = '\0';
			return true;
		}
		else
		{
			pcDestBuf = 0;
			return false;
		}
	}

	static int GetRandomInt(int initNumber, int endNumber)
	{
		VRandom rand;		
		int seconds = VDateTime::Now().GetSecond(VDateTime::LOCAL);

		if (seconds == CutshumotoUtilities::m_ilastSeed)		
			seconds += 5000;			
		
		CutshumotoUtilities::m_ilastSeed = seconds;
	
		rand.Reset(seconds);	

		int value = static_cast<int>(hkvMath::round( rand.GetFloat() * (endNumber - initNumber)));

		return value + initNumber;
	}

	static void SetMainCameraPosAs(VString camKey)
	{
		VisBaseEntity_cl* pCam =  Vision::Game.SearchEntity(camKey);

		if (pCam)
		{
			Vision::Camera.GetMainCamera()->SetUseEulerAngles(true);
			Vision::Camera.GetMainCamera()->SetPosition(pCam->GetPosition());
			Vision::Camera.GetMainCamera()->SetOrientation(pCam->GetOrientation());
		}

	}

	static int uiLen( unsigned int uiNum ) 
	{
		if(uiNum>=1000000000) return 10;
		if(uiNum>=100000000) return 9;
		if(uiNum>=10000000) return 8;
		if(uiNum>=1000000) return 7;
		if(uiNum>=100000) return 6;
		if(uiNum>=10000) return 5;
		if(uiNum>=1000) return 4;
		if(uiNum>=100) return 3;
		if(uiNum>=10) return 2;
		return 1;
	}	

};

#endif