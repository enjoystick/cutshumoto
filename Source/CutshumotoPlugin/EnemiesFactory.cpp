//============================================================================================================
//  EnemiesFactory Class
//	Author: David Escalona Rocha
//============================================================================================================

#include "CutshumotoPluginPCH.h"
#include "EnemiesFactory.h"
#include "CutshumotoUtilities.h"
#include "CmpNormalBehaviour.h"
#include "CmpGhostBehaviour.h"
#include "CmpCivilBehaviour.h"
#include "CmpFatuoBehaviour.h"
#include "CmpNinjaBehaviour.h"
#include "CmpShurikenBehaviour.h"
#include "CmpSpiderlingBehaviour.h"
#include  "CmpKappaBehaviour.h"
#include  "CmpCometBehaviour.h"
#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/Rendering/Effects/BlobShadow.hpp>

#include "CmpSlicing.h"

// 05/08/14
#include <sstream>

const char* NORMAL_ENEMYFILE = "normalEnemy";
const char* NORMALFAST_ENEMYFILE = "normalFastEnemy";
const char* CIVILIAN_ENEMYFILE = "civilEnemy";
const char* ARMORED_ENEMYFILE = "armoredEnemy";
const char* FATUO_ENEMYFILE = "fatuoEnemy";
const char* GHOST_ENEMYFILE = "ghostEnemy";
const char* NINJA_ENEMYFILE = "ninjaEnemy";
const char* SHURIKEN_ENEMYFILE = "shurikenEnemy";
const char* SPIDERLING_ENEMYFILE = "SpiderlingEnemy";
const char* KAPPA_ENEMYFILE = "kappa";
const char* ARMOREDPLUS_ENEMYFILE = "tenguEnemy";
const char* COMET_ENEMYFILE = "cuervo";
const char* FIREBALL_ENEMYFILE = "fireball";

const char* ENEMYFILES_PATH = "EnemiesInfo";
const char* ALTERNATIVE_ENEMYFILES_PATH = "Assets\\EnemiesInfo";
const char* ANCHOR_MODEL = "Models\\anchor.model";

EnemiesFactory* EnemiesFactory::m_pEFinstance = NULL;

using namespace rapidjson;

EnemiesFactory::EnemiesFactory()
{
}

EnemiesFactory::~EnemiesFactory()
{	
	m_aLoadedEnemies.Reset();	
}


//===========================================================================
//  Function CreateEnemy
//	Returns an Entity with the properties and mesh asociated to the type
//===========================================================================

VisBaseEntity_cl* EnemiesFactory::CreateEnemy(eEnemyType eType, unsigned int iLevel, hkvVec3 vPos, bool noBehaviour )
{
	switch (eType)
	{
		case ENormal:		return CreateEnemyFromFile(NORMAL_ENEMYFILE, iLevel, vPos, noBehaviour);	break;
		case EArmored:		return CreateEnemyFromFile(ARMORED_ENEMYFILE ,iLevel, vPos, noBehaviour);	break;
		case ENormalFast:	return CreateEnemyFromFile(NORMALFAST_ENEMYFILE, iLevel, vPos, noBehaviour);break;
		case EGhost:		return CreateEnemyFromFile(GHOST_ENEMYFILE, iLevel, vPos, noBehaviour);		break;		
		case ECivilian:		return CreateEnemyFromFile(CIVILIAN_ENEMYFILE, iLevel, vPos, noBehaviour);	break;		
		case ELauncher:		return CreateEnemyFromFile(NINJA_ENEMYFILE, iLevel, vPos, noBehaviour);		break;
		case EShuriken:		return CreateEnemyFromFile(SHURIKEN_ENEMYFILE, iLevel, vPos, noBehaviour);	break;		
		case EFatuo:		return CreateEnemyFromFile(FATUO_ENEMYFILE, iLevel, vPos, noBehaviour);		break;	
		case ESpiderling:	return CreateEnemyFromFile(SPIDERLING_ENEMYFILE, iLevel, vPos, noBehaviour);break;
		case EArmoredPlus:	return CreateEnemyFromFile(ARMOREDPLUS_ENEMYFILE, iLevel, vPos,noBehaviour);break;
		case EComet:		return CreateEnemyFromFile(COMET_ENEMYFILE, iLevel, vPos, noBehaviour);		break;
		case EKappa:		return CreateEnemyFromFile(KAPPA_ENEMYFILE, iLevel, vPos, noBehaviour);		break;
		case EFireball:		return CreateEnemyFromFile(FIREBALL_ENEMYFILE, iLevel, vPos, noBehaviour);	break;

		default : return NULL;
	}
}
//===========================================================================
//  Function CreateAnchor
//	Returns an Anchor Entity with an enemy type and behaviour following Sergio's specifications
//===========================================================================
/*VisBaseEntity_cl* EnemiesFactory::CreateAnchor(eEnemyType eType, hkvVec3 vPos, hkvVec3 vSpeed, float fRangeCreation, CmpEnemyGenerator* pGeneratorCmp)
{
	//create anchor entity
	VisBaseEntity_cl *pAnchor = Vision::Game.CreateEntity("VisBaseEntity_cl", vPos, ANCHOR_MODEL);
	
	#ifndef _DEBUG
		pAnchor->SetVisibleBitmask(0);
	#endif
	
	//create anchor behaviour component and setup
	CmpAnchorBehaviour* pBehaviourCmp = new CmpAnchorBehaviour();

	pAnchor->AddComponent(pBehaviourCmp);

	pBehaviourCmp->SetAnchorType(eType);
	pBehaviourCmp->SetContDebug(static_cast<int>(eType));
	pBehaviourCmp->SetAnchorSpeed(vSpeed.y);
	pBehaviourCmp->SetRangeEnemyCreation(fRangeCreation);
	pBehaviourCmp->SetGenerateEnemiesInstance(pGeneratorCmp);
	pBehaviourCmp->SetRangeAnchorRemove(RANGE_ANCHORREMOVE);
	
	(eType == ENone) ? pBehaviourCmp->SetHasEnemy(false) : pBehaviourCmp->SetHasEnemy(true);	

	return pAnchor;

}
*/

//===========================================================================
//  Function CreateAnimationComponent
//	Returns an VTransitionStateMachine Component with the animations included in the entity model
//===========================================================================
VTransitionStateMachine* EnemiesFactory::CreateAnimationComponent(VisBaseEntity_cl* pEntity)
{
	VTransitionStateMachine* pStateMachineCmp = new VTransitionStateMachine();
	VTransitionTable *pTable = VTransitionManager::GlobalManager().CreateDefaultTransitionTable(pEntity->GetMesh());
		
	pStateMachineCmp->Init(pTable,true);		

	return pStateMachineCmp;
}

//===========================================================================
//  Function CreateEnemyFromFile
//	Create and returns an entity with a properties component, rigidbody and animation from a json file
//===========================================================================

//Nacho & Sergio 06/08/14: Modificado para que funcione con NombreEnemty_NumLevel
VisBaseEntity_cl* EnemiesFactory::CreateEnemyFromFile (const char* baseFilename, unsigned int iLevel, hkvVec3 vPos, bool noBehaviour)
{
	//Concatenamos
	char filename[MAX_ENEMY_FILENAME_SIZE];
	AppendLevelNum( filename, baseFilename, iLevel );

	char fileContent[MAX_FILE_SIZE];
	CmpEnemyProperties* pNewCmpProp = NULL;
	CmpEnemyProperties* pCmpCache = NULL;
	VisBaseEntity_cl* pEnemy;

	//check if this enemy was loaded
	pCmpCache = EnemyLoaded(filename);

	//if enemy previously loaded, clone stored component
	if (pCmpCache)
	{
		pNewCmpProp = ClonePropertiesComponent(pCmpCache);
	}
	else
	{
		//read enemy file
		CutshumotoUtilities::ReadFile(fileContent, filename, ENEMYFILES_PATH);
		if(!fileContent)
		{
			CutshumotoUtilities::ReadFile(fileContent, filename, ALTERNATIVE_ENEMYFILES_PATH);
			if(!fileContent) return NULL;		
		}		

		Document jsonDoc;
		jsonDoc.Parse<0>(fileContent);

		if (!jsonDoc.IsObject()) return NULL;

		//load properties from file		
		pNewCmpProp = CreatePropertiesComponent(jsonDoc);

		//store component is loaded enemies
		m_aLoadedEnemies[m_aLoadedEnemies.GetValidSize()] = pNewCmpProp;
	}
	
	//setup enemy entity at the position passed plus relative distance vector
	pEnemy = Vision::Game.CreateEntity( "VisBaseEntity_cl", hkvVec3(vPos) + pNewCmpProp->GetRelativeDistance(), pNewCmpProp->GetBaseModel());
			
	pEnemy->SetScaling(pNewCmpProp->GetScale());	
		
	//add properties, rigid body and behaviour
	pEnemy->AddComponent(pNewCmpProp);	
	//pEnemy->AddComponent(CreateRigidBodyComponent(pEnemy, hkpMotion::MOTION_KEYFRAMED)); // Deprecated

	if (!noBehaviour)
		pEnemy->AddComponent(CreateBehaviourComponent(pNewCmpProp->GetEnemyType()));	

	//create animation component
	if (pNewCmpProp->GetAnimated())
		pEnemy->AddComponent(CreateAnimationComponent(pEnemy));

	//set civil and fatuo as friends not enemies
	if (pNewCmpProp->GetEnemyType() == ECivilian || pNewCmpProp->GetEnemyType() == EFatuo || pNewCmpProp->GetEnemyType() == EComet)
		pEnemy->SetEntityKey(FRIEND_ENT_KEY );
	else
		pEnemy->SetEntityKey( ENEMY_ENT_KEY );

	// test for generate blow shadow for each enemy created... it works
	VBlobShadow* pBlow = new VBlobShadow();		
	pBlow->SetShadowTexture("Textures\\blobShadow.dds");
	pBlow->SetSize(100,20);
	pBlow->SetEnabled();
	pEnemy->AddComponent(pBlow);	
	
	// Add trace accuracy to recognice entitites in trace operations
	pEnemy->SetTraceAccuracy( VIS_TRACEACC_OBOX ); // Deprecated

	// for enemies created from c++ code
	reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( pEnemy );

	return pEnemy;
}

//===========================================================================
//  Function CreateBehaviourComponent
//	create a behaviour component depend of enemy type
//===========================================================================

CmpEnemyBehaviour* EnemiesFactory::CreateBehaviourComponent(eEnemyType eType)
{
	switch (eType)
	{
		case ENormal:		return new CmpNormalBehaviour();	
		case EArmored:		return new CmpNormalBehaviour();	
		case ECivilian:		return new CmpCivilBehaviour();	
		case EGhost:		return new CmpGhostBehaviour();
		case EFatuo:		return new CmpFatuoBehaviour();
		case ELauncher:		return new CmpNinjaBehaviour();
		case EShuriken:		return new CmpShurikenBehaviour();
		case ESpiderling:	return new CmpSpiderlingBehaviour();
		case EComet:		return new CmpCometBehaviour();
		case EKappa:		return new CmpKappaBehaviour();
		case EArmoredPlus:	return new CmpNormalBehaviour();
		case EFireball:		return new CmpShurikenBehaviour(true);
		//...
	}

	return NULL;
}

//===========================================================================
//  Function EnemyLoaded
//	Search for an Enemy by eEnemyType in the preloaded enemies array
//===========================================================================

CmpEnemyProperties* EnemiesFactory::EnemyLoaded(eEnemyType eType)
{
	for (unsigned char i = 0; i < m_aLoadedEnemies.GetValidSize(); i++)
	{
		if (m_aLoadedEnemies[i]->GetEnemyType() == eType)
			return m_aLoadedEnemies[i];
	}

	return NULL;
}

//===========================================================================
//  Function EnemyLoaded
//	Search for an Enemy by json asociated file in the preloaded enemies array
//===========================================================================
CmpEnemyProperties* EnemiesFactory::EnemyLoaded(const char* filename)
{
	VString sFile(filename);

	for (unsigned char i = 0; i < m_aLoadedEnemies.GetValidSize(); i++)
	{		
		if (m_aLoadedEnemies[i])
		{

			if (m_aLoadedEnemies[i]->GetFilename())
			{	VString sLoadedFile = m_aLoadedEnemies[i]->GetFilename();
			
				if (sFile == sLoadedFile)
					return m_aLoadedEnemies[i];
			}
		}
	}

	return NULL;
}


//===========================================================================
//  Function CreateRigidBodyComponent
//	create and returns a rigidbody component (Convex Hull) for an entity
//===========================================================================
vHavokRigidBody* EnemiesFactory::CreateRigidBodyComponent(VisBaseEntity_cl* pEntity, hkpMotion::MotionType eType, bool noCollide)
{
	vHavokRigidBody *pRigidBody = new vHavokRigidBody();

	pRigidBody->Initialize();
	vHavokRigidBody::InitTemplate initTempl;

	pRigidBody->InitConvexRb(pEntity->GetMesh(), pEntity->GetScaling(), initTempl);
	pRigidBody->SetMotionType(eType);

	if (noCollide)
		pRigidBody->GetHkRigidBody()->setCollisionFilterInfo(COLLISION_FILTER_GROUP);	

	return pRigidBody;
}

int EnemiesFactory::GetNewFilterCollisionInfo()
{	
	hkUint32 g = m_FilterGroup.getNewSystemGroup();
	hkUint32 collisionFilterInfo = hkpGroupFilter::calcFilterInfo( 30, g, 29 );

	return collisionFilterInfo;
}

VisBaseEntity_cl* EnemiesFactory::CreateEnemyPiece(const char* sPieceName, hkvVec3 vPos, float fScale, bool noCollide)
{
	VisBaseEntity_cl* pPiece = Vision::Game.CreateEntity("VisBaseEntity_cl", vPos, sPieceName);
	pPiece->SetScaling(fScale);
	pPiece->SetEntityKey( SLICEDCHUNK_ENT_KEY );
	
	pPiece->AddComponent(CreateRigidBodyComponent(pPiece, hkpMotion::MOTION_DYNAMIC, noCollide));
	
	return pPiece;
}

//load enemy properties from json parsed doc and fill a new EnemyProperties component
CmpEnemyProperties* EnemiesFactory::CreatePropertiesComponent(rapidjson::Document& jsonDoc)
{
	CmpEnemyProperties* pCmp = new CmpEnemyProperties();
		
	Value& properties = jsonDoc["properties"];
	SizeType pos = 0;

	//behaviour properties
	if (properties[pos].HasMember("type"))				pCmp->SetEnemyType(static_cast<eEnemyType> (properties[pos]["type"].GetInt()));
	if (properties[pos].HasMember("lifes"))				pCmp->SetLifePoints(properties[pos]["lifes"].GetInt());
	if (properties[pos].HasMember("scale"))				pCmp->SetScale(static_cast<float>(properties[pos]["scale"].GetDouble()));
	if (properties[pos].HasMember("SliceDirection"))	pCmp->SetSliceDir(static_cast<eSliceDirection> (properties[pos]["SliceDirection"].GetInt()));
	if (properties[pos].HasMember("animated"))			pCmp->SetAnimated(static_cast<bool>(properties[pos]["animated"].GetInt()));
	if (properties[pos].HasMember("Tempo"))				pCmp->SetTempo(static_cast<float>(properties[pos]["Tempo"].GetDouble()));
	if (properties[pos].HasMember("Cooldown"))			pCmp->SetCooldown(static_cast<float>(properties[pos]["Cooldown"].GetDouble()));
	if (properties[pos].HasMember("score"))				pCmp->SetScore(properties[pos]["score"].GetInt());
	if (properties[pos].HasMember("speed"))
	{
		Value& speed = properties[pos]["speed"];
		hkvVec3 vSpeed;
		SizeType i = 0;

		vSpeed.x = static_cast<float>(speed[i].GetDouble());	i++;
		vSpeed.y = static_cast<float>(speed[i].GetDouble());	i++;
		vSpeed.z = static_cast<float>(speed[i].GetDouble());

		pCmp->SetSpeed(vSpeed);
	}

	if (properties[pos].HasMember("relativeDistance"))
	{
		Value& relDistance = properties[pos]["relativeDistance"];
		hkvVec3 vRelDist;
		SizeType i = 0;

		vRelDist.x = static_cast<float>(relDistance[i].GetDouble());	i++;
		vRelDist.y = static_cast<float>(relDistance[i].GetDouble());	i++;
		vRelDist.z = static_cast<float>(relDistance[i].GetDouble());

		pCmp->SetRelativeDistance(vRelDist);
	}

	//mesh properties
	if (properties[pos].HasMember("baseModel"))		pCmp->SetBaseModel(properties[pos]["baseModel"].GetString());
	//Modificado para que aun no hay trozos
	if (properties[pos].HasMember("topSide"))		pCmp->GetModelPieces()[eTopSide] = properties[pos]["topSide"].GetString();
	if (properties[pos].HasMember("bottomSide"))	pCmp->GetModelPieces()[eBottomSide] = properties[pos]["bottomSide"].GetString();
	if (properties[pos].HasMember("leftSide"))		pCmp->GetModelPieces()[eLeftSide] = properties[pos]["leftSide"].GetString();
	if (properties[pos].HasMember("rightSide"))		pCmp->GetModelPieces()[eRightSide] = properties[pos]["rightSide"].GetString();
	if (properties[pos].HasMember("upRightSide"))	pCmp->GetModelPieces()[eUpRightSide] = properties[pos]["upRightSide"].GetString();
	if (properties[pos].HasMember("upLeftSide"))	pCmp->GetModelPieces()[eUpLeftSide] = properties[pos]["upLeftSide"].GetString();
	if (properties[pos].HasMember("downLeftSide"))	pCmp->GetModelPieces()[eDownLeftSide] = properties[pos]["downLeftSide"].GetString();
	if (properties[pos].HasMember("downRightSide"))	pCmp->GetModelPieces()[eDownRightSide] = properties[pos]["downRightSide"].GetString();

	//Sergio_Gomez(05_08_14): Modificado, nueva variable numLevel
	if (properties[pos].HasMember("numLevel"))		pCmp->SetNumLevel((unsigned int) properties[pos]["numLevel"].GetInt());

	// Concat levelNum
	char filename[MAX_ENEMY_FILENAME_SIZE];
	AppendLevelNum( filename, GetEnemyPropertiesFile(pCmp->GetEnemyType()), pCmp->GetNumLevel() );

	pCmp->SetFilename( filename );
	
	return pCmp;

}

CmpEnemyProperties* EnemiesFactory::EnemyLoaded(unsigned int iLevel)
{
	for (unsigned char i = 0; i < m_aLoadedEnemies.GetValidSize(); i++)
	{
		if (m_aLoadedEnemies[i]->GetNumLevel() == iLevel)
			return m_aLoadedEnemies[i];
	}

	return NULL;
}
//load in memory mesh data and properties of a particular enemy type
bool EnemiesFactory::PreloadEnemy(eEnemyType eType, unsigned int iLevel)
{	
	// Get enemy json filename from certain level
	char filename[MAX_ENEMY_FILENAME_SIZE];
	AppendLevelNum( filename, GetEnemyPropertiesFile( eType ), iLevel );

	if (EnemyLoaded(eType))
	{
		if (EnemyLoaded(iLevel))
		{
			return true;
		}
		else
		{
			// Re-set property values from a existing entity
			char fileContent[MAX_FILE_SIZE];	
			CmpEnemyProperties* pCmpProp = NULL;

			//read enemy file
			CutshumotoUtilities::ReadFile(fileContent, filename, ENEMYFILES_PATH);
			if(!fileContent)	
			{
				CutshumotoUtilities::ReadFile(fileContent, filename, ALTERNATIVE_ENEMYFILES_PATH);		
				if(!fileContent) return NULL;
			}

			//parse json file
			Document jsonDoc;
			jsonDoc.Parse<0>(fileContent);

			if (!jsonDoc.IsObject())	return false;

			//load properties from file
			pCmpProp = CreatePropertiesComponent(jsonDoc);

			for ( unsigned int i = 0; i < m_aLoadedEnemies.GetValidSize(); i++ )
			{
				if ( m_aLoadedEnemies[i]->GetEnemyType() == eType )
				{
					delete m_aLoadedEnemies[i];
					m_aLoadedEnemies[i] = pCmpProp;
				}
			}
		}
	}
	else
	{
		// Create entity, load model and property values
		char fileContent[MAX_FILE_SIZE];	
		CmpEnemyProperties* pCmpProp = NULL;	

		//read enemy file
		CutshumotoUtilities::ReadFile(fileContent, filename, ENEMYFILES_PATH);
		if(!fileContent)	
		{
			CutshumotoUtilities::ReadFile(fileContent, filename, ALTERNATIVE_ENEMYFILES_PATH);		
			if(!fileContent) return NULL;
		}

		//parse json file
		Document jsonDoc;
		jsonDoc.Parse<0>(fileContent);

		if (!jsonDoc.IsObject())	return false;

		//load properties from file
		pCmpProp = CreatePropertiesComponent(jsonDoc);
	
		//preload geometry in order to avoid slowdowns		
		if (pCmpProp->GetBaseModel())
		{
			Vision::Game.CreateEntity( "VisBaseEntity_cl", hkvVec3(0,0,-5000), pCmpProp->GetBaseModel());
			//VMeshManager::GetMeshManager()->LoadDynamicMeshFile(pCmpProp->GetBaseModel());
		}

		for (unsigned char i = 0; i < 8; i++)
		{	
			VString piece = pCmpProp->GetModelPieces()[i];

			if (!piece.IsEmpty())
				Vision::Game.CreateEntity( "VisBaseEntity_cl", hkvVec3(0,0,-5000),piece.AsChar());
				//VMeshManager::GetMeshManager()->LoadDynamicMeshFile(piece.AsChar());
		}
		//save component
		m_aLoadedEnemies[m_aLoadedEnemies.GetValidSize()] = pCmpProp;
	}

	return true;
}

//create a new EnemiesProperties component using a preload data
CmpEnemyProperties* EnemiesFactory::ClonePropertiesComponent(CmpEnemyProperties* pCmp)
{
	if (!pCmp) return NULL;

	CmpEnemyProperties* pNewCmp = new CmpEnemyProperties();

	pNewCmp->SetSpeed(pCmp->GetSpeed());
	pNewCmp->SetRelativeDistance(pCmp->GetRelativeDistance());
	pNewCmp->SetLifePoints(pCmp->GetLifePoints());
	pNewCmp->SetEnemyType(pCmp->GetEnemyType());
	pNewCmp->SetSliceDir(pCmp->GetSliceDir());
	pNewCmp->SetScale(pCmp->GetScale());
	pNewCmp->SetBaseModel(pCmp->GetBaseModel());
	pNewCmp->SetFilename(pCmp->GetFilename());
	pNewCmp->SetAnimated(pCmp->GetAnimated());
	pNewCmp->SetTempo(pCmp->GetTempo());
	pNewCmp->SetCooldown(pCmp->GetCooldown());
	pNewCmp->SetScore(pCmp->GetScore());
	//Sergio_Gomez(05_08_14): Modificado, nueva variable numLevel
	pNewCmp->SetNumLevel(pCmp->GetNumLevel());

	for (unsigned char i = 0; i < 8; i++)
		pNewCmp->GetModelPieces()[i] = pCmp->GetModelPieces()[i];

	return pNewCmp;
}

//returns the properties file of an enemy type
const char* EnemiesFactory::GetEnemyPropertiesFile(eEnemyType eType)
{
	switch (eType)
	{
		case ENormal:		return NORMAL_ENEMYFILE;		break;
		case EArmored:		return ARMORED_ENEMYFILE;		break;
		case ENormalFast:	return NORMALFAST_ENEMYFILE;	break;
		case EGhost:		return GHOST_ENEMYFILE;			break;
		case ECivilian:		return CIVILIAN_ENEMYFILE;		break;		
		case ELauncher:		return NINJA_ENEMYFILE;			break;
		case EShuriken:		return SHURIKEN_ENEMYFILE;		break;
		case EFatuo:		return FATUO_ENEMYFILE;			break;
		case ESpiderling:	return SPIDERLING_ENEMYFILE;	break;
		case EArmoredPlus:	return ARMOREDPLUS_ENEMYFILE;	break;
		case EComet:		return COMET_ENEMYFILE;			break;
		case EKappa:		return KAPPA_ENEMYFILE;			break;
		case EFireball:		return FIREBALL_ENEMYFILE;		break;

		default:			return NULL;
	}

}

//Sergio & Nacho (10/08/14): Concatena al filename con el levelNum
void EnemiesFactory::AppendLevelNum( char* pcDestBuf, const char* pcSrc1BaseFilename, unsigned int uiSrc2LevelNum )
{
	if ( !isdigit( pcSrc1BaseFilename[ strlen( pcSrc1BaseFilename ) - 6 ] ) )
	{
		std::ostringstream ss;
		ss << const_cast<char*>( pcSrc1BaseFilename ) << "_" << uiSrc2LevelNum << ".json";
		int baseFilenameLen = strlen( ss.str().c_str() );
		strncpy( pcDestBuf, ss.str().c_str(), baseFilenameLen );
		pcDestBuf[ baseFilenameLen ] = '\0';
	}
	else
		strcpy_s( pcDestBuf, MAX_ENEMY_FILENAME_SIZE, pcSrc1BaseFilename );
}

//Sergio 10_08_14: Ultimate anti-remove Bug, eliminamos las entidades de SlicedEntities antes de borrarlas por trigger
void EnemiesFactory::RemoveFromSlicedEntities( VisTypedEngineObject_cl* pEntity )
{
	//Remove this entity from sliced entities array
	reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->UntrackSlicedEntity( pEntity );
}

void EnemiesFactory::ResetEnemyLoaded()
{
	for ( unsigned int i = 0; i < m_aLoadedEnemies.GetValidSize(); i++ )
	{
			delete  m_aLoadedEnemies[ i ];
			 m_aLoadedEnemies.Remove(i);
	}
	m_aLoadedEnemies.Reset();
	m_aLoadedEnemies.Pack();
	m_aLoadedEnemies.AdjustSize();
}