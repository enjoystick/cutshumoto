//============================================================================================================
//  EnemiesFactory Class
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef ENEMIESFACTORY_H_INCLUDED
#define ENEMIESFACTORY_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "rapidjson/document.h"
#include "GlobalTypes.h"
#include "CmpEnemyProperties.h"
#include "CmpEnemyBehaviour.h"


class EnemiesFactory
{
	public:
		static EnemiesFactory*		Instance() { if ( m_pEFinstance == 0 ) m_pEFinstance = new EnemiesFactory(); return m_pEFinstance; }
		bool						PreloadEnemy(eEnemyType eType, unsigned int iLevel);
		VisBaseEntity_cl*			CreateEnemy(eEnemyType eType, unsigned int iLevel,hkvVec3 vPos = hkvVec3(0,0,0), bool noBehaviour = false);
		vHavokRigidBody*			CreateRigidBodyComponent(VisBaseEntity_cl* pEntity, hkpMotion::MotionType eType, bool noCollide = false);
		static const char*			GetEnemyPropertiesFile(eEnemyType eType);
		VisBaseEntity_cl*			CreateEnemyPiece(const char* sPieceName, hkvVec3 vPos, float vScale,  bool noCollide = false);
		void						RemoveFromSlicedEntities(VisTypedEngineObject_cl *pOwner);
		void						ResetEnemyLoaded();
		int							GetNewFilterCollisionInfo();

	protected:
		EnemiesFactory();
		~EnemiesFactory();

	private:
		VisBaseEntity_cl*			CreateEnemyFromFile(const char* filename, unsigned int iLevel, hkvVec3 vPos,bool noBehaviour = false);		
		VTransitionStateMachine*	CreateAnimationComponent(VisBaseEntity_cl* pEntity);
		CmpEnemyBehaviour*			CreateBehaviourComponent(eEnemyType eType);
		CmpEnemyProperties*			CreatePropertiesComponent(rapidjson::Document& jsonDoc);
		CmpEnemyProperties*			ClonePropertiesComponent(CmpEnemyProperties* pCmp);
		CmpEnemyProperties*			EnemyLoaded(eEnemyType eType);
		CmpEnemyProperties*			EnemyLoaded(const char* filename);
		CmpEnemyProperties*			EnemyLoaded(unsigned int iLevel);
		void						AppendLevelNum(char* destBuf, const char* baseFilename, unsigned int uiLevelNum);

		static EnemiesFactory*				m_pEFinstance;
		DynArray_cl<CmpEnemyProperties*>	m_aLoadedEnemies;
		hkpGroupFilter						m_FilterGroup;
};

#endif