#include "CutshumotoPluginPCH.h"
#include "EnemyBehaviour.h"

V_IMPLEMENT_SERIAL(EnemyBehaviour,IVObjectComponent,0,Vision::GetEngineModule());

//MyComponent_ComponentManager GenerateEnemies_ComponentManager::g_GlobalManager;




EnemyBehaviourManager EnemyBehaviourManager::g_GlobalManager;

void EnemyBehaviour::SetOwner( VisTypedEngineObject_cl *pOwner )
{
  IVObjectComponent::SetOwner( pOwner );

  // (de-)register our component so it is handled by the global manager
   if (pOwner!=NULL)
  {
    EnemyBehaviourManager::GlobalManager().Instances().AddUnique(this);
    onStartup( pOwner );
  }
  else
  {
    onRemove( pOwner );
    EnemyBehaviourManager::GlobalManager().Instances().SafeRemove(this);    
  }

}

void EnemyBehaviour::onRemove(  VisTypedEngineObject_cl *pOwner )
{
  //  do the component Removal code here.......
  //  [...]
}

void EnemyBehaviour::onStartup( VisTypedEngineObject_cl *pOwner )
{
	float	fromPosZSwipeRange	= 0;
	float	visibilityRange		= 40;
	float	ambushRangeAttack	= 5;
	bool	isAdded				= false;
	bool	isRemoved			= false;
	bool	isLateralInPos		=false;
	shurikenDianaBool=false;
	velShuriken;
	enemyStopped=false;
	hkvVec3 vPos = ((VisObject3D_cl *)GetOwner())->GetPosition();
	if(vPos.x>0)
	{
		derOIzq=true;
	}
	else
	{
		derOIzq=false;
	}

  
}

void EnemyBehaviour::onFrameUpdate()
{
	VisBaseEntity_cl* pOwner;

	pOwner = (VisBaseEntity_cl*)GetOwner();

	if (!pOwner)	return;

	stateMachine = pOwner->Components().GetComponentOfType<VTransitionStateMachine>();
	
	hkvVec3 vPos = pOwner->GetPosition();

	float elapsed=Vision::GetTimer()->GetTimeDifference();
	if(mEnemyType==0 || mEnemyType==1 || mEnemyType==2 || mEnemyType==5)
	{
		if(vPos.y<mRangeEnemyAttack && stateMachine)
		{	
			
			stateMachine->SetState("Attack");
		}
		
		pOwner->SetPosition(vPos.x,vPos.y - (speed * elapsed),vPos.z);
	}
	if(mEnemyType==3)
	{
		if(timerAttack< Vision::GetTimer()->GetTime())
			((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x-speed*elapsed,vPos.y-speedMap*elapsed,vPos.z);
	}
	//SERGIO 15/06/14 A�adidos o cambiados tipo 4,6,41
	if(mEnemyType==4 )
	{
		if(enemyStopped==false)
		{
			((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x,vPos.y-speed*elapsed,vPos.z);
			if((timerAttack< Vision::GetTimer()->GetTime()))
			{
				if (stateMachine)
				{
					stateMachine->SetState("Attack");
					if(vPos.y>-900)
						instanceGenerate->CreateEnemy(EShuriken, vPos, hkvVec3(0,ANCHOR_Y_SPEED,0));
					enemyStopped=true;
					timerAttack=Vision::GetTimer()->GetTime()+tempo;
				}
			}
		}
		if(enemyStopped==true)
		{
			((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x,vPos.y-speedMap*elapsed,vPos.z);
			if((timerAttack< Vision::GetTimer()->GetTime()))
			{
				stateMachine->SetState("Run");
				enemyStopped=false;
				timerAttack=Vision::GetTimer()->GetTime()+cooldown;
			}
		}
	}
	
	if(mEnemyType==6)
	{
	if(enemyStopped==false)
		{
			((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x,vPos.y-speed*elapsed,vPos.z);
			if((timerAttack< Vision::GetTimer()->GetTime()))
			{
				VisBaseEntity_cl* pEntity= (VisBaseEntity_cl*)this->GetOwner();
				pEntity->SetVisibleBitmask(0);
				enemyStopped=true;
				timerAttack=Vision::GetTimer()->GetTime()+tempo;
			}
		}
	if(enemyStopped==true)
		{
			((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x,vPos.y-speedMap*elapsed,vPos.z);
			if((timerAttack< Vision::GetTimer()->GetTime()))
			{
				int randomizer=VRandom().GetInt()%4;
				((VisObject3D_cl *)GetOwner())->SetPosition(randomizer*100-150,vPos.y,vPos.z);
				VisBaseEntity_cl* pEntity= (VisBaseEntity_cl*)this->GetOwner();
				pEntity->SetVisibleBitmask(1);
				enemyStopped=false;
				timerAttack=Vision::GetTimer()->GetTime()+cooldown;
			}
		}
	}

	if(mEnemyType==41)
	{
		if(!shurikenDianaBool)
		{
			shurikenDianaBool=true;
			hkvVec3 dianaShuriken = hkvVec3(VRandom().GetInt()%20-10,-1074.85,-288.79+VRandom().GetInt()%20-10);
			//velShuriken= dianaShuriken - vPos;
			velShuriken= hkvVec3(dianaShuriken.x-vPos.x,dianaShuriken.y-vPos.y,dianaShuriken.z-vPos.z);
			velShuriken.normalize();
		}
		float speedy= speed;
		hkvVec3 Shurikeny= velShuriken;
		((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x+velShuriken.x*speed*elapsed,vPos.y+velShuriken.y*speed*elapsed,vPos.z+velShuriken.z*speed*elapsed);
		
		
		/*if(derOIzq)
		{
			if(vPos.x<-75)
				derOIzq=false;
			else
			((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x-speed*elapsed,vPos.y-speed*elapsed,vPos.z);
		}
		if(!derOIzq)
		{
			if(vPos.x>75)
				derOIzq=true;
			else
			((VisObject3D_cl *)GetOwner())->SetPosition(vPos.x+speed*elapsed,vPos.y-speed*elapsed,vPos.z);
		}*/
	}
	#ifdef _DEBUG
	char prueba[((((sizeof contDebug)+(sizeof contDebug2)+(sizeof contDebug3)) * CHAR_BIT) + 2)/3 + 2];
	sprintf(prueba, "%d :: %d :: %d", contDebug, contDebug2, contDebug3);
	const char* prueba2= prueba;
	Vision::Message.SetTextColor(V_RGBA_GREEN);
	Vision::Message.DrawMessage3D(prueba2,vPos);
	Vision::Message.SetTextColor(V_RGBA_WHITE);
	#endif
	if(vPos.y<mRangeEnemyRemove)
	{
		VisBaseEntity_cl* pruebaDispose = (VisBaseEntity_cl*)this->GetOwner();
		pruebaDispose->Remove();
		
	}


	if(vPos.y<mRangeEnemyRemove)
	{
		VisBaseEntity_cl* pruebaDispose = (VisBaseEntity_cl*)this->GetOwner();
		pruebaDispose->Remove();
		
	}

}

void EnemyBehaviour::SetSpeed(float in)
{
	speed=in;
}

void EnemyBehaviour::SetAnchor(VisTypedEngineObject_cl* in)
{
	anchorAttached=in;
}