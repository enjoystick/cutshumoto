#ifndef ENEMYBEHAVIOUR_H_INCLUDED
#define ENEMYBEHAVIOUR_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "CmpEnemyGenerator.h"
#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/Animation/Transition/VTransitionManager.hpp>

// Versions
#define MYCOMPONENT_VERSION_0          0     // Initial version
#define MYCOMPONENT_VERSION_CURRENT    1     // Current version

class EnemyBehaviour : public IVObjectComponent
{
	public:
		V_DECLARE_SERIAL( EnemyBehaviour, SAMPLEPLUGIN_IMPEXP );


		SAMPLEPLUGIN_IMPEXP void onFrameUpdate();
		SAMPLEPLUGIN_IMPEXP void onStartup( VisTypedEngineObject_cl *pOwner );
		SAMPLEPLUGIN_IMPEXP void onRemove(  VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP VOVERRIDE void SetOwner( VisTypedEngineObject_cl *pOwner );

		SAMPLEPLUGIN_IMPEXP VOVERRIDE BOOL CanAttachToObject( VisTypedEngineObject_cl *pObject, VString &sErrorMsgOut )
		{
			if ( !IVObjectComponent::CanAttachToObject( pObject, sErrorMsgOut ))return FALSE;  

			//  Define criteria here that allows the editor to attach this component to 
			//  the passed object.In this example, the object should be derived from 
			//  VisObject3D_cl eg it can be positionable. 
			if (!pObject->IsOfType( V_RUNTIME_CLASS( VisObject3D_cl )))
			{
				sErrorMsgOut = "Component can only be added to instances of VisObject3D_cl or derived classes.";
				return FALSE;
			}
			return TRUE;
		}

		void SetContDebug(int in){contDebug=in;}
		void SetContDebug2(int in){contDebug2=in;}
		void SetContDebug3(int in){contDebug3=in;}
		void SetSpeed(float in);
		void SetAnchor(VisTypedEngineObject_cl* in);
		void SetType(int in){mEnemyType=in;}
		void SetTimerAttack( float in){timerAttack=in;}
		void SetTempo(float in){tempo=in;}
		void SetCooldown(float in){cooldown=in;}
		void SetSpeedMap(float in){speedMap=in;}
		void SetRangeEnemyRemove(float in){mRangeEnemyRemove=in;}
		void SetRangeEnemyAttack(float in){mRangeEnemyAttack=in;}
		//SERGIO 15/06/14 A�adida variable instanceGenerate
		void SetGenerateEnemiesInstance(CmpEnemyGenerator* in){instanceGenerate=in;}
		void SetStateMachine(VTransitionStateMachine* in){stateMachine=in;}
		VTransitionStateMachine* GetStateMachine(){return stateMachine;}
		
		bool isAlivetrue;
		float speed;
		VisTypedEngineObject_cl* anchorAttached;
		
		//public GenerateEnemies::eEnemyType eType;
		//Mirar como se implementa en c++
	protected:
		CmpEnemyGenerator* instanceGenerate;
		VTransitionStateMachine* stateMachine;
		float 			fromPosZSwipeRange 			;
		float 			visibilityRange				;
		float 			ambushRangeAttack			;
		float			timerAttack					;
		float	tempo;
		float	cooldown;
		float speedMap;
		bool 			isAdded 					;
		bool 			isRemoved 					;
		bool			isLateralInPos				;
		bool	enemyStopped;
		int contDebug;
		int contDebug2;
		int contDebug3;
		int mEnemyType;
		float mRangeEnemyRemove;
		float mRangeEnemyAttack;
		void ChangeMaterial();

		bool shurikenDianaBool;
		hkvVec3 velShuriken;
		bool derOIzq;

		

};

class EnemyBehaviour_Collection : public VRefCountedCollection<EnemyBehaviour> {};

class EnemyBehaviourManager : public IVisCallbackHandler_cl
{
public:
  // should be called at plugin initialization time
	static EnemyBehaviourManager &GlobalManager(){  return g_GlobalManager;  }
  void OneTimeInit()
  {
    Vision::Callbacks.OnUpdateSceneFinished += this; // listen to this callback
  }

  // should be called at plugin de-initialization time
  void OneTimeDeInit()
  {
    Vision::Callbacks.OnUpdateSceneFinished -= this; // de-register
  }

  // implements IVisCallbackHandler_cl
  VOVERRIDE void OnHandleCallback(IVisCallbackDataObject_cl *pData)
  {
    VASSERT(pData->m_pSender==&Vision::Callbacks.OnUpdateSceneFinished);

    // call update function on every component
    const int iCount = m_Components.Count();
    for (int i=0;i<iCount;i++)
      m_Components.GetAt(i)->onFrameUpdate();
  }

  inline EnemyBehaviour_Collection  &Instances() { return m_Components; }
  // holds the collection of all instances of MyComponent
  

  // one global instance of our manager
	protected:
		EnemyBehaviour_Collection  m_Components;
		static EnemyBehaviourManager g_GlobalManager;
};



#endif