#include "CutshumotoPluginPCH.h"
#include "EnemyFactoryLUA.h"
#include "CmpSlicing.h"
#include "CmpEnemyProperties.h"
#include "SoundManager.h"
#include "GameManager.h"
#include <..\Source\Vision\Runtime\EnginePlugins\ThirdParty\FmodEnginePlugin\VFmodManager.hpp> 

#include <..\Source\Vision\Runtime\Engine\System\VisApiPersistentData.hpp>

#include <sstream>
#include <iostream>
#include <string> 

V_IMPLEMENT_SERIAL(EnemyFactoryLUA, VisBaseEntity_cl, 0, &g_myComponentModule);

void EnemyFactoryLUA::InitFunction() 
{
	
}

int	EnemyFactoryLUA::GetEntityLifePoints(char* entityKey)
{
	int retonno = -1;

	if (entityKey)
	{
		VisBaseEntity_cl* pEntity = Vision::Game.SearchEntity(entityKey);
		if (pEntity)
		{
			CmpEnemyProperties* cmp = pEntity->Components().GetComponentOfType<CmpEnemyProperties>();
			if (cmp)
				retonno =  cmp->GetLifePoints();
		}

	}

	return retonno;
}

VisBaseEntity_cl* EnemyFactoryLUA::CreateEnemy(int eType, unsigned int iLevel, hkvVec3 vPos)
{
	VisBaseEntity_cl* pEnemy = EnemiesFactory::Instance()->CreateEnemy(static_cast<eEnemyType>(eType), iLevel, vPos);
	//reinterpret_cast<CmpSlicing*>(BB::pcmpSlicing)->ManageSliceAreaEntity( pEnemy );
	PlayerStats::Instance()->AddScoreMax(pEnemy->Components().GetComponentOfType<CmpEnemyProperties>()->GetScore());
	return pEnemy;
}
//Sergio Gomez &Nacho(05/08/14): A�adido parametro iLevel
bool EnemyFactoryLUA::PreloadEnemy(int eType, unsigned int iLevel)
{
	return EnemiesFactory::Instance()->PreloadEnemy(static_cast<eEnemyType>(eType),  iLevel);	
}

void EnemyFactoryLUA::SetBossFinish(bool finish)
{
	PlayerStats::Instance()->SetBossFinish(finish);
}

bool EnemyFactoryLUA::GetBossFinish()
{
	return PlayerStats::Instance()->GetBossFinish();
}

int	EnemyFactoryLUA::GetCurrentCombo()
{
	return PlayerStats::Instance()->GetCurrentCombo();
}

unsigned int EnemyFactoryLUA::GetScore()
{
	return PlayerStats::Instance()->GetScore();
}
void EnemyFactoryLUA::AddScore(int score)
{
	PlayerStats::Instance()->AddScore(score);
}
void EnemyFactoryLUA::DamagePlayer(int damage)
{
	PlayerStats::Instance()->DamagePlayer(damage);
}

void EnemyFactoryLUA::HealPlayer(int lifes )
{
	PlayerStats::Instance()->HealPlayer(lifes);
}

int EnemyFactoryLUA::GetPlayerLifes()
{
	return PlayerStats::Instance()->GetPlayerLifes();
}

void EnemyFactoryLUA::ResetStats(int initialLifes)
{
	PlayerStats::Instance()->ResetStats(initialLifes);
}

bool EnemyFactoryLUA::SaveStats(char* filename, char* sceneName)
{
	//save data to file
	return PlayerStats::Instance()->SaveStats(filename, sceneName);
}

//06/08/14 Sergio:A�adido por Swig un SetCanBeSliced para el trigger
void EnemyFactoryLUA::SetCanBeSliced(IVObjectComponent* prop, bool cut)
{
	CmpEnemyProperties* _prop = (CmpEnemyProperties*) prop;
	_prop->SetCanBeSliced(cut);
}

void EnemyFactoryLUA::SetCanBeSliced(VisBaseEntity_cl *  prop, bool cut)
{
		CmpEnemyProperties* _prop = (CmpEnemyProperties*) prop;
	_prop->SetCanBeSliced(cut);
}

void EnemyFactoryLUA::RemoveFromSlicedEntities(VisTypedEngineObject_cl* pEntity)
{
	return EnemiesFactory::Instance()->RemoveFromSlicedEntities( pEntity );
}

void EnemyFactoryLUA::OnEndOfLevelReached() 
{
	MyGameManager::GlobalManager().OnEndOfLevelReached();
}

bool EnemyFactoryLUA::PreloadSound(int soundType)
{
	if (SoundManager::Instance()->LoadSound(static_cast<eSounds>(soundType)))
		return true;
	else
		return false;
}

void EnemyFactoryLUA::AppendLevelNum( char* pcDestBuf, unsigned int levelNum)
{
	/*if ( !isdigit( pcSrc1BaseFilename[ strlen( pcSrc1BaseFilename ) - 6 ] ) )
	{
		std::ostringstream ss;
		ss << const_cast<char*>( pcSrc1BaseFilename ) << "_" << uiSrc2LevelNum << ".json";
		int baseFilenameLen = strlen( ss.str().c_str() );
		strncpy( pcDestBuf, ss.str().c_str(), baseFilenameLen );
		pcDestBuf[ baseFilenameLen ] = '\0';
	}
	else
		strcpy_s( pcDestBuf, MAX_ENEMY_FILENAME_SIZE, pcSrc1BaseFilename );*/

	std::ostringstream ss;

	ss<< "Scenes/Level" << "_" << levelNum << ".vscene";
	int nameSceneLen = strlen( ss.str().c_str());
	strncpy( pcDestBuf, ss.str().c_str(), nameSceneLen);
	pcDestBuf[ nameSceneLen ] = '\0';

}
void EnemyFactoryLUA::LoadLevelSwig(unsigned int levelNum)
{
  char levelFilename[50];

  AppendLevelNum(levelFilename, levelNum);

  LoadLevelFilename( levelFilename );
}

void EnemyFactoryLUA::LoadLevelFilename(const char* filename)
{
	// Call GUI logic
	MyGameManager::GlobalManager().PrepareToLoadScene( filename );
	/*
	VisAppLoadSettings settings(filename);
	settings.m_customSearchPaths.Append(":template_root/Assets");
	//LoadScene(settings); 
	Vision::GetApplication()->RequestLoadScene(settings);
	*/
}

void EnemyFactoryLUA::SetCurrentLevel(unsigned int levelIn)
{
	return PlayerStats::Instance()->SetCurrentLevel(levelIn);
}

unsigned int EnemyFactoryLUA::GetCurrentLevel()
{
	return PlayerStats::Instance()->GetCurrentLevel();
}

//Method gives error
void EnemyFactoryLUA::FloatToString(int num, char* pcDestBuf)
{
		std::ostringstream ss;

	ss<< num ;
	int nameSceneLen = strlen( ss.str().c_str());
	strncpy( pcDestBuf, ss.str().c_str(), nameSceneLen);
	pcDestBuf[ nameSceneLen ] = '\0';
}

unsigned int EnemyFactoryLUA::GetScoreMax()
{
	return PlayerStats::Instance()->GetScoreMax();
}


void EnemyFactoryLUA::SetLevelFinish(bool levelFinishIn)
{
	PlayerStats::Instance()->SetLevelFinish(levelFinishIn);
}


bool EnemyFactoryLUA::GetLevelFinish()
{
	return PlayerStats::Instance()->GetLevelFinish();
}

unsigned int EnemyFactoryLUA::CalculateStars(int scoreIn, int scoreMaxIn, bool levelFinish)
{
	return PlayerStats::Instance()->GetPlayerLifes();
}



void EnemyFactoryLUA::MuteAll()
{
	 //VFmodManager::GlobalManager().SetMuteAll(true);
}

void EnemyFactoryLUA::UnMuteAll()
{
	 //VFmodManager::GlobalManager().SetMuteAll(false);
}

void EnemyFactoryLUA::SetStarMax(unsigned int starIn)
{
	PlayerStats::Instance()->SetStarMax(starIn);
}
unsigned int EnemyFactoryLUA::GetStarMax()
{
	return PlayerStats::Instance()->GetStarMax();
}

/*const char *GetKeyLua(VisTypedEngineObject_cl *pOwner)
{
	pOwner->get
}*/

const char *EnemyFactoryLUA::GetKeyLua(VisBaseEntity_cl * prop)
{
	//hkvLog::Error("GetKeyLua:");
	//hkvLog::Error(prop->GetObjectKey());
	return prop->GetObjectKey();
	
}

const char *EnemyFactoryLUA::GetKeyLua(IVObjectComponent * prop)
{
	//hkvLog::Error("GetKeyOtherLua:");
	//hkvLog::Error(reinterpret_cast<VisBaseEntity_cl *>(prop)->GetObjectKey());
	return reinterpret_cast<VisBaseEntity_cl *>(prop)->GetObjectKey();
	
}

void EnemyFactoryLUA::SetSoundFinish(bool soundIn)
{
	return PlayerStats::Instance()->SetSoundFinish(soundIn);
}

bool EnemyFactoryLUA::GetSoundFinish()
{
	return PlayerStats::Instance()->GetSoundFinish();
}

bool EnemyFactoryLUA::IsPickingEnabled() 
{
	return MyGameManager::GlobalManager().IsPickingEnabled();
}