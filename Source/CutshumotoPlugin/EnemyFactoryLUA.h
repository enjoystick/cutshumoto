#ifndef ENEMYFACTORYLUA_H
#define ENEMYFACTORYLUA_H

#include "CutshumotoPluginModule.h"

class EnemyFactoryLUA : public VisBaseEntity_cl 
{
	public:
		VOVERRIDE void InitFunction();
		
		V_DECLARE_SERIAL_DLLEXP(EnemyFactoryLUA, SAMPLEPLUGIN_IMPEXP);
		IMPLEMENT_OBJ_CLASS(EnemyFactoryLUA);

		VisBaseEntity_cl*	CreateEnemy(int eType, unsigned int iLevel, hkvVec3 vPos = hkvVec3(0,0,0));
		bool				PreloadEnemy(int eType, unsigned int iLevel);
		unsigned int		GetScore();		
		void				AddScore(int score);
		void				DamagePlayer(int damage = 1);
		void				HealPlayer(int lifes = 1);
		int					GetPlayerLifes();
		void				ResetStats(int initialLifes = 3);
		bool				SaveStats(char* filename, char* sceneName);
		void				SetCanBeSliced(IVObjectComponent* prop,bool cut);
		void				OnEndOfLevelReached();
		
		void				RemoveFromSlicedEntities(VisTypedEngineObject_cl *pOwner);
		bool				PreloadSound(int eSound);
		//27_08_14 Swig Functions from Sergio
		void				AppendLevelNum( char* pcDestBuf, unsigned int levelNum );
		void				LoadLevelSwig(unsigned int levelNum);
		void				LoadLevelFilename(const char* filename);
		void				SetCurrentLevel(unsigned int levelIn);
		unsigned int		GetCurrentLevel();
		void				FloatToString(int num,char* pcDestBuf);
		//Sergio 29_08_14   
		unsigned int		GetScoreMax();
		void				SetLevelFinish(bool levelFinishIn);
		bool				GetLevelFinish();
		void				SetBossFinish(bool levelFinishIn);
		bool				GetBossFinish();
		unsigned int		CalculateStars(int scoreIn, int scoreMaxIn, bool levelFinish);
		void				SetStarMax(unsigned int starIn);
		unsigned int		GetStarMax();
		//David 07/09
		int					GetEntityLifePoints(char* entityKey);
		void				MuteAll();
		void				UnMuteAll();
		void				SetCanBeSliced(VisBaseEntity_cl * prop,bool cut);
		//const char *		GetKeyLua(VisTypedEngineObject_cl *pOwner);
		const char *		GetKeyLua(VisBaseEntity_cl * prop);
		const char *		GetKeyLua(IVObjectComponent * prop);
		void				SetSoundFinish(bool soundIn);
		bool				GetSoundFinish();
		int					GetCurrentCombo();
		bool				IsPickingEnabled();


	private:
		
};

#endif 