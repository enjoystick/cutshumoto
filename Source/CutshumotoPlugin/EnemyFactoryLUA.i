%include <windows.i>
/*%include <e:\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\vision_types.i>
%include <e:\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VColor.i>
%include <e:\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VTypedObject.i>
%include <e:\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VisApiTypedEngineObject.i>
%include <e:\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VisApiObjectComponent.i>
%include <e:\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VisApiObject3D.i>
%include <e:\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VisApiBaseEntity.i>*/

%include <c:\Havok\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\vision_types.i>
%include <c:\Havok\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VColor.i>
%include <c:\Havok\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VTypedObject.i>
%include <c:\Havok\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VisApiTypedEngineObject.i>
%include <c:\Havok\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VisApiObjectComponent.i>
%include <c:\Havok\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VisApiObject3D.i>
%include <c:\Havok\AnarchySDK\Source\Vision\Runtime\EnginePlugins\VisionEnginePlugin\Scripting\Lua\VisApiBaseEntity.i>


//custom headers for generated source file
%module EnemyFactoryLUA
%{
  
%}
 
%nodefaultctor EnemyFactoryLUA;
%nodefaultdtor EnemyFactoryLUA;
class EnemyFactoryLUA : public VisBaseEntity_cl 
{
	public:
		VisBaseEntity_cl*	CreateEnemy(int eType, unsigned int iLevel, hkvVec3 vPos = hkvVec3(0,0,0));
		bool				PreloadEnemy(int eType, unsigned int iLevel);
		unsigned int		GetScore();		
		void				AddScore(int score);
		void				DamagePlayer(int damage = 1);
		void				HealPlayer(int lifes = 1);
		int					GetPlayerLifes();
		void				ResetStats(int initialLifes = 3);
		bool				SaveStats(char* filename, char* sceneName);
		void				SetCanBeSliced(IVObjectComponent* prop,bool cut);
		void				OnEndOfLevelReached();
		
		void				RemoveFromSlicedEntities(VisTypedEngineObject_cl *pOwner);
		bool				PreloadSound(int eSound);

		void				LoadLevelSwig(unsigned int levelNum);
		void				LoadLevelFilename(const char* filename);
		void				SetCurrentLevel(unsigned int levelIn);
		unsigned int		GetCurrentLevel();
		void				FloatToString(int num,char* pcDestBuf);
		unsigned int		GetScoreMax();
		void				SetLevelFinish(bool levelFinishIn);
		bool				GetLevelFinish();
		void				SetBossFinish(bool levelFinishIn);
		bool				GetBossFinish();
		unsigned int		CalculateStars(int scoreIn, int scoreMaxIn, bool levelFinish);
		void				SetStarMax(unsigned int starIn);
		unsigned int		GetStarMax();
		int					GetEntityLifePoints(char* entityKey);
		void				MuteAll();
		void				UnMuteAll();
		void				SetCanBeSliced(VisBaseEntity_cl * prop,bool cut);
		const char *		GetKeyLua(VisBaseEntity_cl * prop);
		const char *		GetKeyLua(IVObjectComponent * prop);
		void				SetSoundFinish(bool soundIn);
		bool				GetSoundFinish();
		int					GetCurrentCombo();
		bool				IsPickingEnabled();

	%extend {
		VSWIG_CREATE_CAST(EnemyFactoryLUA)
	}
};
