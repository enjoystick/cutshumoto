#include "CutshumotoPluginPCH.h"
#include "GameManager.h"
#include "EnemyFactoryLUA.h"
#include "rapidjson/document.h"
#include "CmpSlicing.h"
#include <sstream>
#include "GUIAnimationManager.h"
#include "CutshumotoUtilities.h"
#include "SoundManager.h"

#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/Entities/CameraPositionEntity.hpp>
#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/Components/VFollowPathComponent.hpp>

#define GUI_RESOURCE_FILEPATH "Dialogs\\MenuSystem.xml"
#define GUI_TEXTURES_PATH "Dialogs\\Textures\\"
#define ALTERNATIVE_GUI_TEXTURES_PATH "Assets\\Dialogs\\Textures\\"
#define TEX_ATLAS_ELEMENTS_FILENAME "gui_elements_tex_atlas"
#define TEX_ATLAS_ELEMENTS_FILEPATH "Dialogs\\Textures\\gui_elements_tex_atlas"
#define TEX_ATLAS_BACKGROUNDS_FILENAME "gui_backgrounds_tex_atlas"
#define TEX_ATLAS_BACKGROUNDS_FILEPATH "Dialogs\\Textures\\gui_backgrounds_tex_atlas"

#define SHOW_FPS false

const VString LOADING_SPLASH_SCREEN_FILES_PATH = "Textures\\";
const VString SCENE_MAIN_MENU_FILEPATH = "Scenes\\Scene_Mundos.vscene";


extern "C" int luaopen_EnemyFactoryLUA(lua_State *);

MyGameManager MyGameManager::g_GameManager;

void MyGameManager::OneTimeInit()
{		
	Vision::Callbacks.OnEditorModeChanged += this;
	Vision::Callbacks.OnAfterSceneLoaded += this;	
	Vision::Callbacks.OnUpdateSceneBegin += this;
	Vision::Callbacks.OnBeforeSceneUnloaded += this;
	Vision::Callbacks.OnEngineInit += this;
	Vision::Callbacks.OnEngineDeInit += this;
	IVScriptManager::OnRegisterScriptFunctions += this;
	IVScriptManager::OnScriptProxyCreation += this;
}

void MyGameManager::OneTimeDeInit()
{	
	Vision::Callbacks.OnEditorModeChanged -= this;
	Vision::Callbacks.OnAfterSceneLoaded -= this;
	Vision::Callbacks.OnUpdateSceneBegin -= this;
	Vision::Callbacks.OnBeforeSceneUnloaded -= this;
	Vision::Callbacks.OnEngineInit -= this;
	Vision::Callbacks.OnEngineDeInit -= this;
	IVScriptManager::OnRegisterScriptFunctions -= this;
	IVScriptManager::OnScriptProxyCreation -= this;
}

void MyGameManager::OnHandleCallback(IVisCallbackDataObject_cl *pData)
{	
	if (pData->m_pSender==&Vision::Callbacks.OnUpdateSceneBegin)	
		OnUpdateSceneBegin();

	else if (pData->m_pSender==&Vision::Callbacks.OnEditorModeChanged)	
		OnEditorModeChanged((VisEditorModeChangedDataObject_cl *)pData);

	else if (pData->m_pSender==&Vision::Callbacks.OnAfterSceneLoaded)	
		OnAfterSceneLoaded( static_cast<VisSceneLoadedDataObject_cl*>(pData) );

	else if (pData->m_pSender==&Vision::Callbacks.OnBeforeSceneUnloaded) 
		OnBeforeSceneUnloaded();	

	else if (pData->m_pSender==&Vision::Callbacks.OnEngineInit)
		OnEngineInit();

	else if (pData->m_pSender==&Vision::Callbacks.OnEngineDeInit)
		OnEngineDeInit();

	else if( pData->m_pSender == &IVScriptManager::OnRegisterScriptFunctions ) 
		OnRegisterScriptFunctions((VScriptResourceManager*)Vision::GetScriptManager());

	else if ( pData->m_pSender == &IVScriptManager::OnScriptProxyCreation ) 
		OnScriptProxyCreation((VScriptCreateStackProxyObject*)pData);

}

void MyGameManager::OnRegisterScriptFunctions(VScriptResourceManager* manager)
{	
	if( manager ) 
	{
		lua_State* pLuaState = manager->GetMasterState();
		if ( pLuaState ) 
			luaopen_EnemyFactoryLUA(pLuaState);
		else 
			Vision::Message.Add(1, "Unable to create Lua EnemyFactoryLUA Module, lua_State is NULL!");		
	}
}

void MyGameManager::OnScriptProxyCreation(VScriptCreateStackProxyObject* scriptData)
{	
	if( !scriptData->m_bProcessed ) 
	{
		int iRetParams = 0;

		if( scriptData->m_pInstance->IsOfType(V_RUNTIME_CLASS(EnemyFactoryLUA)) ) 
			iRetParams = LUA_CallStaticFunction(scriptData->m_pLuaState, "EnemyFactoryLUA", "EnemyFactoryLUA", "Cast", "E>E", scriptData->m_pInstance);		

		if( iRetParams > 0 ) 
		{
			if( lua_isnil(scriptData->m_pLuaState, -1) ) 
				lua_pop(scriptData->m_pLuaState, iRetParams);
			else 
				scriptData->m_bProcessed = true;			
		}
	}	
}

void MyGameManager::OnEngineInit() 
{
	// Get my AppImpl
	m_pApplication = static_cast<VAppBase*>(Vision::GetApplication())->GetAppImpl();
	m_sQueuedSceneToLoad = "";
	m_bIsPickingEnabled = true;
	// Initialize SceneManager state
	SceneManager::g_eCurrentScene = SCENE_MAIN_MENU;
	SceneManager::g_eSceneState = SCENE_STATE_MAIN_MENU_ONE_TIME_INIT;
}

void MyGameManager::OnUpdateSceneBegin()
{
	if ( SHOW_FPS ) 
	{
		//////////////////////////////////////////////////////// FPS output
		/////////////////////////
		static float fFPS = 0.f; // Just assigned once
		// Calculate FPS
		fFPS = 1.f / Vision::GetTimer()->GetTimeDifference();
		// Float to char*
		std::ostringstream ss;
		ss << fFPS;
		// Show FPS
		Vision::Message.Add( 1, ss.str().c_str() );
		/////////////////////////////////////////////////////////////////
	}

#if defined(WIN32)
	hkvVec2 v2MousePos(0.f,0.f);
	Vision::Mouse.GetPosition( v2MousePos.x, v2MousePos.y );
	if ( m_spCursor ) m_spCursor->SetPos( v2MousePos.x, v2MousePos.y );
#endif
	
	// Update GUISystem
	GUIAnimationManager::Instance().Update( Vision::GetTimer()->GetTimeDifference() );
	
	switch ( SceneManager::g_eCurrentScene ) 
	{
	case SCENE_MAIN_MENU:
		{
			if ( SceneManager::g_eSceneState == SCENE_STATE_MAIN_MENU_ONE_TIME_INIT ) 
			{
				if ( GUIAnimationManager::Instance().IsValid() ) 
				{
					GUIAnimation* pBoard = GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_MAIN_MENU );
					pBoard->DemandRefreshTouchArea(); // HACK: Automatic refresh didnt work
					if ( !GUIAnimationManager::Instance().GetAnimation( GAI_BG_LOGO )->IsVisible() ) 
					{
						static float g_fTapToStartTimeCounter = 0.f;
						float fTapToStartCD = 0.7f;
						g_fTapToStartTimeCounter += Vision::GetTimer()->GetTimeDifference();
						if ( g_fTapToStartTimeCounter > fTapToStartCD ) 
						{ // Switch visible state
							g_fTapToStartTimeCounter = 0.f;
							GUIAnimation* pTapToStart = GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TAP_TO_START );
							pTapToStart->SetVisible( !pTapToStart->IsVisible() );
							pTapToStart->SetTouchable( false );
						}
					}
				}
			}
			else if ( SceneManager::g_eSceneState == SCENE_STATE_MAIN_MENU_COVER ) 
			{

			}
			else if ( SceneManager::g_eSceneState == SCENE_STATE_MAIN_MENU_LEVEL_SELECTION ) 
			{

			}
			else if ( SceneManager::g_eSceneState == SCENE_STATE_MAIN_MENU_STARRING ) 
			{

			}
			break;
		}
	case SCENE_IN_GAME:
		{
			if ( SceneManager::g_eSceneState == SCENE_STATE_IN_GAME_INIT ) 
			{

			}
			else if ( SceneManager::g_eSceneState == SCENE_STATE_IN_GAME_RUNNING ) 
			{
				const int iHP = PlayerStats::Instance()->GetPlayerLifes();
				if ( iHP <= 0 && !IsGameOver() ) 
				{ // Game Over, show game results
					SetGameOver( true );
					if ( !GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->IsActiveEaseAnim() ) 
					{
						GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetVisible( true );
						GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetTouchable( true );
						GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->AlphaFromTo( 0.f, 1.f, 0.f, 255.f, Easing::Linear::EaseIn, SceneManager::HandlerOnEasingAnimComplete );
					}
				}
				for ( int i = 1; i <= DEFAULT_LIFES; i++ ) 
				{
					GUIAnimation* pHeart = GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_0 + i - 1 );
					if ( iHP < i ) 
					{
						if ( pHeart->GetCurrentFrame() == 0 && !pHeart->IsActiveEaseAnim() ) 
						{ // Lose heart
							EasingAnimation* pEaseAnim = pHeart->ScaleFromTo( 0.f, .25f, 1.f, .5f, Easing::Elastic::EaseIn, SceneManager::HandlerOnEasingAnimComplete );
							pEaseAnim->SetAutoreverse( true );
						}
					} 
					else if ( iHP >= i ) 
					{
						if ( pHeart->GetCurrentFrame() == 1 && !pHeart->IsActiveEaseAnim() ) 
						{ // Gain heart
							EasingAnimation* pEaseAnim = pHeart->ScaleFromTo( 0.f, .25f, 1.f, 1.5f, Easing::Elastic::EaseIn, SceneManager::HandlerOnEasingAnimComplete );
							pEaseAnim->SetAutoreverse( true );
						}
					}
				}
				// Set label score
				static unsigned int g_uiLastScore = 0;
				unsigned int uiScore = PlayerStats::Instance()->GetScore();
				if ( g_uiLastScore != uiScore ) 
				{
					g_uiLastScore = uiScore;
					m_splblScore->SetText( Helper::StringifyScore( uiScore ).c_str() );
				}
			} 
			else if ( SceneManager::g_eSceneState == SCENE_STATE_IN_GAME_FINISHED ) 
			{
				
			}
			break;
		}
	default:
		break;
	}
}


void MyGameManager::OnEditorModeChanged(VisEditorModeChangedDataObject_cl * pData)
{
	// when vForge switches back from EDITORMODE_PLAYING_IN_GAME, turn off our play the game mode
	if (pData->m_eNewMode != VisEditorManager_cl::EDITORMODE_PLAYING_IN_GAME)
		SetPlayTheGame(false);
}

void MyGameManager::OnAfterSceneLoaded( VisSceneLoadedDataObject_cl* pSceneData )
{
	m_sQueuedSceneToLoad = "";

	if (Vision::Editor.IsPlayingTheGame()) 
		SetPlayTheGame(true);				 	

	if ( !m_spGUIContext ) // Init one time
	{
		// Init GUI Context
		m_spGUIContext = new VGUIMainContext( NULL );
		m_spGUIContext->SetActivate( true );
		VASSERT( m_spGUIContext );
		//Load GUI resources
		bool bResult = false;
		bResult = VGUIManager::GlobalManager().LoadResourceFile( GUI_RESOURCE_FILEPATH );
		VASSERT( bResult );
		// Show dialog
		m_spMainDialog = m_spGUIContext->ShowDialog( "Dialogs\\MainDialog.xml" );
		VASSERT( m_spMainDialog );
		m_spMainDialog->SetVisible( true ); // Visible by default
	#if defined(WIN32)
		// Show cursor
		VGUIManager::GlobalManager().GetDefaultCursor()->SetVisible( true );
		// Mouse pointer for debug custom GUI System
		m_spCursor = new VisScreenMask_cl();
		m_spCursor->LoadFromFile( "Textures\\pa_cursor.png" );
		m_spCursor->SetTransparency( VIS_TRANSP_MULTIPLICATIVE );
		m_spCursor->SetOrder( -1000 ); // Draw on top
		m_spCursor->SetWrapping( false, false );
	#else
		//Hide cursor
		VGUIManager::GlobalManager().GetDefaultCursor()->SetVisible( false );
		m_spCursor = 0;
	#endif
		// Create and set labels
		float fLabelsRelWidth = 600.f / Vision::Video.GetXRes();
		float fLabelsRelHeight = 64.f / Vision::Video.GetYRes();
		float fScoreLabelMargin = 10.f / Vision::Video.GetXRes();
		m_splblScore = Helper::CreateTextLabel( "0", "LeviBrush_32", VColorRef( 132, 40, 21, 255 ), VisFont_cl::ALIGN_RIGHT, VisFont_cl::ALIGN_CENTER, 600.f, 64.f, 1.f - (fLabelsRelWidth + fScoreLabelMargin), 0.025f + fScoreLabelMargin, 0.35f );
		m_spMainDialog->AddControl( m_splblScore ); // Add score label to dialog
		m_splblResultsScore = Helper::CreateTextLabel( "0", "LeviBrush_32", VColorRef( 132, 40, 21, 255 ), VisFont_cl::ALIGN_RIGHT, VisFont_cl::ALIGN_CENTER, 600.f, 64.f, 0.49f - (fLabelsRelWidth + fScoreLabelMargin), 0.79f - fLabelsRelHeight, 0.55f );
		m_spMainDialog->AddControl( m_splblResultsScore ); // Add results score label to dialog
		m_splblResultsEnemiesSlicedCount = Helper::CreateTextLabel( "0", "LeviBrush_32", VColorRef( 132, 40, 21, 255 ), VisFont_cl::ALIGN_RIGHT, VisFont_cl::ALIGN_CENTER, 600.f, 64.f, 0.49f - (fLabelsRelWidth + fScoreLabelMargin), 0.38f - fLabelsRelHeight, 0.55f );
		m_spMainDialog->AddControl( m_splblResultsEnemiesSlicedCount ); // Add results enemies sliced count label to dialog
		m_splblResultsRemainingLives = Helper::CreateTextLabel( "0", "LeviBrush_32", VColorRef( 132, 40, 21, 255 ), VisFont_cl::ALIGN_RIGHT, VisFont_cl::ALIGN_CENTER, 600.f, 64.f, 0.49f - (fLabelsRelWidth + fScoreLabelMargin), 0.61f - fLabelsRelHeight, 0.55f );
		m_spMainDialog->AddControl( m_splblResultsRemainingLives ); // Add results remaining lives label to dialog
	}


	CameraPositionEntity* pCamPos = static_cast<CameraPositionEntity*>( Vision::Game.SearchEntity( CAMPOS_ENT_KEY ) );
	if ( pCamPos )
	{
		if ( !Vision::Editor.IsInEditor() ) 
		{
			// Set camera values
			Vision::Camera.GetMainCamera()->SetPosition( pCamPos->GetPosition() );
			Vision::Camera.GetMainCamera()->SetUseEulerAngles( true );
			Vision::Camera.GetMainCamera()->SetOrientation( pCamPos->GetOrientation() );
			//Nacho & Sergio 06/08/14: Camera ahora obtiene fov
			Vision::Renderer.GetRendererNode(0)->GetViewProperties()->setFov( pCamPos->GetFovX(), 0.f );
			Vision::Renderer.GetRendererNode(0)->OnViewPropertiesChanged();
		}
	}

	// Set cached data
	// CmpSlicing component attached to CameraPosition entity	
	if ( pCamPos )
	{	
		CmpSlicing* pcmpSlicing = pCamPos->Components().GetComponentOfType<CmpSlicing>();
		/*if ( !BB::pcmpSlicing && pCamPos && pcmpSlicing ) 
		{
			BB::pcmpSlicing = pcmpSlicing;
		}*/
		if( BB::pcmpSlicing && pCamPos && pcmpSlicing )
		{
			BB::pcmpSlicing = 0;
			BB::pcmpSlicing = pcmpSlicing;
		}
		if ( !BB::pcmpSlicing && pCamPos && pcmpSlicing ) 
		{
			BB::pcmpSlicing = pcmpSlicing;
		}
	}

	// Hide enemy position placeholder entities
	DynArray_cl<VisBaseEntity_cl*> triggers;
	Vision::Game.SearchEntity("EnemyTrigger", &triggers);
	for (unsigned int i = 0; i < triggers.GetValidSize(); i++)
	{
		triggers[i]->SetVisibleBitmask(0);
	}
	//Sergio 29_08_14 Vuelve invisible al cubo de fin de partida
	Vision::Game.SearchEntity("Finish", &triggers);
	for (unsigned int i = 0; i < triggers.GetValidSize(); i++)
	{
		triggers[i]->SetVisibleBitmask(0);
	}

	int iDotPos = std::string( pSceneData->m_szSceneFileName ).find( "." );
	if ( isdigit( pSceneData->m_szSceneFileName[ iDotPos - 1 ] ) ) 
	{ // Playable scene
		// Set wanted scene state
		SceneManager::g_eCurrentScene = SCENE_IN_GAME;
		SceneManager::g_eSceneState = SCENE_STATE_IN_GAME_INIT;
	} 
	else 
	{ // Main_menu/Selection_level scene
		// Set wanted scene state
		if ( SceneManager::g_eSceneState != SCENE_STATE_MAIN_MENU_ONE_TIME_INIT ) 
		{ // Loaded from playable scene
			// Set wanted scene state
			SceneManager::g_eCurrentScene = SCENE_MAIN_MENU;
			SceneManager::g_eSceneState = SCENE_STATE_MAIN_MENU_LEVEL_SELECTION;
		}
	}
	
	switch ( SceneManager::g_eCurrentScene ) 
	{
	case SCENE_MAIN_MENU:
		{
			if ( SceneManager::g_eSceneState == SCENE_STATE_MAIN_MENU_ONE_TIME_INIT ) 
			{
				// Initialize GUI System
				SceneManager::InitGUI();
			}
			else if ( SceneManager::g_eSceneState == SCENE_STATE_MAIN_MENU_LEVEL_SELECTION ) 
			{
				SceneManager::ShowGUIFromScene( SCENE_MAIN_MENU );
				if ( Helper::IsBiomboClosed() ) Helper::OpenBiombo( 1.5f, 1.5f, false );
			}
			break;
		}
	case SCENE_IN_GAME:
		{
			if ( SceneManager::g_eSceneState == SCENE_STATE_IN_GAME_INIT ) 
			{
				SetGameOver( false );

				SceneManager::ShowGUIFromScene( SCENE_IN_GAME );
				GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->AlphaFromTo( .35f, 1.f, 255.f, 0.f, Easing::Linear::EaseIn, SceneManager::HandlerOnEasingAnimComplete, false );
				SceneManager::g_eSceneState = SCENE_STATE_IN_GAME_RUNNING;
			}
			break;
		}
	default:
		break;
	}
}

void MyGameManager::OnBeforeSceneUnloaded()
{
	SetPlayTheGame(false);		
	
}

void MyGameManager::OnEngineDeInit()
{
	m_spGUIContext->SetActivate( false );
	m_spGUIContext = 0;
	m_spMainDialog = 0;
	m_splblScore = 0;
	m_splblResultsEnemiesSlicedCount = 0;
	m_splblResultsRemainingLives = 0;
	m_splblResultsScore = 0;
	m_spCursor = 0;
	m_pApplication = 0;
	m_bIsMuteFMOD = false;
	m_bIsGameOver = false;
	SceneManager::DeInitGUI();
}


// switch to play-the-game mode
void MyGameManager::SetPlayTheGame(bool bStatus)
{
	if (m_bPlayingTheGame == bStatus)
		return;

	m_bPlayingTheGame = bStatus;

	if (m_bPlayingTheGame)
	{
		// Play the game mode is started
		//Vision::Message.Add(1, "Play the game mode has been started");

	} 
	else
	{
		//the play the game mode has been stopped.
		//clean up all your game specific instances, like e.g. particle effects		
		//VisParticleGroupManager_cl::GlobalManager().Instances().Purge();
		
	}
}

void MyGameManager::LoadScene(const char* nameScene)
{
	VSceneLoader loader;	
	loader.UnloadScene();

	if (!loader.LoadScene(nameScene))
		hkvLog::FatalError("Error opening scene file '%s' : %s\n", nameScene, loader.GetLastError());

	//LoadLevelInfo(nameScene, true);
	
	/* comentado por posibles problemas en android
	VSceneLoader loader;	
	loader.ClearScene();

	if (loader.LoadScene(nameScene))	
		while (!loader.IsFinished())
			loader.Tick();			*/
}

void MyGameManager::LoadQueuedScene() 
{
	if ( m_sQueuedSceneToLoad.empty() ) return;

	VisAppLoadSettings settings(m_sQueuedSceneToLoad.c_str());
	settings.m_customSearchPaths.Append(":template_root/Assets");
	//LoadScene(settings); 
	Vision::GetApplication()->RequestLoadScene(settings);
}

void MyGameManager::OnEndOfLevelReached() 
{
	/*
	// Persist and calculate new game state	
	PlayerStats::Instance()->SetLevelFinish( true );
	std::ostringstream ssNumLevel;
	ssNumLevel << PlayerStats::Instance()->GetCurrentLevel();
	Vision::PersistentData.Load( "levelFinish" );
	Vision::PersistentData.Load( "starMax" );
	// Persist level as finished
	Vision::PersistentData.SetBoolean( "levelFinish", ssNumLevel.str().c_str(), true );
	int iLevelStars = PlayerStats::Instance()->GetPlayerLifes();
	int iPersistedStars = static_cast<int>(Vision::PersistentData.GetNumber( "starMax", ssNumLevel.str().c_str(), 0 ));
	// Match level stars with persisted ones
	if ( iLevelStars > iPersistedStars ) 
		Vision::PersistentData.SetNumber( "starMax", ssNumLevel.str().c_str(), static_cast<float>(iLevelStars) ); // Update level stars
	// Save data
	Vision::PersistentData.SaveAll();
	*/

	GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetVisible( true );
	GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->AlphaFromTo( 0.f, 1.f, 0.f, 255.f, Easing::Linear::EaseIn, SceneManager::HandlerOnEasingAnimComplete );
}

void SceneManager::InitGUI() 
{
	if ( GUIAnimationManager::Instance().IsValid() ) return; // Already initialized

	// Init gui system by loading texture atlases
	bool bTexAtlasLoaded = false;
	bTexAtlasLoaded = GUIAnimationManager::Instance().LoadTexturePackerJSON( TEX_ATLAS_ELEMENTS_FILENAME, GUI_TEXTURES_PATH );
	if ( !bTexAtlasLoaded ) bTexAtlasLoaded = GUIAnimationManager::Instance().LoadTexturePackerJSON( TEX_ATLAS_ELEMENTS_FILENAME, ALTERNATIVE_GUI_TEXTURES_PATH );
	VASSERT( bTexAtlasLoaded );
	bTexAtlasLoaded = GUIAnimationManager::Instance().LoadTexturePackerJSON( TEX_ATLAS_BACKGROUNDS_FILENAME, GUI_TEXTURES_PATH );
	if ( !bTexAtlasLoaded ) bTexAtlasLoaded = GUIAnimationManager::Instance().LoadTexturePackerJSON( TEX_ATLAS_BACKGROUNDS_FILENAME, ALTERNATIVE_GUI_TEXTURES_PATH );
	VASSERT( bTexAtlasLoaded );

	// Loads gui elements
	GUIAnimation* pResult = 0;
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_audio.png", 0, 0, 4, GAI_TOGGLE_SOUND, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_audio.png", 0, 0, 4, GAI_TOGGLE_SOUND_PAUSED, GUIAnimation::GAT_NONE );						VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_pause.png", 0, 0, 4, GAI_TOGGLE_PAUSE, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_score.png", 0, 0, 1, GAI_SPRITE_SCORE, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_lifes.png", 0, 0, 1, GAI_SPRITE_HEART_FRAME, GUIAnimation::GAT_NONE );							VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_heart.png", 0, 0, 2, GAI_SPRITE_HEART_0, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_heart.png", 0, 0, 2, GAI_SPRITE_HEART_1, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_heart.png", 0, 0, 2, GAI_SPRITE_HEART_2, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_table.png", 0, 0, 1, GAI_SPRITE_BOARD, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_resume.png", 0, 0, 2, GAI_BTN_RESUME, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_restart.png", 0, 0, 2, GAI_BTN_RESTART_PAUSED, GUIAnimation::GAT_NONE );						VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_mainmenu.png", 0, 0, 2, GAI_BTN_TO_MAIN_MENU_PAUSED, GUIAnimation::GAT_NONE );					VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_finalscore.png", 0, 0, 1, GAI_SPRITE_FINALSCORE, GUIAnimation::GAT_NONE );						VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_results.png", 0, 0, 2, GAI_SPRITE_RESULT, GUIAnimation::GAT_NONE );							VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_restart_icon.png", 0, 0, 2, GAI_BTN_RESTART_FINISHED, GUIAnimation::GAT_NONE );				VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_mainmenu_icon.png", 0, 0, 2, GAI_BTN_TO_MAIN_MENU_FINISHED, GUIAnimation::GAT_NONE );			VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_star.png", 0, 0, 2, GAI_SPRITE_STAR_0, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_star.png", 0, 0, 2, GAI_SPRITE_STAR_1, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_star.png", 0, 0, 2, GAI_SPRITE_STAR_2, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_star_empty.png", 0, 0, 1, GAI_SPRITE_STAR_EMPTY_0, GUIAnimation::GAT_NONE );					VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_star_empty.png", 0, 0, 1, GAI_SPRITE_STAR_EMPTY_1, GUIAnimation::GAT_NONE );					VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_star_empty.png", 0, 0, 1, GAI_SPRITE_STAR_EMPTY_2, GUIAnimation::GAT_NONE );					VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_cover.png", 0, 0, 1, GAI_SPRITE_DRAW_MAIN_MENU, GUIAnimation::GAT_NONE );						VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_table.png", 0, 0, 1, GAI_BG_BOARD_MAIN_MENU, GUIAnimation::GAT_NONE );							VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_table.png", 0, 0, 1, GAI_BG_BOARD_PAUSED, GUIAnimation::GAT_NONE );							VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_start.png", 0, 0, 1, GAI_SPRITE_TAP_TO_START, GUIAnimation::GAT_NONE );						VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_credits.png", 0, 0, 2, GAI_BTN_STARRING, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_playthegame.png", 0, 0, 2, GAI_BTN_PLAY_THE_GAME, GUIAnimation::GAT_NONE );					VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_back_icon.png", 0, 0, 2, GAI_BTN_BACK, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_go.png", 0, 0, 4, GAI_SPRITE_COUNTER, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_backgroundpause.png", 0, 0, 1, GAI_BG_PAUSED, GUIAnimation::GAT_NONE );						VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_ELEMENTS_FILEPATH, "Gui_enjoystick.png", 0, 0, 1, GAI_BG_LOGO, GUIAnimation::GAT_NONE );								VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_BACKGROUNDS_FILEPATH, "Gui_bg_black.png", 0, 0, 1, GAI_SPRITE_BLACK_UNDER_BIOMBO, GUIAnimation::GAT_NONE );			VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_BACKGROUNDS_FILEPATH, "Gui_bg_black.png", 0, 0, 1, GAI_MODAL_BLACK, GUIAnimation::GAT_NONE );							VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_BACKGROUNDS_FILEPATH, "Gui_bg_black.png", 0, 0, 1, GAI_BG_BLACK_FINISHED, GUIAnimation::GAT_NONE );					VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_BACKGROUNDS_FILEPATH, "gui_bg_biombo_left.png", 0, 0, 1, GAI_BG_BIOMBO_LEFT, GUIAnimation::GAT_NONE );					VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_BACKGROUNDS_FILEPATH, "gui_bg_biombo_right.png", 0, 0, 1, GAI_BG_BIOMBO_RIGHT, GUIAnimation::GAT_NONE );				VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_BACKGROUNDS_FILEPATH, "Gui_bg_credits.png", 0, 0, 6, GAI_SPRITE_BG_STARRING, GUIAnimation::GAT_NONE );					VASSERT( pResult );
	pResult = GUIAnimationManager::Instance().CreateAnimation( TEX_ATLAS_BACKGROUNDS_FILEPATH, "Gui_names.png", 0, 0, 5, GAI_SPRITE_TEXT_STARRING, GUIAnimation::GAT_NONE );					VASSERT( pResult );

	float fGUIScale = Helper::GetUIScalingFactor();
	for ( unsigned int i = 0; i < GAI_COUNT; i++ ) 
	{
		GUIAnimation* pAnim = GUIAnimationManager::Instance().GetAnimation(i);
		if ( pAnim ) 
		{
			// Assign OnTouch handlers
			pAnim->AddOnTouchUpCallback( HandlerOnTouchUp );
			pAnim->AddOnTouchDownCallback( HandlerOnTouchDown );
			// Scale
			pAnim->SetScale( fGUIScale, fGUIScale );
			// Get id
			eGUIAnimID eID = static_cast<eGUIAnimID>(i);
			// Set order
			pAnim->SetOrder( GAO_MIDDLE );
			// Set background textures to fill the screen
			if ( eID == GAI_MODAL_WHITE 
				|| eID == GAI_MODAL_BLACK 
				|| eID == GAI_SPRITE_BLACK_UNDER_BIOMBO 
				|| eID == GAI_BG_BLACK_FINISHED ) pAnim->SetRelativeSize( 1.5f, 1.5f );
			if ( eID == GAI_BG_LOGO 
				|| eID == GAI_BG_PAUSED 
				|| eID == GAI_SPRITE_BOARD 
				|| eID == GAI_SPRITE_BG_STARRING 
				|| eID == GAI_SPRITE_TEXT_STARRING ) pAnim->SetRelativeSize( 1.f, 1.f );
			if ( eID == GAI_BG_BIOMBO_LEFT || eID == GAI_BG_BIOMBO_RIGHT ) pAnim->SetRelativeSize( 0.5f, 1.f );
			// Set top-front order
			if ( eID == GAI_BG_BIOMBO_LEFT 
				|| eID == GAI_BG_BIOMBO_RIGHT 
				|| eID == GAI_BG_LOGO 
				|| eID == GAI_MODAL_BLACK 
				|| eID == GAI_MODAL_WHITE ) pAnim->SetOrder( GAO_MODAL );
			// Set bg order
			if ( eID == GAI_BG_BLACK_FINISHED 
				|| eID == GAI_BG_PAUSED ) pAnim->SetOrder( GAO_BACK );
			// Set audio clips
			if ( eID == GAI_BTN_BACK 
				|| eID == GAI_BTN_PLAY_THE_GAME 
				|| eID == GAI_BTN_RESTART_FINISHED 
				|| eID == GAI_BTN_RESTART_PAUSED 
				|| eID == GAI_BTN_RESUME 
				|| eID == GAI_BTN_STARRING 
				|| eID == GAI_BTN_TO_MAIN_MENU_FINISHED 
				|| eID == GAI_BTN_TO_MAIN_MENU_PAUSED 
				|| eID == GAI_TOGGLE_PAUSE 
				|| eID == GAI_TOGGLE_SOUND 
				|| eID == GAI_TOGGLE_SOUND_PAUSED ) pAnim->SetOnTouchUpSound( eDefaultPushButton );
			//if ( eID == GAI_TOGGLE_PAUSE 
				//|| eID == GAI_BTN_RESUME ) pAnim->SetOnTouchUpSound( ePauseButton );
			//if ( eID == GAI_SPRITE_COUNTER ) pAnim->SetOnEasingStartSound( eCounter );
			//if ( eID == GAI_BG_BIOMBO_LEFT ) pAnim->SetOnEasingStartSound( eBiombo );
			if ( eID == GAI_BG_BOARD_MAIN_MENU ) pAnim->SetOnEasingCompleteSound( eShowButtons );
			// Hide
			pAnim->SetVisible( false );

			// Black screen under biombo
			if ( eID == GAI_SPRITE_BLACK_UNDER_BIOMBO ) 
			{
				pAnim->SetOrder( GAO_MODAL + 10 );
				pAnim->SetVisible( true );
				pAnim->SetTouchable( false );
				pAnim->AlphaTo( 0.f, 0.f, 0.f, Easing::Linear::EaseIn, 0, false );
			}
		}
	}
	SceneManager::OrderGUIElements();
	SceneManager::PositionGUIElements();
	SceneManager::SizeGUIElements();

	// Calculate actual size and position
	for ( unsigned int i = 0; i < GAI_COUNT; i++ ) 
	{
		GUIAnimation* pAnim = GUIAnimationManager::Instance().GetAnimation(i);
		if ( pAnim ) 
		{
			pAnim->DemandRefreshTouchArea();
		}
	}

	SceneManager::ShowGUIFromScene( SCENE_MAIN_MENU );
}

void SceneManager::DeInitGUI() 
{
	GUIAnimationManager::Instance().DeInit();
}

void SceneManager::HandlerOnTouchUp( GUIAnimation* pSender ) 
{
#if (defined(_DEBUG)) 
	std::ostringstream ss;
	ss << "TouchUp Sender ID: " << pSender->GetAnimID() << "\n";
	OutputDebugString( ss.str().c_str() );
#endif

	switch ( pSender->GetAnimID() ) 
	{
	case GAI_BG_BOARD_MAIN_MENU:
		{
			if ( SceneManager::g_eSceneState == SCENE_STATE_MAIN_MENU_ONE_TIME_INIT ) 
			{
				pSender->SetTouchable( false );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TAP_TO_START )->SetVisible( false );
				pSender->ScaleTo( 0.f, 1.5f, pSender->GetScale().x - 0.35f, Easing::Linear::EaseOut, HandlerOnEasingAnimComplete, false );
				GUIAnimation* pMainMenuDraw = GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_DRAW_MAIN_MENU );
				pMainMenuDraw->ScaleFromTo( 0.f, 1.5f, pMainMenuDraw->GetScale().x, pMainMenuDraw->GetScale().x - 0.35f, Easing::Linear::EaseOut, 0, false );
				// Change scene state
				SceneManager::g_eSceneState = SCENE_STATE_MAIN_MENU_COVER;
			}
			break;
		}
	case GAI_BTN_RESUME:
		{
			pSender->SetRenderFrame(0);
			// Depause sounds
			VFmodManager::GlobalManager().SetPauseAll( false );
			GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_PAUSE )->SetRenderFrame(0);
			// Resume game
			g_eSceneState = SCENE_STATE_IN_GAME_RUNNING;
			ShowGUIFromScene( SCENE_IN_GAME );
			g_eSceneState = SCENE_STATE_IN_GAME_PAUSED;
			GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetVisible( true );
			GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetRenderFrame(3);
			GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->ScaleFromTo( 0.f, 0.5, 0.f, 1.f, Easing::Linear::EaseOut, HandlerOnEasingAnimComplete, false );
			break;
		}
	case GAI_BTN_RESTART_PAUSED:
		{
			pSender->SetRenderFrame(0);
			// Depause sounds
			VFmodManager::GlobalManager().SetPauseAll( false );
			// Restart level from the beginning
			GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetTouchable( true );
			GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetVisible( true );
			GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->AlphaFromTo( 0.f, 1.f, 0.f, 255.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete, false );
			// Hide score label
			MyGameManager::GlobalManager().m_splblScore->SetVisible( false );
			break;
		}
	case GAI_BTN_RESTART_FINISHED:
		{
			pSender->SetRenderFrame(0);
			// Restart level from the beginning
			GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetTouchable( true );
			GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetVisible( true );
			GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->AlphaFromTo( 0.f, 1.f, 0.f, 255.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete, false );
			// Hide results labels
			MyGameManager::GlobalManager().m_splblResultsEnemiesSlicedCount->SetVisible( false );
			MyGameManager::GlobalManager().m_splblResultsRemainingLives->SetVisible( false );
			MyGameManager::GlobalManager().m_splblResultsScore->SetVisible( false );
			break;
		}
	case GAI_BTN_TO_MAIN_MENU_FINISHED:
		{
			pSender->SetRenderFrame(0);
			// Load main_menu/seleccion mundo scene
			MyGameManager::GlobalManager().PrepareToLoadScene( SCENE_MAIN_MENU_FILEPATH.AsChar() );
			// Hide results labels
			MyGameManager::GlobalManager().m_splblResultsEnemiesSlicedCount->SetVisible( false );
			MyGameManager::GlobalManager().m_splblResultsRemainingLives->SetVisible( false );
			MyGameManager::GlobalManager().m_splblResultsScore->SetVisible( false );
			break;
		}
	case GAI_BTN_TO_MAIN_MENU_PAUSED: 
		{
			pSender->SetRenderFrame(0);
			// Depause sounds
			VFmodManager::GlobalManager().SetPauseAll( false );
			// Load main_menu/seleccion mundo scene
			MyGameManager::GlobalManager().PrepareToLoadScene( SCENE_MAIN_MENU_FILEPATH.AsChar() );
			// Hide score label
			MyGameManager::GlobalManager().m_splblScore->SetVisible( false );
			break;
		}
	case GAI_TOGGLE_SOUND:
		{
			// FIXME: Save/Load sound state in persistent data
			int iCurrFrame = pSender->GetCurrentFrame();
			if ( iCurrFrame == 1 ) 
			{
				// Turn off sounds
				VFmodManager::GlobalManager().SetMuteAll( true );
				MyGameManager::GlobalManager().SetMuteAllFMOD( true );
				pSender->SetRenderFrame(2);
			}
			else if ( iCurrFrame == 3 ) 
			{
				// Turn on sounds
				VFmodManager::GlobalManager().SetMuteAll( false );
				MyGameManager::GlobalManager().SetMuteAllFMOD( false );
				pSender->SetRenderFrame(0);
			}
			break;
		}
	case GAI_TOGGLE_SOUND_PAUSED:
		{
			// FIXME: Save/Load sound state in persistent data
			int iCurrFrame = pSender->GetCurrentFrame();
			if ( iCurrFrame == 1 ) 
			{
				// Turn off sounds
				VFmodManager::GlobalManager().SetMuteAll( true );
				MyGameManager::GlobalManager().SetMuteAllFMOD( true );
				pSender->SetRenderFrame(2);
			}
			else if ( iCurrFrame == 3 ) 
			{
				// Turn on sounds
				VFmodManager::GlobalManager().SetMuteAll( false );
				MyGameManager::GlobalManager().SetMuteAllFMOD( false );
				pSender->SetRenderFrame(0);
			}
			break;
		}
	case GAI_BTN_STARRING:
		{
			pSender->SetRenderFrame(0);
			if ( !Helper::IsBiomboAnimRunning() ) Helper::CloseBiombo( 0.f, 1.5f );
			g_eSceneState = SCENE_STATE_MAIN_MENU_STARRING;
			break;
		}
	case GAI_BTN_BACK:
		{
			pSender->SetRenderFrame(0);
			if ( SceneManager::g_eSceneState == SCENE_STATE_MAIN_MENU_LEVEL_SELECTION && !Helper::IsBiomboAnimRunning() && !Helper::IsBiomboClosed() ) 
			{
				// Set new scene state
				SceneManager::g_eSceneState = SCENE_STATE_MAIN_MENU_COVER;
				Helper::CloseBiombo( 0.f, 1.5f );
			}
			break;
		}
	case GAI_BTN_PLAY_THE_GAME:
		{
			pSender->SetRenderFrame(0);
			if ( !Helper::IsBiomboAnimRunning() && !Helper::IsBiomboClosed() ) 
			{
				// Set new scene state
				SceneManager::g_eSceneState = SCENE_STATE_MAIN_MENU_LEVEL_SELECTION;
				Helper::CloseBiombo( 0.f, 1.5f );
			}
			break;
		}
	case GAI_TOGGLE_PAUSE:
		{
			int iCurrFrame = pSender->GetCurrentFrame();
			if ( iCurrFrame == 1 ) 
			{
				pSender->SetRenderFrame(2);
				// Pause sounds
				VFmodManager::GlobalManager().SetPauseAll( true );
				// Pause game
				SetPause( true );
				ShowGUIFromScene( SCENE_IN_GAME );
			}
			else if ( iCurrFrame == 3 ) 
			{
				pSender->SetRenderFrame(0);
				// Depause sounds
				VFmodManager::GlobalManager().SetPauseAll( false );
				// Resume game
				SceneManager::g_eSceneState = SCENE_STATE_IN_GAME_RUNNING;
				ShowGUIFromScene( SCENE_IN_GAME );
				SceneManager::g_eSceneState = SCENE_STATE_IN_GAME_PAUSED;
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetRenderFrame(3);
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->ScaleFromTo( 0.f, 0.5, 0.f, 1.f, Easing::Linear::EaseOut, HandlerOnEasingAnimComplete, false );
			}
			break;
		}
	default:
		break;
	}
}

void SceneManager::HandlerOnTouchDown( GUIAnimation* pSender ) 
{
	switch ( pSender->GetAnimID() ) 
	{
	case GAI_BTN_RESUME:
		{
			pSender->SetRenderFrame(1);
			break;
		}
	case GAI_BTN_RESTART_PAUSED:
		{
			pSender->SetRenderFrame(1);
			break;
		}
	case GAI_BTN_RESTART_FINISHED:
		{
			pSender->SetRenderFrame(1);

			PlayerStats::Instance()->SetSoundFinish(false);
			break;
		}
	case GAI_BTN_TO_MAIN_MENU_FINISHED:
		{
			pSender->SetRenderFrame(1);
			break;
		}
	case GAI_BTN_TO_MAIN_MENU_PAUSED: 
		{
			pSender->SetRenderFrame(1);
			break;
		}
	case GAI_TOGGLE_SOUND:
		{
			int iCurrFrame = pSender->GetCurrentFrame();
			if ( iCurrFrame == 0 ) 
				pSender->SetRenderFrame(1);
			else if ( iCurrFrame == 2 ) 
				pSender->SetRenderFrame(3);
			break;
		}
	case GAI_TOGGLE_SOUND_PAUSED:
		{
			int iCurrFrame = pSender->GetCurrentFrame();
			if ( iCurrFrame == 0 ) 
				pSender->SetRenderFrame(1);
			else if ( iCurrFrame == 2 ) 
				pSender->SetRenderFrame(3);
			break;
		}
	case GAI_BTN_STARRING:
		{
			pSender->SetRenderFrame(1);
			break;
		}
	case GAI_BTN_PLAY_THE_GAME:
		{
			pSender->SetRenderFrame(1);
			break;
		}
	case GAI_BTN_BACK:
		{
			pSender->SetRenderFrame(1);
			break;
		}
	case GAI_TOGGLE_PAUSE:
		{
			int iCurrFrame = pSender->GetCurrentFrame();
			if ( iCurrFrame == 0 ) 
				pSender->SetRenderFrame(1);
			else if ( iCurrFrame == 2 && SceneManager::g_eSceneState ) 
				pSender->SetRenderFrame(3);
			break;
		}
	default:
		break;
	}
}

void SceneManager::HandlerOnEasingAnimComplete( GUIAnimation* pSender, eGUIAnimProperty eProperty ) 
{
	switch ( pSender->GetAnimID() ) 
	{
	case GAI_BG_BOARD_MAIN_MENU:
		{
			if ( eProperty == GAP_SCALE ) 
			{
				//SoundManager::Instance()->PlaySound( eShowButtons, hkvVec3::ZeroVector(), false );

				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_STARRING )->ScaleTo( 0.f, 1.5f, 1.f, Easing::Elastic::EaseOut );
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->ScaleTo( 0.f, 1.5f, 1.f, Easing::Elastic::EaseOut );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_PLAY_THE_GAME )->ScaleTo( 0.f, 1.5f, 1.f, Easing::Elastic::EaseOut );
				pSender->SetTouchable( false );
			}
			break;
		}
	case GAI_BG_LOGO:
		{
			if ( eProperty == GAP_ALPHA ) 
			{
				// Trigger menu music
				SoundManager::Instance()->PlaySound( eMainMenu, Vision::Camera.GetMainCamera()->GetPosition(), true );

				if ( pSender->GetTexture()->GetColor().a == 0.f ) pSender->SetVisible( false );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TAP_TO_START )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TAP_TO_START )->SetTouchable( false );
			}
			break;
		}
	case GAI_BG_BIOMBO_RIGHT: case GAI_BG_BIOMBO_LEFT:
		{
			if ( eProperty == GAP_POSITION ) 
			{
				if ( Helper::IsBiomboClosed() ) 
				{
					// HACK: Set biombos pos to fit entire screen and dodge middle gaps
					GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->PositionFromTopLeft( 0.f, 0.f );
					GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_RIGHT )->PositionFromTopRight( 0.f, 0.f );
				}
				if ( g_eSceneState == SCENE_STATE_MAIN_MENU_LEVEL_SELECTION 
					|| g_eSceneState == SCENE_STATE_MAIN_MENU_COVER ) 
				{
					if ( Helper::IsBiomboClosed() ) 
					{
						Helper::OpenBiombo( 1.f, 1.5f );
						ShowGUIFromScene( SCENE_MAIN_MENU );
					}
				} 
				else if ( g_eCurrentScene == SCENE_IN_GAME ) 
				{
					if ( Helper::IsBiomboClosed() && g_eSceneState == SCENE_STATE_IN_GAME_PAUSED 
						|| g_eSceneState == SCENE_STATE_IN_GAME_FINISHED ) 
					{
						SetPause( false );
						// Set wanted scene state
						g_eCurrentScene = SCENE_MAIN_MENU;
						g_eSceneState = SCENE_STATE_MAIN_MENU_LEVEL_SELECTION;
						// Load scene
						MyGameManager::GlobalManager().LoadQueuedScene();
					}
				}
				else if ( g_eSceneState == SCENE_STATE_MAIN_MENU_STARRING ) 
				{
					if ( Helper::IsBiomboClosed() ) 
					{
						// Trigger credit music
						//SoundManager::Instance()->PlaySound( eCredits, Vision::Camera.GetMainCamera()->GetPosition(), true );

						Helper::OpenBiombo( 1.f, 1.5f );
						ShowGUIFromScene( SCENE_MAIN_MENU );
					}
				}
			}
			break;
		}
	case GAI_MODAL_BLACK: 
		{
			if ( eProperty == GAP_ALPHA ) 
			{
				const float fAlphaVal = pSender->GetTexture()->GetColor().a;
				if ( fAlphaVal == 0.f ) pSender->SetVisible( false );
				if ( g_eSceneState == SCENE_STATE_MAIN_MENU_LEVEL_SELECTION && fAlphaVal == 255.f ) 
				{
					// Load scene
					MyGameManager::GlobalManager().LoadQueuedScene();
				} 
				else if ( (g_eSceneState == SCENE_STATE_IN_GAME_PAUSED 
					|| g_eSceneState == SCENE_STATE_IN_GAME_FINISHED) && fAlphaVal == 255.f ) 
				{
					// Reset level  
					RestartLevel();
					g_eSceneState = SCENE_STATE_IN_GAME_INIT;
					ShowGUIFromScene( SCENE_IN_GAME );
					pSender->AlphaFromTo( .5f, 1.f, 255.f, 0.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete, false );
					SetPause( false );
					GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_PAUSE )->SetRenderFrame(0);
				}
				else if ( g_eSceneState == SCENE_STATE_IN_GAME_RUNNING && fAlphaVal == 0.f ) 
				{
					GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetRenderFrame(3);
					GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->ScaleFromTo( 0.f, 0.5, 0.f, 1.f, Easing::Linear::EaseOut, HandlerOnEasingAnimComplete );
				} 
				else if ( g_eSceneState == SCENE_STATE_IN_GAME_RUNNING && fAlphaVal == 255.f )  
				{
					// Hide score label
					MyGameManager::GlobalManager().m_splblScore->SetVisible( false );
					// Hide black modal
					pSender->SetVisible( false );
					SetPause( true ); // Stop cam motion in order to prevent loop of CameraFollowPath
					// Set new scene state
					g_eSceneState = SCENE_STATE_IN_GAME_FINISHED;
					ShowGUIFromScene( SCENE_IN_GAME );
					Helper::SetResultsGUI();
				}
			}
			break;
		}
	case GAI_SPRITE_COUNTER: 
		{
			if ( eProperty == GAP_SCALE ) 
			{
				bool bAffectedByTimeScale = (SceneManager::g_eSceneState == SCENE_STATE_IN_GAME_PAUSED) ? false : true;
				if ( SceneManager::g_eSceneState == SCENE_STATE_IN_GAME_RUNNING 
					|| SceneManager::g_eSceneState == SCENE_STATE_IN_GAME_PAUSED ) 
				{
					int iCurrFrame = pSender->GetCurrentFrame();
					if ( iCurrFrame != 0 ) 
					{ 
						pSender->SetRenderFrame( iCurrFrame - 1 );
						pSender->ScaleFromTo( 0.f, .5f, 0.f, 1.f, Easing::Linear::EaseOut, HandlerOnEasingAnimComplete, bAffectedByTimeScale );
					} 
					else 
					{
						pSender->SetVisible( false );
						if ( SceneManager::g_eSceneState == SCENE_STATE_IN_GAME_PAUSED ) 
						{
							// Resume game
							SetPause( false );
						}
					}
				}
			}
			break;
		}
	case GAI_SPRITE_HEART_0 : case GAI_SPRITE_HEART_1 : case GAI_SPRITE_HEART_2 : 
		{
			if ( eProperty == GAP_SCALE ) 
			{
				int iCurrFrame = pSender->GetCurrentFrame();
				if ( iCurrFrame == 0 ) pSender->SetRenderFrame(1);
				else if ( iCurrFrame == 1 ) pSender->SetRenderFrame(0);
			}
			break;
		}
	case GAI_SPRITE_BOARD:
		{
			if ( eProperty == GAP_POSITION ) 
			{
				if ( g_eSceneState == SCENE_STATE_IN_GAME_FINISHED ) 
				{
					MyGameManager::GlobalManager().m_splblResultsEnemiesSlicedCount->SetVisible( true );
					MyGameManager::GlobalManager().m_splblResultsRemainingLives->SetVisible( true );
					MyGameManager::GlobalManager().m_splblResultsScore->SetVisible( true );
				}
			}
			break;
		}
	case GAI_SPRITE_BG_STARRING:
		{
			if ( eProperty == GAP_ALPHA ) 
			{
				int iCurrFrame = pSender->GetCurrentFrame();
				if ( pSender->GetTexture()->GetColor().a == 255.f ) 
				{
					if ( !(iCurrFrame == (pSender->GetNumFrames() - 1)) )
						pSender->AlphaFromTo( 5.f, .5f, 255.f, 0.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete );
					else
					{
						Helper::CloseBiombo( 5.f, 1.5f );
						g_eSceneState = SCENE_STATE_MAIN_MENU_COVER;
					}
				}
				else
				{
					if ( (iCurrFrame + 1) < pSender->GetNumFrames() ) 
					{
						pSender->SetRenderFrame( iCurrFrame + 1 );
						pSender->AlphaFromTo( 5.f, .5f, 0.f, 255.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete );
					}
				}
			}
			break;
		}
	case GAI_SPRITE_TEXT_STARRING:
		{
			if ( eProperty == GAP_ALPHA ) 
			{
				int iCurrFrame = pSender->GetCurrentFrame();
				if ( pSender->GetTexture()->GetColor().a == 255.f ) 
				{
					pSender->AlphaFromTo( 5.f, .5f, 255.f, 0.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete );
				}
				else
				{
					if ( (iCurrFrame + 1) < pSender->GetNumFrames() ) 
					{
						pSender->SetRenderFrame( iCurrFrame + 1 );
						pSender->AlphaFromTo( 5.f, .5f, 0.f, 255.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete );
					}
				}
			}
			break;
		}
	default:
		break;
	}
}

void SceneManager::ShowGUIFromScene( eScene eWantedScene ) 
{
	// Hide the whole UI
	for ( unsigned int i = 0; i < GAI_COUNT; i++ ) 
	{
		GUIAnimation* pAnim = GUIAnimationManager::Instance().GetAnimation(i);
		if ( pAnim ) 
		{
			if ( pAnim->GetAnimID() == GAI_BG_BIOMBO_LEFT 
			|| pAnim->GetAnimID() == GAI_BG_BIOMBO_RIGHT 
			|| pAnim->GetAnimID() == GAI_SPRITE_BLACK_UNDER_BIOMBO ) continue;
			pAnim->SetVisible( false );
		}
	}
	// Hide labels
	MyGameManager::GlobalManager().m_splblScore->SetVisible( false );
	MyGameManager::GlobalManager().m_splblResultsEnemiesSlicedCount->SetVisible( false );
	MyGameManager::GlobalManager().m_splblResultsRemainingLives->SetVisible( false );
	MyGameManager::GlobalManager().m_splblResultsScore->SetVisible( false );
	// Disable picking by default
	MyGameManager::GlobalManager().SetPickingEnabled( false );


	g_eCurrentScene = eWantedScene;
	switch ( eWantedScene ) 
	{
	case SCENE_MAIN_MENU:
		{
			if ( g_eSceneState == SCENE_STATE_MAIN_MENU_ONE_TIME_INIT ) 
			{
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetScale( 0.f, 0.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetOrder( GAO_FRONT - 1 );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_STARRING )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_STARRING )->SetScale( 0.f, 0.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_STARRING )->SetOrder( GAO_FRONT - 1 );
				GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_MAIN_MENU )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_MAIN_MENU )->SetOrder( GAO_MIDDLE );
				GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_MAIN_MENU )->SetRelativeSize( 1.5f, 1.5f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_DRAW_MAIN_MENU )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_DRAW_MAIN_MENU )->SetTouchable( false );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_DRAW_MAIN_MENU )->SetRelativeSize( 0.9f, 0.9f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_DRAW_MAIN_MENU )->SetOrder( GAO_FRONT );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TAP_TO_START )->SetTouchable( false );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TAP_TO_START )->SetOrder( GAO_MODAL + 1 );
				GUIAnimation* pBgLogo = GUIAnimationManager::Instance().GetAnimation( GAI_BG_LOGO );
				pBgLogo->SetVisible( true );
				pBgLogo->ScaleTo( 1.5f, 2.f, 6.f, Easing::Linear::EaseIn, 0, false );
				pBgLogo->AlphaTo( 1.5f, 1.f, 0.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete, false );
				pBgLogo->ColorTo( 1.5f, 0.5f, V_RGBA_BLACK, Easing::Linear::EaseIn, 0, false );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_PLAY_THE_GAME )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_PLAY_THE_GAME )->SetScale( 0.f, 0.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_PLAY_THE_GAME )->SetOrder( GAO_FRONT - 1 );
			}
			else if ( g_eSceneState == SCENE_STATE_MAIN_MENU_COVER ) 
			{
				if ( MyGameManager::GlobalManager().IsMuteFMOD() ) GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetRenderFrame(2);
				else GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetRenderFrame(0);
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_STARRING )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_MAIN_MENU )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_DRAW_MAIN_MENU )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_PLAY_THE_GAME )->SetVisible( true );
			}
			else if ( g_eSceneState == SCENE_STATE_MAIN_MENU_LEVEL_SELECTION ) 
			{
				// Enable picking
				MyGameManager::GlobalManager().SetPickingEnabled( true );

				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetVisible( true );
				if ( MyGameManager::GlobalManager().IsMuteFMOD() ) GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetRenderFrame(2);
				else GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->SetRenderFrame(0);
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_BACK )->SetVisible( true );
			} 
			else if ( g_eSceneState == SCENE_STATE_MAIN_MENU_STARRING ) 
			{
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BG_STARRING )->SetRenderFrame(0);
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BG_STARRING )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BG_STARRING )->AlphaFromTo( 5.f, .5f, 255.f, 0.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TEXT_STARRING )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TEXT_STARRING )->AlphaTo( 0.f, 0.f, 0.f, Easing::Linear::EaseIn );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TEXT_STARRING )->SetRenderFrame(0);
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TEXT_STARRING )->AlphaFromTo( 5.f, .5f, 0.f, 255.f, Easing::Linear::EaseIn, HandlerOnEasingAnimComplete );
			}
			break;
		}
	case SCENE_IN_GAME: 
		{
			if ( g_eSceneState == SCENE_STATE_IN_GAME_INIT ) 
			{
				GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetVisible( true );
				//GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetTouchable( false );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetScale( 0.f, 0.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_PAUSE )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_PAUSE )->SetRenderFrame(0);
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_FRAME )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_0 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_0 )->SetScale( 1.f, 1.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_1 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_1 )->SetScale( 1.f, 1.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_2 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_2 )->SetScale( 1.f, 1.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_SCORE )->SetVisible( true );
				MyGameManager::GlobalManager().m_splblScore->SetVisible( true );
			} 
			else if ( g_eSceneState == SCENE_STATE_IN_GAME_PAUSED ) 
			{
				//if ( GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->IsActiveEaseAnim() ) GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetVisible( true ); // Why?
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_PAUSE )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_FRAME )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_0 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_1 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_2 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_SCORE )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BG_PAUSED )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_PAUSED )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND_PAUSED )->SetVisible( true );
				if ( MyGameManager::GlobalManager().IsMuteFMOD() ) GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND_PAUSED )->SetRenderFrame(2);
				else GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND_PAUSED )->SetRenderFrame(0);
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESUME )->SetVisible( true );
				if ( !Vision::Game.SearchEntity( "Boss" ) )
					GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESTART_PAUSED )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_TO_MAIN_MENU_PAUSED )->SetVisible( true );
				MyGameManager::GlobalManager().m_splblScore->SetVisible( true );
			}
			else if ( g_eSceneState == SCENE_STATE_IN_GAME_RUNNING ) 
			{
				if ( GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->IsActiveEaseAnim() ) GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_PAUSE )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_FRAME )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_0 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_1 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_2 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_SCORE )->SetVisible( true );
				MyGameManager::GlobalManager().m_splblScore->SetVisible( true );
			} 
			else if ( g_eSceneState == SCENE_STATE_IN_GAME_FINISHED ) 
			{
				// Run anim
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BOARD )->PositionFromTo( 0.f, 1.f, hkvVec2(0.f,-1.f), hkvVec2(0.f,0.f), Easing::Bounce::EaseOut, HandlerOnEasingAnimComplete, false );

				GUIAnimationManager::Instance().GetAnimation( GAI_BG_BLACK_FINISHED )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BOARD )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_FINALSCORE )->SetVisible( true );
				if ( !Vision::Game.SearchEntity( "Boss" ) )
					GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESTART_FINISHED )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_BTN_TO_MAIN_MENU_FINISHED )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_0 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_0 )->SetScale( 0.f, 0.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_1 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_1 )->SetScale( 0.f, 0.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_2 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_2 )->SetScale( 0.f, 0.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_0 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_0 )->SetScale( 1.f, 1.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_1 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_1 )->SetScale( 1.f, 1.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_2 )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_2 )->SetScale( 1.f, 1.f );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->SetVisible( true );
				GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->SetScale( 0.f, 0.f );
			}
			break;
		}
	default:
		break;
	}
}

void SceneManager::OrderGUIElements() 
{
	// In game
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->SetOrder( GAO_FRONT + 100 );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_PAUSED )->SetOrder( GAO_FRONT + 10 );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BOARD )->SetOrder( GAO_FRONT + 5 );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_PAUSED )->SetOrder( GAO_FRONT + 5 );
	GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND_PAUSED )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESUME )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESTART_PAUSED )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_TO_MAIN_MENU_PAUSED )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_FRAME )->SetOrder( GAO_MIDDLE + 50 );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_FINALSCORE )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESTART_FINISHED )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_TO_MAIN_MENU_FINISHED )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_0 )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_1 )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_2 )->SetOrder( GAO_FRONT );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_0 )->SetOrder( GAO_FRONT + 1 );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_1 )->SetOrder( GAO_FRONT + 1 );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_2 )->SetOrder( GAO_FRONT + 1 );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->SetOrder( GAO_FRONT );
}

void SceneManager::SizeGUIElements() 
{

}

void SceneManager::PositionGUIElements() 
{
	// Black/White background
	GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BLACK_FINISHED )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BLACK_UNDER_BIOMBO )->PositionFromCenter( 0.f, 0.f );
	// Credits
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BG_STARRING )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TEXT_STARRING )->PositionFromCenter( 0.f, 0.f );
	// Back button
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_BACK )->PositionFromBottomLeft( 0.f, 0.f );
	// Main menu
	GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND )->PositionFromTopRight( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_STARRING )->PositionFromTopLeft( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_MAIN_MENU )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_DRAW_MAIN_MENU )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_TAP_TO_START )->PositionFromCenter( 0.3f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_LOGO )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_PLAY_THE_GAME )->PositionFromBottom( 0.f );
	// Biombo
	Helper::HideBiombo();
	// In game
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_COUNTER )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_PAUSE )->PositionFromTopLeft( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_FRAME )->PositionFromTop( 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_0 )->PositionFromTop( 0.f, -0.0425f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_1 )->PositionFromTop( 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_HEART_2 )->PositionFromTop( 0.f, 0.0425f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_SCORE )->PositionFromTopRight( 0.f, 0.f );
	GUIAnimation* pBoardPaused = GUIAnimationManager::Instance().GetAnimation( GAI_BG_BOARD_PAUSED );
	pBoardPaused->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_PAUSED )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND_PAUSED )->SetParent( pBoardPaused );
	GUIAnimationManager::Instance().GetAnimation( GAI_TOGGLE_SOUND_PAUSED )->PositionFromTopRight( 0.1f, 0.1f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESUME )->SetParent( pBoardPaused );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESUME )->PositionFromTop( 0.1f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESTART_PAUSED )->SetParent( pBoardPaused );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESTART_PAUSED )->PositionFromTop( 0.4f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_TO_MAIN_MENU_PAUSED )->SetParent( pBoardPaused );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_TO_MAIN_MENU_PAUSED )->PositionFromTop( 0.7f );
	GUIAnimation* pBoardFinished = GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BOARD );
	pBoardFinished->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BLACK_FINISHED )->PositionFromCenter( 0.f, 0.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_FINALSCORE )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_FINALSCORE )->PositionFromTopLeft( 0.07f, 0.07f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESTART_FINISHED )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_RESTART_FINISHED )->PositionFromCenter( 0.255f, 0.18f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_TO_MAIN_MENU_FINISHED )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_BTN_TO_MAIN_MENU_FINISHED )->PositionFromCenter( 0.255f, 0.31f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_0 )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_0 )->PositionFromCenter( -0.03f, 0.1f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_1 )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_1 )->PositionFromCenter( -0.03f, 0.23f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_2 )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_EMPTY_2 )->PositionFromCenter( -0.03f, 0.36f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_0 )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_0 )->PositionFromCenter( -0.03f, 0.1f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_1 )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_1 )->PositionFromCenter( -0.03f, 0.23f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_2 )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_2 )->PositionFromCenter( -0.03f, 0.36f );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->SetParent( pBoardFinished );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->PositionFromCenter( -0.3f, 0.23f );
}


void Helper::CloseBiombo( const float fStartTimeOut, const float fDuration, const bool bAffectedByTimeScale ) 
{
	if ( IsBiomboClosed() ) return;

	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->SetVisible( true );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->PositionTo( fStartTimeOut, fDuration, hkvVec2( 0.f, 0.f ), Easing::Bounce::EaseOut, 0, bAffectedByTimeScale );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_RIGHT )->SetVisible( true );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_RIGHT )->PositionTo( fStartTimeOut, fDuration, hkvVec2( 0.5f, 0.f ), Easing::Bounce::EaseOut, SceneManager::HandlerOnEasingAnimComplete, bAffectedByTimeScale );

	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BLACK_UNDER_BIOMBO )->AlphaTo( fStartTimeOut + fDuration, 0.f, 255.f, Easing::Linear::EaseIn, 0, false );
}

void Helper::OpenBiombo( const float fStartTimeOut, const float fDuration, const bool bAffectedByTimeScale ) 
{
	if ( !IsBiomboClosed() ) return;

	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->SetVisible( true );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->PositionTo( fStartTimeOut, fDuration, hkvVec2( -0.5f, 0.f ), Easing::Linear::EaseIn, 0, bAffectedByTimeScale );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_RIGHT )->SetVisible( true );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_RIGHT )->PositionTo( fStartTimeOut, fDuration, hkvVec2( 1.f, 0.f ), Easing::Linear::EaseIn, SceneManager::HandlerOnEasingAnimComplete, bAffectedByTimeScale );

	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BLACK_UNDER_BIOMBO )->AlphaFromTo( fStartTimeOut, 0.f, 255.f, 0.f, Easing::Linear::EaseIn, 0, bAffectedByTimeScale );
}

void Helper::HideBiombo() 
{
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->StopEasing( GAP_POSITION );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->PositionFromTopRight( 0.f, 1.f );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_RIGHT )->StopEasing( GAP_POSITION );
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_RIGHT )->PositionFromTopLeft( 0.f, 1.f );

	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_BLACK_UNDER_BIOMBO )->AlphaTo( 0.f, 0.f, 0.f, Easing::Linear::EaseIn, 0, false );
}

bool Helper::IsBiomboAnimRunning()
{
	return GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->IsActiveEaseAnim() || GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_RIGHT )->IsActiveEaseAnim();
}

bool Helper::IsBiomboClosed() 
{
	const float fXOffsetThreshold = -0.25f;
	float fRelPosX, fRelPosY;
	GUIAnimationManager::Instance().GetAnimation( GAI_BG_BIOMBO_LEFT )->GetRelativePostition( fRelPosX, fRelPosY );
	if ( fRelPosX < fXOffsetThreshold ) return false; // Opened
	else return true; // Closed
}

float Helper::GetUIScalingFactor() 
{
	const float fReferenceResolution = 1280.0f;
	const float fReferenceDisplaySize = 4.6f;
	const float fKernscheFakeKonstante = 1.73f;
	const float fBagarscherFakeExponent = 0.2f;

	float fScale = 1.0f;
	const hkvVec2 vRes = hkvVec2((float)Vision::Video.GetXRes(), (float)Vision::Video.GetYRes());

#if defined(_VISION_MOBILE) || defined (_VISION_PSP2)  
	const float fDisplaySizeInInch = vRes.getLength() / Vision::Video.GetDeviceDpi();  

	fScale = (vRes.x / fReferenceResolution) * fKernscheFakeKonstante * hkvMath::pow(fReferenceDisplaySize / fDisplaySizeInInch, fBagarscherFakeExponent);

#elif !defined(WIN32)
	fScale = vRes.x / fReferenceResolution;

#endif

	return hkvMath::Max(fScale, 0.5f);
}

void MyGameManager::SetLoadingSplashScreen( const char* pcFilepath ) 
{
	/*
	VLoadingScreen* pLoadingScreen = m_pApplication->GetAppModule<VLoadingScreen>();
	if ( pLoadingScreen ) 
	{
		VLoadingScreenBase::Settings& tRefSettings = pLoadingScreen->GetSettings();
		// Configure settings
		tRefSettings.m_sImagePath = LOADING_SPLASH_SCREEN_FILES_PATH + VString( pcFilepath );
	}
	*/
}

void MyGameManager::PrepareToLoadScene( const char* pcSceneFilepath ) 
{
	if ( IsLoadSceneQueued() ) return;

	m_sQueuedSceneToLoad = std::string( pcSceneFilepath );
	if ( IsPlayableScene( pcSceneFilepath ) ) // 8 == ".vscene" length + 1
	{ // Is a playable scene
		// TODO: Set a new splash screen for loading module in a random way from tips bgs
		// Run fade to black animation
		GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->SetVisible( true );
		GUIAnimationManager::Instance().GetAnimation( GAI_MODAL_BLACK )->AlphaFromTo( 0.f, 1.f, 0.f, 255.f, Easing::Linear::EaseIn, SceneManager::HandlerOnEasingAnimComplete );
	}
	else
	{ // Is main_menu/select_level scene
		// TODO: Set biombo bg as a new splash screen for loading module
		// Close biombo
		bool bAffectedByTimeScale = true;
		if ( Vision::GetTimer()->GetFrozen() ) bAffectedByTimeScale = false;
		Helper::CloseBiombo( 0.f, 1.5f, bAffectedByTimeScale );
	}
}

bool MyGameManager::IsPlayableScene( const char* pcSceneFilename ) 
{
	// If has a digit as end of scene name is a playable one
	if ( isdigit( pcSceneFilename[ strlen( pcSceneFilename ) - 8 ] ) ) return true; // 8 == ".vscene" length + 1
	else return false;
}

// Pause the game in playable scenes
void SceneManager::SetPause( const bool bIsPause ) 
{
	if ( g_eCurrentScene != SCENE_IN_GAME ) return;

	Vision::GetTimer()->SetFrozen( bIsPause );
	if ( bIsPause ) g_eSceneState = SCENE_STATE_IN_GAME_PAUSED; 
	else g_eSceneState = SCENE_STATE_IN_GAME_RUNNING;
}

void SceneManager::RestartLevel() 
{
	MyGameManager::GlobalManager().SetGameOver( false );
	// Camera set to start position
	CameraPositionEntity* pCamPos = static_cast<CameraPositionEntity*>( Vision::Game.SearchEntity( CAMPOS_ENT_KEY ) );
	if ( pCamPos ) 
	{
		VFollowPathComponent* pcmpPath = pCamPos->Components().GetComponentOfType<VFollowPathComponent>();
		if ( pcmpPath ) pcmpPath->Init();
	}
	// Cut line set to start position
	VisBaseEntity_cl* pCutLine = Vision::Game.SearchEntity( CUTLINE_ENT_KEY );
	if ( pCutLine ) 
	{
		VFollowPathComponent* pcmpPath = pCutLine->Components().GetComponentOfType<VFollowPathComponent>();
		if ( pcmpPath ) pcmpPath->Init();
	}
	// Reset stats: score, hp...
	PlayerStats::Instance()->ResetStats();
	MyGameManager::GlobalManager().m_splblScore->SetText( "0" );
	// Remove current visible enemies
	if ( BB::pcmpSlicing ) static_cast<CmpSlicing*>(BB::pcmpSlicing)->PurgeSlicedEntities();
	// Remove sliced chunks from dead enemies
	if ( BB::pcmpSlicing ) static_cast<CmpSlicing*>(BB::pcmpSlicing)->PurgeSlicedChunks();
}

// Return string containing score white-spaced between digits
std::string Helper::StringifyScore( unsigned int uiScore ) 
{
	int uiScoreNumLen = CutshumotoUtilities::uiLen( uiScore );
	unsigned int auiScore[5]; // length == max score value size recheable
	for ( int i = 0; i < uiScoreNumLen; i++ ) 
	{
		auiScore[i] = uiScore % 10;
		uiScore /= 10;
	} 
	std::ostringstream ss;
	for ( int i = uiScoreNumLen - 1; i > -1; i-- ) 
	{
		if ( i == 0 ) ss << auiScore[i];
		else ss << auiScore[i] << " ";
	}

	return ss.str();
}

// Create and set a VTextLabel instance
VTextLabel* Helper::CreateTextLabel( const char* pcText, const char* pcFontName, const VColorRef& tColor, const VisFont_cl::Alignment_e eHozAlign, const VisFont_cl::Alignment_e eVerAlign, const float fWidth, const float fHeight, const float fPosRelX, const float fPosRelY, const float fScaleFactor ) 
{
	VTextLabel* pTextLabel = new VTextLabel();
	pTextLabel->SetText( pcText );
	VisFont_cl* pLeviBrush = Vision::Fonts.FindFont( pcFontName );
	VTextStates &states = pTextLabel->Text();
	states.SetColor( tColor );
	states.SetFont( pLeviBrush );
	states.SetScaling( Helper::GetUIScalingFactor() * 1.5f * fScaleFactor );
	states.SetHorizontalAlignment( eHozAlign );
	states.SetVerticalAlignment( eVerAlign );
	pTextLabel->SetSize( fWidth, fHeight );
	pTextLabel->SetPosition( fPosRelX * static_cast<float>(Vision::Video.GetXRes()), 
		fPosRelY * static_cast<float>(Vision::Video.GetYRes()) );

	return pTextLabel;
}

void Helper::SetResultsGUI()
{
	CameraPositionEntity* pCamPos = static_cast<CameraPositionEntity*>( Vision::Game.SearchEntity( CAMPOS_ENT_KEY ) );
	hkvVec3 pos = hkvVec3(0,0,0);
	PlayerStats::Instance()->SetSoundFinish(true);
	if ( pCamPos )
		pos = pCamPos->GetPosition();

	// Set score achievement in lbl results score
	MyGameManager::GlobalManager().m_splblResultsScore->SetText( Helper::StringifyScore( PlayerStats::Instance()->GetScore() ).c_str() );
	MyGameManager::GlobalManager().m_splblResultsRemainingLives->SetText( Helper::StringifyScore( PlayerStats::Instance()->GetPlayerLifes() ).c_str() );
	MyGameManager::GlobalManager().m_splblResultsEnemiesSlicedCount->SetText( Helper::StringifyScore( PlayerStats::Instance()->GetEnemiesKilled() ).c_str() );
	unsigned int fNumStarsRewarded = PlayerStats::Instance()->CalculateStars( PlayerStats::Instance()->GetScore(), PlayerStats::Instance()->GetScoreMax(), PlayerStats::Instance()->GetCurrentLevel() );
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->SetVisible( true );
	if ( PlayerStats::Instance()->GetPlayerLifes() != 0 ) 
	{ // Level complete
		if ( fNumStarsRewarded >= 1 ) GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_0 )->ScaleFromTo( 1.25f, .5f, 0.f, 1.f, Easing::Elastic::EaseOut, 0, false );
		if ( fNumStarsRewarded >= 2 ) GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_1 )->ScaleFromTo( 1.75f, .5f, 0.f, 1.f, Easing::Elastic::EaseOut, 0, false );
		if ( fNumStarsRewarded >= 3 ) GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_STAR_2 )->ScaleFromTo( 2.25f, .5f, 0.f, 1.f, Easing::Elastic::EaseOut, 0, false );
		GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->SetRenderFrame(0);

		SoundManager::Instance()->PlaySound(eLevelOK,pos,false);
	} 
	else // Level failed
	{
		GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->SetRenderFrame(1);

		SoundManager::Instance()->PlaySound(eLevelFail,pos,false);
	}
	GUIAnimationManager::Instance().GetAnimation( GAI_SPRITE_RESULT )->ScaleFromTo( 0.f, 1.f, 2.f, 1.f, Easing::Back::EaseIn, 0, false );
}
