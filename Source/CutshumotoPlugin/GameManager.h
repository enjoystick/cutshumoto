
#ifndef PLUGINMANAGER_H_INCLUDED
#define PLUGINMANAGER_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "GUIAnimation.h"
#include "EnemyFactoryLUA.h"

#include <Vision/Runtime/Framework/VisionApp/VAppBase.hpp>
#include <Vision/Runtime/Framework/VisionApp/VAppImpl.hpp>
#include <Vision/Runtime/Framework/VisionApp/Modules/VLoadingScreen.hpp>


class GUIAnimation;

namespace SceneManager
{
	static eScene g_eCurrentScene;
	static eSceneState g_eSceneState;

	void InitGUI();
	void DeInitGUI();

	void SetPause( const bool bIsPause );
	void HandlerOnTouchUp( GUIAnimation* pSender );
	void HandlerOnTouchDown( GUIAnimation* pSender );
	void HandlerOnEasingAnimComplete( GUIAnimation* pSender, eGUIAnimProperty eProperty );
	void OrderGUIElements();
	void SizeGUIElements();
	void PositionGUIElements();
	void ShowGUIFromScene( eScene eWantedScene );
	void RestartLevel();
};

namespace Helper 
{
	float GetUIScalingFactor();
	void CloseBiombo( const float fStartTimeOut, const float fDuration, const bool bAffectedByTimeScale = true );
	void OpenBiombo( const float fStartTimeOut, const float fDuration, const bool bAffectedByTimeScale = true );
	void HideBiombo();
	bool IsBiomboClosed();
	bool IsBiomboAnimRunning();
	void SetResultsGUI();
	std::string StringifyScore( unsigned int uiScore );
	VTextLabel* CreateTextLabel( const char* pcText, const char* pcFontName, const VColorRef& tColor, const VisFont_cl::Alignment_e eHozAlign, const VisFont_cl::Alignment_e eVerAlign, const float fWidth, const float fHeight, const float fPosRelX, const float fPosRelY, const float fScaleFactor = 1.f );
}

class MyGameManager : public IVisCallbackHandler_cl
{
public:
	virtual void OnHandleCallback(IVisCallbackDataObject_cl *pData) HKV_OVERRIDE;

	//callback funtionality
	void OneTimeInit();
	void OneTimeDeInit();
	void OnUpdateSceneBegin();
	void OnEditorModeChanged(VisEditorModeChangedDataObject_cl * pData);		
	void OnAfterSceneLoaded(VisSceneLoadedDataObject_cl * pSceneData);
	void OnBeforeSceneUnloaded();
	void OnEngineInit();
	void OnEngineDeInit();
	void OnRegisterScriptFunctions(VScriptResourceManager* manager);
	void OnScriptProxyCreation(VScriptCreateStackProxyObject* scriptData);
		
	void SetPlayTheGame(bool bStatus);
	bool IsPlayingTheGame() { return m_bPlayingTheGame; }		
	void LoadScene(const char* nameScene);
	void LoadQueuedScene();
	VAppImpl* GetAppImpl() const { return m_pApplication; }
	void SetLoadingSplashScreen( const char* pcFilename);
	bool IsLoadSceneQueued() const { return !m_sQueuedSceneToLoad.empty(); }
	void PrepareToLoadScene( const char* pcSceneFilepath );
	void OnEndOfLevelReached();
	void SetMuteAllFMOD( const bool bIsMute ) { m_bIsMuteFMOD = bIsMute; }
	bool IsMuteFMOD() const { return m_bIsMuteFMOD; }
	void SetGameOver( const bool bIsGameOver ) { m_bIsGameOver = bIsGameOver; }
	bool IsGameOver() const { return m_bIsGameOver; }
	void SetPickingEnabled( const bool bEnabled ) { m_bIsPickingEnabled = bEnabled; }
	bool IsPickingEnabled() const { return m_bIsPickingEnabled; }

	// access one global instance of the Game manager
	static MyGameManager& GlobalManager() {return g_GameManager;}

private:
	bool IsPlayableScene( const char* pcSceneFilename );

private:
	bool					m_bPlayingTheGame;				
	static MyGameManager	g_GameManager;
	VAppImpl* m_pApplication;
	std::string m_sQueuedSceneToLoad;
	VSmartPtr<VGUIMainContext> m_spGUIContext;
	VSmartPtr<VDialog> m_spMainDialog;
	VSmartPtr<VisScreenMask_cl> m_spCursor;
	bool m_bIsMuteFMOD;
	bool m_bIsGameOver;
	bool m_bIsPickingEnabled;
	
public:
	VSmartPtr<VTextLabel> m_splblScore;
	VSmartPtr<VTextLabel> m_splblResultsScore;
	VSmartPtr<VTextLabel> m_splblResultsEnemiesSlicedCount;
	VSmartPtr<VTextLabel> m_splblResultsRemainingLives;
};


#endif // PLUGINMANAGER_H_INCLUDED


