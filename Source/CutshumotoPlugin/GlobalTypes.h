#ifndef GLOBALTYPES_H_INCLUDED
#define GLOBALTYPES_H_INCLUDED

#define TOTAL_ENEMIES_TYPE 10

//old deprecated values, will be deleted in short time 
#define ANCHOR_X_INIT -60
#define FLOOR_Z -325
#define ANCHOR_X_DISTANCE 40
#define ANCHOR_Y_DISTANCE 100
#define ANCHOR_Y_SPEED 175
#define RANGE_CREATION -500
#define MAP_SPEED 45
#define RANGE_ENEMYREMOVE -2000
#define RANGE_ANCHORREMOVE -2000
#define RANGE_ENEMYATTACK -1000

#define BLOCKROWS 5
#define BLOCKCOLS 4
//


#define X_FIRST_RAIL -300
#define X_SECOND_RAIL -100
#define X_THIRD_RAIL 100
#define X_FOURTH_RAIL 300

#define DISTANCE_TO_CONSIDERSWIPE 700

#define ENEMY_ENT_KEY			"Enemy"
#define FRIEND_ENT_KEY			"Friend"
#define SLICEDCHUNKS_ENT_KEY	"SlicedChunk"
#define CAMPOS_ENT_KEY			"camPos"
#define CUTLINE_ENT_KEY			"CutLine"
#define SLICEDCHUNK_ENT_KEY		"SlicedChunk"

const int COLLISION_FILTER_GROUP = 65566;

struct ModelVertex_t
{
	hkvVec3 pos, normal;
	hkvVec2 texcoord;
};

struct PolygonData
{
	ModelVertex_t* vertex;
	unsigned short* indices;
	unsigned int vertexCount;
	unsigned int indicesCount;

	PolygonData (unsigned int maxVertexSize, unsigned int maxIndexSize)
	{
		vertex = new ModelVertex_t[maxVertexSize];
		indices = new unsigned short[maxIndexSize];

		vertexCount = 0;
		indicesCount = 0;
	}
};

enum eEnemyType
{
	ENormal = 0,
	EArmored = 1,
	ENormalFast = 2,
	EFatuo = 3,
	ELauncher = 4,
	ECivilian = 5,
	EGhost = 6,
	EArmoredPlus =7,
	ESurprise = 8,
	EComet = 9,
	EKappa = 10,
	ESpiderling = 11,
	EBoss = 12,
	EFireball = 13,
	EShuriken = 41,
	ENone
};

enum eEnemyPiece
{
	eTopSide,
	eBottomSide,
	eLeftSide,
	eRightSide,
	eUpRightSide,
	eUpLeftSide,
	eDownLeftSide,
	eDownRightSide
};

enum eEnemyState
{
	eInitalState,
	eCreating,
	eRunning,
	eRunningBack,
	eAttacking,
	eJumping,
	eDisappeared, 
	eFalling, 
	eRelaxing,
	eDying,
	eTranslating, 	 	
	eResting, 	 	
	eDamage
};

enum eSliceDirection
{
	eAll,
	eHorizontalToLeft,
	eHorizontalToRight,
	eVerticalToUp,
	eVerticalToDown,
	eDiagonalToLeftUp,
	eDiagonalToLeftDown,
	eDiagonalToRightUp,
	eDiagonalToRightDown,
	ePlus,
	eX
};

enum eSceneToLoad
{
	eNoScene,
	eLevelSuccess,
	eLevelFailed
};

enum eMusic
{
	eSpiderBattle
};

enum eSounds
{
	eNoSound = -1, 
	eSpiderScream,
	eSpiderDamaged,
	eSpiderWalkShort,
	eSpiderWalkMid,
	eSpiderWalkLong,
	eSpiderMinion,
	eSpiderDeath,
	eSpiderAmbient,
	eSpiderAmbient2,
	eSpiderAmbient3,
	eSpiderMusic,
	eSpiderAttack,
	ePlayerDamaged,	
	eHitEnemy,
	eSwipe1,
	eSwipe2,
	eHit,
	eParry,
	eKitsuneDeath,
	eKappaDeath,
	eOniDeath,
	eTenguDeath,
	eNinjaDeath,
	eKiteDeath,
	eGhostDeath,
	eFatuoDeath,
	eCivilDeath,
	eKitsuneCry,
	eKappaCry,
	eOniCry,
	eTenguCry,
	eNinjaCry,
	eKiteCry,
	eGhostCry,
	eFatuoCry,
	eCivilCry,
	eButtonAppear,
	eButtonPush,
	eWoodenDoors,
	eShogunMusic,
	eShogunFall,
	eShogunHit,
	eShogunDeath,
	eShogunBoneCrush,
	eShogunPunch,
	eShogunTeleport,
	eShogunTeleport_alt,
	eShogunIntro,
	eDragonRoar,
	eDragonVoice,
	eDragonShoot,
	eDragonDeath,
	eDragonMusic,
	eDragonIntro, 
	eShowButtons, 
	eDefaultPushButton, 
	ePauseButton, 
	eCounter, 
	eSlice, 
	eBiombo, 
	eMainMenu, 
	eCredits,
	eLevelOK,
	eLevelFail
};

enum eLevelNum
{
	eFirst,
	eSecond,
	eThird,
	eFourth,
	eFifth,
	eSix
};


enum eGUIAnimID 
{
	GAI_SPRITE_DRAW_MAIN_MENU = 0, 
	GAI_BG_BOARD_MAIN_MENU, 
	GAI_BG_BOARD_PAUSED, 
	GAI_SPRITE_BLACK_UNDER_BIOMBO, 
	GAI_SPRITE_TAP_TO_START, 
	GAI_BTN_STARRING, 
	GAI_TOGGLE_SOUND, 
	GAI_TOGGLE_SOUND_PAUSED, 
	GAI_TOGGLE_PAUSE, 
	GAI_SPRITE_SCORE, 
	GAI_SPRITE_HEART_FRAME, 
	GAI_SPRITE_HEART_0, 
	GAI_SPRITE_HEART_1, 
	GAI_SPRITE_HEART_2, 
	GAI_BG_PAUSED, 
	GAI_SPRITE_BOARD, 
	GAI_BTN_RESUME, 
	GAI_BTN_RESTART_PAUSED,
	GAI_BTN_TO_MAIN_MENU_PAUSED, 
	GAI_BTN_PLAY_THE_GAME, 
	GAI_SPRITE_FINALSCORE, 
	GAI_SPRITE_RESULT, 
	GAI_BTN_RESTART_FINISHED, 
	GAI_BTN_TO_MAIN_MENU_FINISHED, 
	GAI_SPRITE_STAR_0, 
	GAI_SPRITE_STAR_1, 
	GAI_SPRITE_STAR_2, 
	GAI_SPRITE_STAR_EMPTY_0, 
	GAI_SPRITE_STAR_EMPTY_1, 
	GAI_SPRITE_STAR_EMPTY_2, 
	GAI_BG_BIOMBO_LEFT, 
	GAI_BG_BIOMBO_RIGHT, 
	GAI_BG_LOGO, 
	GAI_MODAL_WHITE, 
	GAI_BG_BLACK_FINISHED, 
	GAI_MODAL_BLACK, 
	GAI_BTN_BACK, 
	GAI_SPRITE_COUNTER, 
	GAI_SPRITE_BG_STARRING, 
	GAI_SPRITE_TEXT_STARRING, 
	GAI_COUNT
};

enum eScene 
{
	SCENE_MAIN_MENU = 0, 
	SCENE_IN_GAME, 
	SCENE_COUNT
};

enum eSceneState 
{
	SCENE_STATE_NONE = -1, 
	SCENE_STATE_MAIN_MENU_ONE_TIME_INIT, 
	SCENE_STATE_MAIN_MENU_COVER, 
	SCENE_STATE_MAIN_MENU_STARRING, 
	SCENE_STATE_MAIN_MENU_LEVEL_SELECTION, 
	SCENE_STATE_IN_GAME_INIT, 
	SCENE_STATE_IN_GAME_RUNNING, 
	SCENE_STATE_IN_GAME_PAUSED, 
	SCENE_STATE_IN_GAME_FINISHED
};


// Class where save the whole data accessed in a recurrent way
class Blackboard
{
public:
	static void* pcmpSlicing;
};
typedef Blackboard BB;

#endif