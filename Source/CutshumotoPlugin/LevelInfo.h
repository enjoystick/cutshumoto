//============================================================================================================
//  LevelInfo Classes
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef LEVELINFO_H_INCLUDED
#define LEVELINFO_H_INCLUDED

#include "CutshumotoPluginPCH.h"
#include "GlobalTypes.h"

//---------------------------------------------------------------------------
// EnemySpawnInfo
//---------------------------------------------------------------------------

struct EnemySpawnInfo
{
	eEnemyType eType;
	unsigned char iRow;
	unsigned char iCol;

	EnemySpawnInfo() {}
	inline EnemySpawnInfo& operator=(const EnemySpawnInfo& other);
	inline bool operator==(const EnemySpawnInfo& other);
	inline bool operator!=(const EnemySpawnInfo& other);

};

inline EnemySpawnInfo& EnemySpawnInfo::operator=(const EnemySpawnInfo& other)
{
	eType = other.eType;
	iRow = other.iRow;
	iCol = other.iCol;	

	return *this;
}

inline bool EnemySpawnInfo::operator==(const EnemySpawnInfo& other)
{
	if (eType != other.eType)
		return false;

	if (iRow != other.iRow)
		return false;

	if (iCol != other.iCol)
		return false;

}

inline bool EnemySpawnInfo::operator!=(const EnemySpawnInfo& other)
{
	if (*this == other)
		return false;
	else
		return true;
}

//---------------------------------------------------------------------------
// BlockSpawnInfo
//---------------------------------------------------------------------------
struct BlockSpawnInfo
{
	//should it change for enum type??
	unsigned char iChunkType;	

	DynArray_cl<EnemySpawnInfo*> aEnemies;

	BlockSpawnInfo() {}
	~BlockSpawnInfo() 
	{ 
		for (unsigned int i = 0; i < aEnemies.GetValidSize(); i++)		
			delete aEnemies[i];

		aEnemies.Reset(); 
	}
	inline BlockSpawnInfo& operator=(const BlockSpawnInfo& other);
	inline bool operator==(const BlockSpawnInfo& other);
	inline bool operator!=(const BlockSpawnInfo& other);
	
};

inline BlockSpawnInfo& BlockSpawnInfo::operator=(const BlockSpawnInfo& other)
{
	iChunkType = other.iChunkType;	
	aEnemies = other.aEnemies;	

	return *this;
}

inline bool BlockSpawnInfo::operator==(const BlockSpawnInfo& other)
{
	if (iChunkType != other.iChunkType)
		return false;

	if (&aEnemies != &other.aEnemies)
		return false;

}

inline bool BlockSpawnInfo::operator!=(const BlockSpawnInfo& other)
{
	if (*this == other)
		return false;
	else
		return true;
}

//---------------------------------------------------------------------------
// LevelInfo
//---------------------------------------------------------------------------
struct LevelInfo
{
	VString sLevelName;
	VString sSuccessLevel;
	VString sFailLevel;
	float fBlockSpawnTime;

	DynArray_cl<BlockSpawnInfo*> aBlocks;

	LevelInfo() {}
	~LevelInfo() 
	{ 
		for (unsigned int i = 0; i < aBlocks.GetValidSize(); i++)		
			delete aBlocks[i];

		aBlocks.Reset(); 
	}

	inline LevelInfo& operator=(const LevelInfo& other);
	inline bool operator==(const LevelInfo& other);
	inline bool operator!=(const LevelInfo& other);
};

inline LevelInfo& LevelInfo::operator=(const LevelInfo& other)
{
	sLevelName = other.sLevelName;
	sSuccessLevel = other.sSuccessLevel;
	sFailLevel = other.sFailLevel;
	fBlockSpawnTime = other.fBlockSpawnTime;

	return *this;
}

inline bool LevelInfo::operator==(const LevelInfo& other)
{
	if (sLevelName != other.sLevelName)
		return false;

	if (sSuccessLevel != other.sSuccessLevel)
		return false;

	if (sFailLevel != other.sFailLevel)
		return false;

	if (fBlockSpawnTime != other.fBlockSpawnTime)
		return false;

	if (&aBlocks != &other.aBlocks)
		return false;

}

inline bool LevelInfo::operator!=(const LevelInfo& other)
{
	if (*this == other)
		return false;
	else
		return true;
}

#endif