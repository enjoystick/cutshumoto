#include "CutshumotoPluginPCH.h"
#include "PlayerStats.h"
#include "CmpCameraEffects.h"

#define INMORTALITY false

const float COMBOTIME = 0.2f;
const float TIMEWITHOUTDEATH = 1;

PlayerStats* PlayerStats::m_pStats = NULL;

unsigned int PlayerStats::GetScore()
{
	return m_iScore;
}
void PlayerStats::AddScore(int score, bool byEnemyDeath)
{
	float elapsedTime = Vision::GetTimer()->GetTime();
	unsigned int uiNewScore = m_iScore + score;
	m_iScore = hkvMath::clamp<unsigned int>( uiNewScore, 0, m_iScoreMax );

	if (byEnemyDeath)
	{		
		//a lot of time since last death,
		if (hkvMath::Abs(elapsedTime - m_fLastKillTime) > TIMEWITHOUTDEATH)
			m_iCurrentCombo = 0;
		
		CheckCombo(elapsedTime);
		m_fLastKillTime = elapsedTime;
		m_iEnemiesKilled++;
	}
}

int PlayerStats::GetCurrentCombo()
{
	if (Vision::GetTimer()->GetTime() - m_fLastKillTime > TIMEWITHOUTDEATH) 
		m_iCurrentCombo = 0;

	return m_iCurrentCombo; 
}

void PlayerStats::DamagePlayer(int damage)
{
	m_iPlayerLifes = (!INMORTALITY) ? hkvMath::clamp( m_iPlayerLifes - damage, 0, DEFAULT_LIFES ) : 3;

	//we suppose that cameraEffects component is attached to camPos object
	VisBaseEntity_cl* pCamera = Vision::Game.SearchEntity( CAMPOS_ENT_KEY );

	if (pCamera)
	{
		CmpCameraEffects* cmpFX = pCamera->Components().GetComponentOfType<CmpCameraEffects>();

		if (cmpFX)
		{
			cmpFX->CameraShake();		
			cmpFX->DamageEffect();
		}
	}

}

void PlayerStats::HealPlayer(int lifes )
{
	if(m_iPlayerLifes < 3)
		m_iPlayerLifes+=lifes;
}

int PlayerStats::GetPlayerLifes()
{
	return hkvMath::clamp( m_iPlayerLifes, 0, 3 );
}

void PlayerStats::ResetStats(int initialLifes)
{
	m_iScore = 0;
	m_iPlayerLifes = initialLifes;
	m_iCurrentCombo = 0;
	m_fLastKillTime = 0;
	m_fIniComboTime = 0;
	m_iScoreMax = 0;
	m_iStarMax = 0;
	m_bLevelFinish=false;
	m_iCurrentLevel=0;
	m_bSoundFinish=false;
	m_bBossFinished = false;
	m_eGameMode = eHistory;
}

bool PlayerStats::CheckCombo(float currentTime)
{		
	bool combo = false;
		
	float timeBetweenKills = hkvMath::Abs(m_fLastKillTime - currentTime);

	if (timeBetweenKills < COMBOTIME)
	{				
		m_iCurrentCombo++;
		combo = true;
	}
	else
	{			
		m_iCurrentCombo = 1;
	}

	return combo;
}

bool PlayerStats::SaveStats(char* filename, char* sceneName)
{
	//save data to file
	return true;
}
		
PlayerStats::PlayerStats()
{
	ResetStats();
}

unsigned int PlayerStats::GetCurrentLevel()
{
	return m_iCurrentLevel;
}

unsigned int PlayerStats::GetScoreMax()
{
	return m_iScoreMax;
}

void PlayerStats::AddScoreMax(int score, bool byEnemyDeath)
{
	m_iScoreMax+=score;

	/*if (byEnemyDeath)
	{
		CheckCombo(Vision::GetTimer()->GetTime());
		m_iEnemiesKilled++;		
	}*/
}

bool PlayerStats::GetLevelFinish()
{
	return m_bLevelFinish;
}
		
void PlayerStats::SetLevelFinish(bool FinishIn)
{
	m_bLevelFinish= FinishIn;
}

void PlayerStats::SetStarMax(unsigned int starIn)
{
	m_iStarMax=starIn;
}
unsigned int PlayerStats::GetStarMax()
{
	return m_iStarMax;
}

void PlayerStats::SetCurrentLevel(unsigned int levelIn)
{
	m_iCurrentLevel=levelIn;
}

unsigned int PlayerStats::CalculateStars(int scoreIn, int scoreMaxIn, bool levelFinish) 
{
	unsigned int scoreLife= PlayerStats::Instance()->GetPlayerLifes();
	if(scoreLife>3)
		scoreLife=3;
	return scoreLife;
}
