//============================================================================================================
//  PlayerStats Class
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef PLAYERSTATS_H_INCLUDED
#define PLAYERSTATS_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "GlobalTypes.h"

#define DEFAULT_LIFES 3

class PlayerStats
{
	public:

		enum eGameMode
		{
			eHistory,
			eEndless
		};

		static PlayerStats* Instance() { if ( m_pStats == 0 ) m_pStats = new PlayerStats(); return m_pStats; }
		unsigned int GetScore();		
		void		AddScore(int score, bool byEnemyDeath = true);
		void		DamagePlayer(int damage = 1);
		void		HealPlayer(int lifes = 1);
		int			GetPlayerLifes();
		void		ResetStats(int initialLifes = DEFAULT_LIFES);
		bool		SaveStats(char* filename, char* sceneName);
		//27_08_14 CHanges from Sergio
		void		SetCurrentLevel(unsigned int levelIn);
		unsigned int GetCurrentLevel();
		//29_08_14 Sergio: To calculate Stars from MaxScore and Check Victory
		unsigned int GetScoreMax();		
		void		AddScoreMax(int score, bool byEnemyDeath = true);
		bool		GetLevelFinish();
		void		SetLevelFinish(bool FinishIn);
		bool		GetBossFinish() { return m_bBossFinished; }
		void		SetBossFinish(bool finish) { m_bBossFinished = finish; }
		void		SetSoundFinish(bool soundIn){m_bSoundFinish=soundIn;}
		bool		GetSoundFinish(){return m_bSoundFinish;}
		int			GetCurrentCombo();
		unsigned int GetEnemiesKilled() const { return m_iEnemiesKilled; }
		void		SetStarMax(unsigned int starIn);
		unsigned int GetStarMax();
		unsigned int CalculateStars(int scoreIn, int scoreMaxIn, bool levelFinish);
		void		SetGameMode(eGameMode mode) {	m_eGameMode = mode; }
		eGameMode	GetGameMode()	{ return m_eGameMode; }
		

	protected:
		PlayerStats();
		~PlayerStats();

	private:

		bool	CheckCombo(float currentTime);
		
		static	PlayerStats*	m_pStats;
		int						m_iPlayerLifes;
		int						m_iCurrentCombo;
		unsigned int			m_iScore;
		unsigned int			m_iEnemiesKilled;
		float					m_fLastKillTime;
		float					m_fIniComboTime;
		unsigned int			m_iCurrentLevel;
		unsigned int			m_iScoreMax;
		bool					m_bLevelFinish;
		unsigned int			m_iStarMax;
		bool					m_bSoundFinish;
		bool					m_bBossFinished;
		eGameMode				m_eGameMode;
		
};

#endif

