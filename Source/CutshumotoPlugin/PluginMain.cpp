#include "CutshumotoPluginPCH.h"
#include "CmpEnemyProperties.h"
#include "CmpSlicing.h"
#include "GameManager.h"
#include "CmpEnemyBehaviour.h"
#include "CmpNormalBehaviour.h"
#include "CmpGhostBehaviour.h"
#include "CmpFatuoBehaviour.h"
#include "CmpCivilBehaviour.h"
#include "CmpNinjaBehaviour.h"
#include "CmpShurikenBehaviour.h"
#include "CmpCometBehaviour.h"
#include "CmpKappaBehaviour.h"
#include "CmpSpiderlingBehaviour.h"
#include "CmpSpiderBehaviour.h"
#include "CmpCameraEffects.h"
#include "CmpHighlight.h"
#include "CmpHighlightManager.h"
#include "CmpDragonBehaviour.h"
#include "CmpShogunBehaviour.h"
#include "CmpFollowPath.h"
#include "CmpEnemyLauncher.h"

#include <Common/Base/KeyCode.h>

// use plugins if supported
VIMPORT IVisPlugin_cl* GetEnginePlugin_vFmodEnginePlugin();
#if defined( HAVOK_PHYSICS_2012_KEYCODE )
VIMPORT IVisPlugin_cl* GetEnginePlugin_vHavok();
#endif
#if defined( HAVOK_AI_KEYCODE )
VIMPORT IVisPlugin_cl* GetEnginePlugin_vHavokAi();
#endif
#if defined( HAVOK_BEHAVIOR_KEYCODE )
VIMPORT IVisPlugin_cl* GetEnginePlugin_vHavokBehavior();
#endif

//============================================================================================================
//  Set up the Plugin Class
//============================================================================================================
class CutshumotoPluginClass : public IVisPlugin_cl
{
public:
	  
  void OnInitEnginePlugin();    
  void OnDeInitEnginePlugin();  

  const char *GetPluginName()
  {
    return "CutshumotoPlugin";  // must match DLL name
  }
};

//  global plugin instance
CutshumotoPluginClass g_myComponents;

//--------------------------------------------------------------------------------------------
//  create a global instance of a VModule class
//  note: g_myComponentModule is defined in stdfx.h
//--------------------------------------------------------------------------------------------
DECLARE_THIS_MODULE(g_myComponentModule, MAKE_VERSION(1,0),
                    "Cutshumoto Plugin", 
                    "Enjoystick",
                    "Cutshumoto Plugin", &g_myComponents);


//--------------------------------------------------------------------------------------------
//  Use this to get and initialize the plugin when you link statically
//--------------------------------------------------------------------------------------------
VEXPORT IVisPlugin_cl* GetEnginePlugin_CutshumotoPlugin(){  return &g_myComponents; }


#if (defined _DLL) || (defined _WINDLL)

	//  The engine uses this to get and initialize the plugin dynamically
	VEXPORT IVisPlugin_cl* GetEnginePlugin(){return GetEnginePlugin_CutshumotoPlugin();}

#endif // _DLL or _WINDLL

void CutshumotoPluginClass::OnInitEnginePlugin()
{
	hkvLog::Info("CutshumotoPluginClass:OnInitEnginePlugin()");
	Vision::RegisterModule(&g_myComponentModule);

  
	// load plugins if supported
	#if defined( HAVOK_PHYSICS_2012_KEYCODE )
		VISION_PLUGIN_ENSURE_LOADED(vHavok);
	#endif
	#if defined( HAVOK_AI_KEYCODE )
		VISION_PLUGIN_ENSURE_LOADED(vHavokAi);
	#endif
	#if defined( HAVOK_BEHAVIOR_KEYCODE )
		VISION_PLUGIN_ENSURE_LOADED(vHavokBehavior);
	#endif
  
	VISION_PLUGIN_ENSURE_LOADED(vFmodEnginePlugin);

	FORCE_LINKDYNCLASS( CmpSlicing );
	FORCE_LINKDYNCLASS( CmpEnemyProperties );	
	FORCE_LINKDYNCLASS( CmpEnemyBehaviour );	
	FORCE_LINKDYNCLASS( CmpNormalBehaviour );	
	FORCE_LINKDYNCLASS( CmpGhostBehaviour );	
	FORCE_LINKDYNCLASS( CmpFatuoBehaviour );
	FORCE_LINKDYNCLASS( CmpCivilBehaviour );
	FORCE_LINKDYNCLASS( CmpNinjaBehaviour );
	FORCE_LINKDYNCLASS( CmpShurikenBehaviour );
	FORCE_LINKDYNCLASS( CmpKappaBehaviour);
	FORCE_LINKDYNCLASS( CmpCometBehaviour);	
	FORCE_LINKDYNCLASS( CmpSpiderBehaviour);	
	FORCE_LINKDYNCLASS( CmpSpiderlingBehaviour);	
	FORCE_LINKDYNCLASS( CmpCameraEffects);	
	FORCE_LINKDYNCLASS( CmpHighlight);
	FORCE_LINKDYNCLASS( CmpDragonBehaviour);	
	FORCE_LINKDYNCLASS( CmpShogunBehaviour);	
	FORCE_LINKDYNCLASS( CmpFollowPath);	
	FORCE_LINKDYNCLASS( CmpEnemyLauncher);	

	// Start our component managers and game manager here....
	MyGameManager::GlobalManager().OneTimeInit();

	CmpSlicing_ComponentManager::GlobalManager().OneTimeInit();
	CmpEnemyProperties_ComponentManager::GlobalManager().OneTimeInit();    	
	CmpEnemyBehaviour_ComponentManager::GlobalManager().OneTimeInit();  
	CmpNormalBehaviour_ComponentManager::GlobalManager().OneTimeInit();
	CmpGhostBehaviour_ComponentManager::GlobalManager().OneTimeInit();  
	CmpFatuoBehaviour_ComponentManager::GlobalManager().OneTimeInit();
	CmpCivilBehaviour_ComponentManager::GlobalManager().OneTimeInit();
	CmpNinjaBehaviour_ComponentManager::GlobalManager().OneTimeInit();
	CmpShurikenBehaviour_ComponentManager::GlobalManager().OneTimeInit();
	CmpKappaBehaviour_ComponentManager::GlobalManager().OneTimeInit();
	CmpCometBehaviour_ComponentManager::GlobalManager().OneTimeInit();	
	CmpSpiderBehaviour_ComponentManager::GlobalManager().OneTimeInit();	
	CmpSpiderlingBehaviour_ComponentManager::GlobalManager().OneTimeInit();			
	CmpCameraEffects_ComponentManager::GlobalManager().OneTimeInit();	
	CmpHighlightComponentManager::s_instance.OneTimeInit();
	CmpDragonBehaviour_ComponentManager::GlobalManager().OneTimeInit();	
	CmpShogunBehaviour_ComponentManager::GlobalManager().OneTimeInit();	
	CmpFollowPath_ComponentManager::GlobalManager().OneTimeInit();	
	CmpEnemyLauncher_ComponentManager::GlobalManager().OneTimeInit();
}

// Called before the plugin is unloaded
void CutshumotoPluginClass::OnDeInitEnginePlugin()
{
	hkvLog::Info("CutshumotoPluginClass:OnDeInitEnginePlugin()");    
  	
	CmpEnemyLauncher_ComponentManager::GlobalManager().OneTimeDeInit();
	CmpFollowPath_ComponentManager::GlobalManager().OneTimeDeInit();	
	CmpShogunBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();
	CmpDragonBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();	
	CmpHighlightComponentManager::s_instance.OneTimeDeInit();
	CmpCameraEffects_ComponentManager::GlobalManager().OneTimeDeInit();			
	CmpSpiderlingBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();	
	CmpSpiderBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();	
	CmpCometBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();
	CmpKappaBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();
	CmpShurikenBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();
	CmpNinjaBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();	
	CmpCivilBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();
	CmpFatuoBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();
	CmpGhostBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();  
	CmpNormalBehaviour_ComponentManager::GlobalManager().OneTimeDeInit();  
	CmpEnemyBehaviour_ComponentManager::GlobalManager().OneTimeDeInit(); 
	CmpEnemyProperties_ComponentManager::GlobalManager().OneTimeDeInit();    	
	CmpSlicing_ComponentManager::GlobalManager().OneTimeDeInit();

	MyGameManager::GlobalManager().OneTimeDeInit();  
  
	// de-register our module when the plugin is de-initialized
	Vision::UnregisterModule(&g_myComponentModule);
}

