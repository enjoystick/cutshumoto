//============================================================================================================
//  SlicedEntity Class
//	Author: David Escalona Rocha
//============================================================================================================

#ifndef SLICEDENTITY_H_INCLUDED
#define SLICEDENTITY_H_INCLUDED

#include "GlobalTypes.h"

class SlicedEntity
{
	public:
		VisBaseEntity_cl* entity;
		hkvVec2 iniCutPoint;
		hkvVec2 endCutPoint;
		hkvVec2 lastCollisionPoint;
		hkvPlane* cutPlane;
		bool stillSlicing;
		eSliceDirection sliceDir;
		VRectanglef rect2d;
		bool isProjectil;
		eEnemyType enemyType;
		
		SlicedEntity() 
		{
			entity = NULL;
			iniCutPoint = hkvVec2::ZeroVector();
			endCutPoint = hkvVec2::ZeroVector();
			lastCollisionPoint = hkvVec2::ZeroVector();
			sliceDir = eAll;
			isProjectil = false;
			enemyType = ENormal;

			cutPlane = NULL;
			stillSlicing = false;
		}

		virtual ~SlicedEntity()
		{
			if ( entity )
				entity->Remove();
			entity = NULL;
		}

		inline SlicedEntity& operator=(const SlicedEntity& other);
		inline bool operator==(const SlicedEntity& other);
		inline bool operator!=(const SlicedEntity& other);


		static SlicedEntity* Create() { return new SlicedEntity(); }

};

inline SlicedEntity& SlicedEntity::operator=(const SlicedEntity& other)
{
	entity = other.entity;
	iniCutPoint = other.iniCutPoint;
	endCutPoint = other.endCutPoint;
	cutPlane = other.cutPlane;
	lastCollisionPoint = other.lastCollisionPoint;
	rect2d = other.rect2d;

	return *this;
}

inline bool SlicedEntity::operator==(const SlicedEntity& other)
{
	if (entity != other.entity)
		return false;

	if (iniCutPoint != other.iniCutPoint)
		return false;

	if (endCutPoint != other.endCutPoint)
		return false;

	if (cutPlane != other.cutPlane)
		return false;

	if (lastCollisionPoint != other.lastCollisionPoint)
		return false;

	if (rect2d != other.rect2d)
		return false;

}

inline bool SlicedEntity::operator!=(const SlicedEntity& other)
{
	if (*this == other)
		return false;
	else
		return true;
}

#endif