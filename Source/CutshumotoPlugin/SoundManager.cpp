//============================================================================================================
//  SoundManager Class
//	Author: David Escalona Rocha
//============================================================================================================
#include "CutshumotoPluginPCH.h"
#include "SoundManager.h"

const char* SOUNDSPATH = "Sounds//";
const char* ALTERNATIVE_SOUNDSPATH = "Assets//Sounds//";
const char* MUSICPATH = "Music//";
const char* ALTERNATIVE_MUSICPATH = "Assets//Music//";

SoundManager* SoundManager::m_pSMInstance = NULL;

SoundManager::SoundManager()
{
}

SoundManager::~SoundManager()
{
}

void SoundManager::PlaySound(eSounds sound, hkvVec3 vPos, bool loop, float vol)
{
	VFmodSoundResource* pRes = LoadSound(sound);

	if (pRes)
	{	
		int iFlag;

		//loop ? iFlag = VFMOD_FLAG_LOOPED : VFMOD_FLAG_NONE;
		if(loop==false)
			iFlag=0;
		if(loop==true)
			iFlag=1;
		//hkvLog::Error("Pasa por sonido");	
		VFmodSoundObject *pSoundObject = pRes->CreateInstance(vPos,iFlag);			
		pSoundObject->SetVolume(vol);
		pSoundObject->Play();				

	}

}

VFmodSoundObject * SoundManager::PlaySoundReturn(eSounds sound, hkvVec3 vPos, bool loop, float vol)
{
	VFmodSoundResource* pRes = LoadSound(sound);

	if (pRes)
	{	
		int iFlag;

		//loop ? iFlag = VFMOD_FLAG_LOOPED : VFMOD_FLAG_NONE;
		if(loop==false)
			iFlag=0;
		if(loop==true)
			iFlag=1;
		//hkvLog::Error("Pasa por sonido");	
		VFmodSoundObject *pSoundObject = pRes->CreateInstance(vPos,iFlag);			
		pSoundObject->SetVolume(vol);
		pSoundObject->Play();				

		return pSoundObject;
	}

	return NULL;
}

VFmodSoundResource* SoundManager::LoadSound(eSounds sound)
{
	VString filename = SOUNDSPATH;
	VString alternativeFile = ALTERNATIVE_SOUNDSPATH;

	filename+= GetSoundFilename(sound);
	alternativeFile+= GetSoundFilename(sound);
	
	VFmodSoundResource* pSound = VFmodManager::GlobalManager().LoadSoundResource(filename);	

	if (!pSound)		
		pSound = VFmodManager::GlobalManager().LoadSoundResource(alternativeFile);		

	return pSound;
}

char* SoundManager::GetSoundFilename (eSounds sound)
{
	switch (sound)
	{
		case eSpiderScream:		return "spider_scream.mp3";
		case eSpiderDamaged:	return "spider_damage.mp3";
		case eSpiderWalkShort:	return "spider_walk1.mp3";
		case eSpiderWalkMid:	return "spider_walk2.mp3";
		case eSpiderWalkLong:	return "spider_walk3.mp3";
		case eSpiderMinion:		return "spider_minion.mp3";
		case eSpiderDeath:		return "spider_death.mp3";
		case eSpiderAmbient:	return "spider_ambient.mp3";
		case eSpiderAmbient2:	return "spider_ambient2.mp3";
		case eSpiderAmbient3:	return "intro_spider.mp3";
		case eSpiderMusic:		return "loop_spider_main.mp3";
		case eSpiderAttack:		return "spider_attack.mp3";
		case ePlayerDamaged:	return "playerdamaged.wav";
		case eHitEnemy:			return "hit.wav";
		case eSwipe1:			return "sword_swipe.wav";
		case eSwipe2:			return "sword_swipe.wav";
		case eHit:				return "FX_paper_tear_03.mp3";
		case eParry:			return "parry_proyectil.wav";
		case eKitsuneDeath:		return "";
		case eKappaDeath:		return "";
		case eOniDeath:			return "";
		case eTenguDeath:		return "";
		case eNinjaDeath:		return "";
		case eKiteDeath:		return "";
		case eGhostDeath:		return "";
		case eFatuoDeath:		return "";
		case eCivilDeath:		return "civil.wav";
		case eKitsuneCry:		return "kitsune.wav";
		case eKappaCry:			return "kappa.wav";
		case eOniCry:			return "oni.wav";
		case eTenguCry:			return "tengu.wav";
		case eNinjaCry:			return "";
		case eKiteCry:			return "";
		case eGhostCry:			return "ghost.wav";
		case eFatuoCry:			return "fuego_fatuo.wav";
		case eCivilCry:			return "civil.wav";
		case eButtonAppear:		return "aparacion_boton.ogg";
		case eButtonPush:		return "boton_menu.ogg";
		case eWoodenDoors:		return "madera.wav";
		case eShogunMusic:		return "loop_shogun_main.mp3";
		case eShogunFall:		return "shogun_fall.mp3";
		case eShogunHit:		return "shogun_hit.wav";
		case eShogunDeath:		return "shogun_death.mp3";
		case eShogunBoneCrush:	return "shogun_bone.mp3";
		case eShogunPunch:		return "shogun_punch.mp3";
		case eShogunTeleport:	return "shogun_teleport.mp3";
		case eShogunTeleport_alt:	return "shogun_teleport2.mp3";
		case eShogunIntro:		return "intro_shogun.mp3";
		case eDragonRoar:		return "dragon_roar.mp3";
		case eDragonVoice:		return "dragon_voice.mp3";
		case eDragonShoot:		return "dragon_shoot.mp3";
		case eDragonDeath:		return "dragon_death.mp3";
		case eDragonMusic:		return "loop_dragon_main.mp3";
		case eDragonIntro:		return "intro_dragon.mp3";
		case eShowButtons:		return "aparicion_boton.wav";
		case eDefaultPushButton:	return "boton_menu.wav";
		case ePauseButton:		return "cutshupausa.wav";
		case eCounter:			return "gui_3_2_1.wav";
		case eSlice:			return "gui_slice.wav";
		case eBiombo:			return "madera.wav";
		case eMainMenu:			return "loop_main_menu.mp3";
		case eCredits:			return "music_credits.mp3";
		case eLevelOK:			return "Music_victory.mp3";
		case eLevelFail:		return "Music_defeat.mp3";
	}

	return NULL;
}

char* SoundManager::GetMusicFilename (eMusic music)
{
	switch (music)
	{
		case eSpiderBattle: return "spiderbattle.mp3";
	}

	return NULL;
}
