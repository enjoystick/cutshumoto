//============================================================================================================
//  SoundManager Class
//	Author: David Escalona Rocha
//============================================================================================================
#ifndef SOUNDMANAGER_H_INCLUDED
#define SOUNDMANAGER_H_INCLUDED

#include "CutshumotoPluginModule.h"
#include "GlobalTypes.h"
#include  "Vision/Runtime/EnginePlugins/ThirdParty/FmodEnginePlugin/VFmodManager.hpp"

class SoundManager
{
	public:
		static SoundManager* Instance() { if ( m_pSMInstance == 0 ) m_pSMInstance = new SoundManager(); return m_pSMInstance; }
		void PlaySound(eSounds sound, hkvVec3 vPos, bool loop, float vol=100);
		VFmodSoundObject* PlaySoundReturn(eSounds sound, hkvVec3 vPos, bool loop, float vol=100);
		VFmodSoundResource* LoadSound(eSounds sound);


	protected:
		SoundManager();
		~SoundManager();

	private:
		
		char* GetSoundFilename (eSounds sound);
		char* GetMusicFilename (eMusic music);

		static SoundManager* m_pSMInstance;
};

#endif

