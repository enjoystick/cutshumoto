#ifndef VERTEXINFO_H_INCLUDED
#define VERTEXINFO_H_INCLUDED

class VertexInfo
{
	hkvVec3 vPosition;
	hkvVec3 vNormal;
	hkvVec2 vUV;
	int iIndex;

};

#endif