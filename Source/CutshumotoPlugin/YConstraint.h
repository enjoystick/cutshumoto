#ifndef YCONSTRAINT_H_INCLUDED
#define YCONSTRAINT_H_INCLUDED

#include <Vision\Runtime\EnginePlugins\VisionEnginePlugin\Particles\ParticleConstraint.hpp> 

class YConstraint:public VisParticleConstraint_cl{
public:

	 YConstraint(VIS_CONSTRAINT_REFLECT_BEHAVIOR_e eReflectMode=CONSTRAINT_REFLECT_NOTHING, float fPersistance=0.2f)
	: VisParticleConstraint_cl(eReflectMode,fPersistance){actualPos=0;}
 
 
  ~YConstraint(){}

	PARTICLE_IMPEXP  virtual void HandleParticles(IVPhysicsParticleCollection_cl *pGroup, float fTimeDelta, VIS_CONSTRAINT_REFLECT_BEHAVIOR_e eForceBehavior);
	float actualPos;
};

#endif